<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
use Carbon\Carbon;
// Route::get('/aa', function () {
//  $now = Carbon::now();
//    // $dateNow1 = $now->toDateTimeString();
//    $dateNow1 = $now;
 
//  sleep(5);
//  $now = Carbon::now();
//    // $dateNow2 = $now->toDateTimeString();
//    $dateNow2 = $now;

//     dd($dateNow1->diffInSeconds($dateNow2)); 
// });
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

//Route::middleware('cors')->resource('/test', 'testController');
/*
Route::get('/plural/{plural}', function ($plural) {
	$plural = str_plural($plural);

	return $plural;
});
*/
/*
Route::get('/ss', function () {
	$channelSecret = '7c231619ab14f6698b7c91a515dff5d4'; // Channel secret string
	$httpRequestBody = '{
		  "replyToken": "nHuyWiB7yP5Zw52FIkcQobQuGDXCTA",
		  "type": "message",
		  "timestamp": 1462629479859,
		  "source": {
		    "type": "user",
		    "userId": "U206d25c2ea6bd87c17655609a1c37cb8"
		  },
		  "message": {
		    "id": "325708",
		    "type": "text",
		    "text": "Hello, world"
		  }
		}'; // Request body string
	$hash = hash_hmac('sha256', $httpRequestBody, $channelSecret, true);
	$signature = base64_encode($hash);
	dd($signature);
});
*/

Route::get('line/receives', 'API\v1\LineWebHoocksController@getReceive');
Route::post('line/receives', 'API\v1\LineWebHoocksController@postReceive');
Route::resource('auto-reply-keyword-folder', 'API\v1\AutoReplyKeywordFolderController');
Route::resource('setting-location', 'API\v1\LocationController');
Route::resource('setting-location-item', 'API\v1\LocationItemController');


Route::group(['middleware' => 'cors'], function() {
    Route::resource('auto-reply-keyword-folder', 'API\v1\AutoReplyKeywordFolderController');
    Route::resource('auto-reply-location', 'API\v1\AutoReplyLocationController');
	Route::resource('setting-location', 'API\v1\LocationController');
	Route::resource('setting-location-item', 'API\v1\LocationItemController');
	Route::resource('setting-user', 'API\v1\UserController');
	// Route::post('setting-user-dt', 'API\v1\UserController@storeUserDt');
	Route::resource('setting-field', 'API\v1\FieldController');
	Route::resource('setting-master-field', 'API\v1\MasterFieldController');
	Route::resource('setting-field-folder', 'API\v1\FieldFolderController');
	Route::resource('setting-phpmailer', 'API\v1\PhpmailerController');
	Route::resource('setting-subscriber-folder', 'API\v1\SubscriberFolderController');

	//subscriber
	Route::resource('setting-subscriber', 'API\v1\SubscriberController');
	Route::get('setting-subscriber-download', 'API\v1\SubscriberController@downloadSubscriber');
	Route::get('setting-subscriber-download-single/{id}', 'API\v1\SubscriberController@downloadSubscriberSingle');
	Route::get('subscriber-get-field/{id}', 'API\v1\SubscriberController@getField');
	Route::get('subscriber-get-data/{id}', 'API\v1\SubscriberController@getData');
	Route::resource('subscriber-category', 'API\v1\Subscriber\SubscriberCategoryController'); //setting ประเภท subscriber
	//

	Route::resource('setting-carousel-folder', 'API\v1\CarouselFolderController');

	Route::resource('setting-carousel', 'API\v1\CarouselController');
	Route::resource('setting-carousel-item', 'API\v1\CarouselItemController');
	Route::get('setting-carousel-download', 'API\v1\CarouselController@exportCarousel');

	Route::resource('line-profilling', 'API\v1\ProfillingController');
	Route::get('line-profilling-achieve/{id}', 'API\v1\ProfillingController@achieve');
	Route::get('line-profilling-un-achieve/{id}', 'API\v1\ProfillingController@unAchieve');
	Route::get('line-profilling-getdata-achieve', 'API\v1\ProfillingController@getDataAchieve');
	Route::resource('line-profilling-folder', 'API\v1\ProfillingFolderController');

	Route::resource('img-upload', 'API\v1\ImageFileController');
	Route::post('img-upload-multiple', 'API\v1\ImageFileController@uploadMultiple');

	Route::resource('tracking-bc', 'API\v1\TrackingBcController');

	Route::resource('list-menu', 'API\v1\ListMenuController');

	Route::post('setting-line-bussiness/{id}/issue', 'API\v1\LineSettingBusinessController@postIssueToken');
	Route::resource('setting-line-bussiness', 'API\v1\LineSettingBusinessController');


	Route::post('auto_reply_default/{id}/active', 'AutoReplyDefaultController@postActive');
	Route::resource('auto_reply_default', 'AutoReplyDefaultController');

	Route::post('auto_reply_keyword/{id}/active', 'AutoReplyKeywordController@postActive');
	Route::resource('auto_reply_keyword', 'AutoReplyKeywordController');
	Route::resource('auto-reply-keyword-sharelocation', 'API\v1\AutoReplyKeyword\AutoReplyKeywordShareLocationController');
	Route::resource('auto-reply-keyword-carousel', 'API\v1\AutoReplyKeyword\AutoReplyKeywordCarouselController');

	Route::post('campaign/{id}/active', 'API\v1\CampaignController@postActive');
	Route::post('campaign/{id}/schedule-active', 'API\v1\CampaignController@scheduleActive');
	Route::post('campaign/{id}/schedule-un-active', 'API\v1\CampaignController@scheduleUnActive');
	Route::resource('campaign', 'API\v1\CampaignController');
	Route::resource('campaign-folder', 'API\v1\CampaignFolderController');

	//RichMessage
	Route::resource('richmessage-folder', 'API\v1\RichMessageFolderController');
	Route::resource('richmessage', 'API\v1\RichmessageV2\RichmessageController');
	Route::post('richmessage-upload', 'API\v1\RichmessageV2\RichmessageController@uploadMultiple');
	//

	//Dashboard
	Route::get('dashboard-1', 'API\v1\Dashboard@report1');
	Route::post('dashboard-report-calendar-campaign', 'API\v1\Dashboard@reportCalendarCampaign');
	Route::post('dashboard-report-tracking-bc', 'API\v1\Dashboard@reportTrackingBC');
	Route::post('dashboard-report-tracking-bc-of-the-day', 'API\v1\Dashboard@reportTrackingBCofTheDay');
	Route::post('dashboard-report-campaign-statistic', 'API\v1\Dashboard@reportCampaignStatistic');
	Route::post('dashboard-report-up-comming-event', 'API\v1\Dashboard@reportUpCommingEvent');
	Route::post('dashboard-report-add-block', 'API\v1\Dashboard@reportFriendAddBlock');
	Route::post('dashboard-recieve-message-monitor', 'API\v1\Dashboard@reportRecieveMessageMonitor');
	Route::post('dashboard-keyword-stat', 'API\v1\Dashboard@reportKeywordStatistic');
	Route::post('dashboard-report-campaign-statistic-campaign', 'API\v1\Dashboard@reportCampaignStatisticCampaign');
	Route::post('dashboard-report-tracking-bc-by-tracking-bc', 'API\v1\Dashboard@reportTrackingBCByTrackingBC');
	//

	//Bot
	Route::resource('setting-bot-connect', 'API\v1\SettingBotConnectController');
	Route::resource('train-bot', 'API\v1\Bot\BottrainController');
	Route::resource('setting-bot', 'API\v1\Bot\SettingBotController');
	Route::get('restart-bot', 'API\v1\Bot\SettingBotController@restartBot');
	Route::resource('setting-bot-upload-csv', 'API\v1\Bot\SettingBotUploadCsvController');
	Route::post('train-bot-csv', 'API\v1\Bot\SettingBotUploadCsvController@uploadCsvTrainBot');
	Route::get('train-bot-result', 'API\v1\Bot\SettingBotUploadCsvController@uploadResult');
	Route::get('train-bot-csv-remove-single/{id}', 'API\v1\Bot\SettingBotUploadCsvController@removeSingle');
	Route::get('train-bot-csv-export/{id}', 'API\v1\Bot\SettingBotUploadCsvController@exportTrainbotCSV');
	Route::get('train-bot-retry/{id}', 'API\v1\Bot\SettingBotUploadCsvController@retryBot');
	//

	//test-upload
	Route::resource('test-upload', 'API\v1\TestUploadFileController');
	//

	//share location
	Route::resource('setting-share-location-folder', 'API\v1\ShareLocation\ShareLocationFolderController');

	Route::resource('setting-share-location', 'API\v1\ShareLocation\ShareLocationController');
	Route::resource('setting-share-location-item', 'API\v1\ShareLocation\ShareLocationItemController');
	Route::get('setting-share-location-download', 'API\v1\ShareLocation\ShareLocationController@exportShareLocation');
	//

	Route::resource('report-bot', 'API\v1\ReportBotController');
	Route::get('report-bot-export-csv', 'API\v1\ReportBotController@exportDataCsv');

	//setting confirmation
	Route::resource('setting-conf-location', 'API\v1\SettingConfirmation\SettingConfirmationShareLocationController');
	Route::resource('setting-conf-carousel', 'API\v1\SettingConfirmation\SettingConfirmationCarouselController');
	//

	//thailand Country
	Route::get('province', 'API\v1\Country\CountryController@getProvince');
	Route::get('all-district', 'API\v1\Country\CountryController@getAllDistrict');
	Route::get('all-sub-district', 'API\v1\Country\CountryController@getAllSubDistrict');
	Route::get('province-district', 'API\v1\Country\CountryController@getProvinceDistrict');
	Route::get('district-sub-district', 'API\v1\Country\CountryController@getDistrictSubDistrict');
	//

	//upload auto-reply
	Route::post('upload-auto-reply', 'API\v1\AutoReplyKeyword\AutoReplyKeywordController@uploadAutoReplyFile');
	//

	//master subscriber
	Route::get('get-field-master-subscriber', 'API\v1\Subscriber\MasterSubscriberController@getFieldMasterSubscriber');
	Route::resource('master-subscriber', 'API\v1\Subscriber\MasterSubscriberController');
	//

	//update data carousel
	Route::post('update-single-row-carousel', 'API\v1\CarouselController@updateSingleRow');
	//

	//update data sharelocation
	Route::post('update-single-row-sharelocation', 'API\v1\ShareLocation\ShareLocationController@updateSingleRow');
	//

	//segment
	Route::resource('setting-segment-folder', 'API\v1\Segment\SegmentFolderController');
	Route::resource('setting-segment', 'API\v1\Segment\SegmentController');
	Route::post('segment-get-subscriber-field', 'API\v1\Segment\SegmentController@getSubscriberListField');
	Route::get('segment-get-beacon', 'API\v1\Segment\SegmentController@getBeacon');
	Route::get('segment-get-campaign', 'API\v1\Segment\SegmentController@getCampaign');
	Route::post('segment-get-data', 'API\v1\Segment\SegmentController@getDataSegment');
	Route::post('segment-get-data-count', 'API\v1\Segment\SegmentController@countDataSegment');
	Route::get('segment-get-subscriber', 'API\v1\Segment\SegmentController@getSubscriber');
	Route::get('segment-get-tracking-source', 'API\v1\Segment\SegmentController@getTrackingSource');
	Route::get('segment-get-tracking-campaign', 'API\v1\Segment\SegmentController@getTrackingCampaign');
	Route::get('segment-get-tracking-ref', 'API\v1\Segment\SegmentController@getTrackingRef');
	Route::post('segment-export', 'API\v1\Segment\SegmentController@segmentExportdata');
	Route::get('get-subscriber-all', 'API\v1\Segment\SegmentController@getSusbcriberAll');
	Route::get('get-segment-name', 'API\v1\Segment\SegmentController@getSegmentName');
	Route::get('get-coupon-name', 'API\v1\Segment\SegmentController@getCouponName');

	Route::resource('setting-quick-segment', 'API\v1\Segment\QuickSegmentController');
	Route::post('setting-quick-segment-upload/{id}', 'API\v1\Segment\QuickSegmentController@uploadQuickSegment');
	Route::get('setting-quick-segment-import-result', 'API\v1\Segment\QuickSegmentController@importResult');
	Route::get('setting-quick-segment-export/{id}', 'API\v1\Segment\QuickSegmentController@exportQuickSegment');
	Route::get('get-quick-segment-name', 'API\v1\Segment\QuickSegmentController@getQuickSegmentName');
	//

	//report location sharing
	Route::get('report-location-sharing', 'API\v1\Report\LocationSharingController@getData');
	Route::get('report-location-sharing-export', 'API\v1\Report\LocationSharingController@exportData');
	//

	//Field
	Route::get('get-field-master-susbcriber', 'API\v1\FieldController@getMasterSubscriberField');
	Route::get('get-field-personalize', 'API\v1\GeneralController\GetDataAllController@getPersonalizeField');
	//

	Route::get('send-message-campaign/{id}', 'API\v1\CampaignController@sendCampaign');

	Route::get('env_angular', 'API\v1\EnvAngularController@getData');

	//Greeting
	Route::post('setting-greeting/{id}/active', 'API\v1\Greeting\GreetingController@postActive');
	Route::resource('setting-greeting', 'API\v1\Greeting\GreetingController');
	//

	//Role Permission
	Route::resource('role-permission', 'API\v1\RolePermission\RolePermissionController');
	//

	//Google Analytic
	Route::resource('google-analytic-setting', 'API\v1\GoogleAnalytic\GoogleAnalyticController');
	//

	//General Controller
	Route::get('get-all-user-profile-id', 'API\v1\GeneralController\GetDataAllController@getAllUserID');
	Route::get('get-all-province', 'API\v1\GeneralController\GetDataAllController@getProvince');
	//

	//Standard Ecommerce
	Route::resource('ecommerce-product', 'API\v1\Ecommerce\ProductController');// Manage Product setting
	Route::get('ecommerce-product-only-name', 'API\v1\Ecommerce\ProductController@onlyName');
	Route::get('ecommerce-product-only-dis/{id}', 'API\v1\Ecommerce\ProductController@onlyDiscount');
	Route::get('ecommerce-product-by-category/{id}', 'API\v1\Ecommerce\ProductController@getDataProductByCategory');

	Route::resource('ecommerce-category', 'API\v1\Ecommerce\CategoryController'); //ecommerce-sub-category setting
	Route::get('ecommerce-sub-category', 'API\v1\Ecommerce\CategoryController@getSubMainCategory');

	Route::resource('ecommerce-product-image-upload', 'API\v1\Ecommerce\ProductUploadImage');
	Route::post('ecommerce-product-image-upload-multiple', 'API\v1\Ecommerce\ProductUploadImage@uploadMultiple');

	Route::resource('ecommerce-order', 'API\v1\Ecommerce\OrderController'); //core order status
	Route::get('ecommerce-order-line-pay-refund/{id}', 'API\v1\Ecommerce\OrderController@paymentLineRefund');
	Route::post('ecommerce-order-payment-upload', 'API\v1\Ecommerce\OrderController@uploadMultiple');
	Route::post('ecommerce-order-payment-update-data', 'API\v1\Ecommerce\OrderController@updatePaymentData');
	Route::post('ecommerce-order-tracking-send-email', 'API\v1\Ecommerce\OrderController@trackingSendEmail');
	Route::post('ecommerce-order-tracking-send-line', 'API\v1\Ecommerce\OrderController@trackingSendLine');
	Route::get('ecommerce-check-order-payment/{id}', 'API\v1\Ecommerce\OrderController@checkOrderPayment');
	Route::delete('ecommerce-order-delete-item/{id}', 'API\v1\Ecommerce\OrderController@deleteOrderItem');
	Route::put('ecommerce-order-update-qty/{id}', 'API\v1\Ecommerce\OrderController@updateOrderItem');
	Route::get('ecommerce-order-export-all', 'API\v1\Ecommerce\OrderController@exportAllOrder');

	Route::resource('ecommerce-customer', 'API\v1\Ecommerce\CustomerController');
	Route::put('ecommerce-customer-update-customer-data/{id}', 'API\v1\Ecommerce\CustomerController@updateCustomerData');
	Route::put('ecommerce-customer-update-shipping-data/{id}', 'API\v1\Ecommerce\CustomerController@updateCustomerShippingAddressData'); //customer ที่ส่งสินค้า edit แล้ว save
	Route::put('ecommerce-customer-update-billing-data/{id}', 'API\v1\Ecommerce\CustomerController@updateCustomerTaxBillingAddressData');// ที่อยู่เจ้าของบัตรที่จ่ายตังค์ edit

	Route::resource('ecommerce-premium-kitchen-ware', 'API\v1\Ecommerce\PremiumKitchenwareEquipmentController');
	Route::post('ecommerce-premium-kitchen-ware-upload-multiple', 'API\v1\Ecommerce\PremiumKitchenwareEquipmentFileController@uploadMultiple');

	Route::resource('ecommerce-exclusive-online', 'API\v1\Ecommerce\ExclusiveOnlineController');
	Route::post('ecommerce-exclusive-online-upload-multiple', 'API\v1\Ecommerce\ExclusiveOnlineFileController@uploadMultiple');

	Route::resource('ecommerce-coupon', 'API\v1\Ecommerce\CouponController');
	Route::post('ecommerce-coupon-image-upload-multiple', 'API\v1\Ecommerce\CouponController@uploadMultiple');

	Route::resource('ecommerce-dt-management-group', 'API\v1\Ecommerce\DTManagementGroupController'); //DT Group
	Route::resource('ecommerce-dt-management', 'API\v1\Ecommerce\DTManagementController');//DT setting
	Route::post('ecommerce-dt-management-index', 'API\v1\Ecommerce\DTManagementController@getDataIndex');
	Route::post('ecommerce-dt-management-upload', 'API\v1\Ecommerce\DTManagementController@uploadData');
	Route::get('ecommerce-dt-management-get-log', 'API\v1\Ecommerce\DTManagementController@getLogData');
	Route::post('ecommerce-dt-management-upload-dt', 'API\v1\Ecommerce\DTManagementController@dtUploadData');

	Route::resource('ecommerce-dt-order', 'API\v1\Ecommerce\DTManagementOrderController'); // Order เฉพาะ DT
	Route::post('ecommerce-dt-order-update-item', 'API\v1\Ecommerce\DTManagementOrderController@updateOrderItem');
	Route::post('ecommerce-dt-order-confirm', 'API\v1\Ecommerce\DTManagementOrderController@updateOrderConfirmation');
	Route::post('ecommerce-dt-order-payment-confirm', 'API\v1\Ecommerce\DTManagementOrderController@updateOrderPaymentConfirmationReadyToShip');
	Route::post('ecommerce-dt-order-shipping-confirm', 'API\v1\Ecommerce\DTManagementOrderController@updateOrderShippingConfirmation');
	Route::post('ecommerce-dt-order-line-customer', 'API\v1\Ecommerce\DTManagementOrderController@confimationSendLineCustomer');
	Route::post('ecommerce-dt-ready-to-ship-line-customer', 'API\v1\Ecommerce\DTManagementOrderController@readyToShipSendLineCustomer');
	Route::post('ecommerce-dt-shipping-line-customer', 'API\v1\Ecommerce\DTManagementOrderController@confirmShippingCustomer');
	Route::post('ecommerce-dt-export', 'API\v1\Ecommerce\DTManagementOrderController@exportDtOrder');

	Route::resource('ecommerce-template-sent-message', 'API\v1\Ecommerce\TemplateSentMessageController');
	Route::post('ecommerce-template-sent-message/{id}/active', 'API\v1\Ecommerce\TemplateSentMessageController@postActive');
	Route::resource('ecommerce-temp-message-folder', 'API\v1\Ecommerce\TemplateSentMessageFolderController');

	Route::resource('ecom-line-temp-order-confirm', 'API\v1\Ecommerce\LineMessageTemplate\OrderConfirmationController');
	Route::post('ecom-line-temp-order-confirm/{id}/active', 'API\v1\Ecommerce\LineMessageTemplate\OrderConfirmationController@postActive');//
	Route::resource('ecom-line-temp-payment-confirm', 'API\v1\Ecommerce\LineMessageTemplate\PaymentConfirmationController');//payment confim menu order status
	Route::post('ecom-line-temp-payment-confirm/{id}/active', 'API\v1\Ecommerce\LineMessageTemplate\PaymentConfirmationController@postActive');
	Route::resource('ecom-line-temp-shipping-confirm', 'API\v1\Ecommerce\LineMessageTemplate\ShippingConfirmationController');//shiiping confim menu order status
	Route::post('ecom-line-temp-shipping-confirm/{id}/active', 'API\v1\Ecommerce\LineMessageTemplate\ShippingConfirmationController@postActive');
	Route::resource('ecom-line-temp-order-noti', 'API\v1\Ecommerce\LineMessageTemplate\OrderNotificationController');
	Route::post('ecom-line-temp-order-noti/{id}/active', 'API\v1\Ecommerce\LineMessageTemplate\OrderNotificationController@postActive');
	Route::resource('ecom-line-temp-order-cancel', 'API\v1\Ecommerce\LineMessageTemplate\OrderCancelationController');
	Route::post('ecom-line-temp-order-cancel/{id}/active', 'API\v1\Ecommerce\LineMessageTemplate\OrderCancelationController@postActive');
	Route::resource('ecom-line-temp-customer-payment', 'API\v1\Ecommerce\LineMessageTemplate\CustomerConfirmPaymentController');
	Route::post('ecom-line-temp-customer-payment/{id}/active', 'API\v1\Ecommerce\LineMessageTemplate\CustomerConfirmPaymentController@postActive');
	Route::resource('ecom-line-temp-order-end', 'API\v1\Ecommerce\LineMessageTemplate\OrderEndController');
	Route::post('ecom-line-temp-order-end/{id}/active', 'API\v1\Ecommerce\LineMessageTemplate\OrderEndController@postActive');
	Route::resource('ecom-line-temp-first-regis', 'API\v1\Ecommerce\LineMessageTemplate\FirstRegisterController');
	Route::post('ecom-line-temp-first-regis/{id}/active', 'API\v1\Ecommerce\LineMessageTemplate\FirstRegisterController@postActive');
	Route::resource('ecom-line-temp-cus-pay-over-48', 'API\v1\Ecommerce\LineMessageTemplate\CustomerPaymentOverdue48Controller');
	Route::post('ecom-line-temp-cus-pay-over-48/{id}/active', 'API\v1\Ecommerce\LineMessageTemplate\CustomerPaymentOverdue48Controller@postActive');
	Route::resource('ecom-line-temp-cus-pay-over-72', 'API\v1\Ecommerce\LineMessageTemplate\CustomerPaymentOverdue72Controller');
	Route::post('ecom-line-temp-cus-pay-over-72/{id}/active', 'API\v1\Ecommerce\LineMessageTemplate\CustomerPaymentOverdue72Controller@postActive');
	Route::resource('ecom-line-temp-conf-after-pur', 'API\v1\Ecommerce\LineMessageTemplate\OrderConfirmationAfterPurchaseController');
	Route::post('ecom-line-temp-conf-after-pur/{id}/active', 'API\v1\Ecommerce\LineMessageTemplate\OrderConfirmationAfterPurchaseController@postActive');
	Route::resource('ecom-line-temp-conf-no-stock', 'API\v1\Ecommerce\LineMessageTemplate\OrderConfirmationNoStockController');
	Route::post('ecom-line-temp-conf-no-stock/{id}/active', 'API\v1\Ecommerce\LineMessageTemplate\OrderConfirmationNoStockController@postActive');

	Route::post('ecom-line-temp-auto-cus-conf-pay-bank', 'API\v1\Ecommerce\LineMessageTemplate\LineMessageTemplateAutoController@customerConfirmPaymentBankTransfer');
	Route::get('ecom-line-temp-auto-cus-conf-pay-bank', 'API\v1\Ecommerce\LineMessageTemplate\LineMessageTemplateAutoController@getCustomerConfirmPaymentBankTransfer');
	Route::post('ecom-line-temp-auto-cus-conf-pay-cod', 'API\v1\Ecommerce\LineMessageTemplate\LineMessageTemplateAutoController@customerConfirmPaymentCOD');
	Route::get('ecom-line-temp-auto-cus-conf-pay-cod', 'API\v1\Ecommerce\LineMessageTemplate\LineMessageTemplateAutoController@getCustomerConfirmPaymentCOD');
	Route::post('ecom-line-temp-auto-order-conf-conf', 'API\v1\Ecommerce\LineMessageTemplate\LineMessageTemplateAutoController@orderConfirmationConfirm');
	Route::get('ecom-line-temp-auto-order-conf-conf', 'API\v1\Ecommerce\LineMessageTemplate\LineMessageTemplateAutoController@getOrderConfirmationConfirm');
	Route::post('ecom-line-temp-auto-order-conf-out', 'API\v1\Ecommerce\LineMessageTemplate\LineMessageTemplateAutoController@orderConfirmationOutStock');
	Route::get('ecom-line-temp-auto-order-conf-out', 'API\v1\Ecommerce\LineMessageTemplate\LineMessageTemplateAutoController@getOrderConfirmationOutStock');
	Route::post('ecom-line-temp-auto-ready-to-ship', 'API\v1\Ecommerce\LineMessageTemplate\LineMessageTemplateAutoController@readyToShip');
	Route::get('ecom-line-temp-auto-ready-to-ship', 'API\v1\Ecommerce\LineMessageTemplate\LineMessageTemplateAutoController@getReadyToShip');
	Route::post('ecom-line-temp-auto-shipping-conf', 'API\v1\Ecommerce\LineMessageTemplate\LineMessageTemplateAutoController@shippingConfirm');
	Route::get('ecom-line-temp-auto-shipping-conf', 'API\v1\Ecommerce\LineMessageTemplate\LineMessageTemplateAutoController@getShippingConfirm');
	Route::post('ecom-line-temp-auto-end-order', 'API\v1\Ecommerce\LineMessageTemplate\LineMessageTemplateAutoController@endOrder');
	Route::get('ecom-line-temp-auto-end-order', 'API\v1\Ecommerce\LineMessageTemplate\LineMessageTemplateAutoController@getEndOrder');
	Route::post('ecom-line-temp-auto-first-regis', 'API\v1\Ecommerce\LineMessageTemplate\LineMessageTemplateAutoController@firstRegister');
	Route::get('ecom-line-temp-auto-first-regis', 'API\v1\Ecommerce\LineMessageTemplate\LineMessageTemplateAutoController@getFirstRegister');
	Route::post('ecom-line-temp-auto-cus-pay-over-48', 'API\v1\Ecommerce\LineMessageTemplate\LineMessageTemplateAutoController@paymentOverdue48');
	Route::get('ecom-line-temp-auto-cus-pay-over-48', 'API\v1\Ecommerce\LineMessageTemplate\LineMessageTemplateAutoController@getPaymentOverdue48');
	Route::post('ecom-line-temp-auto-cus-pay-over-72', 'API\v1\Ecommerce\LineMessageTemplate\LineMessageTemplateAutoController@paymentOverdue72');
	Route::get('ecom-line-temp-auto-cus-pay-over-72', 'API\v1\Ecommerce\LineMessageTemplate\LineMessageTemplateAutoController@getPaymentOverdue72');
	Route::post('ecom-line-temp-auto-conf-after-pur', 'API\v1\Ecommerce\LineMessageTemplate\LineMessageTemplateAutoController@orderConfirmationAfterPurchase');
	Route::get('ecom-line-temp-auto-conf-after-pur', 'API\v1\Ecommerce\LineMessageTemplate\LineMessageTemplateAutoController@getOrderConfirmationAfterPurchase');
	Route::post('ecom-line-temp-auto-conf-no-stock', 'API\v1\Ecommerce\LineMessageTemplate\LineMessageTemplateAutoController@orderConfirmationNoStock');
	Route::get('ecom-line-temp-auto-conf-no-stock', 'API\v1\Ecommerce\LineMessageTemplate\LineMessageTemplateAutoController@getOrderConfirmationNoStock');

	Route::resource('ecom-product-recommend', 'API\v1\Ecommerce\ProductRecommendController');//setting สินค้าแนะนำ

	Route::resource('ecom-custom-page', 'API\v1\Ecommerce\CustomPageController');
	Route::post('ecom-custom-page-by-original-url', 'API\v1\Ecommerce\CustomPageController@getDataByOriginalURL');

	Route::resource('ecom-banner-home', 'API\v1\Ecommerce\Banner\BannerHomeController'); // setting  homepage
	Route::post('ecom-banner-home-img-up-multi', 'API\v1\Ecommerce\Banner\BannerHomeController@uploadMultiple'); //upload หลายรูป  setting  homepage
	Route::resource('ecom-banner-chg-mobile', 'API\v1\Ecommerce\Banner\BannerChangeMobileController');
	Route::post('ecom-banner-chg-mobile-img-up-multi', 'API\v1\Ecommerce\Banner\BannerChangeMobileController@uploadMultiple');
	Route::resource('ecom-banner-otp', 'API\v1\Ecommerce\Banner\BannerOTPController');
	Route::post('ecom-banner-otp-img-up-multi', 'API\v1\Ecommerce\Banner\BannerOTPController@uploadMultiple');
	Route::resource('ecom-banner-register', 'API\v1\Ecommerce\Banner\BannerRegisterController');
	Route::post('ecom-banner-register-img-up-multi', 'API\v1\Ecommerce\Banner\BannerRegisterController@uploadMultiple');
	Route::resource('ecom-banner-thank', 'API\v1\Ecommerce\Banner\BannerThankController');
	Route::post('ecom-banner-thank-img-up-multi', 'API\v1\Ecommerce\Banner\BannerThankController@uploadMultiple');

	Route::get('get-report-conversion', 'API\v1\Ecommerce\Report\ReportConversionController@getDataConversion');
	Route::post('report-conversion-import', 'API\v1\Ecommerce\Report\ReportConversionController@importData');
	Route::get('get-report-conversion-existing', 'API\v1\Ecommerce\Report\ReportConversionController@getDataConversionExisting');

	Route::get('get-ecom-dash-shop-behav', 'API\v1\Ecommerce\Report\DashboardController@getDataDashboardShoppingBehavior');
	Route::get('get-ecom-dash-return-visit', 'API\v1\Ecommerce\Report\DashboardController@getDataDashboardReturningVisitor');
	Route::get('get-ecom-dash-new-visit', 'API\v1\Ecommerce\Report\DashboardController@getDataDashboardNewVisitor');
	Route::post('get-ecom-dash-prod-ban', 'API\v1\Ecommerce\Report\DashboardController@getDataDashboardProductOrBanner');
	Route::post('get-ecom-dash-prod-1', 'API\v1\Ecommerce\Report\DashboardController@getDataDashboardProductPerformance1');
	Route::get('get-ecom-dash-prod-2', 'API\v1\Ecommerce\Report\DashboardController@getDataDashboardProductPerformance2');
	Route::post('get-ecom-dash-product-click', 'API\v1\Ecommerce\Report\DashboardController@productClick');
	Route::post('get-ecom-dash-get-more-order', 'API\v1\Ecommerce\Report\DashboardController@getViewMoreOrder');
	Route::get('get-ecom-dash-get-product-sku', 'API\v1\Ecommerce\Report\DashboardController@getProductSKU');
	Route::get('get-ecom-dash-get-order', 'API\v1\Ecommerce\Report\DashboardController@getOrder');
	//Front-End Ecommerce
	Route::post('ecommerce-customer-register', 'Ecommerce\CustomerRegisterController@registerCustomer');// register ส่งเข้า email
	Route::get('ecommerce-customer-check-register/{id}', 'Ecommerce\CoreFunctionController@checkRegister');
	Route::get('ecommerce-province', 'Ecommerce\CoreFunctionController@getProvince'); //get จังหวัด
	Route::get('ecommerce-province-district', 'Ecommerce\CoreFunctionController@getDistrict');
	Route::get('ecommerce-district-subdistrict', 'Ecommerce\CoreFunctionController@getSubDistrict');
	Route::get('ecommerce-normal-province', 'Ecommerce\CoreFunctionController@getNormalProvince');
	Route::get('ecommerce-normal-province-district', 'Ecommerce\CoreFunctionController@getNormalDistrict');
	Route::get('ecommerce-normal-district-subdistrict', 'Ecommerce\CoreFunctionController@getNormalSubDistrict');


	Route::get('ecommerce-customer-get-otp/{id}', 'Ecommerce\OtpController@getOtp');
	Route::get('ecommerce-customer-check-otp/{id}', 'Ecommerce\OtpController@checkOtpIsActive');
	Route::post('ecommerce-customer-active-otp', 'Ecommerce\OtpController@activeOtp');

	Route::get('ecommerce-customer-check-dt/{postcode}', 'Ecommerce\CoreFunctionController@checkDT');

	Route::get('ecommerce-get-product', 'Ecommerce\ProductController@getProduct');  //เมนูแยกประเภท search
	Route::get('ecommerce-get-product/{id}', 'Ecommerce\ProductController@getShowProduct');

	Route::get('ecommerce-push-notification-customer/{id}', 'Ecommerce\PushNotificationController@pushNotificationCustomer');
	Route::get('ecommerce-push-notification-admin', 'Ecommerce\PushNotificationController@pushNotificationAdmin');
	Route::get('ecommerce-push-notification-dt/{id}', 'Ecommerce\PushNotificationController@pushNotificationDT');

	Route::post('ecommerce-customer-order', 'Ecommerce\OrderController@customerOrder');
	Route::put('ecommerce-customer-order-update-shipping-address/{id}', 'Ecommerce\OrderController@updateOrderShippingAddress'); //แก้ไขมูลในการจัดส่งสินค้า หลังจากกดเลือกสินค้า
	Route::get('ecommerce-order-check-coupon/{couponName}', 'Ecommerce\OrderController@checkCouponData'); //check ข้อมูล Coupon
	Route::get('ecommerce-get-history-order/{id}', 'Ecommerce\OrderController@getOrderHistory'); //แสดงสินค้าที่เคยสั้ง หน้า History
	Route::post('ecommerce-calculate-purchase', 'Ecommerce\OrderController@calculatePurchaseOrder'); //คำนวนราคาใหม่

	Route::post('ecommerce-customer-add-shipping-address', 'Ecommerce\CustomerController@addShippingAddress'); //เพิ่มที่อยู่ หน้า
	Route::get('ecommerce-customer-get-shipping-address/{id}', 'Ecommerce\CustomerController@getShippingAddress'); //
	Route::get('ecommerce-customer-show-shipping-address/{id}', 'Ecommerce\CustomerController@getShippingAddress');
	Route::delete('ecommerce-customer-delete-shipping-address/{id}', 'Ecommerce\CustomerController@deleteShippingAddress'); //ลบข้อมูลที่จัดส่งสินค้า
	Route::post('ecommerce-customer-add-billing-address', 'Ecommerce\CustomerController@addBillingAddress');
	Route::get('ecommerce-customer-get-billing-address/{id}', 'Ecommerce\CustomerController@getBillingAddress');
	Route::delete('ecommerce-customer-delete-billing-address/{id}', 'Ecommerce\CustomerController@deleteBillingAddress');
	Route::get('ecommerce-customer-get-profile/{id}', 'Ecommerce\CustomerController@getCustomerProfile'); //get profile รวมถึง History
	Route::post('ecommerce-customer-image-upload-multiple', 'Ecommerce\CustomerController@uploadMultiple');
	Route::post('ecommerce-customer-change-mobile', 'Ecommerce\CustomerController@changeMobilePhone');
	Route::post('ecommerce-customer-trade-partner', 'Ecommerce\CustomerController@updateDataCustomerTradePartner');
	Route::post('ecommerce-customer-update-img-profile', 'Ecommerce\CustomerController@updateDataCustomerImageProfile');

	Route::post('ecommerce-add-whishlist', 'Ecommerce\WhistlistsController@addWhishlist');// กดหัวใจ whishlist
	Route::get('ecommerce-get-whishlist/{id}', 'Ecommerce\WhistlistsController@getWhishlist');//แสดง whishlist
	Route::delete('ecommerce-get-whishlist/{id}', 'Ecommerce\WhistlistsController@deleteWhishlist');// กดหัวใจ whishlist

	Route::get('ecommerce-get-reward-data', 'Ecommerce\RewardController@getReward');
	Route::put('ecommerce-reward-kitchenware-reedeem/{id}', 'Ecommerce\RewardController@kitchenwareReedeem');
	Route::put('ecommerce-reward-exclusive-reedeem/{id}', 'Ecommerce\RewardController@exclusiveOnlineReedeem');
	Route::get('ecommerce-reward-kitchenware-show/{customerId}/{kitchenwareId}', 'Ecommerce\RewardController@showKitchenware');
	Route::get('ecommerce-reward-exclusive-show/{customerId}/{exclusiveId}', 'Ecommerce\RewardController@showExclusiveOnline');

	Route::post('ecommerce-store-customer-survey', 'Ecommerce\CustomerSurveyController@storeSurvey');
	Route::post('ecommerce-store-customer-survey-item', 'Ecommerce\CustomerSurveyController@storeSurveyItem');

	Route::get('ecommerce-get-customer-coupon/{customerId}', 'Ecommerce\CustomerCouponController@getCustomerCoupon'); //แสดงว่า
	Route::post('ecommerce-customer-reedeem-coupon', 'Ecommerce\CustomerCouponController@reedeemCoupon');
	Route::post('ecommerce-customer-used-coupon', 'Ecommerce\CustomerCouponController@usedCoupon');
	Route::get('ecommerce-customer-get-all-coupon/{id}', 'Ecommerce\CustomerCouponController@getCustomerAllCoupon');

	Route::post('ecommerce-customer-add-shopping-cart', 'Ecommerce\CustomerShoppingCartController@addShoppingCart');// front หน้า เลือกของลงตะกร้า
	Route::get('ecommerce-customer-get-shopping-cart/{id}', 'Ecommerce\CustomerShoppingCartController@getShoppingCart'); // front หน้า แสดงของในกระตร้า
	Route::delete('ecommerce-customer-delete-shopping-cart-item/{id}', 'Ecommerce\CustomerShoppingCartController@deleteShoppingCartItem'); //ลบ front หน้า เลือกของลงตะกร้า
	Route::put('ecommerce-customer-update-shopping-cart/{id}', 'Ecommerce\CustomerShoppingCartController@updateShoppingCart');//เพิ่มจำนวน

	Route::get('ecommerce-get-product-recommend', 'Ecommerce\ProductRecommendController@getProductRecommend'); //สินค้าแนะนำ

	Route::post('ecom-import-product', 'Ecommerce\ProductController@importProduct');
	//

	//

	//Bot JoinGroup And JoinRoom
	Route::post('bot-join-auto-reply-keyword/{id}/active', 'API\v1\BotJoinGroupAndRoom\BotJoinGroupAndRoomAutoReplyKeyword@postActive');
	Route::resource('bot-join-auto-reply-keyword', 'API\v1\BotJoinGroupAndRoom\BotJoinGroupAndRoomAutoReplyKeyword');
	//

	//Template Message
	Route::resource('template-message-folder', 'API\v1\TemplateMessage\TemplateMessageFolderController');
	Route::resource('template-message', 'API\v1\TemplateMessage\TemplateMessageController');
	//

	//Campaign File
	Route::resource('campaign-photo', 'API\v1\Photo\CampaignPhotoController');
	Route::resource('campaign-video', 'API\v1\Video\CampaignVideoController');
	//

	//PageList Label
	Route::resource('page-list', 'API\v1\PageList\PageListController');
	Route::resource('page-list-label', 'API\v1\PageList\PageListLabelController');
	//

	//DownloadFile
	Route::get('get-file-subscriber', 'API\v1\DownloadFile\DownloadFileController@getFileSubscriber');
	//

	///////////////////////////////////////Message File/////////////////////////////////
	Route::post('message-file', 'API\v1\MessageFile\MessageFileController@uploadMultiple');
	/////////////////////////////////////////////////////////////////////////////////////

	//Eplus
	Route::get('eplus-customer-mapping-index', 'API\v1\Eplus\Customer\CustomerController@getIndexData');
	Route::post('eplus-customer-mapping-store', 'API\v1\Eplus\Customer\CustomerController@storeMappingData');
	Route::get('eplus-salesman-mapping-index', 'API\v1\Eplus\Salesman\SalesmanController@getIndexData');
	Route::post('eplus-salesman-mapping-store', 'API\v1\Eplus\Salesman\SalesmanController@storeMappingData');
	Route::get('eplus-salesman-get-salesman-code', 'API\v1\Eplus\Salesman\SalesmanController@getSalesmanCode');

	Route::resource('eplus-customer-temp-message', 'API\v1\Eplus\Customer\TempMessageController');
	Route::resource('eplus-salesman-temp-message', 'API\v1\Eplus\Salesman\TempMessageController');
	//

	///////////////////////////////////////Report///////////////////////////////////
	Route::post('report-keyword-inquiry', 'API\v1\Report\KeywordInquiryController@keywordInquiryReport');
	Route::post('report-keyword-inquiry-export', 'API\v1\Report\KeywordInquiryController@keywordInquiryReportExport');
	////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////Online Exclusive Module///////////////////////////////////
	Route::resource('online-exclusive', 'API\v1\Ecommerce\OnlineExclusiveModule\OnlineExclusiveModuleController');
	Route::post('online-exclusive-upload', 'API\v1\Ecommerce\OnlineExclusiveModule\OnlineExclusiveModuleUploadController@uploadFile');
	Route::post('online-exclusive-to-redeem', 'API\v1\Ecommerce\OnlineExclusiveModule\OnlineExclusiveModuleController@redeem');
	Route::post('online-exclusive-not-redeem', 'API\v1\Ecommerce\OnlineExclusiveModule\OnlineExclusiveModuleController@notRedeem');
	///////////////////////////////////////Online Exclusive Module Front///////////////////////////////
	Route::get('online-exclusive-listing', 'Ecommerce\OnlineExclusiveModule\OnlineExclusiveModuleController@exclusiveModuleListing');
	Route::get('online-exclusive-detail/{id}', 'Ecommerce\OnlineExclusiveModule\OnlineExclusiveModuleController@exclusiveModuleDetail');
	Route::get('online-exclusive-video-detail/{id}', 'Ecommerce\OnlineExclusiveModule\OnlineExclusiveModuleController@exclusiveModuleVideoDetail');
	Route::post('online-exclusive-redeem', 'Ecommerce\OnlineExclusiveModule\OnlineExclusiveModuleController@exclusiveModuleReDeem');
	/////////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////Refresh Schedule//////////////////////////////////////////
	Route::get('schedule-refresh-data', 'API\v1\CoreFunction\ScheduleFunctionController@runSchedule');
	/////////////////////////////////////////////////////////////////////////////////////////////////

});
