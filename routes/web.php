<?php
use Illuminate\Http\Request;
use Carbon\Carbon;
use YellowProject\Ecommerce\Task\CoreFunction;
use YellowProject\Subscriber\MasterSubscriber;
use YellowProject\Ecommerce\CoreFunction as EcomCoreFunction;

Route::get('/test-coupon-page-campaign-register', function()
{
    return view('campaign-give-coupon.register');
});

Route::get('/test-coupon-page-campaign-coupon', function()
{
    return view('campaign-give-coupon.coupon');
});

Route::get('/refresh-schedule-page', function()
{
    return view('schedule-refresh.index');
});

Route::get('/import-dt-management', function()
{
    return view('import-dt.index');
});

Route::get('/test-send-message-httprequest', function()
{
    $fp = fopen(public_path().'/errorlog.txt', 'w');
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://api.line.me/v2/bot/message/push",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_VERBOSE => 1,
      CURLOPT_STDERR => $fp,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "{\r\n    \"to\": \"U82759f4b97da598171d2d2e135550ae3\",\r\n    \"messages\":[\r\n        {\r\n            \"type\":\"text\",\r\n            \"text\":\"Hello, world1\"\r\n        },\r\n        {\r\n            \"type\":\"text\",\r\n            \"text\":\"Hello, world2\"\r\n        }\r\n    ]\r\n}",
      CURLOPT_HTTPHEADER => array(
        "Authorization: Bearer  9ZrdXBEASEnaefUFFhkI5Wx2JdhwoLO72UNt6/oVxOBF2yTh0huWodDxqZGgOCSbPim948+aVzLbvEIezGE4S7kQwcd9WYCXreFD+Z6Sd5O1szIi6+jE7jiItktyL6LFL8Zrtex8yuiaPEk+XQmP/I9PbdgDzCFqoOLOYbqAITQ=",
        "Cache-Control: no-cache",
        "Content-Type: application/json",
        "Postman-Token: 6697c33b-d41e-4fd9-83f8-2476fbcf1a49"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      echo $response;
    }
});

Route::get('/tracking-gen-all-liff', function()
{
    $baseUrl = \URL::to('/');
    $trackings = \YellowProject\TrackingBc::all();
    foreach ($trackings as $key => $tracking) {
      if($tracking->liff_id == ""){
        $urlLiff = $baseUrl."/bc-app/".$tracking->code;
        $responses = \YellowProject\LineFunction\LineFunctionLiff::createLiff($urlLiff);
        if($responses['is_liff'] == 1){
            $tracking->update([
                'is_line_liff' => 1,
                'liff_id' => $responses['liff_id']
            ]);
        }
      }
    }
});

Route::get('/test-send-sms-aws', function()
{
    EcomCoreFunction::sendSms();
});

Route::get('/import-product-page', function()
{
    return view('import-product.index');
});

Route::get('/logout', function()
{
    abort(404);
});

Route::get('test-cut-phonenumber', function()
{
    $phoneNumber = '66855520558';
    $phoneNumberOnly = substr($phoneNumber, 2);
    $realPhoneNumber = '0'.$phoneNumberOnly;
    dd($realPhoneNumber);
});

Route::get('gendata-master-susbcriber', function()
{
    MasterSubscriber::genMasterSubscriber();
});

Route::get('test-send-xxxx', function()
{
    CoreFunction::checkDtTask();
});

Route::get('test-send-real', function()
{
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://api.line.me/v2/bot/message/push",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "{\r\n    \"to\": \"U82759f4b97da598171d2d2e135550ae3\",\r\n    \"messages\":[\r\n        {\r\n            \"type\":\"text\",\r\n            \"text\":\"Hello, world1\"\r\n        },\r\n        {\r\n            \"type\":\"text\",\r\n            \"text\":\"Hello, world2\"\r\n        }\r\n    ]\r\n}",
      CURLOPT_HTTPHEADER => array(
        "Authorization: Bearer 0IvsbispvwZBn6RDrIq4g6OaE9knQi9L6Kj9WGE0osZWw8hYzbfQADou+jcxZjmCNk3s1CC9mWWsnHCG6+y9J1dLKwHfa9SLlZACsc9B4zfjTdUaCth7+QhLAEwUxk9BQVtLGD1p7F5XwbEwXqEUfsboBKSX4lHvvKE4dVWuZjRQ6ro7aIT6DbYnVE6QL0dR",
        "Cache-Control: no-cache",
        "Content-Type: application/json"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      echo $response;
    }
});

Route::get('test-check-product', function()
{
    $dateNow = Carbon::now()->format('Y-m-d H:i:s');
    $datas = \DB::table('dim_ecommerce_order')
      ->where('order_status','Waiting for Payment')
      ->where('send_message_48_hours',0)
      ->where(\DB::raw('TIMESTAMPDIFF(HOUR, created_at, "'.$dateNow.'")'),'>=',48)
      ->get();
    dd($datas);
});


Route::get('clear-data-ecommerce', function () {
  \YellowProject\Ecommerce\Customer\Customer::truncate();
  \YellowProject\Ecommerce\Customer\CustomerCancellation::truncate();
  \YellowProject\Ecommerce\Customer\CustomerImage::truncate();
  \YellowProject\Ecommerce\Customer\CustomerReedeemRewardHistory::truncate();
  \YellowProject\Ecommerce\Customer\CustomerShippingAddress::truncate();
  \YellowProject\Ecommerce\Customer\CustomerTaxBillingAddress::truncate();
  \YellowProject\Ecommerce\Customer\CustomerWishlist::truncate();
  \YellowProject\Ecommerce\Customer\CustomerShippingAddress::truncate();
  \YellowProject\Ecommerce\Customer\CustomerTaxBillingAddress::truncate();
  \YellowProject\Ecommerce\OTP\CustomerOtp::truncate();
  \YellowProject\Ecommerce\OTP\CustomerOtpLog::truncate();
  \YellowProject\Ecommerce\Order\Order::truncate();
  \YellowProject\Ecommerce\Order\OrderAdminHistory::truncate();
  \YellowProject\Ecommerce\Order\OrderConfirmation::truncate();
  \YellowProject\Ecommerce\Order\OrderDTPayment::truncate();
  \YellowProject\Ecommerce\Order\OrderPayment::truncate();
  \YellowProject\Ecommerce\Order\OrderProduct::truncate();
  \YellowProject\Ecommerce\Order\OrderShippingConfirmation::truncate();
  \YellowProject\Ecommerce\Order\OrderTracking::truncate();
  // \YellowProject\Ecommerce\DTManagement\DTManagementRegisterData::truncate();
  // \YellowProject\User::where('is_dt',1)->delete();
});

Route::get('500', function()
{
    abort(500);
});

//SAMMY
Route::get('/sam-test', function () {
    //return view('fwd.show.taxcer-sendmail');
});

Route::get('/full-ecommerce', function () {
  $googleAnalytic = \YellowProject\GoogleAnalytic\GoogleAnalytic::first();
  return view('full-ecommerce/index2')
    ->with('googleAnalytic',$googleAnalytic);
});

Route::get('/ecommerce', function () {
    return view('line-ecom.index');
});

Route::get('/ecom-condition', function () {
    return view('e-comerce.condition');
});

//do not delete
Route::get('storage/line/image/{messageType}/{imageid}', function ($imageid)
{
    //DIRECTORY_SEPARATOR
    // dd(storage_path('public'.DIRECTORY_SEPARATOR .'line'.DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'message'.DIRECTORY_SEPARATOR. $filename));
    return Image::make(storage_path('public'.DIRECTORY_SEPARATOR .'line'.DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'message'.DIRECTORY_SEPARATOR. $imageid))->response();
});

//TODO: samtest
Route::get('/device-test', function () {
    return view('sam-test.index');
});

Route::get('/test-line-login', function () {
    return view('line-login');
});

// Route::get('/hellosoda', function () {
//     return view('hello_soda.index');
// });

Route::group(['middleware' => 'guest'], function() {
  Route::get('/cms-lineofficialadmin', function () {
      return view('auth.login');
  });
});

Route::group(['middleware' => 'auth'], function() {
  Route::get('/cms-lineofficialadmin', function () {
      // dd(\Session::get('FwdKey', ''));
      return view('web-view.index');
  });
  // Route::get('/test', function () {
  //     return view('web-view.index');
  // });
  Route::middleware('cors')->resource('fwd-encrypt', 'API\v1\FWD\FWDEncryptController');
});

Auth::routes();

Route::get('signin', 'HomeController@index');

Route::get('line-login', 'Auth\AuthController@redirectToProvider')->name('line-login');
Route::get('callback', 'Auth\AuthController@handleProviderCallback');
Route::post('line-logout', 'Auth\AuthController@logout')->name('line-logout');

Route::get('dashboard', 'DashboardController@index');

Route::get('bc/{code}', 'RecieveTrackingBCController@bcCenter');
Route::get('bc-recieve/{code}', 'RecieveTrackingBCController@recieveCode');
Route::get('bc-app', 'RecieveTrackingBCController@recieveLiff');
Route::get('dt-recieve/{code}', 'RecieveDTManagementController@recieveCode');
Route::get('ecommerce-payment/{code}', 'RecieveEcommercePaymentController@recieveCode');
Route::get('ecommerce-return-thank', 'RecieveEcommercePaymentController@returnThank');
Route::get('ecommerce-customer-payment-detail/{id}', 'Ecommerce\CustomerConfirmPaymentController@paymentDetail');
Route::get('ecommerce-customer-confirm-payment/{id}', 'Ecommerce\CustomerConfirmPaymentController@confirmPayment');
Route::post('ecommerce-customer-payment-upload', 'Ecommerce\CustomerConfirmPaymentController@uploadPayment'); //หลังupload สลิป //Frontend หน้า upload สลิป
Route::get('ecommerce-customer-confirm-cod/{id}', 'Ecommerce\CustomerConfirmPaymentController@confirmPaymentCOD');
Route::get('ecommerce-customer-confirm-promptpay/{id}', 'Ecommerce\CustomerConfirmPaymentController@confirmPaymentPromptPay');
Route::get('ecommerce-admin-register', 'Ecommerce\RegisterAdminController@getPage');
Route::post('ecommerce-admin-register', 'Ecommerce\RegisterAdminController@storeData');

Route::resource('activity', 'ProfillingController');
// Route::resource('ecom', 'EcomController');
Route::get('shopping-line', function () {
  return redirect()->action('Auth\AuthController@redirectToProvider',['type' => 'ecom']);
});

Route::get('ecommerce-admin-regis', function (Request $request) {
  return redirect()->action('Auth\AuthController@redirectToProvider',['type' => 'ecommerce_admin']);
});

Route::get('ecommerce-payment-receive/{code}', function (Request $request,$code) {
  \Session::put('ecommerce_payment_code', $code);
  return redirect()->action('Auth\AuthController@redirectToProvider',['type' => 'ecommerce_payment']);
});

Route::get('payment/{code}', function (Request $request,$code) {
  \Session::put('ecommerce_payment_code', $code);
  return redirect()->action('Auth\AuthController@redirectToProvider',['type' => 'ecommerce_payment']);
});

Route::get('dt/{code}', function (Request $request,$code) {
  \Session::put('dt_code', $code);
  return redirect()->action('Auth\AuthController@redirectToProvider',['type' => 'dt_code']);
});

// Route::get('bc/{code}', function (Request $request,$code) {
//   \Session::put('tracking_bc_code', $code);
//   return redirect()->action('Auth\AuthController@redirectToProvider',['type' => 'bc_tracking']);
// });

Route::get('/page/errormsg', function () {
    return view('fwd.error-page.index');
});

Route::get('/register', function () {
    return view('errors.404');
});

Route::get('/password/email', function () {
    return view('errors.404');
});


Route::get('upload-auto-reply', function () {
  return view('upload-auto-reply.index');
});

// Auth::routes();

// Route::get('/home', 'HomeController@index');

Route::post('field','FeildController@index');

Route::get('/notfound-page', function () {
    return view('errors.404');
});

Route::get('/test-tableu', function () {
    return view('tableu.index');
});


Route::resource('api-folder-campagin', 'API\v1\CampaignFolderController');


Route::get('gen-data-dt-role', function () {
  GenData::genDtRole();
});


//Eplus
  //Customer
  Route::get('eplus-register', 'Eplus\Customer\PageController@getRegisterPage');
  Route::post('eplus-register', 'Eplus\Customer\CustomerController@storeDataRegister');
  Route::get('eplus-customer', 'Eplus\Customer\PageController@getShowDataCustomer');
  Route::get('eplus-customer-otp', 'Eplus\Customer\PageController@getOTPPage');
  Route::post('eplus-customer-otp', 'Eplus\Customer\OTPController@recieveOTP');
  Route::get('eplus-customer-re-otp/{id}', 'Eplus\Customer\OTPController@newOTP');
  Route::get('eplus-customer-error', 'Eplus\Customer\PageController@getErrorPage');
  //
  //Salesman
  Route::get('eplus-salesman-register', 'Eplus\Salesman\PageController@getRegisterPage');
  Route::post('eplus-salesman-register', 'Eplus\Salesman\SalesmanController@storeDataRegister');
  Route::get('eplus-salesman', 'Eplus\Salesman\PageController@getShowDataCustomer');
  Route::get('eplus-salesman-otp', 'Eplus\Salesman\PageController@getOTPPage');
  Route::post('eplus-salesman-otp', 'Eplus\Salesman\OTPController@recieveOTP');
  Route::get('eplus-salesman-re-otp/{id}', 'Eplus\Salesman\OTPController@newOTP');
  Route::get('eplus-salesman-error', 'Eplus\Salesman\PageController@getErrorPage');
  //

  Route::post('setting-user-dt', 'API\v1\UserController@storeUserDt');
  //Clear Data Customer
  Route::get('clear-data-eplus-customer', function (Request $request) {
    \YellowProject\Eplus\Customer\OTP\CustomerOTP::truncate();
    \YellowProject\Eplus\Customer\OTP\CustomerOTPLog::truncate();
    // \YellowProject\Eplus\Customer\CustomerDataMapping::truncate();
    \YellowProject\Eplus\Customer\CustomerRegisterData::truncate();
  });
  //
  //Clear Data Salesman
  Route::get('clear-data-eplus-salesman', function (Request $request) {
    \YellowProject\Eplus\Salesman\OTP\SalesmanOTP::truncate();
    \YellowProject\Eplus\Salesman\OTP\SalesmanOTPLog::truncate();
    // \YellowProject\Eplus\Salesman\SalesmanDataMapping::truncate();
    \YellowProject\Eplus\Salesman\SalesmanRegisterData::truncate();
  });
  //

  Route::get('test-schedule', 'ScheduleCheck\ScheduleController@checkSchedulePage');
  Route::get('test-schedule-start', 'ScheduleCheck\ScheduleController@scheduleStart');

  Route::get('give-coupon-register', 'Ecommerce\CampaignGiveCouponController@mainPage');
  Route::post('give-coupon-register-store', 'Ecommerce\CampaignGiveCouponController@registerPageStore');
  Route::get('give-coupon', 'Ecommerce\CampaignGiveCouponController@couponPage');
//
