<!DOCTYPE html>
<html>

<head>
    <meta charset=utf-8>
    <meta name=viewport content="width=device-width,initial-scale=1">
    <link rel="shortcut icon" href=./asset-ecommerce2/favicon.png>
    <title>TH | Unilever Food Solutions | Unilever Food Solutions</title>
    <meta name=csrf-token content="{{ csrf_token() }}">
    <meta name=hostname content="{{ Request::root() }}">
    <meta name=comeback_url content="{{ Request::root() }}">
    <link rel=stylesheet type=text/css href=./asset-ecommerce2/css/bootstrap.min.css>
    <link rel=stylesheet href="https://fonts.googleapis.com/css?family=Prompt:400,700">
    <link rel=stylesheet type=text/css href=./asset-ecommerce2/css/sumoselect.min.css>
    <link rel=stylesheet type=text/css href=./asset-ecommerce2/css/font-awesome.css>
    <link rel=stylesheet type=text/css href=./asset-ecommerce2/css/awesome-bootstrap-checkbox.css>
    <link rel=stylesheet type=text/css href=./asset-ecommerce2/css/sidebar.css>
    <link rel=stylesheet type=text/css href=./asset-ecommerce2/css/owl.carousel.min.css>
    <link rel="stylesheet" type="text/css" href="./asset-ecommerce2/css/jquery-ui.min.css">
    
    <link href=/asset-ecommerce2/core/app.327a67f68ab4c75b16b5092aadd4f1d4.css rel=stylesheet>
</head>

<body>
    <div id=app></div>
    <input id="xooFdswG14" type="hidden" value="{{\Session::get('line-user_id', '0')}}" />
    <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
    <script>
        window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
        ga('create', '{{$googleAnalytic->google_analytic_data}}', 'auto'); ga('send', 'pageview')

        var UDM = {};
        UDM.globalbrand='Foods MultiBrands';
        UDM.localbrand='Unilever Food Solutions';
        UDM.category='Foods';
        UDM.channel='Brand Site';
        UDM.country='Thailand';
        UDM.sitetype='Non-Avinash eCommerce';
        UDM.evq=[];
        UDM.setCustomDimension = 	{'dimension11': 'LOGGED/UNLOGGED VISIT', 'dimension13': 'UFS CHANNEL', 'dimension14': 'AREA', 'dimension15': 'TEMPLATE'}; 

        (function(d,u) {
          if(d.domain.indexOf('uat')!=-1 || d.domain.indexOf('stage')!=-1) {
            u=('https:'==document.location.protocol?'https://':'http://')+'wa-uat.unileversolutions.com';
            UDM.gid='4ebd8def9711eb35147f0d6ba2244e47';
            UDM.gaa='UA-54293156-50';
          }
          else {
            u=('https:'==document.location.protocol?'https://':'http://')+'wa-ap.unileversolutions.com';
            UDM.gid='06d38140b574e7af4972d672653da2fa';
            UDM.gaa='UA-57100066-9';
            UDM.dom='.ufsshoponline.com';
          }
        
          document.write("<scr" + "ipt type='text/javascript' src='" + u + "/ct/" + UDM.gid + "/u.js'></scr" + "ipt>");
        })(document);
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-6048393-48"></script>
    <script>
      window.dataLayer = window.dataLayer || [];

      function gtag() {
        dataLayer.push(arguments);
      }
      gtag('js', new Date());

      gtag('config', 'UA-6048393-48');
      gtag("config", "UA-6048393-48", {
        page_title: `TH | Unilever Food Solutions | Unilever Food Solutions`,
        page_path: `/full-ecommerce`
      });
    </script>

    <script>
      (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
          (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
          m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
      })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

      ga('create', 'UA-6048393-48', 'auto');
      ga('send', 'pageview');
    </script>
    <!-- End Google Analytics -->

    <script type=text/javascript src=./asset-ecommerce2/js/jquery-3.2.1.min.js></script>
    <script type=text/javascript src=./asset-ecommerce2/js/jquery-block-ui.js></script>
    <script type=text/javascript src=./asset-ecommerce2/js/popper.min.js></script>
    <script type=text/javascript src=./asset-ecommerce2/js/bootstrap.min.js></script>
    <script type=text/javascript src=./asset-ecommerce2/js/jquery.sumoselect.min.js></script>
    <script type=text/javascript src=./asset-ecommerce2/js/sidebar.js></script>
    <script type=text/javascript src=./asset-ecommerce2/js/owl.carousel.min.js></script>
    <script type="text/javascript" src="./asset-ecommerce2/js/jquery-ui.min.js"></script>
    
    <script type=text/javascript src=/asset-ecommerce2/core/manifest.4287ab605e97172978b1.js></script>
    <script type=text/javascript src=/asset-ecommerce2/core/vendor.74463eb92bea3ddda1c9.js></script>
    <script type=text/javascript src=/asset-ecommerce2/core/app.ee6015a0b117b8d81235.js></script>
</body>
</body>

</html>