<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TH | Unilever Food Solutions | Unilever Food Solutions</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="/ecommerce/campaign-give-coupon/font/font.css">
    <link rel="stylesheet" type="text/css" href="/ecommerce/campaign-give-coupon/css/style.css">

</head>
<body>
	<div class="main">
		<div class="header">
			<img src="/ecommerce/campaign-give-coupon/img/header.png">
		</div>
		<div class="form">
			<div class="title">
				<div class="h1">แบบฟอร์มลงทะเบียน</div>
			</div>
			<form action="{{ action('Ecommerce\CampaignGiveCouponController@registerPageStore') }}" method="POST" class="needs-validation" novalidate>
				{!! csrf_field() !!}
				<input type="hidden" name="line_user_id" value="{{ $lineUserProfile->id }}">
				<div class="form-group">
					<input type="text" name="first_name" class="input form-control" placeholder="ชื่อ*" value="{{ $datas['first_name'] }}" required>
					<div class="invalid-feedback"></div>
				</div>
				<div class="form-group">
					<input type="text" name="last_name" class="input form-control" placeholder="นามสกุล*" value="{{ $datas['last_name'] }}" required>
					<div class="invalid-feedback"></div>
				</div>
				<div class="form-group">
					<input type="text" name="address" class="input form-control" placeholder="ที่อยู่*" value="{{ $datas['address'] }}" required>
					<div class="invalid-feedback"></div>
				</div>
				<div class="form-group">
					<input type="text" name="post_code" class="input form-control" pattern="[0-9]{5}" maxlength="5" placeholder="รหัสไปรษณีย์*" value="{{ $datas['post_code'] }}" required onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
					<div class="invalid-feedback"></div>
				</div>
				<div class="form-group">
					<input type="text" name="phone_number" class="input form-control" pattern="[0-9]{10}" maxlength="10" placeholder="เบอร์โทรศัพท์*" value="{{ $datas['phone_number'] }}" required onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
					<div class="invalid-feedback"></div>
				</div>
				<div class="form-group">
					<input type="email" name="email" class="input form-control" placeholder="อีเมล*" value="{{ $datas['email'] }}" required>
					<div class="invalid-feedback"></div>
				</div>
				<div class="form-group">
					<input type="text" name="shop_name" class="input form-control" placeholder="ชื่อร้าน*" required>
					<div class="invalid-feedback"></div>
				</div>
				<div class="form-group">
					<select name="shop_type" class="input form-control" required>
						<option value="" disabled selected>ประเภทร้านค้า*</option>
						<option>ร้านอาหาร</option>
						<option>ร้านเบเกอรี่</option>
						<option>โรงแรม</option>
						<option>ร้านอาหารแบบห้องแถว/รถเข็น</option>
						<option>ร้านอาหารไทย</option>
						<option>ร้านอาหารจีน</option>
						<option>ร้านอาหารยุโรป</option>
						<option>ร้านอาหารญี่ปุ่น</option>
						<option>ร้านบาร์บีคิว/บุฟเฟ่</option>
						<option>ร้านกาแฟ</option>
						<option>โรงอาหาร</option>
						<option>ร้านอาหารที่มีสาขา</option>
						<option>อื่นๆ</option>
					</select>
					<div class="invalid-feedback"></div>
				</div>
				<div class="form-group">
					<input type="text" name="embroidered_name" class="input form-control" placeholder="ชื่อที่ต้องการปัก* ไม่เกิน 12 ตัวอักษร" maxlength="12" required>
					<div class="invalid-feedback"></div>
				</div>
				<div class="checkbox">
					<input type="checkbox" name="is_active_term" id="term"  class="input-checkbox form-check-input" required>
					<label for="term" class="mb-0">ยินดียอมรับเงื่อนไขตามที่กำหนดใน <u data-toggle="modal" data-target="#ModalCenter"> Term & Condition</u></label>
					<div class="invalid-feedback">ยอมรับเงื่อนไขตามที่กำหนด</div>
				</div>
				<div class="checkbox">
					<input type="checkbox" name="is_active_news" id="news" class="input-checkbox"> 
					<label for="news">ยินดียอมรับข่าวสารและกิจกรรมประชาสัมพันธ์จากทางยูนิลิเวอร์</label>
				</div>

				<button class="btn-register" type="submit"> ลงทะเบียน</button>

			</form>

		</div>
		<div class="footer"></div>

	</div>


	<div class="modal fade" id="ModalCenter" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
		    <div class="modal-content">
		        <div class="modal-body">
		        	<div class="text">
						<div class="text-title text-center">
							<img src="/ecommerce/campaign-give-coupon/img/icon.png" class="icon">
							<h2>Term & Conditions</h2>
						</div>
						<ul>
							<li>ต่อที่ 1 ลดราคา 10 บาทต่อถุง จากราคา ณ จุดขาย</li>
							<li>ต่อที่ 2 สแกน QR code เพื่อลงทะเบียนรับของสมนาคุณ ผ้ากันเปื้อนรุ่นพิเศษ  ฟรี*</li>
							<li>ต่อที่ 3 เมื่อลงทะเบียนแล้ว รับโค้ดส่วนลด มูลค่า 100 บาท* เพื่อใช้ในการซื้อสินค้าใน UFS ช้อปออนไลน์* </li>
						</ul>
						<h5>หมายเหตุ</h5>
						<ul>
							<li>รายการส่งเสริมการขายนี้ สำหรับสินค้า ผงปรุงครบรส ตราคนอร์ อร่อยชัวร์ รสหมู และรสไก่ ขนาด 800 กรัม เท่านั้น</li>
							<li>รายการส่งเสริมการขายนี้ เริ่มตั้งแต่วันที่ 1 มี.ค. 62 - 31 พ.ค. 62 หรือจนกว่าสินค้ารุ่นโปรโมชั่นนี้จะหมด ณ จุดขาย</li>
							<li>ของสมนาคุณ ผ้ากันเปื้อนรุ่นพิเศษ จำกัด 1 ผืนต่อลูกค้า 1 ราย และจำกัดเฉพาะ 5,000 ท่านแรกที่ลงทะเบียนเข้ามา ตั้งแต่ 1 มี.ค. 62 – 31 พ.ค. 62 เท่านั้น และลูกค้าต้องปฏิบัติตามข้อกำหนดที่ระบุไว้ในขั้นตอนการลงทะเบียน</li>
							<li>โค้ดส่วนลด e-discount code มูลค่า 100 บาท ใช้สำหรับเป็นส่วนลดในการซื้อสินค้าใน UFS ช้อปออนไลน์ (https://www.ufsshoponline.com/bc/ufsshoponline)เมื่อมียอดซื้อก่อนหักส่วนลดตั้งแต่ 2500 บาทขึ้นไป</li>
							<li>โค้ดส่วนลด e-discount code สามารถใช้ได้ตั้งแต่ 1 มี.ค. 62 – 31 พ.ค.62 เท่านั้น</li>
							<li>ทางบริษัทฯ ขอสงวนสิทฺธิ์ในการให้ของสมนาคุณ กรณีที่ตรวจพบการลงทะเบียนซ้ำซ้อน</li>
							<li>ขอสงวนสิทธิ์เฉพาะผู้ประกอบการเท่านั้น</li>
							<li>เงื่อนไขเป็นไปตามที่ทางบริษัทกำหนด และอาจมีการเปลี่ยนแปลงโดยที่ไม่ต้องแจ้งให้ทราบล่วงหน้า</li>
							<li>สอบถามข้อมูลเพิ่มเติมได้ที่ 02 554 2400 </li>
						</ul>
						<button class="btn-register mt-25"  data-dismiss="modal"> Agree</button>
					</div>
		       </div>
		    </div>
		 </div>
	</div>

	<div class="modal fade" id="ModalCenterValidation" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
		    <div class="modal-content">
		        <div class="modal-body">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		        	<div class="text text-center">
		        		<h4 class="my-5">กรุณากรอกข้อมูลให้ครบถ้วน</h4>
					</div>
		       </div>
		    </div>
		 </div>
	</div>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<script>
	// Example starter JavaScript for disabling form submissions if there are invalid fields
	(function() {
	  'use strict';
	  window.addEventListener('load', function() {
	    // Fetch all the forms we want to apply custom Bootstrap validation styles to
	    var forms = document.getElementsByClassName('needs-validation');
	    // Loop over them and prevent submission
	    var validation = Array.prototype.filter.call(forms, function(form) {
	      form.addEventListener('submit', function(event) {
	        if (form.checkValidity() === false) {
	          event.preventDefault();
	          event.stopPropagation();
	        }
	        form.classList.add('was-validated');
	      }, false);
	    });
	  }, false);
	})();
	</script>
	<script type="text/javascript">
		// $('#ModalCenterValidation').modal('show');
	</script>
</body>
</html>