<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TH | Unilever Food Solutions | Unilever Food Solutions</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="/ecommerce/campaign-give-coupon/font/font.css">
    <link rel="stylesheet" type="text/css" href="/ecommerce/campaign-give-coupon/css/style.css">
</head>
<body>
	<div class="main code">
		<div class="header">
			<img src="/ecommerce/campaign-give-coupon/img/logo.png" class="logo">
			<div class="h1">ขอบคุณที่ร่วมกิจกรรมกับเรา</div>
			<div class="h3">รับเพิ่ม <span>โค้ดส่วนลด 100 บาท</span> สำหรับช้อปใน UFS ช้อปออนไลน์</div>
			<div class="text-center">
				<div class="box">
					<div class="h3">กรอกโค้ด <span>'aroysure'</span></div>
				</div>
			</div>
		</div>
		<div class="image">
			<button class="btn-login" onclick="redirectPage()">เข้าสู่ UFS ช้อปออนไลน์</button>
			<img src="/ecommerce/campaign-give-coupon/img/bg2.png">
		</div>
	</div>
</body>
</html>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script type="text/javascript">
	$(function() {
	    var delayInMilliseconds = 10000; //10 second

		setTimeout(function() {
		  redirectPage();
		}, delayInMilliseconds);
	});
	function redirectPage(){
		location.href = 'https://foodsolution-uat.yellow-idea.com/bc/ecom';
	}
</script>