function CommonKeyPressIsAlpha(e) {
    e = e || event;
    var keypressed = String.fromCharCode(e.keyCode || e.which);
    var matched = (/^[ก-ฮะาิีึืุูโเแำไใ่้ั๊๋์a-z]+$/i).test(keypressed);
    if (matched === false) {
        return false;

    }
}

function CommonKeyPressIsNumber(e) {
    e = e || event;
    var keypressed = String.fromCharCode(e.keyCode || e.which);
    var matched = (/^[0-9]+$/i).test(keypressed);
    if (matched === false) {
        return false;

    }
}