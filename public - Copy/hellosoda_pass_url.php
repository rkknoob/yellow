<?php 

/**
 * Redirect with POST data.
 *
 * @param string $url URL.
 * @param array $post_data POST data. Example: array('foo' => 'var', 'id' => 123)
 * @param array $headers Optional. Extra headers to send.
 */

if($_GET["platfrom"]=="Hello Soda"){
    header('Location: '.$_GET["url"]);
    exit();
}else{
    $data_facebook = json_decode( $_GET["facebook_profile"]);
    $data_facebook_array =  (array) $data_facebook;

    $_arrays = array(
        'id' => $_GET["facebook_profile"],//$data_facebook_array['id'] , 
        'is_success' => $_GET["is_success"] ,
        'uid' => $_GET["uid"]
    );

    /*
    $_arrays = array(
        'first_name' => $data_facebook_array['first_name'],
        'gender' => $data_facebook_array['gender'],
        'last_name' => $data_facebook_array['last_name'],
        'link' => $data_facebook_array['link'],
        'locale' => $data_facebook_array['locale'],
        'id' => $data_facebook_array['id'],
        'name' => $data_facebook_array['name'],
        'timezone' => $data_facebook_array['timezone'],
        'updated_time' => $data_facebook_array['updated_time'],
        'verified' => $data_facebook_array['verified'],
        'birthday' => $data_facebook_array['birthday'],
        'picture' => $data_facebook_array['picture'],
        'thumbnail' => $data_facebook_array['thumbnail'],
        'workid' => $data_facebook_array['work'][0]->id ,
        'workdescription' => $data_facebook_array['work'][0]->description ,
        'workstartdate' => $data_facebook_array['work'][0]->start_date ,
        'workenddate' => $data_facebook_array['work'][0]->end_date ,
        'employerid'=> $data_facebook_array['work'][0]->employer->id,
        'employername'=> $data_facebook_array['work'][0]->employer->name ,
        'locationId' => $data_facebook_array['location']->id ,
        'locationname' => $data_facebook_array['location']->name
    );
    */
    //print_r($_arrays);
    //exit();
    function curl_redirect($url, array $data, array $headers = null){
        
        $curl = curl_init();
        //curl_setopt($ch, CURLOPT_POST, true);
        $len_1 = strlen($_GET["facebook_profile"]);
        $len_2 = strlen($_GET["is_success"]);
        $len_3 = strlen($_GET["uid"]);
        //$len_result = ($len_1+$len_2+$len_3);
        
        curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_POST => true,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_HTTPHEADER => array(
            "Content-length: 0",
            "id: ".$_GET["facebook_profile"]."",
            "is_success: ".$_GET["is_success"],
            "uid: ".$_GET["uid"]
        ) 
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
        echo "cURL Error #:" . $err;
        } else {
        echo $response;
        }
    }
    //yellowdevelop.yellow-idea.com/api/testGetHeader
    curl_redirect($_GET["url"],$_arrays);

}

?>