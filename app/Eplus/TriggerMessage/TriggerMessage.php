<?php

namespace YellowProject\Eplus\TriggerMessage;

use Illuminate\Database\Eloquent\Model;
use YellowProject\LineUserProfile;
use YellowProject\LineWebHooks;
use YellowProject\Eplus\Customer\CustomerDataMapping;
use YellowProject\Eplus\Customer\CustomerRegisterData;
use YellowProject\Eplus\Salesman\SalesmanDataMapping;
use YellowProject\Eplus\Salesman\SalesmanRegisterData;
use YellowProject\Eplus\Customer\TempMessage as CustomerTempMessage;
use YellowProject\Eplus\Salesman\TempMessage as SalesmanTempMessage;
use YellowProject\LineSettingBusiness;
use Carbon\Carbon;
use Log;

class TriggerMessage extends Model
{
	public static function setDataCustomer($customerDataMapping)
    {
        $messages = collect([]);

        $tempMessages = CustomerTempMessage::all();
        if($customerDataMapping){
            foreach ($tempMessages as $key => $tempMessage) {
                $data = [
                    "type" => "text",
                    "text" => trim(self::sentPayload($tempMessage->playload,$customerDataMapping))
                ];
                $messages->push($data);
            }
        }

        return $messages;
    }

    public static function setDataSalesman($customerDataMapping)
    {
        $messages = collect([]);

        $tempMessages = SalesmanTempMessage::all();
        if($customerDataMapping){
            foreach ($tempMessages as $key => $tempMessage) {
                $data = [
                    "type" => "text",
                    "text" => trim(self::sentPayload($tempMessage->playload,$customerDataMapping))
                ];
                $messages->push($data);
            }
        }

        return $messages;
    }

    public static function sentPayload($payload,$customerDataMapping)
    {
        $string='';
        $subscriberID = '';
        $keyword = '';
        $newPayloads = $payload;

        $newPayloads = str_replace(trim('&nbsp;'), ' ', trim($newPayloads));
        $newPayloads = str_replace(trim(' '), ' ', trim($newPayloads));
        $newPayloads = preg_replace('#(www\.|https?:\/\/){1}[a-zA-Z0-9]{2,}\.[a-zA-Z0-9]{2,}(\S*)#i', ' $0', $newPayloads);
        $keywords = preg_split("/\s+/", $newPayloads);
        foreach ($keywords as $key => $messageText) {
            $string .= " ".$messageText;
        }
        $keyword = $string;
        // dd($keyword);
        $valueForQuery = collect();
        $regStrings = preg_split("/[@##][@###]+/",$string);
        foreach ($regStrings as $regString) {
          if(trim($regString) !=''){
                $first = substr($regString, 0, 2);
                if($first == '{[') {
                    $last = substr($regString,-2);
                    if($last == ']}'){
                        $data = substr($regString,2,strlen($regString)-4);
                        $valueForQuery->push($data);
                    }
                }
            }
        }
        foreach($valueForQuery as $value){
            $data = str_replace(".png", "", $value);
            $lineEmoticon = LineEmoticon::where('file_name',$data)->first();
            // dd($lineEmoticon->sent_unicode);
            if(!is_null($lineEmoticon)){
                $keyword = str_replace('&nbsp;', ' ', trim($keyword));
                $keyword = str_replace('@##'.trim('{['.$value.']}@###'), ' '.$lineEmoticon->sent_unicode, trim($keyword));
            }
        }
        $keyword = preg_replace_callback("~\(([^\)]*)\)~", function($s) {
            return str_replace(" ", "%S", "($s[1])");
        }, $keyword);
        $payloads = explode(" ", $keyword);

        foreach ($payloads as $key => $value) {
            if($payloads[$key] != ""){
                // preg_match('#\<(.*?)\>#', $payloads[$key], $match);
                $payloads[$key] = str_replace("%S", " ", $payloads[$key]);
                preg_match('#\(\[.*?\]\)#', $payloads[$key], $match);
                if(count($match) > 0){
                    $keyword = str_replace('([', '', $match[0]);
                    $keyword = str_replace('])', '', $keyword);
                    $match[0] = trim($keyword);
                    if($match[0] == 'BannerEPlus'){
                        $payloads[$key] = $customerDataMapping->banner_e_plus;
                    }
                    if($match[0] == 'BannerCode'){
                        $payloads[$key] = $customerDataMapping->banner_code;
                    }
                    if($match[0] == 'UpdateToPeriod'){
                        $payloads[$key] = $customerDataMapping->update_to_period;
                    }
                    if($match[0] == 'TargetPeriod'){
                        $payloads[$key] = $customerDataMapping->update_to_period;
                    }
                    if($match[0] == 'TargetToValue'){
                        $payloads[$key] = $customerDataMapping->target_to_vue;
                    }
                    if($match[0] == 'FromTo'){
                        $payloads[$key] = $customerDataMapping->from_to;
                    }
                    if($match[0] == 'UpdateToValue'){
                        $payloads[$key] = $customerDataMapping->update_to_value;
                    }
                    if($match[0] == 'RemainingToValue'){
                        $payloads[$key] = $customerDataMapping->remaining_to_value;
                    }
                    if($match[0] == 'SalesmanTel'){
                        $payloads[$key] = $customerDataMapping->salesman_tel;
                    }
                    if($match[0] == 'SalesmanName'){
                        $payloads[$key] = $customerDataMapping->salesman_name;
                    }
                    $payloads[$key] = trim($payloads[$key]);
                }
            }
        }
        // dd($payloads);
        $keyword = implode(" ", $payloads);

        $keyword = preg_replace("/<span[^>]+\>/i", "", $keyword);
        // $keyword = str_replace('\n', PHP_EOL, $keyword);
        $keyword = str_replace(' ##newline## ', PHP_EOL, $keyword);
        $keyword = str_replace('##newline##', PHP_EOL, $keyword);
        return trim($keyword);
    }

    public static function sentMessageCustomer($customerDataMapping,$customerRegisterData)
    {
        $datas = collect();
        $lineUserProfile = LineUserProfile::find($customerRegisterData->line_user_id);
        $lineSettingBusiness = LineSettingBusiness::where('active', 1)->first();

        $datas->put('token', 'Bearer '.$lineSettingBusiness->channel_access_token);
        $datas->put('sentUrl', 'https://api.line.me/v2/bot/message/push');

        $message = self::setDataCustomer($customerDataMapping);
            
        $data = collect([
            "to" => $lineUserProfile->mid,
            "messages"   => $message
        ]);

        $datas->put('data', $data->toJson());
        LineWebHooks::sent($datas);
    }

    public static function sentMessageSalesman($customerDataMapping,$salesmanRegisterData)
    {
        $datas = collect();
        $lineUserProfile = LineUserProfile::find($salesmanRegisterData->line_user_id);
        $lineSettingBusiness = LineSettingBusiness::where('active', 1)->first();

        $datas->put('token', 'Bearer '.$lineSettingBusiness->channel_access_token);
        $datas->put('sentUrl', 'https://api.line.me/v2/bot/message/push');

        $message = self::setDataSalesman($customerDataMapping);
            
        $data = collect([
            "to" => $lineUserProfile->mid,
            "messages"   => $message
        ]);

        $datas->put('data', $data->toJson());
        LineWebHooks::sent($datas);
    }
}
