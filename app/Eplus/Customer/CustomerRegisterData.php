<?php

namespace YellowProject\Eplus\Customer;

use Illuminate\Database\Eloquent\Model;

class CustomerRegisterData extends Model
{
    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dim_eplus_customer_register_data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'line_user_id',
        'banner_code',
    ];
}
