<?php

namespace YellowProject\Eplus\Customer;

use Illuminate\Database\Eloquent\Model;

class TempMessage extends Model
{
    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dim_eplus_temp_customer';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'seq',
        'type',
        'display',
        'playload',
    ];
}
