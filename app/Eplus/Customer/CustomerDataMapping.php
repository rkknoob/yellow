<?php

namespace YellowProject\Eplus\Customer;

use Illuminate\Database\Eloquent\Model;

class CustomerDataMapping extends Model
{
    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dim_eplus_customer_register_mapping_data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'banner_code',
        'banner_master',
		'banner_e_plus',
		'customer_tel',
		'asm',
		'fs_salesman_code',
		'salesman_name',
		'salesman_tel',
		'target_period',
		'target_to_vue',
		'update_to_period',
		'from_to',
        'update_to_value',
		'remaining_to_value',
    ];
}
