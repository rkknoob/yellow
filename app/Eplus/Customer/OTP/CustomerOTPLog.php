<?php

namespace YellowProject\Eplus\Customer\OTP;

use Illuminate\Database\Eloquent\Model;

class CustomerOTPLog extends Model
{
    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dim_eplus_customer_otp_log';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'eplus_customer_register_data_id',
        'otp',
		'otp_ref',
		'phone_number',
    ];
}
