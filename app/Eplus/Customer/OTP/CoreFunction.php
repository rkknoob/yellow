<?php

namespace YellowProject\Eplus\Customer\OTP;

use Illuminate\Database\Eloquent\Model;
use YellowProject\Eplus\Customer\OTP\CustomerOTP;
use YellowProject\Eplus\Customer\OTP\CustomerOTPLog;
use YellowProject\Eplus\Customer\CustomerDataMapping;
use YellowProject\Eplus\Customer\CustomerRegisterData;

class CoreFunction extends Model
{
    public static function genRefOtp($length = 6)
    {
    	$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function genOTP($length = 6)
    {
    	$characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        // $otp = rand(000000, 999999);
        return $randomString;
    }

    public static function genOTPData($customerRegisterData,$lineUserProfile)
    {
    	$customerOTP = CustomerOTP::where('eplus_customer_register_data_id',$customerRegisterData->id)->first();
    	if($customerOTP){
    		$customerOTP->delete();
    	}

    	$customerOTP = CustomerOTP::create([
    		'eplus_customer_register_data_id' => $customerRegisterData->id,
    		'otp' => self::genOTP(),
    		'otp_ref' => self::genRefOtp(),
    		'phone_number' => $lineUserProfile->phone_number,
    	]);

    	CustomerOTPLog::create([
    		'eplus_customer_register_data_id' => $customerRegisterData->id,
    		'otp' => self::genOTP(),
    		'otp_ref' => self::genRefOtp(),
    		'phone_number' => $lineUserProfile->phone_number,
    	]);

    	return $customerOTP;
    }
}
