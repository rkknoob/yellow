<?php

namespace YellowProject\Eplus\Salesman;

use Illuminate\Database\Eloquent\Model;

class SalesmanRegisterData extends Model
{
    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dim_eplus_salesman_register_data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'line_user_id',
        'fs_salesman_code',
    ];
}
