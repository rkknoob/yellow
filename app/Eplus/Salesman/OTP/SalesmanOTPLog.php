<?php

namespace YellowProject\Eplus\Salesman\OTP;

use Illuminate\Database\Eloquent\Model;

class SalesmanOTPLog extends Model
{
    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dim_eplus_sales_otp_log';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'eplus_salesman_register_data_id',
        'otp',
		'otp_ref',
		'phone_number',
    ];
}
