<?php

namespace YellowProject\Eplus\Salesman\OTP;

use Illuminate\Database\Eloquent\Model;
use YellowProject\Eplus\Salesman\OTP\SalesmanOTP;
use YellowProject\Eplus\Salesman\OTP\SalesmanOTPLog;
use YellowProject\Eplus\Salesman\SalesmanDataMapping;
use YellowProject\Eplus\Salesman\SalesmanRegisterData;

class CoreFunction extends Model
{
    public static function genRefOtp($length = 6)
    {
    	$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function genOTP($length = 6)
    {
    	$characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        // $otp = rand(000000, 999999);
        return $randomString;
    }

    public static function genOTPData($salesmanRegisterData,$lineUserProfile)
    {
    	$salesmanOTP = SalesmanOTP::where('eplus_salesman_register_data_id',$salesmanRegisterData->id)->first();
    	if($salesmanOTP){
    		$salesmanOTP->delete();
    	}

    	$salesmanOTP = SalesmanOTP::create([
    		'eplus_salesman_register_data_id' => $salesmanRegisterData->id,
    		'otp' => self::genOTP(),
    		'otp_ref' => self::genRefOtp(),
    		'phone_number' => $lineUserProfile->phone_number,
    	]);

    	SalesmanOTPLog::create([
    		'eplus_salesman_register_data_id' => $salesmanRegisterData->id,
    		'otp' => self::genOTP(),
    		'otp_ref' => self::genRefOtp(),
    		'phone_number' => $lineUserProfile->phone_number,
    	]);

    	return $salesmanOTP;
    }
}
