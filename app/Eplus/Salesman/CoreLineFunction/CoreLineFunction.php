<?php

namespace YellowProject\Eplus\Salesman\CoreLineFunction;

use Illuminate\Database\Eloquent\Model;
use YellowProject\LineUserProfile;
use YellowProject\LineWebHooks;
use YellowProject\Eplus\Salesman\SalesmanDataMapping;
use YellowProject\Eplus\Salesman\SalesmanRegisterData;
use YellowProject\Eplus\Customer\CustomerDataMapping;
use YellowProject\Eplus\Salesman\TempMessage;
use YellowProject\LineSettingBusiness;
use Carbon\Carbon;
use Log;

class CoreLineFunction extends Model
{
    public static function setData($bannerCode)
    {
        // $datas = collect();
        $messages = collect([]);

        $customerDataMapping = CustomerDataMapping::where('banner_code',$bannerCode)->first();
        $tempMessages = TempMessage::all();
        if($customerDataMapping){
            foreach ($tempMessages as $key => $tempMessage) {
                $data = [
                    "type" => "text",
                    "text" => trim(self::sentPayload($tempMessage->playload,$customerDataMapping))
                ];
                $messages->push($data);
            }
            // $text1 = "สวัสดีค่ะครับ ร้าน ([BannerEPlus]) ##newline## (Banner Code ([BannerCode])) ##newline## รายละเอียดอัพเดท ณ วันที่ ([UpdateToPeriod])";
            // $data = [
            //     "type" => "text",
            //     "text" => trim(self::sentPayload($text1,$customerDataMapping))
            // ];
            // $messages->push($data);
            // $text2 = "เป้าหมายยอดซื้อ ([TargetPeriod]) ##newline## ([TargetToValue]) บาท ##newline## ยอดซื้อจริง ณ วันที่ ([FromTo]) ##newline## ([UpdateToValue]) บาท ##newline## เป้าหมายยอดซื้อคงเหลือ: ([RemainingToValue]) บาท";
            // $data = [
            //     "type" => "text",
            //     "text" => trim(self::sentPayload($text2,$customerDataMapping))
            // ];
            // $messages->push($data);
            // $text3 = "สอบถามข้อมูลเพิ่มเติมได้ที่ ##newline## โทร ([SalesmanTel]) ##newline## ชื่อพนักงานขาย ([SalesmanName])";
            // $data = [
            //     "type" => "text",
            //     "text" => trim(self::sentPayload($text3,$customerDataMapping))
            // ];
            // $messages->push($data);
        }
        // $data = [
        //     "type" => "text",
        //     "text" => trim(self::sentPayload($dataMypolicy->payload_1,array(),$LineBizConCustomer))
        // ];

        return $messages;
        // $lineSettingBusiness = LineSettingBusiness::where('active', 1)->first();
        
        // $datas->put('sentUrl', 'https://api.line.me/v2/bot/message/push');
        // $datas->put('token', 'Bearer '.$lineSettingBusiness->channel_access_token);
        // $data = collect([
        //     "to" => $lineUserProfile->mid,
        //     "messages"   => $messages,
        // ]);
        // $datas->put('data', $data->toJson());
        // LineWebHooks::sent($datas);
    }

    public static function sentPayload($payload,$customerDataMapping)
    {
        $string='';
        $subscriberID = '';
        $keyword = '';
        $newPayloads = $payload;

        $newPayloads = str_replace(trim('&nbsp;'), ' ', trim($newPayloads));
        $newPayloads = str_replace(trim(' '), ' ', trim($newPayloads));
        $newPayloads = preg_replace('#(www\.|https?:\/\/){1}[a-zA-Z0-9]{2,}\.[a-zA-Z0-9]{2,}(\S*)#i', ' $0', $newPayloads);
        $keywords = preg_split("/\s+/", $newPayloads);
        foreach ($keywords as $key => $messageText) {
            $string .= " ".$messageText;
        }
        $keyword = $string;
        // dd($keyword);
        $valueForQuery = collect();
        $regStrings = preg_split("/[@##][@###]+/",$string);
        foreach ($regStrings as $regString) {
          if(trim($regString) !=''){
                $first = substr($regString, 0, 2);
                if($first == '{[') {
                    $last = substr($regString,-2);
                    if($last == ']}'){
                        $data = substr($regString,2,strlen($regString)-4);
                        $valueForQuery->push($data);
                    }
                }
            }
        }
        foreach($valueForQuery as $value){
            $data = str_replace(".png", "", $value);
            $lineEmoticon = LineEmoticon::where('file_name',$data)->first();
            // dd($lineEmoticon->sent_unicode);
            if(!is_null($lineEmoticon)){
                $keyword = str_replace('&nbsp;', ' ', trim($keyword));
                $keyword = str_replace('@##'.trim('{['.$value.']}@###'), ' '.$lineEmoticon->sent_unicode, trim($keyword));
            }
        }
        $keyword = preg_replace_callback("~\(([^\)]*)\)~", function($s) {
            return str_replace(" ", "%S", "($s[1])");
        }, $keyword);
        $payloads = explode(" ", $keyword);

        foreach ($payloads as $key => $value) {
            if($payloads[$key] != ""){
                // preg_match('#\<(.*?)\>#', $payloads[$key], $match);
                $payloads[$key] = str_replace("%S", " ", $payloads[$key]);
                preg_match('#\(\[.*?\]\)#', $payloads[$key], $match);
                if(count($match) > 0){
                    $keyword = str_replace('([', '', $match[0]);
                    $keyword = str_replace('])', '', $keyword);
                    $match[0] = trim($keyword);
                    if($match[0] == 'BannerEPlus'){
                        $payloads[$key] = $customerDataMapping->banner_e_plus;
                    }
                    if($match[0] == 'BannerCode'){
                        $payloads[$key] = $customerDataMapping->banner_code;
                    }
                    if($match[0] == 'UpdateToPeriod'){
                        $payloads[$key] = $customerDataMapping->update_to_period;
                    }
                    if($match[0] == 'TargetPeriod'){
                        $payloads[$key] = $customerDataMapping->update_to_period;
                    }
                    if($match[0] == 'TargetToValue'){
                        $payloads[$key] = $customerDataMapping->target_to_vue;
                    }
                    if($match[0] == 'FromTo'){
                        $payloads[$key] = $customerDataMapping->from_to;
                    }
                    if($match[0] == 'UpdateToValue'){
                        $payloads[$key] = $customerDataMapping->update_to_value;
                    }
                    if($match[0] == 'RemainingToValue'){
                        $payloads[$key] = $customerDataMapping->remaining_to_value;
                    }
                    if($match[0] == 'SalesmanTel'){
                        $payloads[$key] = $customerDataMapping->salesman_tel;
                    }
                    if($match[0] == 'SalesmanName'){
                        $payloads[$key] = $customerDataMapping->salesman_name;
                    }
                    $payloads[$key] = trim($payloads[$key]);
                }
            }
        }
        // dd($payloads);
        $keyword = implode(" ", $payloads);

        $keyword = preg_replace("/<span[^>]+\>/i", "", $keyword);
        // $keyword = str_replace('\n', PHP_EOL, $keyword);
        $keyword = str_replace(' ##newline## ', PHP_EOL, $keyword);
        $keyword = str_replace('##newline##', PHP_EOL, $keyword);
        return trim($keyword);
    }

    public static function sentMessage($arrDatas, $dateStartNow = null)
    {
        $datas = collect();
        foreach ($arrDatas['events'] as $arr) {
            if (isset($arr['message']) && sizeof($arr['message']) > 0) {
                if($arr['message']['type'] == 'location') {
                    // Log::debug('Receive Location');
                }  else if($arr['message']['type'] == 'text') {
                   $text  = $arr['message']['text'];
                }
            }
            if (isset($arr['source'])) {
                $datas->put('type', $arr['source']['type']);
                if ($arr['source']['type'] == 'group') {
                    $datas->put('groupId', $arr['source']['groupId']);
                } else if ($arr['source']['type'] == 'room') {
                    $datas->put('roomId', $arr['source']['roomId']);
                } else  if ($arr['source']['type'] == 'user'){
                    $datas->put('userId', $arr['source']['userId']);
                } else {
                    //   $datas->put('userId', $arr['source']['userId']);
                }
            } 
            $datas->put('replyToken', $arr['replyToken']);
            $datas->put('sourceType', $arr['source']['type']);
            $datas->put('timestamp', $arr['timestamp']);
        }

        $lineUserProfile = LineUserProfile::where('mid',$datas['userId'])->first();
        $lineSettingBusiness = LineSettingBusiness::where('active', 1)->first();

        $datas->put('token', 'Bearer '.$lineSettingBusiness->channel_access_token);
        $datas->put('sentUrl', 'https://api.line.me/v2/bot/message/push');

        // $messages[]  = [
        //     "type" =>"text",
        //     "text" => "กรุณาส่งเป็นข้อความ text",
        // ];
        // $message = collect($messages);

        $salesmanRegisterData = SalesmanRegisterData::where('line_user_id',$lineUserProfile->id)->first();
        $customerDataMappings = CustomerDataMapping::where('fs_salesman_code',$salesmanRegisterData->fs_salesman_code)->get();
        foreach ($customerDataMappings as $key => $customerDataMapping) {
            $message = self::setData($customerDataMapping->banner_code);
            if($message->count() > 0){
                $now = Carbon::now();
                $dateNow2 = $now;
                
                $data = collect([
                    "to" => $datas['userId'],
                    "replyToken" => $datas['replyToken'], // for test
                    "messages"   => $message
                ]);

                $datas->put('data', $data->toJson());
                LineWebHooks::sent($datas);
            }
        }
    }
}
