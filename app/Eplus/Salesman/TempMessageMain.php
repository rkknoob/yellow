<?php

namespace YellowProject\Eplus\Salesman;

use Illuminate\Database\Eloquent\Model;

class TempMessageMain extends Model
{
    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dim_eplus_temp_salesman_main';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'alt_text',
    ];
}
