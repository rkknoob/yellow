<?php

namespace YellowProject\Eplus\Salesman;

use Illuminate\Database\Eloquent\Model;

class SalesmanDataMapping extends Model
{
    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dim_eplus_salesman_register_mapping_data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'banner_code',
        'banner_master',
		'banner_e_plus',
		'customer_tel',
		'asm',
		'fs_salesman_code',
		'salesman_name',
		'salesman_tel',
    ];
}
