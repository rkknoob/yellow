<?php

namespace YellowProject\Http\Controllers\Eplus\Customer;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Eplus\Customer\CustomerDataMapping;
use YellowProject\Eplus\Customer\CustomerRegisterData;
use YellowProject\Eplus\Customer\OTP\CoreFunction;
use YellowProject\Eplus\Customer\OTP\CustomerOTP;
use YellowProject\Eplus\Customer\OTP\CustomerOTPLog;

class PageController extends Controller
{
    public function getRegisterPage()
    {
    	$lineUserProfile = \Session::get('line-login', '');
    	\Session::put('line-login', '');
    	if($lineUserProfile == ''){
    		return redirect()->action('Eplus\Customer\PageController@getErrorPage');
    	}
    	$customerRegisterData = CustomerRegisterData::where('line_user_id',$lineUserProfile->id)->first();
    	if($customerRegisterData && $customerRegisterData->eplus_customer_register_mapping_id != ''){
    		$customerOTP = CustomerOTP::where('eplus_customer_register_data_id',$customerRegisterData->id)->where('is_active',1)->first();
    		\Session::put('line-login', $lineUserProfile);
    		if(!$customerOTP){
    			return redirect()->action('Eplus\Customer\PageController@getOTPPage');
    		}
    		return redirect()->action('Eplus\Customer\PageController@getShowDataCustomer');
    	}

    	if(!$customerRegisterData){
    		CustomerRegisterData::create([
    			'line_user_id' => $lineUserProfile->id
    		]);
    	}

    	return view('eplus.customer.register.index')
    		->with('lineUserProfile',$lineUserProfile);
    }

    public function getShowDataCustomer()
    {
    	$lineUserProfile = \Session::get('line-login', '');
    	\Session::put('line-login', '');
    	if($lineUserProfile == ''){
    		return redirect()->action('Eplus\Customer\PageController@getErrorPage');
    	}

    	$customerRegisterData = CustomerRegisterData::where('line_user_id',$lineUserProfile->id)->first();
        $customerDataMapping = CustomerDataMapping::where('banner_code',$customerRegisterData->banner_code)->first();

    	return view('eplus.customer.show-data.index')
    		->with('customerDataMapping',$customerDataMapping); 
    }

    public function getOTPPage()
    {
    	$lineUserProfile = \Session::get('line-login', '');
    	\Session::put('line-login', '');
    	if($lineUserProfile == ''){
    		return redirect()->action('Eplus\Customer\PageController@getErrorPage');
    	}
    	$customerRegisterData = CustomerRegisterData::where('line_user_id',$lineUserProfile->id)->first();
    	$otpData = CoreFunction::genOTPData($customerRegisterData,$lineUserProfile);
        $otpData->update([
            'is_active' => 1,
        ]);
        \Session::put('line-login', $lineUserProfile);
        return redirect()->action('Eplus\Customer\PageController@getShowDataCustomer');

    	// return view('eplus.customer.otp.index')
    	// 	->with('otpData',$otpData)
    	// 	->with('lineUserProfile',$lineUserProfile);
    }

    public function getErrorPage()
    {
    	return view('eplus.error.index');
    }
}
