<?php

namespace YellowProject\Http\Controllers\Eplus\Customer;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Eplus\Customer\OTP\CoreFunction;
use YellowProject\Eplus\Customer\OTP\CustomerOTP;
use YellowProject\Eplus\Customer\OTP\CustomerOTPLog;
use YellowProject\Eplus\Customer\CustomerRegisterData;
use YellowProject\LineUserProfile;

class OTPController extends Controller
{
    public function recieveOTP(Request $request)
    {
    	$lineUserId = $request->line_user_id;
    	$lineUserProfile = LineUserProfile::find($lineUserId);
    	$otp = $request->otp;
    	$otpRef = $request->otp_ref;
    	$customerRegisterData = CustomerRegisterData::where('line_user_id',$lineUserId)->first();
    	$customerOTP = CustomerOTP::where('eplus_customer_register_data_id',$customerRegisterData->id)->first();
    	if($otp == $customerOTP->otp && $otpRef == $customerOTP->otp_ref){
    		$customerOTP->update([
    			'is_active' => 1
    		]);
    		\Session::put('line-login', $lineUserProfile);
    		return redirect()->action('Eplus\Customer\PageController@getShowDataCustomer');
    	}else{
    		return redirect()->action('Eplus\Customer\PageController@getErrorPage');
    	}
    }

    public function newOTP($id)
    {
    	$lineUserId = $id;
    	$lineUserProfile = LineUserProfile::find($lineUserId);
    	$customerRegisterData = CustomerRegisterData::where('line_user_id',$lineUserProfile->id)->first();
    	$otpData = CoreFunction::genOTPData($customerRegisterData,$lineUserProfile);

    	return response()->json([
            'otpData'   => $otpData,
        ]);
    }
}
