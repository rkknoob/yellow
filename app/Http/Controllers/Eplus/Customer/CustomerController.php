<?php

namespace YellowProject\Http\Controllers\Eplus\Customer;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Eplus\Customer\CustomerDataMapping;
use YellowProject\Eplus\Customer\CustomerRegisterData;
use YellowProject\LineUserProfile;
use YellowProject\Eplus\Customer\OTP\CustomerOTP;
use YellowProject\Eplus\Customer\OTP\CustomerOTPLog;

class CustomerController extends Controller
{
    public function storeDataRegister(Request $request)
    {
    	$bannerCode = $request->banner_code;
    	$lineUserId = $request->line_user_id;
    	$customerDataMapping = CustomerDataMapping::where('banner_code',$bannerCode)->first();
    	if($customerDataMapping){
    		$customerRegisterData = CustomerRegisterData::where('line_user_id',$lineUserId)->first();
    		if($customerRegisterData){
    			$customerRegisterData->update([
    				'banner_code' => $bannerCode
    			]);
    			$lineUserProfile = LineUserProfile::find($lineUserId);
    			\Session::put('line-login', $lineUserProfile);
                return redirect()->action('Eplus\Customer\PageController@getOTPPage');
    			// return redirect()->action('Eplus\Customer\PageController@getShowDataCustomer');
    		}else{
    			return redirect()->action('Eplus\Customer\PageController@getErrorPage');
    		}
    	}else{
    		return redirect()->action('Eplus\Customer\PageController@getErrorPage');
    	}
    }
}
