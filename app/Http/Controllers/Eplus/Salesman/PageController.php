<?php

namespace YellowProject\Http\Controllers\Eplus\Salesman;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Eplus\Salesman\SalesmanDataMapping;
use YellowProject\Eplus\Salesman\SalesmanRegisterData;
use YellowProject\Eplus\Salesman\OTP\CoreFunction;
use YellowProject\Eplus\Salesman\OTP\SalesmanOTP;
use YellowProject\Eplus\Salesman\OTP\SalesmanOTPLog;

class PageController extends Controller
{
    public function getRegisterPage()
    {
    	$lineUserProfile = \Session::get('line-login', '');
    	\Session::put('line-login', '');
    	if($lineUserProfile == ''){
    		return redirect()->action('Eplus\Salesman\PageController@getErrorPage');
    	}
    	$salesmanRegisterData = SalesmanRegisterData::where('line_user_id',$lineUserProfile->id)->first();
    	if($salesmanRegisterData && $salesmanRegisterData->fs_salesman_code != ''){
    		$salesmanOTP = SalesmanOTP::where('eplus_salesman_register_data_id',$salesmanRegisterData->id)->where('is_active',1)->first();
    		\Session::put('line-login', $lineUserProfile);
    		if(!$salesmanOTP){
    			return redirect()->action('Eplus\Salesman\PageController@getOTPPage');
    		}
    		return redirect()->action('Eplus\Salesman\PageController@getShowDataCustomer');
    	}

    	if(!$salesmanRegisterData){
    		SalesmanRegisterData::create([
    			'line_user_id' => $lineUserProfile->id
    		]);
    	}

    	return view('eplus.salesman.register.index')
    		->with('lineUserProfile',$lineUserProfile);
    }

    public function getShowDataCustomer()
    {
    	$lineUserProfile = \Session::get('line-login', '');
    	\Session::put('line-login', '');
    	if($lineUserProfile == ''){
    		return redirect()->action('Eplus\Salesman\PageController@getErrorPage');
    	}

    	$salesmanRegisterData = SalesmanRegisterData::where('line_user_id',$lineUserProfile->id)->first();
    	$salesmanDataMapping = SalesmanDataMapping::where('fs_salesman_code',$salesmanRegisterData->fs_salesman_code)->first();

    	return view('eplus.salesman.show-data.index')
    		->with('salesmanDataMapping',$salesmanDataMapping); 
    }

    public function getOTPPage()
    {
    	$lineUserProfile = \Session::get('line-login', '');
    	\Session::put('line-login', '');
    	if($lineUserProfile == ''){
    		return redirect()->action('Eplus\Salesman\PageController@getErrorPage');
    	}
    	$salesmanRegisterData = SalesmanRegisterData::where('line_user_id',$lineUserProfile->id)->first();
    	$otpData = CoreFunction::genOTPData($salesmanRegisterData,$lineUserProfile);
        $otpData->update([
            'is_active' => 1,
        ]);
        \Session::put('line-login', $lineUserProfile);
        return redirect()->action('Eplus\Salesman\PageController@getShowDataCustomer');

    	// return view('eplus.salesman.otp.index')
    	// 	->with('otpData',$otpData)
    	// 	->with('lineUserProfile',$lineUserProfile);
    }

    public function getErrorPage()
    {
    	return view('eplus.error.index');
    }
}
