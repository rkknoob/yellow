<?php

namespace YellowProject\Http\Controllers\Eplus\Salesman;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Eplus\Salesman\SalesmanDataMapping;
use YellowProject\Eplus\Salesman\SalesmanRegisterData;
use YellowProject\LineUserProfile;
use YellowProject\Eplus\Salesman\OTP\SalesmanOTP;
use YellowProject\Eplus\Salesman\OTP\SalesmanOTPLog;

class SalesmanController extends Controller
{
    public function storeDataRegister(Request $request)
    {
    	$fsSalesman = $request->fs_salesman_code;
    	$lineUserId = $request->line_user_id;
    	$salesmanDataMapping = SalesmanDataMapping::where('fs_salesman_code',$fsSalesman)->first();
    	if($salesmanDataMapping){
    		$salesmanRegisterData = SalesmanRegisterData::where('line_user_id',$lineUserId)->first();
    		if($salesmanRegisterData){
    			$salesmanRegisterData->update([
    				'fs_salesman_code' => $salesmanDataMapping->fs_salesman_code
    			]);
    			$lineUserProfile = LineUserProfile::find($lineUserId);
    			\Session::put('line-login', $lineUserProfile);
                return redirect()->action('Eplus\Salesman\PageController@getOTPPage');
    			// return redirect()->action('Eplus\Customer\PageController@getShowDataCustomer');
    		}else{
    			return redirect()->action('Eplus\Salesman\PageController@getErrorPage');
    		}
    	}else{
    		return redirect()->action('Eplus\Salesman\PageController@getErrorPage');
    	}
    }
}
