<?php

namespace YellowProject\Http\Controllers\Eplus\Salesman;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Eplus\Salesman\OTP\CoreFunction;
use YellowProject\Eplus\Salesman\OTP\SalesmanOTP;
use YellowProject\Eplus\Salesman\OTP\SalesmanOTPLog;
use YellowProject\Eplus\Salesman\SalesmanRegisterData;
use YellowProject\LineUserProfile;

class OTPController extends Controller
{
    public function recieveOTP(Request $request)
    {
    	$lineUserId = $request->line_user_id;
    	$lineUserProfile = LineUserProfile::find($lineUserId);
    	$otp = $request->otp;
    	$otpRef = $request->otp_ref;
    	$salesmanRegisterData = SalesmanRegisterData::where('line_user_id',$lineUserId)->first();
    	$SalesmanOTP = SalesmanOTP::where('eplus_salesman_register_data_id',$salesmanRegisterData->id)->first();
    	if($otp == $SalesmanOTP->otp && $otpRef == $SalesmanOTP->otp_ref){
    		$SalesmanOTP->update([
    			'is_active' => 1
    		]);
    		\Session::put('line-login', $lineUserProfile);
    		return redirect()->action('Eplus\Salesman\PageController@getShowDataCustomer');
    	}else{
    		return redirect()->action('Eplus\Salesman\PageController@getErrorPage');
    	}
    }

    public function newOTP($id)
    {
    	$lineUserId = $id;
    	$lineUserProfile = LineUserProfile::find($lineUserId);
    	$salesmanRegisterData = SalesmanRegisterData::where('line_user_id',$lineUserProfile->id)->first();
    	$otpData = CoreFunction::genOTPData($salesmanRegisterData,$lineUserProfile);

    	return response()->json([
            'otpData'   => $otpData,
        ]);
    }
}
