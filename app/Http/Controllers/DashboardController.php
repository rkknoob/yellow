<?php

namespace YellowProject\Http\Controllers;

use Illuminate\Http\Request;
use YellowProject\LineUserProfile;

class DashboardController extends Controller
{
 
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }
 
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $lineUser = $request->all();
        $type = $request->type;
        // dd($type);
        // $type = \Session::get('fwd-type', '');
        // $type = (isset($request->type))? $request->type : \Session::get('fwd-type', '');
        $lineUser = \Session::get('line-login', '');
        if($lineUser == ""){
            return view('errors.404');
        }
        $lineUserProfile = LineUserProfile::where('mid',$lineUser->mid)->first();

        if($type == 'leadform'){
            return redirect()->action('ProfillingController@show',['route' => 'preference']);
        }
        
        if($type == 'bc_tracking'){
           $code = \Session::get('tracking_bc_code', '');
           \Session::put('tracking_bc_code', '');
           return redirect()->action('RecieveTrackingBCController@recieveCode',['code' => $code]);
           // dd($code);
        }

        if($type == 'ecommerce_payment'){
           $code = \Session::get('ecommerce_payment_code', '');
           \Session::put('ecommerce_payment_code', '');
           return redirect()->action('RecieveEcommercePaymentController@recieveCode',['code' => $code]);
           // dd($code);
        }

        if($type == 'ecommerce_admin'){
            \Session::put('ecommerce_admin_line_user_id', $lineUserProfile->id);
            return redirect()->action('Ecommerce\RegisterAdminController@getPage');
           // dd($code);
        }

        if($type == 'dt_code'){
           $code = \Session::get('dt_code', '');
           \Session::put('dt_code', '');
           return redirect()->action('RecieveDTManagementController@recieveCode',['code' => $code]);
           // dd($code);
        }
    }
}
