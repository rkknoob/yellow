<?php

namespace YellowProject\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\Order\Order;
use YellowProject\Ecommerce\Order\OrderProduct;
use YellowProject\Ecommerce\Coupon\Coupon;
use YellowProject\Ecommerce\Customer\Customer;
use YellowProject\Ecommerce\Customer\CustomerCoupon;
use YellowProject\Ecommerce\Customer\CustomerCouponLog;
use YellowProject\Ecommerce\Product;
use YellowProject\Ecommerce\ShoppingCart\ShoppingCart;
use YellowProject\Ecommerce\ShoppingCart\ShoppingCartLog;
use YellowProject\Ecommerce\ShoppingCart\ShoppingCartItem;
use YellowProject\Ecommerce\CoreLineFunction\LineFunction;
use YellowProject\Ecommerce\CoreEmailFunction\EmailFunction;
use YellowProject\GeneralFunction\CoreFunction;
use YellowProject\Ecommerce\Log\LogSession;
use YellowProject\Ecommerce\Order\OrderDTPayment;
use Carbon\Carbon;

class OrderController extends Controller
{
    public function customerOrder(Request $request)
    {
        $dateNow = Carbon::now()->format('Y-m-d H:i:s');
        $summary = 0;
        $customerId = $request->ecommerce_customer_id;
        $couponCode = $request->coupon_code;
        $productItems = $request->product_items;
        $countProduct = count($productItems);
        $allQty = 0;
        $couponDiscountPrice = 0;
        $allRewardPoint = 0;
        $totalProductVat = 0;
        $orderReward = 0;
        $orderId = Order::genOderId();
        $request['order_id'] = $orderId;
        $request['order_date'] = $dateNow;
        $request['order_status'] = "Waiting for Payment";
        $order = Order::create($request->all());

        if($countProduct > 0){
            foreach ($productItems as $key => $productItem) {
                $qty = $productItem['qty'];
                $product = Product::find($productItem['product_id']);
                $productPrice = $product->price*$qty;
                $productRewardPoint = $product->reward_point*$qty;
                $productVat = $product->vat;
                $productDiscount = $product->productDiscount;
                $productDiscountPrice = 0;
                $productPriceAfterVat = 0;
                if($productDiscount){
                    if($productDiscount->start_date <= $dateNow && $productDiscount->end_date >= $dateNow){
                        $productDiscountPrice = ($product->price - $productDiscount->discount_price)*$qty;
                        // $productPrice = $productPrice-$productDiscountPrice;
                        $productPrice = $productDiscount->discount_price*$qty;
                    }
                }
                if($productVat > 0){
                    $productPriceAfterVat = $productPrice + (($productPrice*$productVat)/100);
                }else{
                    $productPriceAfterVat = $productPrice;
                }
                OrderProduct::create([
                    'order_id' => $order->id,
                    'product_id' => $product->id,
                    'original_price' => $product->price,
                    'price' => $productPrice,
                    'quanlity' => $qty,
                    'tax_amount' => $qty,
                    'tax_percent' => $productVat,
                    'discount_amount' => $productDiscountPrice,
                    'total' => $productPriceAfterVat
                ]);
                $summary += $productPriceAfterVat;
                $allQty += $qty;
                $allRewardPoint += $productRewardPoint;
            }
        }else{
            return response()->json([
                'msg_return' => 'ไม่มีสินค้า',
                'code_return' => 2,
            ]);
        }

        $request['sub_total'] = $summary;

        if($couponCode != ""){
            $coupon = Coupon::where('code',$couponCode)->first();
            if($coupon){
                $minimumPurchase = $coupon->minimum_purchase;
                if($summary >= $minimumPurchase){
                    $discounts = $coupon->Discounts;
                    if($discounts){
                        $discount = $discounts->first();
                        if($discount){
                            if($discount->type == 'Percentage'){
                                $discountPercent = $discount->discount;
                                $discountPrice = ($summary*$discountPercent)/100;
                            }else{
                                $discountPrice = $discount->discount;
                            }
                            $couponDiscountPrice = $discountPrice;
                            $summary = $summary - $discountPrice;
                            CustomerCoupon::create([
                                'ecommerce_coupon_id' => $coupon->id,
                                'ecommerce_customer_id' => $customerId,
                                'status' => 'used'
                            ]);
                            CustomerCouponLog::create([
                                'ecommerce_coupon_id' => $coupon->id,
                                'ecommerce_customer_id' => $customerId,
                                'action' => 'used'
                            ]);
                        }
                    }
                }
            }
        }
        $request['grand_total'] = $summary;
        $request['coupon_code_discount'] = $couponCode;
        $request['coupon_code_discount_amount'] = $couponDiscountPrice;
        $request['total_paid'] = $summary;
        $request['total_due'] = $summary;
        // $orderReward = $summary/100;
        // $orderReward = floor($orderReward);
        // $allRewardPoint += $orderReward;
        // $request['order_reward_point'] = $orderReward;
        // $request['total_reward_point'] = $allRewardPoint;
        $request['order_reward_point'] = 0;
        $request['total_reward_point'] = 0;

        $order->update($request->all());

        $customer = Customer::find($customerId);
        $customerRewardPoint = $customer->reward_point;
        $customerNewRewardPoint = $customerRewardPoint + $allRewardPoint;
        $customer->update([
            'reward_point' => $customerNewRewardPoint
        ]);

        $shoppingCart = ShoppingCart::where('ecommerce_customer_id',$customer->id)->first();
        if($shoppingCart){
            $datas = $shoppingCart->toArray();
            ShoppingCartLog::create($datas);
            $shoppingCart->delete();
        }

        $baseUrl = \URL::to('/');
        $paymentCode = CoreFunction::genCode();
        $paymentURL = $baseUrl."/payment/".$paymentCode;

        OrderDTPayment::create([
            'order_id' => $order->id,
            'dt_id' => $order->dt_id,
            'ecommerce_customer_id' => $customerId,
            'payment_code' => $paymentCode,
            'payment_url' => $paymentURL
        ]);

        $logSession = LogSession::where('ecommerce_customer_id',$customer->id)->where('is_active',1)->first();
        if($logSession){
            $logSession->update([
                'is_check_out' => 1,
                'order_id' => $order->id
            ]);
        }

        // EmailFunction::emailToCustomer($customer,$order);

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
            'id' => $order->id,
            'payment_url' => $paymentURL,
        ]);
    }

    public function checkCouponData($couponCode)
    {
    	$couponData = collect([]);
    	$coupon = Coupon::where('name',$couponCode)->first();
    	if($coupon){
    		$coupon->Discounts;
    		$couponData = $coupon;
    	}else{
    		return response()->json([
	            'msg_return' => 'ไม่พบข้อมูล',
	            'code_return' => 2,
	        ]);
    	}

    	return response()->json([
    		'msg_return' => 'พบข้อมูล',
            'code_return' => 1,
            'datas' => $couponData,
        ]);
    }

    public function updateOrderShippingAddress(Request $request,$id)
    {
    	$order = Order::find($id);
    	$order->update([
    		'ecommerce_shipping_address_id' => $request->ecommerce_shipping_address_id,
            'operator_shipping_date' => $request->operator_shipping_date,
            'operator_shipping_time' => $request->operator_shipping_time
    	]);

    	return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function getOrderHistory($id)
    {
        $datas = [];
        $orders = Order::where('ecommerce_customer_id',$id)->get();
        if($orders->count() > 0){
            foreach ($orders as $orderKey => $order) {
                $orderProducts = $order->orderProducts;
                if($orderProducts){
                    foreach ($orderProducts as $key => $orderProduct) {
                        $product = $orderProduct->product;
                        if($product){
                            $productImages = $product->productImages;
                            if($productImages->count() > 0){
                                foreach ($productImages as $key => $productImage) {
                                    $productImage->image;
                                }
                            }
                        }
                    }
                }
                $datas['order_histories'][$orderKey] = $order;
            }
        }

        return response()->json([
            'datas' => $datas,
        ]);
    }

    public function calculatePurchaseOrder(Request $request)
    {
        $datas = [];
        $dateNow = Carbon::now()->format('Y-m-d H:i:s');
        $summary = 0;
        $summaryOriginal = 0;
        $customerId = $request->ecommerce_customer_id;
        $couponCode = $request->coupon_code;
        $productItems = $request->product_items;
        $countProduct = count($productItems);
        $allQty = 0;
        $couponReturnMessage = "ไม่ได้ใส่รหัสคูปอง";
        $couponReturnCode = 0;
        $couponDiscountPrice = 0;
        $allRewardPoint = 0;
        $orderReward = 0;
        $customer = Customer::find($customerId);
        if($countProduct > 0){
            foreach ($productItems as $key => $productItem) {
                $qty = $productItem['qty'];
                $product = Product::find($productItem['product_id']);
                $productPrice = $product->price*$qty;
                $productRewardPoint = $product->reward_point*$qty;
                $summaryOriginal += $productPrice;
                $productVat = $product->vat;
                $productDiscount = $product->productDiscount;
                $productDiscountPrice = 0;
                $productPriceAfterVat = 0;
                if($productDiscount){
                    if($productDiscount->start_date <= $dateNow && $productDiscount->end_date >= $dateNow){
                        // $productDiscountPrice = $productDiscount->discount_price*$qty;
                        // $productPrice = $productPrice-$productDiscountPrice;
                        $productPrice = $productDiscount->discount_price*$qty;
                    }
                }
                if($productVat > 0){
                    $productPriceAfterVat = $productPrice + (($productPrice*$productVat)/100);
                }else{
                    $productPriceAfterVat = $productPrice;
                }
                $summary += $productPriceAfterVat;
                $allQty += $qty;
                $allRewardPoint += $productRewardPoint;
            }
        }else{
            return response()->json([
                'msg_return' => 'ไม่มีสินค้า',
                'code_return' => 2,
            ]);
        }
        
        if($couponCode != ""){
            $coupon = Coupon::where('code',$couponCode)->first();
            if($coupon){
                $customerCoupon = CustomerCoupon::where('ecommerce_coupon_id',$coupon->id);
                $customerCouponCount = $customerCoupon->count();
                $customerCoupon = $customerCoupon->where('ecommerce_customer_id',$customerId)->first();
                if(!$customerCoupon){
                    if($coupon->user_per_coupon > $customerCouponCount){
                        $minimumPurchase = $coupon->minimum_purchase;
                        if($summary >= $minimumPurchase){
                            $discounts = $coupon->Discounts;
                            if($discounts){
                                $discount = $discounts->first();
                                if($discount){
                                    if($discount->type == 'Percentage'){
                                        $discountPercent = $discount->discount;
                                        $discountPrice = ($summary*$discountPercent)/100;
                                        
                                    }else{
                                        $discountPrice = $discount->discount;
                                    }
                                    $couponDiscountPrice = $discountPrice;
                                    $summary = $summary - $discountPrice;
                                }
                            }
                        }else{
                            $couponReturnMessage = "จำนวนสินค้าไม่เพียงพอต่อการใช้คูปอง";
                            $couponReturnCode = 2;
                        }
                    }else{
                        $couponReturnMessage = "คูปองหมด";
                        $couponReturnCode = 3;
                    }
                }else{
                    $couponReturnMessage = "รับคูปองไปแล้ว";
                    $couponReturnCode = 4;
                }
            }
        }

        $orderReward = $summary/100;
        $orderReward = floor($orderReward);
        $allRewardPoint += $orderReward;

        if($summaryOriginal < 2500){
            return response()->json([
                'msg_return' => 'ไม่สามารถสั่งซื้อสินค้าได้เนื่องจากยอดเงินต่ำ',
                'code_return' => 3,
                'coupon_msg_return' => $couponReturnMessage,
                'coupon_code_return' => $couponReturnCode,
                'coupon_discount_price' => $couponDiscountPrice,
                'summary' => $summary,
                'customer_reward_point' => $customer->reward_point,
                'total_reward_point' => 0
            ]);
        }

        return response()->json([
            'code_return' => 1,
            'coupon_msg_return' => $couponReturnMessage,
            'coupon_code_return' => $couponReturnCode,
            'coupon_discount_price' => $couponDiscountPrice,
            'summary' => $summary,
            'customer_reward_point' => $customer->reward_point,
            'total_reward_point' => $allRewardPoint
        ]);
    }
}
