<?php

namespace YellowProject\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\OTP\CustomerOtp;
use YellowProject\Ecommerce\OTP\CustomerOtpLog;
use YellowProject\Ecommerce\Customer\Customer;
use Carbon\Carbon;

class OtpController extends Controller
{
    public function getOtp($id)
    {
    	$lineUserID = $id;
    	$customer = Customer::where('line_user_id',$lineUserID)->first();
    	if($customer){
    		$dateNow = Carbon::now()->setTimezone('Asia/Bangkok')->format('Y-m-d H:i:s');
    		$dateOtpTimeOut = Carbon::now()->addMinute(1)->format('Y-m-d H:i:s');
	        $otpCode = CustomerOtp::genOtp();
	        $otpRefCode = CustomerOtp::genRefOtp();
    		$showPhoneNumber = CustomerOtp::showPhoneNumberView($customer->phone_number);
    		$customerOtp = CustomerOtp::where('ecommerce_customer_id',$customer->id)->first();
    		if($customerOtp){
    			if($customerOtp->is_active){
    				return response()->json([
			            'return_code' => 3,
			            'msg' => 'ท่านได้ทำการยืนยัน OTP เรียบร้อยแล้ว'
			        ]);
    			}else{
    				$customerOtp->update([
    					'otp_ref' => $otpRefCode,
    					'otp_code' => $otpCode,
    					'is_active' => 0
    				]);

    				CustomerOtpLog::create([
    					'ecommerce_customer_id' => $customer->id,
    					'otp_ref' => $otpRefCode,
    					'otp_code' => $otpCode,
    					'flag_status' => 'Success'
    				]);
    			}
    		}else{
    			CustomerOtp::create([
					'ecommerce_customer_id' => $customer->id,
					'otp_ref' => $otpRefCode,
					'otp_code' => $otpCode,
					'is_active' => 1
				]);

    			CustomerOtpLog::create([
					'ecommerce_customer_id' => $customer->id,
					'otp_ref' => $otpRefCode,
					'otp_code' => $otpCode,
					'flag_status' => 'Success'
				]);
    		}
	    	return response()->json([
	            'return_code' => 1,
	            'phone_number' => $showPhoneNumber,
	            'ref_otp_code' => $otpRefCode,
	            'otp_code' => $otpCode,
	            'customer' => $customer,
	            'date_time_now' => strtotime($dateNow),
	            'date_time_otp_timeout' => strtotime($dateOtpTimeOut),
	        ]);
    	}else{
    		return response()->json([
	            'return_code' => 2,
	            'msg' => 'ไม่พบข้อมูล Customer'
	        ]);
    	}
    }

    public function checkOtpIsActive($id)
    {
    	$lineUserID = $id;
    	$customer = Customer::where('line_user_id',$lineUserID)->first();
    	if($customer){
    		$customerOtp = CustomerOtp::where('ecommerce_customer_id',$customer->id)->where('is_active',1)->first();
    		if($customerOtp){
    			return response()->json([
		            'return_code' => 1,
		            'msg' => 'ำการยืนยัน OTP แล้ว'
		        ]);
    		}else{
    			return response()->json([
		            'return_code' => 0,
		            'msg' => 'ยังไม่ได้ทำการยืนยัน OTP'
		        ]);
    		}
    	}else{
    		return response()->json([
	            'return_code' => 2,
	            'msg' => 'ไม่พบข้อมูล Customer'
	        ]);
    	}
    }

    public function activeOtp(Request $request)
    {
        $otpRef = $request->otp_ref;
        $otpCode = $request->otp_code;
        $customerOtp = CustomerOtp::where('otp_ref',$otpRef)->where('otp_code',$otpCode)->where('is_active',0)->first();
        if($customerOtp){
            $customerOtp->update([
                'is_active' => 1
            ]);

            return response()->json([
                'return_code' => 1,
                'msg' => 'ำการยืนยัน OTP แล้ว'
            ]);
        }else{
            return response()->json([
                'return_code' => 2,
                'msg' => 'ไม่พบข้อมูล OTP'
            ]);
        }
    }

}
