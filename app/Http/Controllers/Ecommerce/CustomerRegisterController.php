<?php

namespace YellowProject\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\Customer\Customer;
use YellowProject\Ecommerce\Coupon\Coupon;
use YellowProject\Ecommerce\CoreLineFunction\LineFunction;
use YellowProject\Ecommerce\Customer\CustomerImage;
use YellowProject\LineUserProfile;
use YellowProject\SubscriberLine;
use YellowProject\SubscriberItem;
use YellowProject\Ecommerce\Log\LogSession;
// use YellowProject\Ecommerce\CoreLineFunction\CustomerFirstRegister;
use Carbon\Carbon;

class CustomerRegisterController extends Controller
{
    public function registerCustomer(Request $request)
    {
        $dateNow = Carbon::now();
        
    	$lineUserID = $request->line_user_id;
        $lineUserProfile = LineUserProfile::find($lineUserID);
        $subscriberLine = SubscriberLine::where('subscriber_id',1)->where('line_user_id',$lineUserProfile->id)->first();
        if(!$subscriberLine){
            $subscriberLine = SubscriberLine::create([
                'subscriber_id' => 1,
                'line_user_id' => $lineUserProfile->id
            ]);
        }
        // $request['customer_code'] = 'D'.$year.$month.'000001';
    	$customer = Customer::where('line_user_id',$lineUserID)->first();
        $customerCount = Customer::all()->count();
    	if($customer){
    		$customer->update($request->all());
    	}else{
            $datas = $request->all();
            $customerImage = CustomerImage::create([
                'img_url' => $lineUserProfile->avatar,
            ]);
            $datas['img_id'] = $customerImage->id;
            $year = $dateNow->format('Y');
            $month = $dateNow->format('m');
            $number = $customerCount + 1;
            $number = str_pad($number, 6, '0', STR_PAD_LEFT);
            $datas['customer_code'] = 'D'.$year.$month.$number;
    		$customer = Customer::create($datas);
            Coupon::genCouponFirstRegister();

            SubscriberItem::create([
                'subscriber_line_id' => $subscriberLine->id,
                'field_id' => 1,
                'value' => $customer->customer_code
            ]);

            SubscriberItem::create([
                'subscriber_line_id' => $subscriberLine->id,
                'field_id' => 2,
                'value' => $customer->first_name
            ]);

            SubscriberItem::create([
                'subscriber_line_id' => $subscriberLine->id,
                'field_id' => 3,
                'value' => $customer->last_name
            ]);

            SubscriberItem::create([
                'subscriber_line_id' => $subscriberLine->id,
                'field_id' => 4,
                'value' => $lineUserProfile->phone_number
            ]);

            $logSession = LogSession::where('line_user_id',$lineUserProfile->id)->where('is_active',1)->first();
            if($logSession){
                $logSession->update([
                    'ecommerce_customer_id' => $customer->id
                ]);
            }
            // CustomerFirstRegister::sentMessageCustomerFirstRegister($customer);
    	}

    	return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }
}
