<?php

namespace YellowProject\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\CampaignGiveCoupon\Register;
use YellowProject\LineUserProfile;

class CampaignGiveCouponController extends Controller
{
    public function mainPage()
    {
    	$authUser = \Session::get('line-login', '');
    	\Session::put('line-login', '');
    	if($authUser == ""){
    		return redirect('/bc/GIVECOUPON');
    	}
    	$register = Register::where('line_user_id',$authUser->id)->first();
    	if($register){
    		return redirect('/give-coupon');
    	}

    	return view('campaign-give-coupon.register')
    		->with('lineUserProfile',$authUser);
    }

    public function registerPageStore(Request $request)
    {
    	$lineUserId = $request->line_user_id;
    	$register = Register::where('line_user_id',$lineUserId)->first();
    	if($register){
    		$register->update($request->all());
    	}else{
    		Register::create($request->all());
    	}

    	return redirect('/give-coupon');
    }

    public function couponPage()
    {
    	return view('campaign-give-coupon.coupon');
    }
}
