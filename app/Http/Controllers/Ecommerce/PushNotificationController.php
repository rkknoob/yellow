<?php

namespace YellowProject\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\CoreFunction;
use YellowProject\Ecommerce\Customer\Customer;
use YellowProject\Ecommerce\CoreLineFunction\LineFunction;
use YellowProject\Ecommerce\Order\Order;

class PushNotificationController extends Controller
{
    public function pushNotificationCustomer(Request $request,$id)
    {
        $orderId = $request->order_id;
    	$customerID = $id;
        $customer = Customer::find($customerID);
    	$order = Order::find($orderId);
    	$lineUserProfile = $customer->lineUserProfile;
    	// $sendLineMessageCustomer = CoreFunction::sendLineMessageCustomer($lineUserProfile);
        LineFunction::sentMessageCustomerEndOrder($customer,$order);
    	// $sendEmailCustomer = CoreFunction::sendEmailCustomer($customer);
    	return response()->json([
            'msg_return' => 'ส่งข้อมูลสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function pushNotificationAdmin(Request $request)
    {
    	$sendLineMessageAdmin = CoreFunction::sendLineMessageAdmin();
    	// $sendEmailAdmin = CoreFunction::sendEmailAdmin();
    	return response()->json([
            'msg_return' => 'ส่งข้อมูลสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function pushNotificationDT(Request $request,$id)
    {
    	// $orderId = $request->order_id;
     //    $customerID = $id;
     //    $customer = Customer::find($customerID);
     //    $order = Order::find($orderId);
     //    $lineUserProfile = $customer->lineUserProfile;
     //    LineFunction::sentMessageDTEndOrder($customer,$order);

        // $sendLineMessageCustomer = CoreFunction::sendLineMessageCustomer($lineUserProfile);
        // LineFunction::sentMessageDTEndOrder($customer,$order);
        // $sendEmailCustomer = CoreFunction::sendEmailCustomer($customer);
        return response()->json([
            'msg_return' => 'ส่งข้อมูลสำเร็จ',
            'code_return' => 1,
        ]);
    }
}
