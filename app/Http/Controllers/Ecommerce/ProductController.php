<?php

namespace YellowProject\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\Product;
use YellowProject\Ecommerce\Category;
use YellowProject\Ecommerce\ProductCategory;
use YellowProject\Ecommerce\ProductImage;
use YellowProject\Ecommerce\ProductMeta;
use YellowProject\Ecommerce\ProductDiscount;
use YellowProject\Ecommerce\Image\Image;
use YellowProject\Ecommerce\Customer\CustomerWishlist;
use YellowProject\Ecommerce\RecieveKeyword\RecieveKeyword;
use YellowProject\Ecommerce\Log\LogSession;
use YellowProject\Ecommerce\Log\LogViewProduct;
use YellowProject\TrackingBc;
use Excel;

class ProductController extends Controller
{
    public function getProduct(Request $request)
    {
        $datas = [];
        $searchKeyword = $request->search_keyword;
        $categoryId = $request->category_id;
        $start = $request->start;
        $limit = $request->limit;
        $customerId = $request->ecommerce_customer_id;
        $products = Product::orderBy('source_order')->orderByDesc('updated_at');
        $products->where('status','Published');
        if($searchKeyword != ""){
            RecieveKeyword::create([
                'ecommerce_customer_id' => $customerId,
                'keyword' => $searchKeyword
            ]);
            $products->where('name','like','%'.$searchKeyword.'%');
        }
        if($categoryId != -1){
            $products->whereHas('productCategories', function ($query) use ($categoryId) {
                $query->where('ecommerce_category_id',$categoryId);
            })->get();
        }
        $productCounts = $products->count();
        $products = $products->skip($start-1)->take($limit);
        $products = $products->get();
        foreach ($products as $key => $product) {
            $productDiscount = $product->productDiscount;
            $productCategories = $product->productCategories;
            $productImages = $product->productImages;
            $isWishlist = 0;
            $customerWishlist = CustomerWishlist::where('ecommerce_customer_id',$customerId)->where('ecommerce_product_id',$product->id)->first();
            if($customerWishlist){
                $isWishlist = 1;
            }

            $datas[$key]['id'] = $product->id;
            $datas[$key]['name'] = $product->name;
            $datas[$key]['desc'] = $product->desc;
            $datas[$key]['short_desc'] = $product->short_desc;
            $datas[$key]['price'] = $product->price;
            $datas[$key]['price_unit'] = $product->price_unit;
            $datas[$key]['start_date'] = $product->start_date;
            $datas[$key]['end_date'] = $product->end_date;
            $datas[$key]['status'] = $product->status;
            $datas[$key]['product_brand'] = $product->product_brand;
            $datas[$key]['product_name'] = $product->product_name;
            $datas[$key]['product_size'] = $product->product_size;
            $datas[$key]['source_order'] = $product->source_order;
            $datas[$key]['img_url'] = "";
            $datas[$key]['is_wishlist'] = $isWishlist;
            if($productCategories->count() > 0){
                foreach ($productCategories as $catKey => $productCategory) {
                    $category = $productCategory->category;
                    $datas[$key]['categories'][$catKey]['id'] = $productCategory->ecommerce_category_id;
                    $datas[$key]['categories'][$catKey]['name'] = $category->name;
                }
            }
            if($productDiscount){
                $datas[$key]['discount']['priority'] = $productDiscount->priority;
                $datas[$key]['discount']['discount_price'] = $productDiscount->discount_price;
                $datas[$key]['discount']['start_date'] = $productDiscount->start_date;
                $datas[$key]['discount']['end_date'] = $productDiscount->end_date;
            }
            if($productImages->count() > 0){
                foreach ($productImages as $keyProductImage => $productImage) {
                    $imageData = $productImage->image;
                    $datas[$key]['images'][$keyProductImage]['img_url'] = "";
                    $datas[$key]['images'][$keyProductImage]['img_size'] = "";
                    if($imageData){
                        $datas[$key]['img_url'] = $imageData->img_url;
                        $datas[$key]['images'][$keyProductImage]['img_url'] = $imageData->img_url;
                        $datas[$key]['images'][$keyProductImage]['img_size'] = $imageData->img_size;
                    }
                    $datas[$key]['images'][$keyProductImage]['product_img_id'] = $productImage->product_img_id;
                    $datas[$key]['images'][$keyProductImage]['name'] = $productImage->name;
                    $datas[$key]['images'][$keyProductImage]['seq'] = $productImage->seq;
                    $datas[$key]['images'][$keyProductImage]['is_base'] = $productImage->is_base;
                    $datas[$key]['images'][$keyProductImage]['is_small'] = $productImage->is_small;
                    $datas[$key]['images'][$keyProductImage]['is_thumbnail'] = $productImage->is_thumbnail;
                }
            }
        }

        return response()->json([
            'datas' => $datas,
            'count_product' => $productCounts,
        ]);
    }

    public function getShowProduct(Request $request,$id)
    {
        $customerId = $request->ecommerce_customer_id;
        $product = Product::find($id);
        $productMeta = $product->productMeta;
        $productDiscount = $product->productDiscount;
        $productCategories = $product->productCategories;
        $productRelatedProducts = $product->productRelatedProducts;
        $productImages = $product->productImages;
        $isWishlist = 0;
        $customerWishlist = CustomerWishlist::where('ecommerce_customer_id',$customerId)->where('ecommerce_product_id',$product->id)->first();
        if($customerWishlist){
            $isWishlist = 1;
        }

        $datas['id'] = $product->id;
        $datas['name'] = $product->name;
        $datas['desc'] = $product->desc;
        $datas['short_desc'] = $product->short_desc;
        $datas['start_date'] = $product->start_date;
        $datas['end_date'] = $product->end_date;
        $datas['sku'] = $product->sku;
        $datas['price'] = $product->price;
        $datas['price_unit'] = $product->price_unit;
        $datas['shipping_cost'] = $product->shipping_cost;
        $datas['vat'] = $product->vat;
        $datas['source_order'] = $product->source_order;
        $datas['out_of_stock_status'] = $product->out_of_stock_status;
        $datas['status'] = $product->status;
        $datas['product_brand'] = $product->product_brand;
        $datas['product_name'] = $product->product_name;
        $datas['product_size'] = $product->product_size;
        $datas['reward_point'] = $product->reward_point;
        $datas['img_url'] = "";
        $datas['is_wishlist'] = $isWishlist;
        $datas['categories'] = [];
        $datas['meta'] = [];
        $datas['discount'] = [];
        $datas['images'] = [];
        $datas['product_related_products'] = [];
        if($productCategories->count() > 0){
            foreach ($productCategories as $catKey => $productCategory) {
                $category = $productCategory->category;
                $datas['categories'][$catKey]['id'] = $productCategory->ecommerce_category_id;
                $datas['categories'][$catKey]['name'] = $category->name;
            }
        }
        if($productMeta){
            $datas['meta']['title'] = $productMeta->title;
            $datas['meta']['keyword'] = $productMeta->keyword;
            $datas['meta']['desc'] = $productMeta->desc;
        }
        if($productDiscount){
            $datas['discount']['priority'] = $productDiscount->priority;
            $datas['discount']['discount_price'] = $productDiscount->discount_price;
            $datas['discount']['start_date'] = $productDiscount->start_date;
            $datas['discount']['end_date'] = $productDiscount->end_date;
        }
        if($productImages->count() > 0){
            foreach ($productImages as $key => $productImage) {
                $imageData = $productImage->image;
                $datas['images'][$key]['img_url'] = "";
                $datas['images'][$key]['img_size'] = "";
                if($imageData){
                    $datas['img_url'] = $imageData->img_url;
                    $datas['images'][$key]['img_url'] = $imageData->img_url;
                    $datas['images'][$key]['img_size'] = $imageData->img_size;
                }
                $datas['images'][$key]['product_img_id'] = $productImage->product_img_id;
                $datas['images'][$key]['name'] = $productImage->name;
                $datas['images'][$key]['seq'] = $productImage->seq;
                $datas['images'][$key]['is_base'] = $productImage->is_base;
                $datas['images'][$key]['is_small'] = $productImage->is_small;
                $datas['images'][$key]['is_thumbnail'] = $productImage->is_thumbnail;
            }
        }
        if($productRelatedProducts->count() > 0){
            foreach ($productRelatedProducts as $key => $productRelatedProduct) {
                $datas['product_related_products'][$key]['value'] = $productRelatedProduct->value;
            }
        }

        $logSession = LogSession::where('ecommerce_customer_id',$customerId)->where('is_active',1)->first();
        if($logSession){
            $logSession->update([
                'is_product_views' => 1
            ]);
        }

        LogViewProduct::create([
            'ecommerce_customer_id' => $customerId,
            'product_id' => $id
        ]);

        return response()->json([
            'datas' => $datas,
        ]);
    }

    public function importProduct(Request $request)
    {
        $results = Excel::load($request->product_files, function($reader) {
        })->all()->toArray();
        foreach ($results as $key => $result) {
            $products = [];

            $category = Category::where('name',$result['categores'])->first();
            $baseUrl = \URL::to('/');
            $trackingBc = TrackingBc::create([
                'code' => $result['url_track'],
                'tracking_source' => $result['tracking_source'],
                'tracking_campaign' => $result['tracking_campagin'],
                'tracking_ref' => $result['tracking_ref'],
                'generated_short_url' => $baseUrl."/bc/".$result['url_track'],
                'generated_full_url' => $baseUrl."/bc/".$result['url_track']."?"."yl_source=".$result['tracking_source']."&yl_campaign=".$result['tracking_campagin']."&yl_ref=".$result['tracking_ref'],
                'desc' => $result['tracking_description'],
                'is_route_name' => 1,
            ]);

            $products['name'] = $result['name'];
            $products['desc'] = $result['description'];
            $products['short_desc'] = $result['short_description'];
            $products['start_date'] = $result['start_date'];
            $products['end_date'] = $result['end_date'];
            $products['sku'] = $result['sku'];
            $products['price'] = $result['price_thb'];
            $products['price_unit'] = $result['price_unit'];
            $products['shipping_cost'] = $result['shipping_cost_thb'];
            $products['vat'] = $result['vat_type'];
            $products['source_order'] = $result['sort_order'];
            $products['status'] = $result['status'];
            $products['reward_point'] = 0;
            $products['product_brand'] = $result['product_brand'];
            $products['product_size'] = $result['product_size'];
            $products['tracking_bc_id'] = $trackingBc->id;

            $product = Product::create($products);

            $trackingBc->update([
                'original_url' => $baseUrl."/full-ecommerce#/detail/".$product->id
            ]);

            ProductCategory::create([
                'ecommerce_product_id' => $product->id,
                'ecommerce_category_id' => $category->id
            ]);

            ProductMeta::create([
                'ecommerce_product_id' => $product->id,
                'title' => $result['meta_title'],
                'keyword' => $result['meta_keyword'],
                'desc' => $result['meta_description'],
            ]);

            ProductDiscount::create([
                'ecommerce_product_id' => $product->id,
                'priority' => 1,
                'discount_price' => $result['discount_price'],
                'start_date' => $result['start_date'],
                'end_date' => $result['available_date'],
            ]);

            $image = Image::create([
                'img_url' => 'Waiting Image',
                'img_size' => '',
            ]);

            $productImage = ProductImage::create([
                'ecommerce_product_id' => $product->id,
                'product_img_id' => $image->id,
                'name' => $result['name'],
                'seq' => 1,
                'is_base' => 1,
                'is_small' => 1,
                'is_thumbnail' => 1
            ]);
        }
    }
}
