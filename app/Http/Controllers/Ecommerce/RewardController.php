<?php

namespace YellowProject\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\PremiumKitchenwareEquipment\PremiumKitchenwareEquipment;
use YellowProject\Ecommerce\PremiumKitchenwareEquipment\PremiumKitchenwareEquipmentUser;
use YellowProject\Ecommerce\ExclusiveOnline\ExclusiveOnline;
use YellowProject\Ecommerce\ExclusiveOnline\ExclusiveOnlineUser;
use YellowProject\Ecommerce\Customer\Customer;
use YellowProject\Ecommerce\Customer\CustomerReedeemRewardHistory;

class RewardController extends Controller
{
    public function getReward()
    {
    	$datas = [];
    	$premiumKitchenwareEquipments = PremiumKitchenwareEquipment::all();
    	$exclusiveOnlines = ExclusiveOnline::all();
    	foreach ($premiumKitchenwareEquipments as $key => $premiumKitchenwareEquipment) {
    		$datas['premium_kitchenware_equipment'] = $premiumKitchenwareEquipment;
    	}
    	foreach ($exclusiveOnlines as $key => $exclusiveOnline) {
    		$datas['exclusive_online'] = $exclusiveOnline;
    	}

    	return response()->json([
            'datas' => $datas,
        ]);
    }

    public function kitchenwareReedeem(Request $request,$id)
    {
    	$customerId = $request->ecommerce_customer_id;
    	$premiumKitchenwareEquipment = PremiumKitchenwareEquipment::find($id);
    	$premiumKitchenwareEquipmentUser = PremiumKitchenwareEquipmentUser::where('premium_kitchenware_id',$id)->where('ecommerce_customer_id',$customerId)->first();
    	$customer = Customer::find($customerId);
    	if(!$customer){
    		return response()->json([
	            'msg_return' => 'ไม่พบข้อมูล Customer',
	            'code_return' => 4,
	        ]);
    	}
    	if($premiumKitchenwareEquipmentUser){
    		return response()->json([
	            'msg_return' => 'ทำการ Reedeem Reward นี้ไปแล้ว',
	            'code_return' => 3,
	        ]);
    	}
    	$customerRewardPoint = $customer->reward_point;
    	$ep = $premiumKitchenwareEquipment->ep;
    	$subtractRewardPoint = $customerRewardPoint - $ep;
    	if($subtractRewardPoint < 0){
    		return response()->json([
	            'msg_return' => 'Reward Point ไม่เพียงพอ',
	            'code_return' => 5,
	        ]);
    	}
    	$customer->update([
    		'reward_point' => $subtractRewardPoint,
    	]);
    	CustomerReedeemRewardHistory::create([
    		'ecommerce_customer_id' => $customerId,
    		'type' => 'Premium Kitchenware or Equipment',
    		'ep' => $ep
    	]);
    	if($premiumKitchenwareEquipment){
    		PremiumKitchenwareEquipmentUser::create([
    			'premium_kitchenware_id' => $id,
    			'ecommerce_customer_id' => $customerId,
    			'status' => 'Waiting Approve'
    		]);
    	}else{
    		return response()->json([
	            'msg_return' => 'ไม่พบข้อมูล Reward',
	            'code_return' => 2,
	        ]);
    	}

    	return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function exclusiveOnlineReedeem(Request $request,$id)
    {
    	$customerId = $request->ecommerce_customer_id;
    	$exclusiveOnline = ExclusiveOnline::find($id);
    	$exclusiveOnlineUser = ExclusiveOnlineUser::where('exclusive_online_id',$id)->where('ecommerce_customer_id',$customerId)->first();
    	$customer = Customer::find($customerId);
    	if(!$customer){
    		return response()->json([
	            'msg_return' => 'ไม่พบข้อมูล Customer',
	            'code_return' => 4,
	        ]);
    	}
    	if($exclusiveOnlineUser){
    		return response()->json([
	            'msg_return' => 'ทำการ Reedeem Reward นี้ไปแล้ว',
	            'code_return' => 3,
	        ]);
    	}
    	$customerRewardPoint = $customer->reward_point;
    	$ep = $exclusiveOnline->ep;
    	$subtractRewardPoint = $customerRewardPoint - $ep;
    	if($subtractRewardPoint < 0){
    		return response()->json([
	            'msg_return' => 'Reward Point ไม่เพียงพอ',
	            'code_return' => 5,
	        ]);
    	}
    	$customer->update([
    		'reward_point' => $subtractRewardPoint,
    	]);
    	CustomerReedeemRewardHistory::create([
    		'ecommerce_customer_id' => $customerId,
    		'type' => 'Exclusive Online',
    		'ep' => $ep
    	]);
    	if($exclusiveOnline){
    		ExclusiveOnlineUser::create([
    			'exclusive_online_id' => $id,
    			'ecommerce_customer_id' => $customerId,
    			'status' => 'Waiting Approve'
    		]);
    	}else{
    		return response()->json([
	            'msg_return' => 'ไม่พบข้อมูล Reward',
	            'code_return' => 2,
	        ]);
    	}

    	return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function showKitchenware($customerId,$kitchenwareId)
    {
    	$datas = [];
    	$premiumKitchenwareEquipmentUser = PremiumKitchenwareEquipmentUser::where('premium_kitchenware_id',$kitchenwareId)->where('ecommerce_customer_id',$customerId)->where('status','Approve')->first();
    	if($premiumKitchenwareEquipmentUser){
    		$videoFiles = $premiumKitchenwareEquipmentUser->videoFiles;
    		$pdfFiles = $premiumKitchenwareEquipmentUser->pdfFiles;
    		if($videoFiles){
    			foreach ($videoFiles as $key => $videoFile) {
    				$videoFile->file;
    			}
    		}
    		if($pdfFiles){
    			foreach ($pdfFiles as $key => $pdfFile) {
    				$pdfFile->file;
    			}
    		}
    		$datas = $premiumKitchenwareEquipmentUser;
    		return response()->json([
	            'datas' => $datas,
	            'code_return' => 1,
	        ]);
    	}else{
    		return response()->json([
	            'msg_return' => 'ไม่มพบข้อมูล Reward',
	            'code_return' => 2,
	        ]);
    	}
    }

    public function showExclusiveOnline($customerId,$exclusiveId)
    {
    	$datas = [];
    	$exclusiveOnlineUser = ExclusiveOnlineUser::where('exclusive_online_id',$exclusiveId)->where('ecommerce_customer_id',$customerId)->where('status','Approve')->first();
    	if($exclusiveOnlineUser){
    		$videoFiles = $exclusiveOnlineUser->videoFiles;
    		$pdfFiles = $exclusiveOnlineUser->pdfFiles;
    		if($videoFiles){
    			foreach ($videoFiles as $key => $videoFile) {
    				$videoFile->file;
    			}
    		}
    		if($pdfFiles){
    			foreach ($pdfFiles as $key => $pdfFile) {
    				$pdfFile->file;
    			}
    		}
    		$datas = $exclusiveOnlineUser;
    		return response()->json([
	            'datas' => $datas,
	            'code_return' => 1,
	        ]);
    	}else{
    		return response()->json([
	            'msg_return' => 'ไม่มพบข้อมูล Reward',
	            'code_return' => 2,
	        ]);
    	}
    }
}
