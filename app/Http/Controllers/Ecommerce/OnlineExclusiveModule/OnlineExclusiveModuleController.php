<?php

namespace YellowProject\Http\Controllers\Ecommerce\OnlineExclusiveModule;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\OnlineExclusiveModule\OnlineExclusiveModuleCourse;
use YellowProject\Ecommerce\OnlineExclusiveModule\OnlineExclusiveModuleCourseVideo;
use YellowProject\Ecommerce\OnlineExclusiveModule\OnlineExclusiveModuleCustomerReDeem;
use YellowProject\Ecommerce\Customer\Customer;

class OnlineExclusiveModuleController extends Controller
{
    public function exclusiveModuleListing(Request $request)
    {
    	$datas = [];
    	$datas['my_online_module'] = [];
    	$datas['online_module'] = [];
        $onlineExclusiveModuleCustomerReDeems = OnlineExclusiveModuleCustomerReDeem::where('line_user_id',$request->line_user_id)->get();
        $courseIds = $onlineExclusiveModuleCustomerReDeems->pluck('online_exclusive_course_id')->toArray();
        $onlineExclusiveModuleCourses = OnlineExclusiveModuleCourse::whereNotIn('id',$courseIds)->get();
        $count = 0;
        foreach ($onlineExclusiveModuleCustomerReDeems as $key => $onlineExclusiveModuleCustomerReDeem) {
            $datas['my_online_module'][$count]['id'] = $onlineExclusiveModuleCustomerReDeem->online_exclusive_course_id;
            $datas['my_online_module'][$count]['img_url'] = $onlineExclusiveModuleCustomerReDeem->img_url;
            $datas['my_online_module'][$count]['course_name'] = $onlineExclusiveModuleCustomerReDeem->course_name;
            $datas['my_online_module'][$count]['description'] = $onlineExclusiveModuleCustomerReDeem->description;
            $datas['my_online_module'][$count]['ep'] = $onlineExclusiveModuleCustomerReDeem->ep;
            $count++;
        }
        $count = 0;
        foreach ($onlineExclusiveModuleCourses as $key => $onlineExclusiveModuleCourse) {
            $datas['online_module'][$count]['id'] = $onlineExclusiveModuleCourse->id;
            $datas['online_module'][$count]['img_url'] = $onlineExclusiveModuleCourse->img_url;
            $datas['online_module'][$count]['course_name'] = $onlineExclusiveModuleCourse->course_name;
            $datas['online_module'][$count]['description'] = $onlineExclusiveModuleCourse->description;
            $datas['online_module'][$count]['ep'] = $onlineExclusiveModuleCourse->ep;
            $count++;
        }
    	// $onlineExclusiveModuleCourses = OnlineExclusiveModuleCourse::select(
    	// 	'id',
    	// 	'img_url',
     //        'course_name',
     //        'description',
     //        'ep'
    	// )->where('status','published')->orderByDesc('created_at')->get();
    	// $datas['online_module'] = $onlineExclusiveModuleCourses->toArray();

    	return response()->json($datas);
    }

    public function exclusiveModuleDetail($id)
    {
    	$datas = [];
    	$onlineExclusiveModuleCourse = OnlineExclusiveModuleCourse::find($id);
    	$datas['id'] = $onlineExclusiveModuleCourse->id;
    	$datas['img_url'] = $onlineExclusiveModuleCourse->img_url;
    	$datas['course_name'] = $onlineExclusiveModuleCourse->course_name;
    	$datas['description'] = $onlineExclusiveModuleCourse->description;
    	$datas['ep'] = $onlineExclusiveModuleCourse->ep;

    	return response()->json($datas);
    }

    public function exclusiveModuleVideoDetail($id)
    {
    	$datas = [];
    	$onlineExclusiveModuleCourse = OnlineExclusiveModuleCourse::find($id);
        $onlineExclusiveModuleCourseVideos = OnlineExclusiveModuleCourseVideo::where('online_exclusive_course_id',$onlineExclusiveModuleCourse->id)->get();
        $datas['id'] = $onlineExclusiveModuleCourse->id;
        $datas['course_name'] = $onlineExclusiveModuleCourse->course_name;
        $datas['video'] = [];
        foreach ($onlineExclusiveModuleCourseVideos as $key => $onlineExclusiveModuleCourseVideo) {
            $datas['video'][$key]['name'] = $onlineExclusiveModuleCourseVideo->name;
            $datas['video'][$key]['thumbnail_img_url'] = $onlineExclusiveModuleCourseVideo->thumbnail_img_url;
            $datas['video'][$key]['manual_video_url'] = $onlineExclusiveModuleCourseVideo->manual_video_url;
            $datas['video'][$key]['upload_video_url'] = $onlineExclusiveModuleCourseVideo->upload_video_url;
        }

    	return response()->json($datas);
    }

    public function exclusiveModuleReDeem(Request $request)
    {
        $lineUserId = $request->line_user_id;
        $itemId = $request->item_id;
        $customer = Customer::where('line_user_id',$lineUserId)->first();
        if(!$customer){
            return response()->json([
                'msg_return' => 'Not Found Customer',
                'code_return' => 2,
            ]);
        }
        $onlineExclusiveModuleCourse = OnlineExclusiveModuleCourse::find($itemId);
        if(!$onlineExclusiveModuleCourse){
            return response()->json([
                'msg_return' => 'Not Found Course',
                'code_return' => 2,
            ]);
        }
        if($customer->reward_point < $onlineExclusiveModuleCourse->ep){
             return response()->json([
                'msg_return' => 'Not ReDeem Point Not Enough',
                'code_return' => 2,
            ]);
        }
        $customerPoint = $customer->reward_point;
        $itemEP = $onlineExclusiveModuleCourse->ep;
        $diffPoint = $customerPoint - $itemEP;
        $customer->update([
            'reward_point' => $diffPoint
        ]);
        OnlineExclusiveModuleCustomerReDeem::create([
            'line_user_id' => $lineUserId,
            'customer_id' => $customer->id,
            'online_exclusive_course_id' => $onlineExclusiveModuleCourse->id,
            'course_name' => $onlineExclusiveModuleCourse->course_name,
            'description' => $onlineExclusiveModuleCourse->description,
            'img_url' => $onlineExclusiveModuleCourse->img_url,
            'ep' => $onlineExclusiveModuleCourse->ep,
            'redeem_date' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        return response()->json([
            'msg_return' => 'SUCCESS',
            'code_return' => 1,
        ]);
    }
}
