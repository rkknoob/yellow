<?php

namespace YellowProject\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\Customer\Customer;
use YellowProject\Ecommerce\Customer\CustomerShippingAddress;
use YellowProject\Ecommerce\Customer\CustomerTaxBillingAddress;
use YellowProject\Ecommerce\Customer\CustomerImage;
use YellowProject\Ecommerce\OTP\CustomerOtp;
use YellowProject\Ecommerce\CoreLineFunction\CustomerFirstRegister;
use YellowProject\LineUserProfile;
use YellowProject\SubscriberLine;
use YellowProject\SubscriberItem;
use Carbon\Carbon;
use URL;

class CustomerController extends Controller
{
    public function addShippingAddress(Request $request)
    {
    	$customerShippingAddress = CustomerShippingAddress::create($request->all());
        $customer = Customer::find($customerShippingAddress->ecommerce_customer_id);
        $lineUserProfile = LineUserProfile::find($customer->line_user_id);
        $subscriberLine = SubscriberLine::where('subscriber_id',1)->where('line_user_id',$lineUserProfile->id)->first();
        if(!$subscriberLine){
            $subscriberLine = SubscriberLine::create([
                'subscriber_id' => 1,
                'line_user_id' => $lineUserProfile->id
            ]);
        }
        $subscriberItem = SubscriberItem::where('subscriber_line_id',$subscriberLine->id)->where('field_id',5)->first();
        if(!$subscriberItem){
            SubscriberItem::create([
                'subscriber_line_id' => $subscriberLine->id,
                'field_id' => 5,
                'value' => $customerShippingAddress->address
            ]);
            SubscriberItem::create([
                'subscriber_line_id' => $subscriberLine->id,
                'field_id' => 6,
                'value' => $customerShippingAddress->district
            ]);
            SubscriberItem::create([
                'subscriber_line_id' => $subscriberLine->id,
                'field_id' => 7,
                'value' => $customerShippingAddress->sub_district
            ]);
            SubscriberItem::create([
                'subscriber_line_id' => $subscriberLine->id,
                'field_id' => 8,
                'value' => $customerShippingAddress->province
            ]);
            SubscriberItem::create([
                'subscriber_line_id' => $subscriberLine->id,
                'field_id' => 9,
                'value' => $customerShippingAddress->post_code
            ]);
            SubscriberItem::create([
                'subscriber_line_id' => $subscriberLine->id,
                'field_id' => 10,
                'value' => $customerShippingAddress->tel
            ]);
        }

    	return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function getShippingAddress($id)
    {
    	$customerShippingAddresses = CustomerShippingAddress::where('ecommerce_customer_id',$id)->get();

    	return response()->json([
            'datas' => $customerShippingAddresses,
        ]);
    }

    public function showShippingAddress($id)
    {
        $customerShippingAddresse = CustomerShippingAddress::find($id);

        return response()->json([
            'datas' => $customerShippingAddresse,
        ]);
    }

    public function deleteShippingAddress($id)
    {
        $customerShippingAddresse = CustomerShippingAddress::find($id);
        if($customerShippingAddresse){
            $customerShippingAddresse->delete();
        }

        return response()->json([
            'msg_return' => 'ลบข้อมูลสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function addBillingAddress(Request $request)
    {
        CustomerTaxBillingAddress::create($request->all());

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function getBillingAddress($id)
    {
        $customerTaxBillingAddress = CustomerTaxBillingAddress::where('ecommerce_customer_id',$id)->get();

        return response()->json([
            'datas' => $customerTaxBillingAddress,
        ]);
    }

    public function deleteBillingAddress($id)
    {
        $customerTaxBillingAddress = CustomerTaxBillingAddress::find($id);
        if($customerTaxBillingAddress){
            $customerTaxBillingAddress->delete();
        }

        return response()->json([
            'msg_return' => 'ลบข้อมูลสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function getCustomerProfile($id)
    {
    	$datas = [];
    	$customer = Customer::find($id);
    	if($customer){
    		$customerShippingAddresses = $customer->customerShippingAddresses;
    		$orders = $customer->orders;
            $customerImage = $customer->customerImage;
            $customerRewardKitchenWares = $customer->customerRewardKitchenWares;
    		$customerRewardExclusives = $customer->customerRewardExclusives;
            $datas['customer_code'] = $customer->customer_code;
    		$datas['id'] = $customer->id;
    		$datas['first_name'] = $customer->first_name;
            $datas['last_name'] = $customer->last_name;
            $datas['email'] = $customer->email;
            $datas['city'] = $customer->city;
            $datas['phone_number'] = $customer->phone_number;
            $datas['reward_point'] = $customer->reward_point;
            $datas['market_name'] = $customer->market_name;
            $datas['market_type'] = $customer->market_type;
            $datas['post_code'] = $customer->post_code;
    		$datas['dt_id'] = $customer->dt_id;
    		$datas['customer_img_url'] = ($customerImage)? $customerImage->img_url : null;
    		$datas['customer_order_histories'] = [];
    		$datas['customer_reward_items'] = [];
    		$datas['customer_check_personal_coupon'] = [];
    		$datas['customer_shipping_addresses'] = [];
    		if($customerShippingAddresses){
    			foreach ($customerShippingAddresses as $key => $customerShippingAddress) {
    				$datas['customer_shipping_addresses'][$key]['name'] = $customerShippingAddress->name;
    				$datas['customer_shipping_addresses'][$key]['last_name'] = $customerShippingAddress->last_name;
    				$datas['customer_shipping_addresses'][$key]['company_name'] = $customerShippingAddress->company_name;
    				$datas['customer_shipping_addresses'][$key]['address'] = $customerShippingAddress->address;
    				$datas['customer_shipping_addresses'][$key]['district'] = $customerShippingAddress->district;
    				$datas['customer_shipping_addresses'][$key]['sub_district'] = $customerShippingAddress->sub_district;
    				$datas['customer_shipping_addresses'][$key]['province'] = $customerShippingAddress->province;
    				$datas['customer_shipping_addresses'][$key]['post_code'] = $customerShippingAddress->post_code;
    			}
    		}
    		if($orders){
    			foreach ($orders as $key => $order) {
    				$order->orderProducts;
    				$datas['customer_order_histories'][$key] = $order;
    			}
    		}
            if($customerRewardKitchenWares){
                foreach ($customerRewardKitchenWares as $key => $customerRewardKitchenWare) {
                    $customerRewardKitchenWare->premiumKitchenwareEquipment;
                }
                $datas['customer_reward_items']['premium_kitchenware_or_equipment'] = $customerRewardKitchenWares;
            }
            if($customerRewardExclusives){
                foreach ($customerRewardExclusives as $key => $customerRewardExclusive) {
                    $customerRewardExclusive->exclusiveOnline;
                }
                $datas['customer_reward_items']['exclusive_online'] = $customerRewardExclusives;
            }
    	}

    	return response()->json([
            'datas' => $datas,
        ]);
    }

    public function uploadMultiple(Request $request)
    {
        // dd($request->all());
        CustomerImage::checkFolderEcomCustomer();
        $datas = collect();
        if($request->img_items){
            foreach ($request->img_items as $key => $img_item) {
                $dateNow = Carbon::now()->format('dmY_His');
                $fileImage = $img_item;
                $type = null;
                // ImageFile::checkFolderDefaultPath();
                $destinationPath = 'ecommerce/customer'; // upload path
                $extension = $fileImage->getClientOriginalExtension(); // getting image extension
                $fileName = $dateNow.'-'.$key.'.'.$extension; // renameing image
                $fileImage->move($destinationPath, $fileName); // uploading file to given path

                $imageFile = CustomerImage::create([
                    'img_url' => URL::to('/')."/".$destinationPath."/".$fileName,
                    // 'img_size' => $img_item->img_size,
                    'img_size' => null,
                ]);

                $datas->put($key,$imageFile);
            }
        }

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
            'datas' => $datas,
        ]);
    }

    public function changeMobilePhone(Request $request)
    {
        $customerId = $request->ecommerce_customer_id;
        $customer = Customer::find($customerId);
        if($customer){
            $customer->update([
                'phone_number' => $request->phone_number
            ]);

            CustomerOtp::where('ecommerce_customer_id',$customerId)->delete();
        }else{
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูล Customer',
                'code_return' => 2,
            ]);
        }

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function updateDataCustomerTradePartner(Request $request)
    {
        $customerId = $request->ecommerce_customer_id;
        $customer = Customer::find($customerId);
        $customer->update([
            'post_code' => $request->post_code,
            'dt_id' => $request->dt_id
        ]);

        CustomerFirstRegister::sentMessageCustomerFirstRegister($customer);

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function updateDataCustomerImageProfile(Request $request)
    {
        $customerId = $request->ecommerce_customer_id;
        $imageId = $request->img_id;
        $customer = Customer::find($customerId);
        $customer->update([
            'img_id' => $imageId
        ]);

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }
}
