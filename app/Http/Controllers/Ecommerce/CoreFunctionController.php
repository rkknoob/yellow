<?php

namespace YellowProject\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\Customer\Customer;
use YellowProject\Country\Province;
use YellowProject\Country\District;
use YellowProject\Country\SubDistrict;
use YellowProject\Ecommerce\DTManagement\DTManagement;
use YellowProject\Ecommerce\DTManagement\DTManagementPostCode;
use YellowProject\LineUserProfile;
use YellowProject\Ecommerce\Customer\CustomerImage;

class CoreFunctionController extends Controller
{
    public function checkRegister($id)
    {
        $dateNow = \Carbon\Carbon::now();
        $datas = [];
    	$isRegister = 0;
    	$lineUserID = $id;
        $customerId = 0;
    	$customer = Customer::where('line_user_id',$lineUserID)->first();
        $lineUserProfile = LineUserProfile::find($lineUserID);
    	if($customer){
            $customerId = $customer->id;
            if($customer->dt_id != -1 && $customer->dt_id != ""){
        		$isRegister = 1;
            }
    	}else{
            $customerCount = Customer::all()->count();
            $year = $dateNow->format('Y');
            $month = $dateNow->format('m');
            $number = $customerCount + 1;
            $number = str_pad($number, 6, '0', STR_PAD_LEFT);
            $customerCode = 'D'.$year.$month.$number;
            $customerImage = CustomerImage::create([
                'img_url' => $lineUserProfile->avatar,
            ]);
            $customer = Customer::create([
                'customer_code' => $customerCode,
                'line_user_id' => $lineUserProfile->id,
                'first_name' => null,
                'last_name' => null,
                'email' => $lineUserProfile->email,
                'city' => null,
                'phone_number' => $lineUserProfile->phone_number,
                'reward_point' => 0,
                'img_id' => $customerImage->id,
                'market_name' => null,
                'market_type' => null,
                'post_code' => null,
                'dt_id' => null
            ]);
            $customerId = $customer->id;
            $isRegister = 1;
        }

    	return response()->json([
            'is_register' => $isRegister,
            'ecommerce_customer_id' => $customerId,
            'customer' => $customer,
            'phone_number' => $lineUserProfile->phone_number
        ]);
    }

    public function checkDT($postCode)
    {
        $DTManagement = DTManagement::orderBy('seq')->whereHas('postCodes', function ($query) use ($postCode) {
            $query->where('post_code',$postCode);
        })->where('status','active')->first();
    	if($DTManagement){
    		return response()->json([
	            'return_code' => 1,
	            'msg' => 'พบข้อมูล DT',
                'dt_id' => $DTManagement->id,
	            'dt_code' => $postCode,
	            'dt_name' => $DTManagement->name,
	        ]);
    	}else{
    		return response()->json([
	            'return_code' => 0,
	            'msg' => 'ไม่พบข้อมูล DT',
	        ]);
    	}
    	
    }

    public function getProvince(Request $request)
    {
        $DTManagements = DTManagementPostCode::whereHas('dtManagement', function ($query) {
            $query->where('status','active');
        })->pluck('post_code')->toArray();

        $provinces = Province::whereHas('districts', function ($query) use ($DTManagements) {
            $query->whereHas('subDistricts', function ($query) use ($DTManagements) {
                $query->whereIn('post_code',$DTManagements);
            });
        })->get();

        return response()->json([
            'datas' => $provinces,
        ]);
    }

    public function getDistrict(Request $request)
    {
        $DTManagements = DTManagementPostCode::whereHas('dtManagement', function ($query) {
            $query->where('status','active');
        })->pluck('post_code')->toArray();
        $provinceId = $request->province_id;
        if($provinceId != -1){
            $province = Province::find($provinceId);
            if($province){
                $districts = District::where('province_id',$province->id)->whereHas('subDistricts', function ($query) use ($DTManagements) {
                    $query->whereIn('post_code',$DTManagements);
                })->get();
            }else{
                $districts = collect();
            }
        }else{
            $districts = collect();
        }
        
        return response()->json([
            'datas' => $districts,
        ]);
    }

    public function getSubDistrict(Request $request)
    {
        $DTManagements = DTManagementPostCode::whereHas('dtManagement', function ($query) {
            $query->where('status','active');
        })->pluck('post_code')->toArray();
        $districtId = $request->district_id;
        if($districtId == -1){
            $subDistricts = collect();
        }else{
            $district = District::find($districtId);
            if($district){
                $subDistricts = SubDistrict::where('district_id',$district->id)->whereIn('post_code',$DTManagements)->get();
            }else{
                $subDistricts = collect();
            }
        }

        return response()->json([
            'datas' => $subDistricts,
        ]);
    }

    public function getNormalProvince(Request $request)
    {
        $provinces = Province::all();

        return response()->json([
            'datas' => $provinces,
        ]);
    }

    public function getNormalDistrict(Request $request)
    {
        $provinceId = $request->province_id;
        if($provinceId != -1){
            $province = Province::find($provinceId);
            if($province){
                $districts = District::where('province_id',$province->id)->get();
            }else{
                $districts = collect();
            }
        }else{
            $districts = collect();
        }
        
        return response()->json([
            'datas' => $districts,
        ]);
    }

    public function getNormalSubDistrict(Request $request)
    {
        $districtId = $request->district_id;
        if($districtId == -1){
            $subDistricts = collect();
        }else{
            $district = District::find($districtId);
            if($district){
                $subDistricts = SubDistrict::where('district_id',$district->id)->get();
            }else{
                $subDistricts = collect();
            }
        }

        return response()->json([
            'datas' => $subDistricts,
        ]);
    }
}
