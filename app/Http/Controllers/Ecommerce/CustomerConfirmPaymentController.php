<?php

namespace YellowProject\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\Order\Order;
use YellowProject\Ecommerce\Image\Image;
use YellowProject\Ecommerce\Order\OrderPayment;
use YellowProject\Ecommerce\Order\OrderDTPayment;
use YellowProject\Ecommerce\CoreLineFunction\OrderConfirmPaymentNotification;
use YellowProject\Ecommerce\DTManagement\DTManagementBankDetail;
use Carbon\Carbon;
use URL;

class CustomerConfirmPaymentController extends Controller
{
    public function paymentDetail($id)
    {
        $order = Order::find($id);
        $customer = $order->customer;
        $orderPayment = OrderPayment::where('order_id',$order->id)->first();
        $DTManagementBankDetails = DTManagementBankDetail::where('ecommerce_dt_management_id',$order->dt_id)->get();
        if($orderPayment){
            return view('ecommerce.thank')
                ->with('text','ท่านได้ทำการแจ้งการชำระเงินเรียบร้อยแล้ว');
        }
        $orderDTPayment = OrderDTPayment::where('order_id',$order->id)->first();
        return view('ecommerce.payment')
            ->with('order',$order)
            ->with('customer',$customer)
            ->with('orderDTPayment',$orderDTPayment)
            ->with('DTManagementBankDetails',$DTManagementBankDetails);
    }

    public function confirmPayment($id)
    {
    	$order = Order::find($id);
    	$orderPayment = OrderPayment::where('order_id',$order->id)->first();
        $DTManagementBankDetails = DTManagementBankDetail::where('ecommerce_dt_management_id',$order->dt_id)->get();
    	if($orderPayment){
    		return view('ecommerce.thank')
        		->with('text','ท่านได้ทำการแจ้งการชำระเงินเรียบร้อยแล้ว');
    	}
    	return view('ecommerce.confirm-payment')
    		->with('order',$order)
            ->with('DTManagementBankDetails',$DTManagementBankDetails);
    }

    public function confirmPaymentCOD($id)
    {
    	$dateNow = Carbon::now()->format('dmY_His');
    	$transferDate = Carbon::now()->format('Y-m-d H:i:s');
    	$datas = [];
    	$order = Order::find($id);
        if($order){
            $order->update([
                'order_status' => 'Customer Payment Confirmation',
                'payment_information' => 'COD'
            ]);
        }
    	$dateNow = Carbon::now()->format('dmY_His');
    	$datas['order_id'] = $order->id;
    	$datas['payment_transaction_id'] = OrderPayment::genPaymentId();
        $datas['payment_date_submit'] = $transferDate;
        $datas['payment_amount'] = $order->total_paid;
        $datas['payment_type'] = "COD";

        $orderPayment = OrderPayment::create($datas);
    	OrderConfirmPaymentNotification::pushNotificationCustomer($order,$orderPayment);

    	return view('ecommerce.thank')
        		->with('text','ขอบคุณสำหรับการชำระเงินแบบชำระเงินปลายทาง');
    }

    public function confirmPaymentPromptPay($id)
    {	
    	$dateNow = Carbon::now()->format('dmY_His');
    	$transferDate = Carbon::now()->format('Y-m-d H:i:s');
    	$datas = [];
    	$order = Order::find($id);
        if($order){
            $order->update([
                'order_status' => 'Customer Payment Confirmation',
                'payment_information' => 'PromptPay'
            ]);
        }
    	$dateNow = Carbon::now()->format('dmY_His');
    	$datas['order_id'] = $order->id;
    	$datas['payment_transaction_id'] = OrderPayment::genPaymentId();
        $datas['payment_date_submit'] = $transferDate;
        $datas['payment_amount'] = $order->total_paid;
        $datas['payment_type'] = "PromtPay";
        $orderPayment = OrderPayment::create($datas);
        OrderConfirmPaymentNotification::pushNotificationCustomer($order,$orderPayment);

    	return view('ecommerce.thank')
        		->with('text','ขอบคุณสำหรับการชำระเงินแบบ PromtPay');
    }

    public function uploadPayment(Request $request)
    {
    	$dateNow = Carbon::now()->format('dmY_His');
    	// $transferDate = Carbon::now()->format('Y-m-d H:i:s');
        $type = null;

        if(isset($request->file_img)){
        	$fileImage = $request->file_img;

	        Image::checkFolderEcomPayment();
	        $destinationPath = 'ecommerce/payment'; // upload path

	        $extension = $fileImage->getClientOriginalExtension(); // getting image extension
	        // $fileName = rand(111111,999999).'.'.$extension; // renameing image
	        $fileName = $dateNow.'.'.$extension; // renameing image
	        $fileImage->move($destinationPath, $fileName); // uploading file to given path

	        $img = Image::create([
	            'img_url' => URL::to('/')."/".$destinationPath."/".$fileName,
	            'img_size' => $request->img_size,
	        ]);
        	$request['payment_img_id'] = $img->id;
        }

        $order = Order::where('order_id',$request->order_code)->first();

        $request['payment_transaction_id'] = OrderPayment::genPaymentId();
        // $request['payment_date_submit'] = $transferDate;
        $request['payment_date_submit'] = Carbon::createFromFormat('m/d/Y H:i', $request->transfer_date.' '.$request->transfer_time_hour.':'.$request->transfer_time_minute) ;
        $request['order_id'] = $order->id;
        $request['payment_type'] = "Transfer Money";

        $orderPayment = OrderPayment::create($request->all());
        if($order){
            $order->update([
                'order_status' => 'Customer Payment Confirmation',
                'payment_information' => 'Bank Transfer'
            ]);
        }
        OrderConfirmPaymentNotification::pushNotificationCustomer($order,$orderPayment);

        return view('ecommerce.thank')
        		->with('text','ขอบคุณสำหรับการชำระเงินแบบโอนเงิน');
    }

    public function pushNotiCustomerConfirmPayment($order)
    {
    	$customer = $order->customer;

    	return 1;
    }
}
