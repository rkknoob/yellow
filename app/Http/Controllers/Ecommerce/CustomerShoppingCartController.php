<?php

namespace YellowProject\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\ShoppingCart\ShoppingCart;
use YellowProject\Ecommerce\ShoppingCart\ShoppingCartLog;
use YellowProject\Ecommerce\ShoppingCart\ShoppingCartItem;
use YellowProject\Ecommerce\Customer\Customer;
use YellowProject\Ecommerce\Product;
use YellowProject\Ecommerce\Log\LogSession;

class CustomerShoppingCartController extends Controller
{
    public function addShoppingCart(Request $request)
    {
    	$customerId = $request->ecommerce_customer_id;
    	$customer = Customer::find($customerId);
    	if(!$customer){
    		return response()->json([
	            'msg_return' => 'ไม่พบข้อมูล Customer',
	            'code_return' => 2,
	        ]);
    	}
    	$shoppingCart = ShoppingCart::where('ecommerce_customer_id',$customerId)->first();
    	if($shoppingCart){
    		$datas = $shoppingCart->toArray();
    		ShoppingCartLog::create($datas);
    		$shoppingCart->delete();
    	}

    	$shoppingCart = ShoppingCart::create($request->all());
    	if(isset($request->items)){
    		foreach ($request->items as $key => $item) {
    			$item['ecommerce_shopping_cart_id'] = $shoppingCart->id;
    			ShoppingCartItem::create($item);
    		}
    	}

        $logSession = LogSession::where('ecommerce_customer_id',$customerId)->where('is_active',1)->first();
        if($logSession){
            $logSession->update([
                'is_add_to_cart' => 1
            ]);
        }

    	return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function getShoppingCart($id)
    {
    	$datas = [];
    	$customerId = $id;
    	$shoppingCart = ShoppingCart::where('ecommerce_customer_id',$id)->first();
    	if(!$shoppingCart){
	    	return response()->json([
	            'msg_return' => 'ไม่พบข้อมูล Shopping Cart',
	            'code_return' => 2,
	        ]);
    	}
    	$datas = $shoppingCart->toArray();
    	$shoppingCartItems = $shoppingCart->items;
    	foreach ($shoppingCartItems as $key => $shoppingCartItem) {
            $product = Product::find($shoppingCartItem->product_id);
            $shoppingCartItem['price_unit'] = $product->price_unit;
    		$datas['items'][] = $shoppingCartItem->toArray();
    	}

    	return response()->json([
            'datas' => $datas,
        ]);
    }

    public function deleteShoppingCartItem($id)
    {
    	$shoppingCartItem = ShoppingCartItem::find($id);
    	if(!$shoppingCartItem){
	    	return response()->json([
	            'msg_return' => 'ไม่พบข้อมูล Shopping Cart Item',
	            'code_return' => 2,
	        ]);
    	}
    	$shoppingCartItem->delete();

    	return response()->json([
            'msg_return' => 'ลบข้อมูลสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function updateShoppingCart(Request $request,$id)
    {
    	$shoppingCart = ShoppingCart::find($id);
    	if(!$shoppingCart){
	    	return response()->json([
	            'msg_return' => 'ไม่พบข้อมูล Shopping Cart',
	            'code_return' => 2,
	        ]);
    	}
    	$shoppingCart->update($request->all());
    	$shoppingCartItems = ShoppingCartItem::where('ecommerce_shopping_cart_id',$shoppingCart->id)->delete();
    	if(isset($request->items)){
    		foreach ($request->items as $key => $item) {
    			$item['ecommerce_shopping_cart_id'] = $shoppingCart->id;
    			ShoppingCartItem::create($item);
    		}
    	}

        $logSession = LogSession::where('ecommerce_customer_id',$shoppingCart->ecommerce_customer_id)->where('is_active',1)->first();
        if($logSession){
            $logSession->update([
                'is_add_to_cart' => 1
            ]);
        }

    	return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }
}
