<?php

namespace YellowProject\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\AdminLineUser;
use YellowProject\User;

class RegisterAdminController extends Controller
{
    public function getPage(Request $request)
    {
    	// dd($request->all());
    	$lineUserId = \Session::get('ecommerce_admin_line_user_id', '');
    	\Session::put('ecommerce_admin_line_user_id', '');
    	if($lineUserId == ''){
    		return view('ecommerce.admin-register.thank')
                ->with('message','ไม่พบข้อมูล LineUser');
    	}
    	$adminLineUser = AdminLineUser::where('line_user_id',$lineUserId)->first();
    	if($adminLineUser){
    		return view('ecommerce.admin-register.thank')
                ->with('message','ท่านได้ทำการลงทะเบียน Admin แล้ว');
    	}
    	return view('ecommerce.admin-register.index')
    		->with('lineUserProfileId',$lineUserId);
    }

    public function storeData(Request $request)
    {
    	$email = $request->email;
    	$isUser = 0;
    	$user = User::where('email',$email)->first();
    	if($user){
    		$isUser = 1;
    	}

    	AdminLineUser::create([
    		'line_user_id' => $request->line_user_id,
    		'email' => $email,
    		'is_user' => $isUser
    	]);

    	return view('ecommerce.admin-register.thank')
            ->with('message','บันทึกข้อมูลสำเร็จ');
    }
}
