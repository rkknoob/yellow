<?php

namespace YellowProject\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\Coupon\Coupon;
use YellowProject\Ecommerce\Customer\CustomerCoupon;
use YellowProject\Ecommerce\Customer\CustomerCouponLog;

class CustomerCouponController extends Controller
{
    public function getCustomerCoupon($customerId)
    {
    	$datas = [];
    	// $customerCoupons = CustomerCoupon::pluck('ecommerce_coupon_id')->toArray();
    	$coupons = Coupon::all();
    	foreach ($coupons as $key => $coupon) {
    		$customerCoupon = CustomerCoupon::where('ecommerce_coupon_id',$coupon->id)->where('ecommerce_customer_id',$customerId)->first();
    		$customerCouponStatus = "";
    		if($customerCoupon){
    			$customerCouponStatus = $customerCoupon->status;
    		}
            $coupon['img_url'] = ($coupon->image)? $coupon->image->img_url : "";
    		$coupon['customer_coupon_status'] = $customerCouponStatus;
    		$datas[$key] = $coupon;
    	}

    	return response()->json([
            'datas' => $datas,
        ]);
    }

    public function reedeemCoupon(Request $request)
    {
    	$couponId = $request->ecommerce_coupon_id;
    	$customerId = $request->ecommerce_customer_id;
    	$customerCoupon = CustomerCoupon::where('ecommerce_coupon_id',$couponId)->where('ecommerce_customer_id',$customerId)->first();
    	if($customerCoupon){
    		return response()->json([
	            'msg_return' => 'ท่านได้ทำการ reedeem หรือ used แล้ว',
	            'code_return' => 2,
	        ]);
    	}

    	CustomerCoupon::create([
    		'ecommerce_coupon_id' => $couponId,
    		'ecommerce_customer_id' => $customerId,
    		'status' => 'reedeem'
    	]);

    	CustomerCouponLog::create([
    		'ecommerce_coupon_id' => $couponId,
    		'ecommerce_customer_id' => $customerId,
    		'action' => 'reedeem'
    	]);


    	return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function usedCoupon(Request $request)
    {
    	$couponId = $request->ecommerce_coupon_id;
    	$customerId = $request->ecommerce_customer_id;
    	$customerCoupon = CustomerCoupon::where('ecommerce_coupon_id',$couponId)->where('ecommerce_customer_id',$customerId)->first();
    	if($customerCoupon && $customerCoupon->status == 'used'){
    		return response()->json([
	            'msg_return' => 'ท่านได้ทำการ used แล้ว',
	            'code_return' => 2,
	        ]);
    	}

    	CustomerCoupon::create([
    		'ecommerce_coupon_id' => $couponId,
    		'ecommerce_customer_id' => $customerId,
    		'status' => 'used'
    	]);

    	CustomerCouponLog::create([
    		'ecommerce_coupon_id' => $couponId,
    		'ecommerce_customer_id' => $customerId,
    		'action' => 'used'
    	]);


    	return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function getCustomerAllCoupon($id)
    {
        $datas = [];
        $customerCoupons = CustomerCoupon::where('ecommerce_customer_id',$id)->pluck('ecommerce_coupon_id')->toArray();
        $coupons = Coupon::whereNotIn('id',$customerCoupons)->get();
        foreach ($coupons as $key => $coupon) {
            $coupon['img_url'] = ($coupon->image)? $coupon->image->img_url : "";
            $datas[$key] = $coupon;
        }

        return response()->json([
            'datas' => $datas,
        ]);
    }
}
