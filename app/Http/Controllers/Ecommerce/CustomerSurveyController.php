<?php

namespace YellowProject\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\Survey\CustomerSurvey;
use YellowProject\Ecommerce\Survey\CustomerSurveyItem;

class CustomerSurveyController extends Controller
{
    public function storeSurvey(Request $request)
    {
    	$customerId = $request->ecommerce_customer_id;
    	$discount = $request->discount;

    	$customerSurvey = CustomerSurvey::create([
    		'ecommerce_customer_id' => $customerId,
    		'is_active' => 0,
    		'discount' => $discount
    	]);

    	return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
            'ecommerce_customer_survey_id' => $customerSurvey->id
        ]);
    }

    public function storeSurveyItem(Request $request)
    {
    	$customerSurveyId = $request->ecommerce_customer_survey_id;
    	$customerSurvey = CustomerSurvey::find($customerSurveyId);
    	if($customerSurvey){
    		CustomerSurveyItem::create([
    			'ecommerce_customer_survey_id' => $customerSurvey->id,
    			'label' => $request->label,
    			'value' => $request->value,
    			'seq' => $request->seq,
    		]);

    		return response()->json([
	            'msg_return' => 'บันทึกสำเร็จ',
	            'code_return' => 1,
	            'ecommerce_customer_survey_id' => $customerSurvey->id
	        ]);
    	}else{
    		return response()->json([
	            'msg_return' => 'ไม่พบข้อมูล Survey',
	            'code_return' => 2,
	        ]);
    	}
    }
}
