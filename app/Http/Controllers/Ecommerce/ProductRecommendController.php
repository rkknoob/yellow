<?php

namespace YellowProject\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\ProductRecommend\ProductRecommend;

class ProductRecommendController extends Controller
{
    public function getProductRecommend()
    {
    	$productRecommend = ProductRecommend::where('is_active',1)->first();
        if($productRecommend){
        	if($productRecommend->product1){
        		$product1 = $productRecommend->product1;
        		if($product1->productImages){
        			$productImages = $product1->productImages;
        			foreach ($productImages as $key => $productImage) {
        				$productImage->image;
        			}
        		}
        	}
        	if($productRecommend->product2){
        		$product2 = $productRecommend->product2;
        		if($product2->productImages){
        			$productImages = $product2->productImages;
        			foreach ($productImages as $key => $productImage) {
        				$productImage->image;
        			}
        		}
        	}
        	if($productRecommend->product3){
        		$product3 = $productRecommend->product3;
        		if($product3->productImages){
        			$productImages = $product3->productImages;
        			foreach ($productImages as $key => $productImage) {
        				$productImage->image;
        			}
        		}
        	}
        	if($productRecommend->product4){
        		$product4 = $productRecommend->product4;
        		if($product4->productImages){
        			$productImages = $product4->productImages;
        			foreach ($productImages as $key => $productImage) {
        				$productImage->image;
        			}
        		}
        	}
        }

    	return response()->json([
            'datas' => $productRecommend,
        ]);
    }
}
