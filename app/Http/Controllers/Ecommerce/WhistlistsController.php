<?php

namespace YellowProject\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\Customer\CustomerWishlist;

class WhistlistsController extends Controller
{
    public function addWhishlist(Request $request)
    {
    	CustomerWishlist::create($request->all());

    	return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function getWhishlist($id)
    {
    	$datas = [];
    	$customerWhishlists = CustomerWishlist::where('ecommerce_customer_id',$id)->get();
    	foreach ($customerWhishlists as $key => $customerWhishlist) {
    		$ecomProduct = $customerWhishlist->product;
    		if($ecomProduct){
                $product = $ecomProduct;
    			$productDiscount = $product->productDiscount;
                $productCategories = $product->productCategories;
                $productImages = $product->productImages;

                $datas[$key]['id'] = $product->id;
                $datas[$key]['name'] = $product->name;
                $datas[$key]['desc'] = $product->desc;
                $datas[$key]['short_desc'] = $product->short_desc;
                $datas[$key]['price'] = $product->price;
                $datas[$key]['price_unit'] = $product->price_unit;
                $datas[$key]['start_date'] = $product->start_date;
                $datas[$key]['end_date'] = $product->end_date;
                $datas[$key]['status'] = $product->status;
                $datas[$key]['product_brand'] = $product->product_brand;
                $datas[$key]['product_name'] = $product->product_name;
                $datas[$key]['product_size'] = $product->product_size;
                $datas[$key]['img_url'] = "";
                if($productCategories->count() > 0){
                    foreach ($productCategories as $catKey => $productCategory) {
                        $category = $productCategory->category;
                        $datas[$key]['categories'][$catKey]['id'] = $productCategory->ecommerce_category_id;
                        $datas[$key]['categories'][$catKey]['name'] = $category->name;
                    }
                }
                if($productDiscount){
                    $datas[$key]['discount']['priority'] = $productDiscount->priority;
                    $datas[$key]['discount']['discount_price'] = $productDiscount->discount_price;
                    $datas[$key]['discount']['start_date'] = $productDiscount->start_date;
                    $datas[$key]['discount']['end_date'] = $productDiscount->end_date;
                }
                if($productImages->count() > 0){
                    foreach ($productImages as $keyProductImage => $productImage) {
                        $imageData = $productImage->image;
                        $datas[$key]['images'][$keyProductImage]['img_url'] = "";
                        $datas[$key]['images'][$keyProductImage]['img_size'] = "";
                        if($imageData){
                            $datas[$key]['img_url'] = $imageData->img_url;
                            $datas[$key]['images'][$keyProductImage]['img_url'] = $imageData->img_url;
                            $datas[$key]['images'][$keyProductImage]['img_size'] = $imageData->img_size;
                        }
                        $datas[$key]['images'][$keyProductImage]['product_img_id'] = $productImage->product_img_id;
                        $datas[$key]['images'][$keyProductImage]['name'] = $productImage->name;
                        $datas[$key]['images'][$keyProductImage]['seq'] = $productImage->seq;
                        $datas[$key]['images'][$keyProductImage]['is_base'] = $productImage->is_base;
                        $datas[$key]['images'][$keyProductImage]['is_small'] = $productImage->is_small;
                        $datas[$key]['images'][$keyProductImage]['is_thumbnail'] = $productImage->is_thumbnail;
                    }
                }
    		}
    	}

    	return response()->json([
            'datas' => $datas,
        ]);
    }

    public function deleteWhishlist(Request $request,$id)
    {
    	CustomerWishlist::where('ecommerce_customer_id',$id)->where('ecommerce_product_id',$request->ecommerce_product_id)->delete();
    	
    	return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }
}
