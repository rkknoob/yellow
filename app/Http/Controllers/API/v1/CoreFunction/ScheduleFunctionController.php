<?php

namespace YellowProject\Http\Controllers\API\v1\CoreFunction;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\JobScheduleFunction;
use YellowProject\ScheduleCheck\ScheduleCheckStatus;

class ScheduleFunctionController extends Controller
{
    public function runSchedule()
    {
    	ScheduleCheckStatus::testPushMessage();
    	// JobScheduleFunction::checkFileDownloadExp();
    }
}
