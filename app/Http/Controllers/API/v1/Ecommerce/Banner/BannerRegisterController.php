<?php

namespace YellowProject\Http\Controllers\API\v1\Ecommerce\Banner;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\Banner\Banner;
use YellowProject\Ecommerce\Banner\BannerImage;
use YellowProject\Ecommerce\Banner\BannerItem;
use Carbon\Carbon;
use URL;

class BannerRegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $datas = [];
        // $banner = Banner::find($id);
        $banner = Banner::where('type','register')->first();
        if($banner){
            $bannerItems = $banner->items;
            $datas['id'] = $banner->id;
            $datas['delay_time'] = $banner->delay_time;
            $datas['items'] = [];
            foreach ($bannerItems as $key => $bannerItem) {
                $datas['items'][$key]['img_url'] = $bannerItem->img_url;
            }
        }

        return response()->json([
            'datas' => $datas,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $banner = Banner::where('type','register')->first();
        $request['type'] = 'register';
        if($banner){
            $banner->update($request->all());
        }else{
            $banner = Banner::create($request->all());
        }
        BannerItem::where('ecommerce_banner_id',$banner->id)->delete();
        foreach ($request->items as $key => $item) {
            BannerItem::create([
                'ecommerce_banner_id' => $banner->id,
                'img_url' => $item['img_url']
            ]);
        }
        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function uploadMultiple(Request $request)
    {
        BannerImage::checkFolderEcomBanner();
        $datas = collect();
        if($request->img_items){
            foreach ($request->img_items as $key => $img_item) {
                $dateNow = Carbon::now()->format('dmY_His');
                $fileImage = $img_item;
                $type = null;
                // ImageFile::checkFolderDefaultPath();
                $destinationPath = 'ecommerce/banner'; // upload path
                $extension = $fileImage->getClientOriginalExtension(); // getting image extension
                $fileName = $dateNow.'-'.$key.'.'.$extension; // renameing image
                $fileImage->move($destinationPath, $fileName); // uploading file to given path

                $imageFile = BannerImage::create([
                    'img_url' => URL::to('/')."/".$destinationPath."/".$fileName,
                    'img_size' => null,
                ]);

                $datas->put($key,$imageFile);
            }
        }

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
            'datas' => $datas,
        ]);
    }
}
