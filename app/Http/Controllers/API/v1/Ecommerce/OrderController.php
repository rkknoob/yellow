<?php

namespace YellowProject\Http\Controllers\API\v1\Ecommerce;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\Order\Order;
use YellowProject\Ecommerce\Order\OrderProduct;
use YellowProject\Ecommerce\Order\OrderPayment;
use YellowProject\Ecommerce\Image\Image as ImageFile;
use YellowProject\Ecommerce\ShoppingCart\ShoppingCart;
use YellowProject\Ecommerce\ShoppingCart\ShoppingCartItem;
use YellowProject\Ecommerce\Product;
use YellowProject\Ecommerce\Customer\Customer;
use YellowProject\DownloadFile\DownloadFileMain;
use YellowProject\Ecommerce\DTManagement\DTManagement;
use Carbon\Carbon;
use URL;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $datas = [];
        $orders = Order::orderByDesc('order_id')->get();
        $count = 0;
        foreach ($orders as $key => $order) {
            $DTManagement = DTManagement::find($order->dt_id);
            $datas[$count]['order_running_id'] = $count;
            $datas[$count]['id'] = $order->id;
            $datas[$count]['order_id'] = $order->order_id;
            $datas[$count]['dt'] = ($DTManagement)? $DTManagement->name : null ;
            $datas[$count]['purchased_on'] = $order->created_at->format('d/m/Y H:i');
            $datas[$count]['customer'] = $order->customer->first_name;
            $datas[$count]['ship_to'] = $order->customer->first_name;
            $datas[$count]['base_price'] = $order->sub_total;
            $datas[$count]['purchased_price'] = $order->total_due;
            $datas[$count]['status'] = $order->order_status;
            $count++;
        }

        return response()->json([
            'datas' => $datas,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    { //หลังกด Edit

        $datas = [];
        $datas['details']['order_detail'] = [];
        $datas['details']['customer'] = [];
        $datas['details']['billing_address'] = [];
        $datas['details']['shipping_address'] = [];
        $datas['details']['shopping_cart'] = [];
        $datas['details']['summary'] = [];

        $datas['payment_details'] = [];

        $datas['order_confirmation'] = [];

        $datas['tracking'] = [];

        $datas['histories'] = [];

        $order = Order::find($id);
        $customer = $order->customer;
        $customerBillingAddress = $customer->CustomerTaxBillingAddress;
        $customerShippingAddresses = $order->customerShippingAddresses;
        $orderProducts = $order->orderProducts;
        $payment = $order->orderPayment;
        $tracking = $order->orderTracking;
        $histories = $order->orderAdminHistories;
        $orderConfirmation = $order->orderConfirmation;
        $orderConfirmationPaymentReadyToShip = $order->orderConfirmationPaymentReadyToShip;
        $orderShippingConfirmation = $order->orderShippingConfirmation;

        if($order){
            $datas['details']['order_detail']['id'] = $order->id;
            $datas['details']['order_detail']['order_id'] = $order->order_id;
            $datas['details']['order_detail']['order_date'] = $order->created_at->format('d/m/Y H:i:s');
            $datas['details']['order_detail']['order_status'] = $order->order_status;
            $datas['details']['order_detail']['grand_total'] = $order->grand_total;
            $datas['details']['order_detail']['payment_information'] = $order->payment_information;
        }

        if($customer){
            $datas['details']['customer']['id'] = $customer->id;
            $datas['details']['customer']['customer_code'] = $customer->customer_code;
            $datas['details']['customer']['name'] = $customer->first_name;
            $datas['details']['customer']['last_name'] = $customer->last_name;
            $datas['details']['customer']['email'] = $customer->lineUserProfile->email;
            $datas['details']['customer']['city'] = $customer->city;
            $datas['details']['customer']['phone_number'] = $customer->phone_number;
            $datas['details']['customer']['reward_point'] = $customer->reward_point;
        }

        if($customerBillingAddress){
            $datas['details']['billing_address']['id'] = $customerBillingAddress->id;
            $datas['details']['billing_address']['name'] = $customerBillingAddress->name;
            $datas['details']['billing_address']['last_name'] = $customerBillingAddress->last_name;
            $datas['details']['billing_address']['company_name'] = $customerBillingAddress->company_name;
            $datas['details']['billing_address']['address'] = $customerBillingAddress->address;
            $datas['details']['billing_address']['district'] = $customerBillingAddress->district;
            $datas['details']['billing_address']['sub_district'] = $customerBillingAddress->sub_district;
            $datas['details']['billing_address']['province'] = $customerBillingAddress->province;
            $datas['details']['billing_address']['post_code'] = $customerBillingAddress->post_code;
            $datas['details']['billing_address']['tel'] = $customerBillingAddress->tel;
            $datas['details']['billing_address']['tax_id'] = $customerBillingAddress->tax_id;
        }

        if($customerShippingAddresses){
            foreach ($customerShippingAddresses as $key => $customerShippingAddress) {
                $datas['details']['shipping_address'][$key]['id'] = $customerShippingAddress->id;
                $datas['details']['shipping_address'][$key]['name'] = $customerShippingAddress->name;
                $datas['details']['shipping_address'][$key]['last_name'] = $customerShippingAddress->last_name;
                $datas['details']['shipping_address'][$key]['company_name'] = $customerShippingAddress->company_name;
                $datas['details']['shipping_address'][$key]['address'] = $customerShippingAddress->address;
                $datas['details']['shipping_address'][$key]['district'] = $customerShippingAddress->district;
                $datas['details']['shipping_address'][$key]['sub_district'] = $customerShippingAddress->sub_district;
                $datas['details']['shipping_address'][$key]['province'] = $customerShippingAddress->province;
                $datas['details']['shipping_address'][$key]['post_code'] = $customerShippingAddress->post_code;
                $datas['details']['shipping_address'][$key]['tel'] = $customerShippingAddress->tel;
                $datas['details']['shipping_address'][$key]['store_name'] = $customerShippingAddress->store_name;
                $datas['details']['shipping_address'][$key]['store_type'] = $customerShippingAddress->store_type;
                $datas['details']['shipping_address'][$key]['latitude'] = $customerShippingAddress->latitude;
                $datas['details']['shipping_address'][$key]['longitude'] = $customerShippingAddress->longitude;
            }
        }

        $summaryDiscountTotalCount = 0;

        if($orderProducts){
            foreach ($orderProducts as $key => $orderProduct) {
                $product = $orderProduct->product;
                if($product){
                    $datas['details']['shopping_cart'][$key]['id'] = $product->id;
                    $datas['details']['shopping_cart'][$key]['shopping_cart_item_id'] = $orderProduct->id;
                    $datas['details']['shopping_cart'][$key]['product_name'] = $product->name;
                    $datas['details']['shopping_cart'][$key]['original_price'] = $orderProduct->original_price;
                    $datas['details']['shopping_cart'][$key]['price'] = $orderProduct->price;
                    $datas['details']['shopping_cart'][$key]['quanlity'] = $orderProduct->quanlity;
                    $datas['details']['shopping_cart'][$key]['tex_amount'] = $orderProduct->tex_amount;
                    $datas['details']['shopping_cart'][$key]['tax_percent'] = $orderProduct->tax_percent;
                    $datas['details']['shopping_cart'][$key]['discount_amount'] = $orderProduct->discount_amount;
                    $datas['details']['shopping_cart'][$key]['total'] = $orderProduct->total;
                    $datas['details']['shopping_cart'][$key]['price_per_unit'] = $product->price;
                    $datas['details']['shopping_cart'][$key]['unit'] = $product->price_unit;
                    $datas['details']['shopping_cart'][$key]['sku'] = $product->sku;
                    $datas['details']['shopping_cart'][$key]['product_size'] = $product->product_size;
                    $datas['details']['shopping_cart'][$key]['total'] = $orderProduct->total;
                    $summaryDiscountTotalCount += $orderProduct->discount_amount;
                }
            }
        }

        if($order){
            $datas['details']['summary']['sub_total'] = $order->sub_total;
            $datas['details']['summary']['tax'] = $order->tax;
            $datas['details']['summary']['shipping_cost'] = $order->shipping_cost;
            $datas['details']['summary']['grand_total'] = $order->grand_total;
            $datas['details']['summary']['coupon_code_discount'] = $order->coupon_code_discount;
            $datas['details']['summary']['coupon_code_discount_amount'] = $order->coupon_code_discount_amount;
            $datas['details']['summary']['total_discount_product'] = $summaryDiscountTotalCount;
            $datas['details']['summary']['total_discount_amount'] = $summaryDiscountTotalCount + $order->coupon_code_discount_amount;
            $datas['details']['summary']['total_paid'] = $order->total_paid;
            $datas['details']['summary']['total_refunded'] = $order->total_refunded;
            $datas['details']['summary']['total_due'] = $order->total_due;
        }

        if($orderConfirmation){
            $datas['order_confirmation']['product_avaliable_status'] = $orderConfirmation->product_avaliable_status;
            $datas['order_confirmation']['product_avaliable_date'] = $orderConfirmation->product_avaliable_date;
            $datas['order_confirmation']['email_to_customer_template_id'] = $orderConfirmation->email_to_customer_template_id;
            $datas['order_confirmation']['email_to_dt_template_id'] = $orderConfirmation->email_to_dt_template_id;
            $datas['order_confirmation']['email_to_admin_template_id'] = $orderConfirmation->email_to_admin_template_id;
            $datas['order_confirmation']['line_to_customer_template_id'] = $orderConfirmation->line_to_customer_template_id;
            $datas['order_confirmation']['line_to_dt_template_id'] = $orderConfirmation->line_to_dt_template_id;
            $datas['order_confirmation']['line_to_admin_template_id'] = $orderConfirmation->line_to_admin_template_id;
        }

        $datas['order_payment_confirmation_ready_to_ship']['dt_shipping_date'] = ($orderConfirmationPaymentReadyToShip)? $orderConfirmationPaymentReadyToShip->dt_shipping_date : $order->operator_shipping_date;
        $datas['order_payment_confirmation_ready_to_ship']['dt_shipping_date_time'] = ($orderConfirmationPaymentReadyToShip)? $orderConfirmationPaymentReadyToShip->dt_shipping_date_time : $order->operator_shipping_time;
        if($orderConfirmationPaymentReadyToShip){
            $datas['order_payment_confirmation_ready_to_ship']['shipping_status'] = $orderConfirmationPaymentReadyToShip->shipping_status;
            $datas['order_payment_confirmation_ready_to_ship']['comment'] = $orderConfirmationPaymentReadyToShip->comment;
            $datas['order_payment_confirmation_ready_to_ship']['email_to_customer_template_id'] = $orderConfirmationPaymentReadyToShip->email_to_customer_template_id;
            $datas['order_payment_confirmation_ready_to_ship']['email_to_dt_template_id'] = $orderConfirmationPaymentReadyToShip->email_to_dt_template_id;
            $datas['order_payment_confirmation_ready_to_ship']['email_to_admin_template_id'] = $orderConfirmationPaymentReadyToShip->email_to_admin_template_id;
            $datas['order_payment_confirmation_ready_to_ship']['line_to_customer_template_id'] = $orderConfirmationPaymentReadyToShip->line_to_customer_template_id;
            $datas['order_payment_confirmation_ready_to_ship']['line_to_dt_template_id'] = $orderConfirmationPaymentReadyToShip->line_to_dt_template_id;
            $datas['order_payment_confirmation_ready_to_ship']['line_to_admin_template_id'] = $orderConfirmationPaymentReadyToShip->line_to_admin_template_id;
        }

        $datas['order_shipping_confirm']['dt_shipping_date'] = ($orderShippingConfirmation)? $orderShippingConfirmation->dt_shipping_date : $order->operator_shipping_date;
        $datas['order_shipping_confirm']['dt_shipping_date_time'] = ($orderShippingConfirmation)? $orderShippingConfirmation->dt_shipping_date_time : $order->operator_shipping_time;
        if($orderShippingConfirmation){
            $datas['order_shipping_confirm']['shipping_status'] = $orderShippingConfirmation->shipping_status;
            $datas['order_shipping_confirm']['comment'] = $orderShippingConfirmation->comment;
            $datas['order_shipping_confirm']['email_to_customer_template_id'] = $orderShippingConfirmation->email_to_customer_template_id;
            $datas['order_shipping_confirm']['email_to_dt_template_id'] = $orderShippingConfirmation->email_to_dt_template_id;
            $datas['order_shipping_confirm']['email_to_admin_template_id'] = $orderShippingConfirmation->email_to_admin_template_id;
            $datas['order_shipping_confirm']['line_to_customer_template_id'] = $orderShippingConfirmation->line_to_customer_template_id;
            $datas['order_shipping_confirm']['line_to_dt_template_id'] = $orderShippingConfirmation->line_to_dt_template_id;
            $datas['order_shipping_confirm']['line_to_admin_template_id'] = $orderShippingConfirmation->line_to_admin_template_id;
        }

        if($payment){
            $image = $payment->image;
            $datas['payment_details']['payment_date_submit'] = $payment->payment_date_submit;
            $datas['payment_details']['payment_transaction_id'] = $payment->payment_transaction_id;
            $datas['payment_details']['payment_amount'] = $payment->payment_amount;
            $datas['payment_details']['payment_date_refund'] = $payment->payment_date_refund;
            $datas['payment_details']['payment_refund_transaction_id'] = $payment->payment_refund_transaction_id;
            $datas['payment_details']['payment_refund_status'] = $payment->payment_refund_status;
            $datas['payment_details']['payment_refund_amount'] = $payment->payment_refund_amount;
            $datas['payment_details']['payment_type'] = $payment->payment_type;
            $datas['payment_details']['img_url'] = ($image)? $image->img_url : "";
        }

        if($tracking){
            $datas['tracking']['tracking_status'] = $tracking->tracking_status;
            $datas['tracking']['tracking'] = $tracking->tracking;
            $datas['tracking']['check_tracking'] = $tracking->check_tracking;
            $datas['tracking']['email_to_customer_template_id'] = $tracking->email_to_customer_template_id;
            $datas['tracking']['email_to_customer_status'] = $tracking->email_to_customer_status;
            $datas['tracking']['email_to_dt_template_id'] = $tracking->email_to_dt_template_id;
            $datas['tracking']['email_to_dt_status'] = $tracking->email_to_dt_status;
            $datas['tracking']['email_to_admin_template_id'] = $tracking->email_to_admin_template_id;
            $datas['tracking']['email_to_admin_status'] = $tracking->email_to_admin_status;
            $datas['tracking']['line_to_customer_template_id'] = $tracking->line_to_customer_template_id;
            $datas['tracking']['line_to_customer_status'] = $tracking->line_to_customer_status;
            $datas['tracking']['line_to_dt_template_id'] = $tracking->line_to_dt_template_id;
            $datas['tracking']['line_to_dt_status'] = $tracking->line_to_dt_status;
            $datas['tracking']['line_to_admin_template_id'] = $tracking->line_to_admin_template_id;
            $datas['tracking']['line_to_admin_status'] = $tracking->line_to_admin_status;
        }

        if($histories){
            foreach ($histories as $key => $history) {
                $datas['histories'][$key]['date_time'] = $history->created_at->format('d/m/Y H:i:s');
                $datas['histories'][$key]['detail'] = $history->detail;
                $datas['histories'][$key]['notification'] = "Notified";
            }
        }

        return response()->json([
            'datas' => $datas,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $order = Order::find($id);
        if($order){
            $order->delete();
        }

        return response()->json([
            'msg_return' => 'ลบข้อมูลสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function uploadMultiple(Request $request)
    {

        // dd($request->all());
        ImageFile::checkFolderEcomPayment();
        $datas = collect();
        if($request->img_items){
            foreach ($request->img_items as $key => $img_item) {
                $dateNow = Carbon::now()->format('dmY_His');
                $fileImage = $img_item;
                $type = null;
                // ImageFile::checkFolderDefaultPath();
                $destinationPath = 'ecommerce/payment'; // upload path
                $extension = $fileImage->getClientOriginalExtension(); // getting image extension
                $fileName = $dateNow.'-'.$key.'.'.$extension; // renameing image
                $fileImage->move($destinationPath, $fileName); // uploading file to given path

                $imageFile = ImageFile::create([
                    'img_url' => URL::to('/')."/".$destinationPath."/".$fileName,
                    // 'img_size' => $img_item->img_size,
                    'img_size' => null,
                ]);

                $datas->put($key,$imageFile);
            }
        }

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
            'datas' => $datas,
        ]);
    }

    public function updatePaymentData(Request $request)
    {

        $orderId = $request->order_id;
        $orderPayment = OrderPayment::where('order_id',$orderId)->first();
        if($orderPayment){
            $datas = $orderPayment->update($request->all());
        }else{
            $datas = OrderPayment::create($request->all());
        }

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
            'datas' => $datas,
        ]);
    }

    public function paymentLineRefund($id)
    {

        $orderId = $id;
        $order = Order::find($orderId);
        if($order){
            $order->update([
                'order_status' => 'LINE Pay Refund'
            ]);
        }else{
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูล order',
                'code_return' => 3,
            ]);
        }
        $orderPayment = OrderPayment::where('order_id',$orderId)->first();
        if($orderPayment){
            $datas = $orderPayment->update([
                'payment_date_refund' => Carbon::now()->format('Y-m-d H:i:s'),
                'payment_refund_transaction_id' => 're0000001',
                'payment_refund_status' => 'success',
                'payment_refund_amount' => 500
            ]);
        }else{
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูล payment',
                'code_return' => 4,
            ]);
        }

        // return response()->json([
        //     'msg_return' => 'ไม่สามารถ refund ได้',
        //     'code_return' => 2,
        // ]);

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function trackingSendEmail(Request $request)
    {

        $type = $request->type;
        $orderId = $request->order_id;
        $datas = collect([
            'order_id' => $orderId,
            'email_type' => $type,
            'status' => 'success',
        ]);

        return response()->json([
            'datas' => $datas,
        ]);
    }

    public function trackingSendLine(Request $request)
    {

        $type = $request->type;
        $orderId = $request->order_id;
        $datas = collect([
            'order_id' => $orderId,
            'email_type' => $type,
            'status' => 'success',
        ]);

        return response()->json([
            'datas' => $datas,
        ]);
    }

    public function checkOrderPayment($orderId)
    {

        $isPaid = 0;
        $orderPayment = OrderPayment::where('order_id',$orderId)->first();
        if($orderPayment){
            $isPaid = 1;
        }
        return $isPaid;
    }

    public function deleteOrderItem($id)
    {

        $dateNow = Carbon::now()->format('Y-m-d H:i:s');
        $orderProduct = OrderProduct::find($id);
        $summary = 0;
        $subTotal = 0;
        $allQty = 0;
        $couponDiscountPrice = 0;
        $allRewardPoint = 0;
        $totalProductVat = 0;
        $orderReward = 0;
        if(!$orderProduct){
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูล Shopping Cart Item',
                'code_return' => 2,
            ]);
        }
        $order = Order::find($orderProduct->order_id);
        $oldAllRewardPoint = $order->total_reward_point;
        $orderProduct->delete();

        $orderProducts = OrderProduct::where('order_id',$order->id)->get();
        foreach ($orderProducts as $key => $orderProduct) {
            $qty = $orderProduct->quanlity;
            $product = Product::find($orderProduct->product_id);
            $productPrice = $product->price*$qty;
            $productRewardPoint = $product->reward_point*$qty;
            $productVat = $product->vat;
            $productDiscount = $product->productDiscount;
            $productDiscountPrice = 0;
            $productPriceAfterVat = 0;
            if($productDiscount){
                if($productDiscount->start_date <= $dateNow && $productDiscount->end_date >= $dateNow){
                    $productPrice = $productDiscount->discount_price*$qty;
                }
            }
            if($productVat > 0){
                $productPriceAfterVat = $productPrice + (($productPrice*$productVat)/100);
            }else{
                $productPriceAfterVat = $productPrice;
            }
            $orderProduct->update([
                'order_id' => $order->id,
                'product_id' => $product->id,
                'original_price' => $product->price,
                'price' => $productPrice,
                'quanlity' => $qty,
                'tax_amount' => $qty,
                'tax_percent' => $productVat,
                'discount_amount' => $productDiscountPrice,
                'total' => $productPriceAfterVat
            ]);
            $summary += $productPriceAfterVat;
            $allQty += $qty;
            $allRewardPoint += $productRewardPoint;
        }

        $subTotal = $summary;
        $summary = $summary - $order->coupon_code_discount_amount;
        $orderReward = $summary/100;
        $orderReward = floor($orderReward);
        $allRewardPoint += $orderReward;

        $order->update([
            'sub_total' => $subTotal,
            'grand_total' => $summary,
            'total_paid' => $summary,
            'total_due' => $summary,
            'order_reward_point' => $orderReward,
            'total_reward_point' => $allRewardPoint,
        ]);

        $customer = Customer::find($order->ecommerce_customer_id);
        $customerRewardPoint = $customer->reward_point;
        $customerNewRewardPoint = ($customerRewardPoint - $oldAllRewardPoint) + $allRewardPoint;
        $customer->update([
            'reward_point' => $customerNewRewardPoint
        ]);

        return response()->json([
            'msg_return' => 'ลบข้อมูลสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function updateOrderItem(Request $request,$id)
    {

        $dateNow = Carbon::now()->format('Y-m-d H:i:s');
        $orderProduct = OrderProduct::find($id);
        $summary = 0;
        $subTotal = 0;
        $allQty = 0;
        $couponDiscountPrice = 0;
        $allRewardPoint = 0;
        $totalProductVat = 0;
        $orderReward = 0;
        if(!$orderProduct){
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูล Shopping Cart Item',
                'code_return' => 2,
            ]);
        }

        $orderProduct->update([
            'quanlity' => $request->quanlity
        ]);

        $order = Order::find($orderProduct->order_id);
        $oldAllRewardPoint = $order->total_reward_point;
        $orderProducts = OrderProduct::where('order_id',$order->id)->get();
        foreach ($orderProducts as $key => $orderProduct) {
            $qty = $orderProduct->quanlity;
            $product = Product::find($orderProduct->product_id);
            $productPrice = $product->price*$qty;
            $productRewardPoint = $product->reward_point*$qty;
            $productVat = $product->vat;
            $productDiscount = $product->productDiscount;
            $productDiscountPrice = 0;
            $productPriceAfterVat = 0;
            if($productDiscount){
                if($productDiscount->start_date <= $dateNow && $productDiscount->end_date >= $dateNow){
                    $productPrice = $productDiscount->discount_price*$qty;
                }
            }
            if($productVat > 0){
                $productPriceAfterVat = $productPrice + (($productPrice*$productVat)/100);
            }else{
                $productPriceAfterVat = $productPrice;
            }
            $orderProduct->update([
                'order_id' => $order->id,
                'product_id' => $product->id,
                'original_price' => $product->price,
                'price' => $productPrice,
                'quanlity' => $qty,
                'tax_amount' => $qty,
                'tax_percent' => $productVat,
                'discount_amount' => $productDiscountPrice,
                'total' => $productPriceAfterVat
            ]);
            $summary += $productPriceAfterVat;
            $allQty += $qty;
            $allRewardPoint += $productRewardPoint;
        }

        $subTotal = $summary;
        $summary = $summary - $order->coupon_code_discount_amount;
        $orderReward = $summary/100;
        $orderReward = floor($orderReward);
        $allRewardPoint += $orderReward;

        $order->update([
            'sub_total' => $subTotal,
            'grand_total' => $summary,
            'total_paid' => $summary,
            'total_due' => $summary,
            'order_reward_point' => $orderReward,
            'total_reward_point' => $allRewardPoint,
        ]);

        $customer = Customer::find($order->ecommerce_customer_id);
        $customerRewardPoint = $customer->reward_point;
        $customerNewRewardPoint = ($customerRewardPoint - $oldAllRewardPoint) + $allRewardPoint;
        $customer->update([
            'reward_point' => $customerNewRewardPoint
        ]);

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function exportAllOrder(Request $request)
    {

        $serialized = serialize($request->all());
        
        DownloadFileMain::create([
          'main_id' => 0,
          'type' => 'order_all',
          'requests' => $serialized,
          'is_active' => 1
        ]);
        
        return response()->json([
          'return_code' => 1,
          'msg' => 'Success"',
        ]);
    }
}
