<?php

namespace YellowProject\Http\Controllers\API\v1\Ecommerce;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\Order\Order;
use YellowProject\Ecommerce\Order\OrderPayment;
use YellowProject\Ecommerce\Order\OrderConfirmation;
use YellowProject\Ecommerce\Order\OrderShippingConfirmation;
use YellowProject\Ecommerce\Order\OrderConfirmationPaymentReadyToShip;
use YellowProject\Ecommerce\Order\OrderCustomizeLog;
use YellowProject\Ecommerce\Order\OrderProduct;
use YellowProject\Ecommerce\Image\Image as ImageFile;
use YellowProject\Ecommerce\DTManagement\DTManagementRegisterData;
use YellowProject\Ecommerce\DTManagement\DTManagement;
use YellowProject\Ecommerce\Order\OrderDTPayment;
use YellowProject\GeneralFunction\CoreFunction;
use YellowProject\Ecommerce\CoreLineFunction\OrderConfirmNotification;
use YellowProject\Ecommerce\CoreLineFunction\OrderConfirmShippingNotification;
use YellowProject\Ecommerce\CoreLineFunction\OrderConfirmPaymentReadyToShipNotification;
use YellowProject\Ecommerce\Customer\Customer;
use YellowProject\Ecommerce\Product;
use YellowProject\Ecommerce\Log\DTConfirmationSendMessage;
use YellowProject\Ecommerce\Log\LogSession;
use YellowProject\DownloadFile\DownloadFileMain;
use Carbon\Carbon;
use URL;

class DTManagementOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $dtId = $request->dt_id;
        $datas = [];
        $orders = Order::where('dt_id',$dtId)->orderByDesc('order_id')->get();
        $count = 0;
        foreach ($orders as $key => $order) {
            $datas[$count]['order_running_id'] = $count;
            $datas[$count]['id'] = $order->id;
            $datas[$count]['order_id'] = $order->order_id;
            $datas[$count]['purchased_on'] = $order->created_at->format('d/m/Y H:i');
            $datas[$count]['customer'] = $order->customer->first_name;
            $datas[$count]['ship_to'] = $order->customer->first_name;
            $datas[$count]['base_price'] = $order->grand_total;
            $datas[$count]['purchased_price'] = $order->total_due;
            $datas[$count]['status'] = $order->order_status;
            $count++;
        }

        return response()->json([
            'datas' => $datas,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $datas = [];
        $datas['details']['order_detail'] = [];
        $datas['details']['customer'] = [];
        $datas['details']['billing_address'] = [];
        $datas['details']['shipping_address'] = [];
        $datas['details']['shopping_cart'] = [];
        $datas['details']['summary'] = [];

        $datas['payment_details'] = [];

        $datas['tracking'] = [];

        $datas['histories'] = [];

        $order = Order::find($id);
        $customer = $order->customer;
        $customerBillingAddress = $customer->CustomerTaxBillingAddress;
        $customerShippingAddresses = $order->customerShippingAddresses;
        $orderProducts = $order->orderProducts;
        $payment = $order->orderPayment;
        $tracking = $order->orderTracking;
        $histories = $order->orderAdminHistories;

        if($order){
            $datas['details']['order_detail']['id'] = $order->id;
            $datas['details']['order_detail']['order_id'] = $order->order_id;
            $datas['details']['order_detail']['order_date'] = $order->created_at->format('d/m/Y H:i:s');
            $datas['details']['order_detail']['order_status'] = $order->order_status;
            $datas['details']['order_detail']['grand_total'] = $order->grand_total;
            $datas['details']['order_detail']['payment_information'] = $order->payment_information;
        }

        if($customer){
            $datas['details']['customer']['id'] = $customer->id;
            $datas['details']['customer']['name'] = $customer->first_name." ".$customer->last_name;
            $datas['details']['customer']['email'] = $customer->email;
            $datas['details']['customer']['city'] = $customer->city;
            $datas['details']['customer']['phone_number'] = $customer->phone_number;
        }

        if($customerBillingAddress){
            $datas['details']['billing_address']['id'] = $customerBillingAddress->id;
            $datas['details']['billing_address']['name'] = $customerBillingAddress->name;
            $datas['details']['billing_address']['company_name'] = $customerBillingAddress->company_name;
            $datas['details']['billing_address']['address'] = $customerBillingAddress->address;
            $datas['details']['billing_address']['district'] = $customerBillingAddress->district;
            $datas['details']['billing_address']['sub_district'] = $customerBillingAddress->sub_district;
            $datas['details']['billing_address']['province'] = $customerBillingAddress->province;
            $datas['details']['billing_address']['post_code'] = $customerBillingAddress->post_code;
            $datas['details']['billing_address']['tel'] = $customerBillingAddress->tel;
            $datas['details']['billing_address']['tax_id'] = $customerBillingAddress->tax_id;
        }

        if($customerShippingAddresses){
            foreach ($customerShippingAddresses as $key => $customerShippingAddress) {
                $datas['details']['shipping_address'][$key]['id'] = $customerShippingAddress->id;
                $datas['details']['shipping_address'][$key]['name'] = $customerShippingAddress->name;
                $datas['details']['shipping_address'][$key]['company_name'] = $customerShippingAddress->company_name;
                $datas['details']['shipping_address'][$key]['address'] = $customerShippingAddress->address;
                $datas['details']['shipping_address'][$key]['district'] = $customerShippingAddress->district;
                $datas['details']['shipping_address'][$key]['sub_district'] = $customerShippingAddress->sub_district;
                $datas['details']['shipping_address'][$key]['province'] = $customerShippingAddress->province;
                $datas['details']['shipping_address'][$key]['post_code'] = $customerShippingAddress->post_code;
                $datas['details']['shipping_address'][$key]['tel'] = $customerShippingAddress->tel;
            }
        }

        if($orderProducts){
            foreach ($orderProducts as $key => $orderProduct) {
                $product = $orderProduct->product;
                if($product){
                    $datas['details']['shopping_cart'][$key]['id'] = $product->id;
                    $datas['details']['shopping_cart'][$key]['product_name'] = $product->name;
                    $datas['details']['shopping_cart'][$key]['original_price'] = $product->original_price;
                    $datas['details']['shopping_cart'][$key]['price'] = $product->price;
                    $datas['details']['shopping_cart'][$key]['quanlity'] = $product->quanlity;
                    $datas['details']['shopping_cart'][$key]['tex_amount'] = $product->tex_amount;
                    $datas['details']['shopping_cart'][$key]['tax_percent'] = $product->tax_percent;
                    $datas['details']['shopping_cart'][$key]['discount_amount'] = $product->discount_amount;
                    $datas['details']['shopping_cart'][$key]['total'] = $product->total;
                }
            }
        }

        if($order){
            $datas['details']['summary']['sub_total'] = $order->sub_total;
            $datas['details']['summary']['tax'] = $order->tax;
            $datas['details']['summary']['shipping_cost'] = $order->shipping_cost;
            $datas['details']['summary']['grand_total'] = $order->grand_total;
            $datas['details']['summary']['coupon_code_discount'] = $order->coupon_code_discount;
            $datas['details']['summary']['coupon_code_discount_amount'] = $order->coupon_code_discount_amount;
            $datas['details']['summary']['total_paid'] = $order->total_paid;
            $datas['details']['summary']['total_refunded'] = $order->total_refunded;
            $datas['details']['summary']['total_due'] = $order->total_due;
        }

        if($payment){
            $image = $payment->image;
            $datas['payment_details']['payment_date_submit'] = $payment->payment_date_submit;
            $datas['payment_details']['payment_transaction_id'] = $payment->payment_transaction_id;
            $datas['payment_details']['payment_amount'] = $payment->payment_amount;
            $datas['payment_details']['payment_date_refund'] = $payment->payment_date_refund;
            $datas['payment_details']['payment_refund_transaction_id'] = $payment->payment_refund_transaction_id;
            $datas['payment_details']['payment_refund_status'] = $payment->payment_refund_status;
            $datas['payment_details']['payment_refund_amount'] = $payment->payment_refund_amount;
            $datas['payment_details']['payment_type'] = $payment->payment_type;
            $datas['payment_details']['img_url'] = ($image)? $image->img_url : "";
        }

        if($tracking){
            $datas['tracking']['tracking_status'] = $tracking->tracking_status;
            $datas['tracking']['tracking'] = $tracking->tracking;
            $datas['tracking']['check_tracking'] = $tracking->check_tracking;
            $datas['tracking']['email_to_customer_template_id'] = $tracking->email_to_customer_template_id;
            $datas['tracking']['email_to_customer_status'] = $tracking->email_to_customer_status;
            $datas['tracking']['email_to_dt_template_id'] = $tracking->email_to_dt_template_id;
            $datas['tracking']['email_to_dt_status'] = $tracking->email_to_dt_status;
            $datas['tracking']['email_to_admin_template_id'] = $tracking->email_to_admin_template_id;
            $datas['tracking']['email_to_admin_status'] = $tracking->email_to_admin_status;
            $datas['tracking']['line_to_customer_template_id'] = $tracking->line_to_customer_template_id;
            $datas['tracking']['line_to_customer_status'] = $tracking->line_to_customer_status;
            $datas['tracking']['line_to_dt_template_id'] = $tracking->line_to_dt_template_id;
            $datas['tracking']['line_to_dt_status'] = $tracking->line_to_dt_status;
            $datas['tracking']['line_to_admin_template_id'] = $tracking->line_to_admin_template_id;
            $datas['tracking']['line_to_admin_status'] = $tracking->line_to_admin_status;
        }

        if($histories){
            foreach ($histories as $key => $history) {
                $datas['histories'][$key]['date_time'] = $history->created_at->format('d/m/Y H:i:s');
                $datas['histories'][$key]['detail'] = $history->detail;
                $datas['histories'][$key]['notification'] = "Notified";
            }
        }

        return response()->json([
            'datas' => $datas,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function uploadMultiple(Request $request)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        // dd($request->all());
        ImageFile::checkFolderEcomPayment();
        $datas = collect();
        if($request->img_items){
            foreach ($request->img_items as $key => $img_item) {
                $dateNow = Carbon::now()->format('dmY_His');
                $fileImage = $img_item;
                $type = null;
                // ImageFile::checkFolderDefaultPath();
                $destinationPath = 'ecommerce/payment'; // upload path
                $extension = $fileImage->getClientOriginalExtension(); // getting image extension
                $fileName = $dateNow.'-'.$key.'.'.$extension; // renameing image
                $fileImage->move($destinationPath, $fileName); // uploading file to given path

                $imageFile = ImageFile::create([
                    'img_url' => URL::to('/')."/".$destinationPath."/".$fileName,
                    // 'img_size' => $img_item->img_size,
                    'img_size' => null,
                ]);

                $datas->put($key,$imageFile);
            }
        }

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
            'datas' => $datas,
        ]);
    }

    public function updatePaymentData(Request $request)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $orderId = $request->order_id;
        $orderPayment = OrderPayment::where('order_id',$orderId)->first();
        if($orderPayment){
            $datas = $orderPayment->update($request->all());
        }else{
            $datas = OrderPayment::create($request->all());
        }

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
            'datas' => $datas,
        ]);
    }

    public function paymentLineRefund($id)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $orderId = $id;
        $order = Order::find($orderId);
        if($order){
            $order->update([
                'order_status' => 'LINE Pay Refund'
            ]);
        }else{
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูล order',
                'code_return' => 3,
            ]);
        }
        $orderPayment = OrderPayment::where('order_id',$orderId)->first();
        if($orderPayment){
            $datas = $orderPayment->update([
                'payment_date_refund' => Carbon::now()->format('Y-m-d H:i:s'),
                'payment_refund_transaction_id' => 're0000001',
                'payment_refund_status' => 'success',
                'payment_refund_amount' => 500
            ]);
        }else{
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูล payment',
                'code_return' => 4,
            ]);
        }

        // return response()->json([
        //     'msg_return' => 'ไม่สามารถ refund ได้',
        //     'code_return' => 2,
        // ]);

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function updateOrderConfirmation(Request $request)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $orderId = $request->order_id;
        $order = Order::find($orderId);
        $order->update([
            'order_status' => 'DT Confirmation Order'
        ]);
        $orderConfirmation = OrderConfirmation::where('order_id',$orderId)->first();
        if($orderConfirmation){
            $orderConfirmation->update($request->all());
        }else{
            OrderConfirmation::create($request->all());
        }

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function updateOrderPaymentConfirmationReadyToShip(Request $request)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $orderId = $request->order_id;
        $order = Order::find($orderId);
        $order->update([
            'order_status' => 'DT Confirmation Payment Ready To Ship'
        ]);
        $orderConfirmationPaymentReadyToShip = OrderConfirmationPaymentReadyToShip::where('order_id',$orderId)->first();
        if($orderConfirmationPaymentReadyToShip){
            $orderConfirmationPaymentReadyToShip->update($request->all());
        }else{
            OrderConfirmationPaymentReadyToShip::create($request->all());
        }

        $logSession = LogSession::where('ecommerce_customer_id',$order->ecommerce_customer_id)->where('order_id',$orderId)->first();
        if($logSession){
            $logSession->update([
                'is_transaction' => 1,
            ]);
        }

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function updateOrderShippingConfirmation(Request $request)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $summary = 0;
        $subTotal = 0;
        $allQty = 0;
        $couponDiscountPrice = 0;
        $allRewardPoint = 0;
        $totalProductVat = 0;
        $orderReward = 0;

        $orderId = $request->order_id;
        $order = Order::find($orderId);
        $dateNow = $order->created_at->format('Y-m-d H:i:s');
        $order->update([
            'order_status' => 'DT Confirmation Shipping'
        ]);
        $orderShippingConfirmation = OrderShippingConfirmation::where('order_id',$orderId)->first();
        if($orderShippingConfirmation){
            $orderShippingConfirmation->update($request->all());
        }else{
            OrderShippingConfirmation::create($request->all());
        }

        $orderProducts = OrderProduct::where('order_id',$order->id)->get();
        foreach ($orderProducts as $key => $orderProduct) {
            $qty = $orderProduct->quanlity;
            $product = Product::find($orderProduct->product_id);
            $productPrice = $product->price*$qty;
            $productRewardPoint = $product->reward_point*$qty;
            $productVat = $product->vat;
            $productDiscount = $product->productDiscount;
            $productDiscountPrice = 0;
            $productPriceAfterVat = 0;
            if($productDiscount){
                if($productDiscount->start_date <= $dateNow && $productDiscount->end_date >= $dateNow){
                    $productPrice = $productDiscount->discount_price*$qty;
                }
            }
            if($productVat > 0){
                $productPriceAfterVat = $productPrice + (($productPrice*$productVat)/100);
            }else{
                $productPriceAfterVat = $productPrice;
            }
            $summary += $productPriceAfterVat;
            $allQty += $qty;
            $allRewardPoint += $productRewardPoint;
        }

        $subTotal = $summary;
        $summary = $summary - $order->coupon_code_discount_amount;
        $orderReward = $summary/100;
        $orderReward = floor($orderReward);
        $allRewardPoint += $orderReward;

        $order->update([
            'order_reward_point' => $orderReward,
            'total_reward_point' => $allRewardPoint,
        ]);

        $customer = Customer::find($order->ecommerce_customer_id);
        $customerRewardPoint = $customer->reward_point;
        $customerNewRewardPoint = $customerRewardPoint + $allRewardPoint;
        $customer->update([
            'reward_point' => $customerNewRewardPoint
        ]);

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function confimationSendLineCustomer(Request $request)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $userId = $request->user_id;
        $orderId = $request->order_id;
        $order = Order::find($orderId);
        if(!$order){
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูลออเดอร์',
                'code_return' => 2,
            ]);
        }
        $customerId = $order->ecommerce_customer_id;
        $customer = Customer::find($customerId);
        if(!$customer){
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูล Customer',
                'code_return' => 3,
            ]);
        }
        $DTManagementRegisterData = DTManagementRegisterData::where('user_id',$userId)->first();
        if(!$DTManagementRegisterData){
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูล DT',
                'code_return' => 4,
            ]);
        }
        $DTManagement = DTManagement::where('dt_code_login',$DTManagementRegisterData->dt_code)->first();
        if(!$DTManagement){
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูล DT',
                'code_return' => 5,
            ]);
        }
        $orderConfirmation = OrderConfirmation::where('order_id',$orderId)->first();
        if(!$orderConfirmation){
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูล OrderConfirmation',
                'code_return' => 6,
            ]);
        }
        $orderShippingConfirmation = OrderShippingConfirmation::where('order_id',$orderId)->first();
        // if(!$orderShippingConfirmation){
        //     return response()->json([
        //         'msg_return' => 'ไม่พบข้อมูล OrderShippingConfirmation',
        //         'code_return' => 7,
        //     ]);
        // }
        $baseUrl = \URL::to('/');
        $paymentCode = CoreFunction::genCode();
        $paymentURL = $baseUrl."/ecommerce-payment-receive/".$paymentCode;

        OrderDTPayment::create([
            'order_id' => $orderId,
            'dt_id' => $DTManagement->id,
            'ecommerce_customer_id' => $customerId,
            'payment_code' => $paymentCode,
            'payment_url' => $paymentURL
        ]);

        $DTConfirmationSendMessage = DTConfirmationSendMessage::where('order_id',$orderId)->where('ecommerce_customer_id',$customer->id)->where('dt_id',$DTManagement->id)->where('type','dt_confirm')->where(\DB::raw('TIMESTAMPDIFF(MINUTE,created_at,NOW())'),'<=',5)->first();
        $dateNow = Carbon::now('Asia/Bangkok')->format('Y-m-d H:i:s');
        $DTConfirmationSendMessageCheck = DTConfirmationSendMessage::select(
            \DB::raw('TIMESTAMPDIFF(MINUTE,created_at,NOW())'),
            'order_id',
            'dt_id'
        )
            ->where('order_id',$orderId)->where('ecommerce_customer_id',$customer->id)
            ->where('dt_id',$DTManagement->id)->where('type','dt_confirm')
            ->where(\DB::raw('TIMESTAMPDIFF(MINUTE,created_at,NOW())'),'<=',5)
            ->first();
        \Log::debug($DTConfirmationSendMessageCheck);
        if($DTConfirmationSendMessage){
            return response()->json([
                'msg_return' => 'ระบบพึ่งได้ทำการส่งข้อความไป กรุณารอ 5 นาที',
                'code_return' => 1,
            ]);
        }

        OrderConfirmNotification::pushNotificationCustomer($order,$customer,$paymentURL,$orderConfirmation);

        DTConfirmationSendMessage::create([
            'order_id' => $orderId,
            'ecommerce_customer_id' => $customer->id,
            'dt_id' => $DTManagement->id,
            'type' => 'dt_confirm'
        ]);

        return response()->json([
            'msg_return' => 'ส่งข้อมูลสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function readyToShipSendLineCustomer(Request $request)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $userId = $request->user_id;
        $orderId = $request->order_id;
        $order = Order::find($orderId);
        if(!$order){
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูลออเดอร์',
                'code_return' => 2,
            ]);
        }
        $customerId = $order->ecommerce_customer_id;
        $customer = Customer::find($customerId);
        if(!$customer){
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูล Customer',
                'code_return' => 3,
            ]);
        }
        $DTManagementRegisterData = DTManagementRegisterData::where('user_id',$userId)->first();
        if(!$DTManagementRegisterData){
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูล DT',
                'code_return' => 4,
            ]);
        }
        $DTManagement = DTManagement::where('dt_code_login',$DTManagementRegisterData->dt_code)->first();
        if(!$DTManagement){
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูล DT',
                'code_return' => 5,
            ]);
        }
        $orderConfirmation = OrderConfirmation::where('order_id',$orderId)->first();
        if(!$orderConfirmation){
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูล OrderConfirmation',
                'code_return' => 6,
            ]);
        }
        $orderConfirmationPaymentReadyToShip = OrderConfirmationPaymentReadyToShip::where('order_id',$orderId)->first();
        if(!$orderConfirmationPaymentReadyToShip){
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูล OrderConfirmationPaymentReadyToShip',
                'code_return' => 7,
            ]);
        }
        $orderDTPayment = OrderDTPayment::where('order_id',$order->id)->where('dt_id',$DTManagement->id)->first();
        if(!$orderDTPayment){
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูล OrderDTPayment',
                'code_return' => 8,
            ]);
        }
        // $orderPayment = OrderPayment::where('order_id',$order->id)->first();
        // if(!$orderPayment){
        //     return response()->json([
        //         'msg_return' => 'ไม่พบข้อมูล OrderPayment',
        //         'code_return' => 9,
        //     ]);
        // }
        $DTConfirmationSendMessage = DTConfirmationSendMessage::where('order_id',$orderId)->where('ecommerce_customer_id',$customer->id)->where('dt_id',$DTManagement->id)->where('type','dt_ready_to_ship')->where(\DB::raw('TIMESTAMPDIFF(MINUTE,created_at,NOW())'),'<=',5)->first();
        if($DTConfirmationSendMessage){
            return response()->json([
                'msg_return' => 'ระบบพึ่งได้ทำการส่งข้อความไป กรุณารอ 5 นาที',
                'code_return' => 1,
            ]);
        }

        OrderConfirmPaymentReadyToShipNotification::pushNotificationCustomer($order,$customer,$orderConfirmation,$orderConfirmationPaymentReadyToShip);
        DTConfirmationSendMessage::create([
            'order_id' => $orderId,
            'ecommerce_customer_id' => $customer->id,
            'dt_id' => $DTManagement->id,
            'type' => 'dt_ready_to_ship'
        ]);

        return response()->json([
            'msg_return' => 'ส่งข้อมูลสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function confirmShippingCustomer(Request $request)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $userId = $request->user_id;
        $orderId = $request->order_id;
        $order = Order::find($orderId);
        if(!$order){
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูลออเดอร์',
                'code_return' => 2,
            ]);
        }
        $customerId = $order->ecommerce_customer_id;
        $customer = Customer::find($customerId);
        if(!$customer){
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูล Customer',
                'code_return' => 3,
            ]);
        }
        $DTManagementRegisterData = DTManagementRegisterData::where('user_id',$userId)->first();
        if(!$DTManagementRegisterData){
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูล DT',
                'code_return' => 4,
            ]);
        }
        $DTManagement = DTManagement::where('dt_code_login',$DTManagementRegisterData->dt_code)->first();
        if(!$DTManagement){
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูล DT',
                'code_return' => 5,
            ]);
        }
        $orderConfirmation = OrderConfirmation::where('order_id',$orderId)->first();
        if(!$orderConfirmation){
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูล OrderConfirmation',
                'code_return' => 6,
            ]);
        }
        $orderShippingConfirmation = OrderShippingConfirmation::where('order_id',$orderId)->first();
        if(!$orderShippingConfirmation){
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูล OrderShippingConfirmation',
                'code_return' => 7,
            ]);
        }
        $orderDTPayment = OrderDTPayment::where('order_id',$order->id)->where('dt_id',$DTManagement->id)->first();
        if(!$orderDTPayment){
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูล OrderDTPayment',
                'code_return' => 8,
            ]);
        }
        $orderPayment = collect();
        // $orderPayment = OrderPayment::where('order_id',$order->id)->first();
        // if(!$orderPayment){
        //     return response()->json([
        //         'msg_return' => 'ไม่พบข้อมูล OrderPayment',
        //         'code_return' => 9,
        //     ]);
        // }
        $DTConfirmationSendMessage = DTConfirmationSendMessage::where('order_id',$orderId)->where('ecommerce_customer_id',$customer->id)->where('dt_id',$DTManagement->id)->where('type','dt_confirm_shipping')->where(\DB::raw('TIMESTAMPDIFF(MINUTE,created_at,NOW())'),'<=',5)->first();
        if($DTConfirmationSendMessage){
            return response()->json([
                'msg_return' => 'ระบบพึ่งได้ทำการส่งข้อความไป กรุณารอ 5 นาที',
                'code_return' => 1,
            ]);
        }

        OrderConfirmShippingNotification::pushNotificationShippingCustomer($order,$customer,$orderPayment,$orderConfirmation);
        DTConfirmationSendMessage::create([
            'order_id' => $orderId,
            'ecommerce_customer_id' => $customer->id,
            'dt_id' => $DTManagement->id,
            'type' => 'dt_confirm_shipping'
        ]);

        return response()->json([
            'msg_return' => 'ส่งข้อมูลสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function trackingSendEmail(Request $request)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $type = $request->type;
        $orderId = $request->order_id;
        $datas = collect([
            'order_id' => $orderId,
            'email_type' => $type,
            'status' => 'success',
        ]);

        return response()->json([
            'datas' => $datas,
        ]);
    }

    public function trackingSendLine(Request $request)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $type = $request->type;
        $orderId = $request->order_id;
        $datas = collect([
            'order_id' => $orderId,
            'email_type' => $type,
            'status' => 'success',
        ]);

        return response()->json([
            'datas' => $datas,
        ]);
    }

    public function updateOrderItem(Request $request)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $userId = $request->user_id;
        $orderId = $request->order_id;
        $orderItemId = $request->shopping_cart_item_id;
        $quantityNew = $request->quantity;
        $dateNow = Carbon::now()->format('Y-m-d H:i:s');
        $order = Order::find($orderId);
        if(!$order){
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูลออเดอร์',
                'code_return' => 2,
            ]);
        }
        
        $DTManagementRegisterData = DTManagementRegisterData::where('user_id',$userId)->first();
        if(!$DTManagementRegisterData){
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูล DT',
                'code_return' => 4,
            ]);
        }
        $DTManagement = DTManagement::where('dt_code_login',$DTManagementRegisterData->dt_code)->first();
        if(!$DTManagement){
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูล DT',
                'code_return' => 5,
            ]);
        }

        $orderProduct = OrderProduct::find($orderItemId);
        
        $logOrderProduct = $orderProduct->toArray();
        $logOrderProduct['user_id'] = $userId;
        $logOrderProduct['dt_id'] = $DTManagement->id;

        OrderCustomizeLog::create($logOrderProduct);

        $orderProduct->update([
            'quanlity' => $quantityNew
        ]);

        $orderProducts = $order->orderProducts;
        $summary = 0;
        $summaryOriginal = 0;
        $allQty = 0;
        $allRewardPoint = 0;

        foreach ($orderProducts as $key => $productItem) {
            $product = Product::find($productItem->product_id);
            $qty = $productItem->quanlity;
            $productPrice = $productItem->original_price*$qty;
            $summaryOriginal += $productPrice;
            $productVat = $product->vat;
            $productDiscount = $product->productDiscount;
            $productDiscountPrice = 0;
            $productPriceAfterVat = 0;
            if($productDiscount){
                if($productDiscount->start_date <= $dateNow && $productDiscount->end_date >= $dateNow){
                    // $productDiscountPrice = $productDiscount->discount_price*$qty;
                    // $productPrice = $productPrice-$productDiscountPrice;
                    $productPrice = $productDiscount->discount_price*$qty;
                }
            }
            if($productVat > 0){
                $productPriceAfterVat = $productPrice + (($productPrice*$productVat)/100);
            }else{
                $productPriceAfterVat = $productPrice;
            }
            $productItem->update([
                'price' => $productPrice,
                'quanlity' => $qty,
                'tax_amount' => 0,
                'tax_percent' => $productVat,
                'discount_amount' => $productDiscountPrice,
                'total' => $productPriceAfterVat
            ]);
            $summary += $productPriceAfterVat;
            $allQty += $qty;
        }
        $dataItems = [];
        $dataItems['sub_total'] = $summary;

        $discountPrice = $order->coupon_code_discount_amount;
        $summary = $summary - $discountPrice;


        $dataItems['grand_total'] = $summary;
        $dataItems['total_paid'] = $summary;
        $dataItems['total_due'] = $summary;
        
        $order->update($dataItems);

        return response()->json([
            'msg_return' => 'บันทึกข้อมูลสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function exportDtOrder(Request $request)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $serialized = serialize($request->all());
        
        DownloadFileMain::create([
          'main_id' => 0,
          'type' => 'order_dt',
          'requests' => $serialized,
          'is_active' => 1
        ]);
        
        return response()->json([
          'return_code' => 1,
          'msg' => 'Success"',
        ]);
    }
}
