<?php

namespace YellowProject\Http\Controllers\API\v1\Ecommerce\OnlineExclusiveModule;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\OnlineExclusiveModule\OnlineExclusiveModuleCourse;
use YellowProject\Ecommerce\OnlineExclusiveModule\OnlineExclusiveModuleCourseVideo;
use YellowProject\Ecommerce\OnlineExclusiveModule\OnlineExclusiveModuleCustomerReDeem;
use YellowProject\Ecommerce\Customer\Customer;

class OnlineExclusiveModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $onlineExclusiveModuleCourses = OnlineExclusiveModuleCourse::select(
            'id',
            'course_name',
            'ep',
            \DB::raw('DATE_FORMAT(created_at, "%d/%m/%Y") as created_date'),
            'status'
        )->orderByDesc('created_at')->get();

        return response()->json([
            'datas' => $onlineExclusiveModuleCourses,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $onlineExclusiveModuleCourse = OnlineExclusiveModuleCourse::create($request->all());
        $videos = $request->video;
        foreach ($videos as $key => $video) {
            $video['online_exclusive_course_id'] = $onlineExclusiveModuleCourse->id; //ดึง id จาก  insert OnlineExclusiveModuleCourse
            OnlineExclusiveModuleCourseVideo::create($video); // insert ลง อีก Table
        }

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $datas = [];
        $onlineExclusiveModuleCourse = OnlineExclusiveModuleCourse::find($id);
        $onlineExclusiveModuleCourseVideos = OnlineExclusiveModuleCourseVideo::where('online_exclusive_course_id',$onlineExclusiveModuleCourse->id)->get();
        $datas['id'] = $onlineExclusiveModuleCourse->id;
        $datas['course_name'] = $onlineExclusiveModuleCourse->course_name;
        $datas['description'] = $onlineExclusiveModuleCourse->description;
        $datas['img_url'] = $onlineExclusiveModuleCourse->img_url;
        $datas['ep'] = $onlineExclusiveModuleCourse->ep;
        $datas['status'] = $onlineExclusiveModuleCourse->status;
        $datas['video'] = [];
        foreach ($onlineExclusiveModuleCourseVideos as $key => $onlineExclusiveModuleCourseVideo) {
            $datas['video'][$key]['name'] = $onlineExclusiveModuleCourseVideo->name;
            $datas['video'][$key]['thumbnail_img_url'] = $onlineExclusiveModuleCourseVideo->thumbnail_img_url;
            $datas['video'][$key]['manual_video_url'] = $onlineExclusiveModuleCourseVideo->manual_video_url;
            $datas['video'][$key]['upload_video_url'] = $onlineExclusiveModuleCourseVideo->upload_video_url;
        }
        
        return response()->json($datas);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $onlineExclusiveModuleCourse = OnlineExclusiveModuleCourse::find($id);
        $onlineExclusiveModuleCourse->update($request->all());

        OnlineExclusiveModuleCourseVideo::where('online_exclusive_course_id',$onlineExclusiveModuleCourse->id)->delete();

        $videos = $request->video;
        foreach ($videos as $key => $video) {
            $video['online_exclusive_course_id'] = $onlineExclusiveModuleCourse->id;
            OnlineExclusiveModuleCourseVideo::create($video);
        }

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $onlineExclusiveModuleCourse = OnlineExclusiveModuleCourse::find($id);
        OnlineExclusiveModuleCourseVideo::where('online_exclusive_course_id',$onlineExclusiveModuleCourse->id)->delete();
        $onlineExclusiveModuleCourse->delete();

        return response()->json([
            'msg_return' => 'ลบสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function redeem(Request $request)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $lineUserId = $request->line_user_id;
        $itemId = $request->item_id;
        $customer = Customer::where('line_user_id',$lineUserId)->first();
        if(!$customer){
            return response()->json([
                'msg_return' => 'Not Found Customer',
                'code_return' => 2,
            ]);
        }
        $onlineExclusiveModuleCourse = OnlineExclusiveModuleCourse::find($itemId);
        if(!$onlineExclusiveModuleCourse){
            return response()->json([
                'msg_return' => 'Not Found Course',
                'code_return' => 2,
            ]);
        }
        
        OnlineExclusiveModuleCustomerReDeem::create([
            'line_user_id' => $lineUserId,
            'customer_id' => $customer->id,
            'online_exclusive_course_id' => $onlineExclusiveModuleCourse->id,
            'course_name' => $onlineExclusiveModuleCourse->course_name,
            'description' => $onlineExclusiveModuleCourse->description,
            'img_url' => $onlineExclusiveModuleCourse->img_url,
            'ep' => $onlineExclusiveModuleCourse->ep,
            'redeem_date' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        return response()->json([
            'msg_return' => 'SUCCESS',
            'code_return' => 1,
        ]);
    }

    public function notRedeem(Request $request)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $lineUserId = $request->line_user_id;
        $itemId = $request->item_id;
        $customer = Customer::where('line_user_id',$lineUserId)->first();
        if(!$customer){
            return response()->json([
                'msg_return' => 'Not Found Customer',
                'code_return' => 2,
            ]);
        }
        $onlineExclusiveModuleCourse = OnlineExclusiveModuleCourse::find($itemId);
        if(!$onlineExclusiveModuleCourse){
            return response()->json([
                'msg_return' => 'Not Found Course',
                'code_return' => 2,
            ]);
        }

        OnlineExclusiveModuleCustomerReDeem::where('line_user_id',$lineUserId)->where('online_exclusive_course_id',$itemId)->delete();

        return response()->json([
            'msg_return' => 'SUCCESS',
            'code_return' => 1,
        ]);
    }
}
