<?php

namespace YellowProject\Http\Controllers\API\v1\Ecommerce\OnlineExclusiveModule;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\OnlineExclusiveModule\OnlineExclusiveModuleCourseFiles;
use Carbon\Carbon;
use URL;

class OnlineExclusiveModuleUploadController extends Controller
{
    public function uploadFile(Request $request)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $destinationPath = 'file_uploads/online_exclusive';
        OnlineExclusiveModuleCourseFiles::checkFolderDefaultPath($destinationPath);
        $dateNow = Carbon::now()->format('dmY_His');
        $type = $request->type;
        $fileData = $request->file_data;
        $extension = $fileData->getClientOriginalExtension(); // getting image extension
        $fileName = $dateNow.'.'.$extension; // renameing image
        $fileData->move($destinationPath, $fileName); // uploading file to given path

        $OnlineExclusiveModuleCourseFile = OnlineExclusiveModuleCourseFiles::create([
            'url_path' => URL::to('/')."/".$destinationPath."/".$fileName,
            'type' => $type
        ]);

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
            'datas' => $OnlineExclusiveModuleCourseFile,
        ]);
    }
}
