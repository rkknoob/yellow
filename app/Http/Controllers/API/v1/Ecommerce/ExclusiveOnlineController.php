<?php

namespace YellowProject\Http\Controllers\API\v1\Ecommerce;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\ExclusiveOnline\ExclusiveOnline;
use YellowProject\Ecommerce\ExclusiveOnline\ExclusiveOnlinePdf;
use YellowProject\Ecommerce\ExclusiveOnline\ExclusiveOnlineVideo;
use YellowProject\Ecommerce\ExclusiveOnline\ExclusiveOnlineUser;

class ExclusiveOnlineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $datas = [];
        $exclusiveOnlines = ExclusiveOnline::all();
        foreach ($exclusiveOnlines as $key => $exclusiveOnline) {
            $datas[$key]['name'] = $exclusiveOnline->course_name;
            $datas[$key]['ep'] = $exclusiveOnline->ep;
            $datas[$key]['create_date'] = $exclusiveOnline->created_at->format('d-m-Y');
            $datas[$key]['status'] = $exclusiveOnline->status;
        }

        return response()->json([
            'datas' => $datas,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $exclusiveOnline = ExclusiveOnline::create($request->all());
        if(isset($request->exclusive_online_videos)){
            $videos = $request->exclusive_online_videos;
            foreach ($videos as $key => $video) {
                $video['exclusive_online_id'] = $exclusiveOnline->id;
                ExclusiveOnlineVideo::create($video);
            }
        }
        if(isset($request->exclusive_online_pdfs)){
            $pdfs = $request->exclusive_online_pdfs;
            foreach ($pdfs as $key => $pdf) {
                $pdf['exclusive_online_id'] = $exclusiveOnline->id;
                ExclusiveOnlinePdf::create($pdf);
            }
        }
        if(isset($request->exclusive_online_customers)){
            $customers = $request->exclusive_online_customers;
            foreach ($customers as $key => $customer) {
                $customer['exclusive_online_id'] = $exclusiveOnline->id;
                ExclusiveOnlineUser::create($customer);
            }
        }

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $datas = [];
        $exclusiveOnline = ExclusiveOnline::find($id);
        $videoFiles = $exclusiveOnline->videoFiles;
        $pdfFiles = $exclusiveOnline->pdfFiles;
        $customers = $exclusiveOnline->customers;
        $datas['course_name'] = $exclusiveOnline->course_name;
        $datas['desc'] = $exclusiveOnline->desc;
        $datas['avaliable_strat_date'] = $exclusiveOnline->avaliable_strat_date;
        $datas['avaliable_end_date'] = $exclusiveOnline->avaliable_end_date;
        $datas['hours_duration'] = $exclusiveOnline->hours_duration;
        $datas['ep'] = $exclusiveOnline->ep;
        $datas['status'] = $exclusiveOnline->status;
        if($videoFiles){
            foreach ($videoFiles as $key => $videoFile) {
                $file = $videoFile->file;
                $datas['exclusive_online_videos'][$key]['exclusive_online_id'] = $videoFile->exclusive_online_id;
                $datas['exclusive_online_videos'][$key]['ecommerce_exclusive_online_file_id'] = $videoFile->ecommerce_exclusive_online_file_id;
                $datas['exclusive_online_videos'][$key]['label'] = $videoFile->label;
                $datas['exclusive_online_videos'][$key]['link_path'] = ($file)? $file->link_path : "";
            }
        }
        if($pdfFiles){
            foreach ($pdfFiles as $key => $pdfFile) {
                $file = $pdfFile->file;
                $datas['exclusive_online_pdfs'][$key]['exclusive_online_id'] = $pdfFile->exclusive_online_id;
                $datas['exclusive_online_pdfs'][$key]['ecommerce_exclusive_online_file_id'] = $pdfFile->ecommerce_exclusive_online_file_id;
                $datas['exclusive_online_pdfs'][$key]['label'] = $pdfFile->label;
                $datas['exclusive_online_pdfs'][$key]['link_path'] = ($file)? $file->link_path : "";
            }
        }
        if($customers){
            foreach ($customers as $key => $customer) {
                $ecommerceCustomer = $customer->ecommerceCustomer;
                $datas['exclusive_online_customers'][$key]['exclusive_online_id'] = $customer->exclusive_online_id;
                $datas['exclusive_online_customers'][$key]['ecommerce_customer_id'] = $customer->ecommerce_customer_id;
                $datas['exclusive_online_customers'][$key]['status'] = $customer->status;
                $datas['exclusive_online_customers'][$key]['first_name'] = ($ecommerceCustomer)? $ecommerceCustomer->first_name : "";
                $datas['exclusive_online_customers'][$key]['last_name'] = ($ecommerceCustomer)? $ecommerceCustomer->last_name : "";
                $datas['exclusive_online_customers'][$key]['phone_number'] = ($ecommerceCustomer)? $ecommerceCustomer->phone_number : "";
                $datas['exclusive_online_customers'][$key]['reward_point'] = ($ecommerceCustomer)? $ecommerceCustomer->reward_point : "";
                $datas['exclusive_online_customers'][$key]['created_at'] = ($ecommerceCustomer)? $customer->created_at->format('d/m/Y') : "";
            }
        }

        return response()->json([
            'datas' => $datas,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $exclusiveOnline = ExclusiveOnline::find($id);
        $exclusiveOnline->update($request->all());
        if($exclusiveOnline){
            $videoFiles = $exclusiveOnline->videoFiles;
            $pdfFiles = $exclusiveOnline->pdfFiles;
            $customers = $exclusiveOnline->customers;
            if($videoFiles){
                foreach ($videoFiles as $key => $videoFile) {
                    $videoFile->delete();
                }
            }
            if($pdfFiles){
                foreach ($pdfFiles as $key => $pdfFile) {
                    $pdfFile->delete();
                }
            }
            if($customers){
                foreach ($customers as $key => $customer) {
                    $customer->delete();
                }
            }
        }

        if(isset($request->exclusive_online_videos)){
            $videos = $request->exclusive_online_videos;
            foreach ($videos as $key => $video) {
                $video['exclusive_online_id'] = $exclusiveOnline->id;
                ExclusiveOnlineVideo::create($video);
            }
        }
        if(isset($request->exclusive_online_pdfs)){
            $pdfs = $request->exclusive_online_pdfs;
            foreach ($pdfs as $key => $pdf) {
                $pdf['exclusive_online_id'] = $exclusiveOnline->id;
                ExclusiveOnlinePdf::create($pdf);
            }
        }
        if(isset($request->exclusive_online_customers)){
            $customers = $request->exclusive_online_customers;
            foreach ($customers as $key => $customer) {
                $customer['exclusive_online_id'] = $exclusiveOnline->id;
                ExclusiveOnlineUser::create($customer);
            }
        }

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $ExclusiveOnline = ExclusiveOnline::find($id);
        if($ExclusiveOnline){
            $videoFiles = $ExclusiveOnline->videoFiles;
            $pdfFiles = $ExclusiveOnline->pdfFiles;
            $customers = $ExclusiveOnline->customers;
            if($videoFiles){
                foreach ($videoFiles as $key => $videoFile) {
                    $videoFile->delete();
                }
            }
            if($pdfFiles){
                foreach ($pdfFiles as $key => $pdfFile) {
                    $pdfFile->delete();
                }
            }
            if($customers){
                foreach ($customers as $key => $customer) {
                    $customer->delete();
                }
            }
        }

        return response()->json([
            'msg_return' => 'ลบสำเร็จ',
            'code_return' => 1,
        ]);
    }
}
