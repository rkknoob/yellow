<?php

namespace YellowProject\Http\Controllers\API\v1\Ecommerce;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\TemplateSentMessage\TemplateSentMessageMain as AutoReplyKeyword;
use YellowProject\Ecommerce\TemplateSentMessage\TemplateSentMessageItem as AutoReplyKeywordItem;
use YellowProject\Ecommerce\TemplateSentMessage\TemplateSentMessage as AutoReplyKeywordMessage;
use YellowProject\Ecommerce\TemplateSentMessage\TemplateSentMessageSticker as AutoReplyKeywordSticker;

class TemplateSentMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $autoReplyKeywords  = AutoReplyKeyword::all();
       
        return response()->json([
            'datas'          => $autoReplyKeywords,
            'countAutoReply' => $autoReplyKeywords->count(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $arrErrsField = array();
        $colectErrsField= collect();
        if (!isset($request->title) || (isset($request->title) && trim($request->title) == "") ) {
            $arrErrsField[] = 'title';
        }

        if (!isset($request->keywords) || (isset($request->keywords) && sizeof($request->keywords) == 0) ) {
            $arrErrsField[] = 'keywords';
        }

        if (sizeof($arrErrsField) > 0) {
           
            return response()->json([
                'msg_return' => 'VALID_REQUIRE',
                'code_return' => 1,
                'items'       => $arrErrsField
            ]);
        }

        $dup = AutoReplyKeyword::checkDuplicate($request->title);
        if ($dup) {
           
            return response()->json([
                'msg_return' => 'ERR_DUPPLICATE',
                'code_return' => 1,
                'items'       => ['title']
            ]);
        }

        $autoReplyKeyword = AutoReplyKeyword::firstOrNew([
            'active'   => $request->active,
            'title'    => $request->title,
            'sent_date'    => $request->sent_date,
            'last_sent_date'    => $request->last_sent_date,
            'conf'    => $request->conf,
            'is_open_bot'    => $request->is_open_bot,
        ]);

        if ($autoReplyKeyword->exists) {

            return response()->json([
                'msg_return' => 'ERR_DUPPLICATE',
                'code_return' => 1,
                'items'       => ['title']
            ]);

        } else {
            // autoReplyKeyword created from 'new'; does not exist in database.
            $autoReplyKeyword->save();
            if (isset($request->items) && sizeof($request->items) > 0) {
                foreach ($request->items as $item) {
                    switch ($item['type']) {
                        case 'text':
                            foreach ($item['value'] as $value) {
                                $autoReplyKeywordMessage = new AutoReplyKeywordMessage([
                                    'message' => $value['playload'],
                                    'display' => $value['display'],
                                ]);
                            }
                            $autoReplyKeywordMessage->save();
                            $autoReplyKeywordItem = new AutoReplyKeywordItem([
                                'dim_ecommerce_template_sent_message_id' => $autoReplyKeyword->id,
                                'message_type_id'           => $item['type'],
                                'seq_no'                    => $item['seq_no'],
                                'auto_reply_message_id'     => $autoReplyKeywordMessage->id,
                                'auto_reply_sticker_id'     => null,
                                'auto_reply_richmessage_id' => null,
                            ]);
                            $autoReplyKeywordItem->save();
                           break;
                        case 'sticker':
                            foreach ($item['value'] as $value) {
                                $autoReplyKeywordSticker = new AutoReplyKeywordSticker([
                                    'packageId' => $value['package_id'],
                                    'stickerId' => $value['stricker_id'],
                                    'display' => $value['display'],
                                ]);
                            }
                            $autoReplyKeywordSticker->save();
                            $autoReplyKeywordItem = new AutoReplyKeywordItem([
                                'dim_ecommerce_template_sent_message_id' => $autoReplyKeyword->id,
                                'message_type_id'           => $item['type'],
                                'seq_no'                    => $item['seq_no'],
                                'auto_reply_message_id'     => null,
                                'auto_reply_sticker_id'     => $autoReplyKeywordSticker->id,
                                'auto_reply_richmessage_id' => null,
                            ]);    
                            $autoReplyKeywordItem->save();
                           break;
                        case 'imagemap':
                            $autoReplyKeywordItem = new AutoReplyKeywordItem([
                                'dim_ecommerce_template_sent_message_id' => $autoReplyKeyword->id,
                                'message_type_id'           => $item['type'],
                                'seq_no'                    => $item['seq_no'],
                                'auto_reply_message_id'     => null,
                                'auto_reply_sticker_id'     => null,
                                'auto_reply_richmessage_id' => $item['value'][0]['auto_reply_richmessage_id'],
                            ]);    
                            $autoReplyKeywordItem->save();
                           break;
                       default:
                           # code...
                           break;
                    }
                }
            }

            return response()->json([
                'msg_return' => 'INSERT_SUCCESS',
                'code_return' => 1,
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $autoReplyKeyword  = AutoReplyKeyword::find($id);
        if ($autoReplyKeyword) {
            
            $items  = $autoReplyKeyword->autoReplyKeyWordItems;
            // return response()->json($items);
            $arr = array();
            foreach ($items as $item) {
               $arr[] = array(
                    "id" => $item->id ,
                    "seq_no"    => $item->seq_no,
                    "type"      => $item->messageType->type,
                    "value"     => [
                        $item->show_message
                    ]
                );
            }

            $data  = array(
                "id"                =>  $autoReplyKeyword->id,
                "title"             =>  $autoReplyKeyword->title,
                'sent_date'         =>  $autoReplyKeyword->sent_date,
                'last_sent_date'    =>  $autoReplyKeyword->last_sent_date,
                "active"            =>  $autoReplyKeyword->active,
                "conf"              =>  $autoReplyKeyword->conf,
                "is_open_bot"       =>  $autoReplyKeyword->is_open_bot,
                "items"             =>  $arr
            );
    
            return json_encode($data, JSON_UNESCAPED_UNICODE);

        }

        return response()->json([
            'msg_return' => 'ERR_NOTFOUND',
            'code_return' => 1,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $autoReplyKeyword  = AutoReplyKeyword::find($id);
        if ($autoReplyKeyword) {
            $arrErrsField = array();
            $colectErrsField= collect();

            if (!isset($request->title) || (isset($request->title) && trim($request->title) == "") ) {
                $arrErrsField[] = 'title';
            }

            if (sizeof($arrErrsField) > 0) {
                $colectErrsField = $colectErrsField->push([
                    'VALID_REQUIRE' => $arrErrsField
                ]);

                return response()->json([
                    'msg_return'    => 'VALID_REQUIRE',
                    'code_return' => 1,
                    'items'       => $arrErrsField
                ]);
            }

            $dup = AutoReplyKeyword::checkDuplicate($request->title, $id);
            if ($dup) {
               
                return response()->json([
                    'msg_return' => 'ERR_DUPPLICATE',
                    'code_return' => 1,
                    'items'       => ['title']
                ]);
            }

            $autoReplyKeyword = $autoReplyKeyword->update([
                'folder_id'             => $request->folder_id,
                'active'                => $request->active,
                'title'                 => $request->title,
                'sent_date'             => $request->sent_date,
                'last_sent_date'        => $request->last_sent_date,
                'conf'                  => $request->conf,
                'is_open_bot'           => $request->is_open_bot,
            ]);
            $autoReplyKeyword  = AutoReplyKeyword::find($id); //Select Again After Code Update

            if (isset($request->items) && sizeof($request->items) > 0) {
                $autoReplyKeywordItems = $autoReplyKeyword->autoReplyKeyWordItems;

                if(sizeof($autoReplyKeywordItems) > 0) {
                    foreach ($autoReplyKeywordItems as  $autoReplyKeywordItem) {
                       switch ( $autoReplyKeywordItem->messageType->type) {
                            case 'text':
                                $autoReplyKeywordMessage = $autoReplyKeywordItem->message()->forceDelete();
                                break;
                            case 'sticker':
                                $autoReplyKeywordSticker = $autoReplyKeywordItem->sticker()->forceDelete();
                                break;
                        }

                    }
                    $autoReplyKeyword->autoReplyKeyWordItems()->forceDelete();

                }

                foreach ($request->items as $item) {

                    switch ($item['type']) {
                        case 'text':
                            foreach ($item['value'] as $value) {
                                $autoReplyKeywordMessage = new AutoReplyKeywordMessage([
                                    'message' => $value['playload'],
                                    'display' => $value['display'],

                                ]);
                            }
                            $autoReplyKeywordMessage->save();
                            $autoReplyKeywordItem = new AutoReplyKeywordItem([
                                'dim_ecommerce_template_sent_message_id' => $id,
                                'message_type_id'           => $item['type'],
                                'seq_no'                    => $item['seq_no'],
                                'auto_reply_message_id'     => $autoReplyKeywordMessage->id,
                                'auto_reply_sticker_id'     => null,
                                'auto_reply_richmessage_id'     => null,
                            ]);
                            $autoReplyKeywordItem->save();
                           break;
                        case 'sticker':
                            foreach ($item['value'] as $value) {
                                $autoReplyKeywordSticker = new AutoReplyKeywordSticker([
                                    'packageId' => $value['package_id'],
                                    'stickerId' => $value['stricker_id'],
                                    'display' => $value['display'],
                                ]);
                            }
                            $autoReplyKeywordSticker->save();
                            $autoReplyKeywordItem = new AutoReplyKeywordItem([
                                'dim_ecommerce_template_sent_message_id' => $id,
                                'message_type_id'           => $item['type'],
                                'seq_no'                    => $item['seq_no'],
                                'auto_reply_message_id'     => null,
                                'auto_reply_sticker_id'     => $autoReplyKeywordSticker->id,
                                'auto_reply_richmessage_id'     => null,
                            ]);    
                            $autoReplyKeywordItem->save();

                           break;
                        case 'imagemap':
                            $autoReplyKeywordItem = new AutoReplyKeywordItem([
                                'dim_ecommerce_template_sent_message_id' => $autoReplyKeyword->id,
                                'message_type_id'           => $item['type'],
                                'seq_no'                    => $item['seq_no'],
                                'auto_reply_message_id'     => null,
                                'auto_reply_sticker_id'     => null,
                                'auto_reply_richmessage_id' => $item['value'][0]['auto_reply_richmessage_id'],
                            ]);    
                            $autoReplyKeywordItem->save();
                           break;

                       default:
                           # code...
                       break;
                    }
                }

                return response()->json([
                    'msg_return'    => 'UPDATE_SUCCESS',
                    'code_return'   => 1,
                ]);
            }
        }

        return response()->json([
            'msg_return' => 'ERR_NOTFOUND',
            'code_return' => 1,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $autoReplyKeyword  = AutoReplyKeyword::find($id);
        if ($autoReplyKeyword) {
            if ($autoReplyKeyword->active) {
                return response()->json([
                    'msg_return' => 'ลบไม่สำเร็จ ข้อมูลถูกใช้งานอยู่',
                    'code_return' => 1,
                ]);
            } else {
               $autoReplyKeyword->forceDelete();
                return response()->json([
                    'msg_return' => 'ลบสำเร็จ',
                    'code_return' => 1,
                ]); 
            }
            
        }
    }

    public function postActive(Request $request, $id)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $autoReplyKeyword  = AutoReplyKeyword::find($id);
       
        if ($autoReplyKeyword) {
            $autoReplyKeyword->update([
                'active'  => $request->active,
            ]);

            return response()->json([
                'msg_return' => 'บันทึกสำเร็จ',
                'code_return' => 1,
            ]);
        }
    }
}
