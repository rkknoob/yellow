<?php

namespace YellowProject\Http\Controllers\API\v1\Ecommerce;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\DTManagement\DTManagement;
use YellowProject\GeneralFunction\CoreFunction;
use YellowProject\Ecommerce\DTManagement\DTManagementUpload;
use YellowProject\Ecommerce\DTManagement\DTManagementUploadItem;
use YellowProject\Ecommerce\DTManagement\DTManagementLog;
use YellowProject\Ecommerce\DTManagement\DTManagementPostCode;
use YellowProject\Ecommerce\DTManagement\DTManagementRegisterData;
use YellowProject\User;
use YellowProject\Ecommerce\Task\SetTask;
use URL;

class DTManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $baseUrl = \URL::to('/');
        if(isset($request->dt_code_login) && $request->dt_code_login != ""){
            $DTManagementSearch = DTManagement::where('dt_code_login',$request->dt_code_login)->first();
            if($DTManagementSearch){
              return response()->json([
                  'msg_return' => 'ชื่อ Code ซ้ำกัน',
                  'code_return' => 21,
              ]);
            }
        }else{
            $request['dt_code_login'] = CoreFunction::genCode();
        }
        $request['dt_url_login'] = $baseUrl."/dt/".$request['dt_code_login'];

        $DTManagement = DTManagement::create($request->all());

        SetTask::create([
          'type' => 'dt_send_message',
          'main_id' => $DTManagement->id,
          'is_active' => 1
        ]);

        if(isset($request->post_code)){
          foreach ($request->post_code as $key => $post_code) {
            DTManagementPostCode::create([
              'ecommerce_dt_management_id' => $DTManagement->id,
              'post_code' => $post_code
            ]);
          }
        }

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
            'dt_code_login' => $request['dt_code_login'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $datas = [];
        $DTManagement = DTManagement::find($id);
        $datas = $DTManagement->toArray();
        $postCodes = $DTManagement->postCodes;
        foreach ($postCodes as $key => $postCode) {
          $datas['post_code'][$key] = $postCode->post_code;
        }

        return response()->json([
            'datas' => $datas,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $baseUrl = \URL::to('/');
        $DTManagement = DTManagement::find($id);
        $oldDtCodeLogin = $DTManagement->dt_code_login;
        if(isset($request->dt_code_login) && $request->dt_code_login != "" && $DTManagement->dt_code_login != $request->dt_code_login){
            $DTManagementSearch = DTManagement::where('dt_code_login',$request->dt_code_login)->first();
            if($DTManagementSearch){
              return response()->json([
                  'msg_return' => 'ชื่อ Code ซ้ำกัน',
                  'code_return' => 21,
              ]);
            }
        }else{
          if($request->dt_code_login == ""){
            $request['dt_code_login'] = CoreFunction::genCode();
          }else{
            $request['dt_code_login'] = $request->dt_code_login;
          }
        }
        $request['dt_url_login'] = $baseUrl."/dt/".$request['dt_code_login'];

        $DTManagement->update($request->all());
        $DTManagementRegisterData = DTManagementRegisterData::where('dt_code',$oldDtCodeLogin);
        $DTManagementRegisterData->update([
          'dt_code' => $request['dt_code_login']
        ]);

        SetTask::create([
          'type' => 'dt_send_message',
          'main_id' => $DTManagement->id,
          'is_active' => 1
        ]);

        DTManagementPostCode::where('ecommerce_dt_management_id',$DTManagement->id)->delete();

        if(isset($request->post_code)){
          foreach ($request->post_code as $key => $post_code) {
            DTManagementPostCode::create([
              'ecommerce_dt_management_id' => $DTManagement->id,
              'post_code' => $post_code
            ]);
          }
        }

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
            'dt_code_login' => $request['dt_code_login'],
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $DTManagement = DTManagement::find($id);
        $DTManagement->delete();

        return response()->json([
            'msg_return' => 'ลบข้อมูลสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function uploadData(Request $request)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $datas = [];
        $countNewUser = 0;
        $DTManagementUpload = DTManagementUpload::create([
            'count_record' => $countNewUser,
        ]);
        if($request->items){
            foreach ($request->items as $key => $item) {
                $baseUrl = \URL::to('/');
                if(isset($item['dt_code_login']) && $item['dt_code_login'] != ""){
                    $DTManagementSearch = DTManagement::where('dt_code_login',$item['dt_code_login'])->first();
                    if($DTManagementSearch){
                      $item['remark'] = 'Code Login Duplicate';
                      $item['dt_code_login'] = "";
                    }
                }else{
                    $item['dt_code_login'] = CoreFunction::genCode();
                }
                if($item['dt_code_login'] != ""){
                    $item['dt_url_login'] = $baseUrl."/dt/".$item['dt_code_login'];
                }

                $item['ecommerce_dt_management_upload_id'] = $DTManagementUpload->id;
                $DTManagement = DTManagement::create($item);
                DTManagementUploadItem::create($item);
                if(isset($item['post_code'])){
                  $postCodes = explode(',', $item['post_code']);
                  foreach ($postCodes as $key => $postCode) {
                    DTManagementPostCode::create([
                      'ecommerce_dt_management_id' => $DTManagement->id,
                      'post_code' => $postCode
                    ]);
                  }
                }
                $countNewUser++;
            }
        }

        $DTManagementUpload->update([
            'count_record' => $countNewUser,
        ]);

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function getDataIndex(Request $request)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $datas = [];
        $searchName = $request->name;
        $searchNameCondition = $request->name_condition;
        $searchAddress = $request->address;
        $searchAddressCondition = $request->address_condition;
        $searchProvince = $request->province;
        $searchProvinceCondition = $request->province_condition;
        $searchPostcode = $request->post_code;
        $searchPostcodeCondition = $request->post_code_condition;
        $searchDTGroupId = $request->ecommerce_dt_management_group_id;
        $searchDTGroupIdCondition = $request->ecommerce_dt_management_group_id_condition;
        $searchStatus = $request->status;
        $searchStatusCondition = $request->status_condition;
        $searchSeq = $request->seq;
        $searchSeqCondition = $request->seq_condition;
        $userId = $request->user_id;
        $user = User::find($userId);
        $DTManagements = DTManagement::orderByDesc('updated_at');
        if($user->is_dt){
          $DTManagementRegisterData = DTManagementRegisterData::where('user_id',$userId)->first();
          if($DTManagementRegisterData){
            $DTManagements->where('dt_code_login',$DTManagementRegisterData->dt_code);
          }else{
            $DTManagements->where('dt_code_login',"");
          }
        }
        if($searchName != "" && $searchNameCondition != -1){
            $DTManagements->where('name',$searchNameCondition,$searchName);
        }
        if($searchAddress != "" && $searchAddressCondition != -1){
            $DTManagements->where('address',$searchAddressCondition,$searchAddress);
        }
        if($searchProvince != "" && $searchProvinceCondition != -1){
            $DTManagements->where('province',$searchProvinceCondition,$searchProvince);
        }
        if($searchDTGroupId != "" && $searchDTGroupIdCondition != -1){
            $DTManagements->where('ecommerce_dt_management_group_id',$searchDTGroupIdCondition,$searchDTGroupId);
        }
        if($searchStatus != "" && $searchStatusCondition != -1){
            $DTManagements->where('status',$searchStatusCondition,$searchStatus);
        }
        if($searchSeq != "" && $searchSeqCondition != -1){
            $DTManagements->where('seq',$searchSeqCondition,$searchSeq);
        }
        if($searchPostcode != "" && $searchPostcodeCondition != -1){
            // $DTManagements->where('post_code',$searchPostcodeCondition,$searchPostcode);
          $DTManagements->whereHas('postCodes', function ($query) use ($searchPostcode,$searchPostcodeCondition) {
            $query->where('post_code',$searchPostcodeCondition,$searchPostcode);
          });
        }
        $DTManagements = $DTManagements->get();
        foreach ($DTManagements as $key => $DTManagement) {
          $dtManahementGroup = $DTManagement->dtGroup;
          $datas[$key] = $DTManagement->toArray();
          $postCodes = $DTManagement->postCodes;
          foreach ($postCodes as $postCodeIndex => $postCode) {
            $datas[$key]['post_code'][$postCodeIndex] = $postCode->post_code;
          }
          $datas[$key]['dt_group_name'] = ($dtManahementGroup)? $dtManahementGroup->name : "";
        }

        return response()->json([
            'datas' => $datas,
        ]);
    }

    public function getLogData(Request $request)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $datas = [];
        $DTManagementLogs = DTManagementLog::orderBy('updated_at');
        $DTManagementLogs = $DTManagementLogs->get();
        foreach ($DTManagementLogs as $key => $DTManagementLog) {
            $user = $DTManagementLog->user;
            $datas[$key]['id'] = $DTManagementLog->id;
            $datas[$key]['user_email'] = $user->email;
            $datas[$key]['activity'] = $DTManagementLog->activity;
            $datas[$key]['action'] = $DTManagementLog->action;
            $datas[$key]['created_at'] = $DTManagementLog->created_at;
        }

        return response()->json([
            'datas' => $datas,
        ]);
    }

}
