<?php

namespace YellowProject\Http\Controllers\API\v1\Ecommerce;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\PremiumKitchenwareEquipment\PremiumKitchenwareEquipment;
use YellowProject\Ecommerce\PremiumKitchenwareEquipment\PremiumKitchenwareEquipmentPdf;
use YellowProject\Ecommerce\PremiumKitchenwareEquipment\PremiumKitchenwareEquipmentVideo;
use YellowProject\Ecommerce\PremiumKitchenwareEquipment\PremiumKitchenwareEquipmentUser;

class PremiumKitchenwareEquipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $datas = [];
        $premiumKitchenwareEquipments = PremiumKitchenwareEquipment::all();
        foreach ($premiumKitchenwareEquipments as $key => $premiumKitchenwareEquipment) {
            $datas[$key]['name'] = $premiumKitchenwareEquipment->course_name;
            $datas[$key]['ep'] = $premiumKitchenwareEquipment->ep;
            $datas[$key]['create_date'] = $premiumKitchenwareEquipment->created_at->format('d-m-Y');
            $datas[$key]['status'] = $premiumKitchenwareEquipment->status;
        }

        return response()->json([
            'datas' => $datas,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $premiumKitchenwareEquipment = PremiumKitchenwareEquipment::create($request->all());
        if(isset($request->premium_kitchen_ware_videos)){
            $videos = $request->premium_kitchen_ware_videos;
            foreach ($videos as $key => $video) {
                $video['premium_kitchenware_id'] = $premiumKitchenwareEquipment->id;
                PremiumKitchenwareEquipmentVideo::create($video);
            }
        }
        if(isset($request->premium_kitchen_ware_pdfs)){
            $pdfs = $request->premium_kitchen_ware_pdfs;
            foreach ($pdfs as $key => $pdf) {
                $pdf['premium_kitchenware_id'] = $premiumKitchenwareEquipment->id;
                PremiumKitchenwareEquipmentPdf::create($pdf);
            }
        }
        if(isset($request->premium_kitchen_ware_customers)){
            $customers = $request->premium_kitchen_ware_customers;
            foreach ($customers as $key => $customer) {
                $customer['premium_kitchenware_id'] = $premiumKitchenwareEquipment->id;
                PremiumKitchenwareEquipmentUser::create($customer);
            }
        }

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $datas = [];
        $premiumKitchenwareEquipment = PremiumKitchenwareEquipment::find($id); //ค้นหา id
        $videoFiles = $premiumKitchenwareEquipment->videoFiles;  //กำหนดให้  premiumKitchenwareEquipment->videoFiles = vieofiles ถูกผูกไว้แล้ว
        $pdfFiles = $premiumKitchenwareEquipment->pdfFiles;
        $customers = $premiumKitchenwareEquipment->customers;
        $datas['course_name'] = $premiumKitchenwareEquipment->course_name; //กำหนดค่า couese_name ใน data[course]
        $datas['desc'] = $premiumKitchenwareEquipment->desc;
        $datas['avaliable_strat_date'] = $premiumKitchenwareEquipment->avaliable_strat_date;
        $datas['avaliable_end_date'] = $premiumKitchenwareEquipment->avaliable_end_date;
        $datas['hours_duration'] = $premiumKitchenwareEquipment->hours_duration;
        $datas['ep'] = $premiumKitchenwareEquipment->ep;
        $datas['status'] = $premiumKitchenwareEquipment->status;
        if($videoFiles){//
            foreach ($videoFiles as $key => $videoFile) {
                $file = $videoFile->file;
                $datas['premium_kitchen_ware_videos'][$key]['premium_kitchenware_id'] = $videoFile->premium_kitchenware_id;
                $datas['premium_kitchen_ware_videos'][$key]['ecommerce_premium_kitchenware_or_equipment_file_id'] = $videoFile->ecommerce_premium_kitchenware_or_equipment_file_id;
                $datas['premium_kitchen_ware_videos'][$key]['label'] = $videoFile->label;
                $datas['premium_kitchen_ware_videos'][$key]['link_path'] = ($file)? $file->link_path : "";
            }
        }
        if($pdfFiles){
            foreach ($pdfFiles as $key => $pdfFile) {
                $file = $pdfFile->file;
                $datas['premium_kitchen_ware_pdfs'][$key]['premium_kitchenware_id'] = $pdfFile->premium_kitchenware_id;
                $datas['premium_kitchen_ware_pdfs'][$key]['ecommerce_premium_kitchenware_or_equipment_file_id'] = $pdfFile->ecommerce_premium_kitchenware_or_equipment_file_id;
                $datas['premium_kitchen_ware_pdfs'][$key]['label'] = $pdfFile->label;
                $datas['premium_kitchen_ware_pdfs'][$key]['link_path'] = ($file)? $file->link_path : "";
            }
        }
        if($customers){
            foreach ($customers as $key => $customer) {
                $ecommerceCustomer = $customer->ecommerceCustomer;
                $datas['premium_kitchen_ware_customers'][$key]['premium_kitchenware_id'] = $customer->premium_kitchenware_id;
                $datas['premium_kitchen_ware_customers'][$key]['ecommerce_customer_id'] = $customer->ecommerce_customer_id;
                $datas['premium_kitchen_ware_customers'][$key]['status'] = $customer->status;
                $datas['premium_kitchen_ware_customers'][$key]['first_name'] = ($ecommerceCustomer)? $ecommerceCustomer->first_name : "";
                $datas['premium_kitchen_ware_customers'][$key]['last_name'] = ($ecommerceCustomer)? $ecommerceCustomer->last_name : "";
                $datas['premium_kitchen_ware_customers'][$key]['phone_number'] = ($ecommerceCustomer)? $ecommerceCustomer->phone_number : "";
                $datas['premium_kitchen_ware_customers'][$key]['reward_point'] = ($ecommerceCustomer)? $ecommerceCustomer->reward_point : "";
                $datas['premium_kitchen_ware_customers'][$key]['created_at'] = ($ecommerceCustomer)? $customer->created_at->format('d/m/Y') : "";
            }
        }

        return response()->json([
            'datas' => $datas,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $premiumKitchenwareEquipment = PremiumKitchenwareEquipment::find($id);
        $premiumKitchenwareEquipment->update($request->all());
        if($premiumKitchenwareEquipment){
            $videoFiles = $premiumKitchenwareEquipment->videoFiles;
            $pdfFiles = $premiumKitchenwareEquipment->pdfFiles;
            $customers = $premiumKitchenwareEquipment->customers;
            if($videoFiles){
                foreach ($videoFiles as $key => $videoFile) {
                    $videoFile->delete();
                }
            }
            if($pdfFiles){
                foreach ($pdfFiles as $key => $pdfFile) {
                    $pdfFile->delete();
                }
            }
            if($customers){
                foreach ($customers as $key => $customer) {
                    $customer->delete();
                }
            }
        }

        if(isset($request->premium_kitchen_ware_videos)){
            $videos = $request->premium_kitchen_ware_videos;
            foreach ($videos as $key => $video) {
                $video['premium_kitchenware_id'] = $premiumKitchenwareEquipment->id;
                PremiumKitchenwareEquipmentVideo::create($video);
            }
        }
        if(isset($request->premium_kitchen_ware_pdfs)){
            $pdfs = $request->premium_kitchen_ware_pdfs;
            foreach ($pdfs as $key => $pdf) {
                $pdf['premium_kitchenware_id'] = $premiumKitchenwareEquipment->id;
                PremiumKitchenwareEquipmentPdf::create($pdf);
            }
        }
        if(isset($request->premium_kitchen_ware_customers)){
            $customers = $request->premium_kitchen_ware_customers;
            foreach ($customers as $key => $customer) {
                $customer['premium_kitchenware_id'] = $premiumKitchenwareEquipment->id;
                PremiumKitchenwareEquipmentUser::create($customer);
            }
        }

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $premiumKitchenwareEquipment = PremiumKitchenwareEquipment::find($id);
        if($premiumKitchenwareEquipment){
            $videoFiles = $premiumKitchenwareEquipment->videoFiles;
            $pdfFiles = $premiumKitchenwareEquipment->pdfFiles;
            $customers = $premiumKitchenwareEquipment->customers;
            if($videoFiles){
                foreach ($videoFiles as $key => $videoFile) {
                    $videoFile->delete();
                }
            }
            if($pdfFiles){
                foreach ($pdfFiles as $key => $pdfFile) {
                    $pdfFile->delete();
                }
            }
            if($customers){
                foreach ($customers as $key => $customer) {
                    $customer->delete();
                }
            }
        }

        return response()->json([
            'msg_return' => 'ลบสำเร็จ',
            'code_return' => 1,
        ]);
    }
}
