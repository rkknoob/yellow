<?php

namespace YellowProject\Http\Controllers\API\v1\Ecommerce;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\Product;
use YellowProject\Ecommerce\ProductCategory;
use YellowProject\Ecommerce\ProductMeta;
use YellowProject\Ecommerce\ProductDiscount;
use YellowProject\Ecommerce\ProductImage;
use YellowProject\Ecommerce\ProductRelatedProduct;
use YellowProject\TrackingBc;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $datas = [];
        $products = Product::all();
        foreach ($products as $key => $product) {
            $productDiscount = $product->productDiscount;
            $datas[$key]['id'] = $product->id;
            $datas[$key]['name'] = $product->name;
            $datas[$key]['price'] = $product->price;
            $datas[$key]['start_date'] = $product->start_date;
            $datas[$key]['end_date'] = $product->end_date;
            $datas[$key]['status'] = $product->status;
            $datas[$key]['product_brand'] = $product->product_brand;
            $datas[$key]['product_name'] = $product->product_name;
            $datas[$key]['product_size'] = $product->product_size;
            $datas[$key]['price_unit'] = $product->price_unit;
            $datas[$key]['created_at'] = $product->created_at;
            $datas[$key]['updated_at'] = $product->updated_at;
            $datas[$key]['categories'] = [];
            $productCategories = $product->productCategories;
            foreach ($productCategories as $categoryKey => $productCategory) {
                $category = $productCategory->category;
                $datas[$key]['categories'][$categoryKey]['id'] = $category->id;
                $datas[$key]['categories'][$categoryKey]['is_sub_category'] = $category->is_sub_category;
                $datas[$key]['categories'][$categoryKey]['main_category_id'] = $category->main_category_id;
                $datas[$key]['categories'][$categoryKey]['name'] = $category->name;
                $datas[$key]['categories'][$categoryKey]['desc'] = $category->desc;
                $datas[$key]['categories'][$categoryKey]['meta_title'] = $category->meta_title;
                $datas[$key]['categories'][$categoryKey]['meta_keywords'] = $category->meta_keywords;
                $datas[$key]['categories'][$categoryKey]['meta_desc'] = $category->meta_desc;
                $datas[$key]['categories'][$categoryKey]['status'] = $category->status;
            }
            $datas[$key]['sku'] = $product->sku;
            $datas[$key]['sale_price'] = ($productDiscount)? $productDiscount->discount_price : null;
        }

        return response()->json([
            'datas' => $datas,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if(isset($request->tracking_url)){
            $code = $request['tracking_url']['code'];
            $trackingBcSearch = TrackingBc::where('code',$code)->first();
            if($trackingBcSearch){
              return response()->json([
                  'msg_return' => 'ชื่อ Tracking ซ้ำกัน',
                  'code_return' => 21,
              ]);
            }
        }

        $baseUrl = \URL::to('/');
        $trackingBcDatas = $request['tracking_url'];
        if($request['tracking_url']['is_route_name'] == 0){
            $trackingBcDatas['code'] = TrackingBc::generateCode();
        }
        $trackingBcDatas['generated_short_url'] = $baseUrl."/bc/".$trackingBcDatas['code'];
        $trackingBcDatas['generated_full_url'] = $baseUrl."/bc/".$trackingBcDatas['code']."?"."yl_source=".$trackingBcDatas['tracking_source']."&yl_campaign=".$trackingBcDatas['tracking_campaign']."&yl_ref=".$trackingBcDatas['tracking_ref'];
        // $trackingBcDatas = $request['tracking_url'];
        $trackingBc = TrackingBc::create($trackingBcDatas);
        $request['tracking_bc_id'] = $trackingBc->id;

        $product = Product::create($request->all());
        $trackingBc->update([
            'original_url' => $baseUrl."/full-ecommerce#/detail/".$product->id
        ]);
        if(isset($request->categories)){
            $categories = $request->categories;
            foreach ($categories as $key => $category) {
                ProductCategory::create([
                    'ecommerce_product_id' => $product->id,
                    'ecommerce_category_id' => $category,
                ]);
            }
        }
        if(isset($request->meta)){
            $productMeta = $request->meta;
            $productMeta['ecommerce_product_id'] = $product->id;
            ProductMeta::create($productMeta);
        }
        if(isset($request->discount)){
            $productDiscount = $request->discount;
            $productDiscount['ecommerce_product_id'] = $product->id;
            ProductDiscount::create($productDiscount);
        }
        if(isset($request->images)){
            $productImages = $request->images;
            foreach ($productImages as $key => $productImage) {
                $productImage['ecommerce_product_id'] = $product->id;
                ProductImage::create($productImage);
            }
        }
        if(isset($request->product_related_products)){
            $productRelatedProducts = $request->product_related_products;
            foreach ($productRelatedProducts as $key => $productRelatedProduct) {
                $productRelatedProduct['ecommerce_product_id'] = $product->id;
                ProductRelatedProduct::create($productRelatedProduct);
            }
        }


        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
            'id' => $product->id,
            'tracking_bc' => $product->trackingBc,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $datas = [];
        $product = Product::find($id);
        $trackingBc = $product->trackingBc;
        $productCategories = ProductCategory::where('ecommerce_product_id',$product->id)->get();
        $productMeta = ProductMeta::where('ecommerce_product_id',$product->id)->first();
        $productDiscount = ProductDiscount::where('ecommerce_product_id',$product->id)->first();
        $productImages = ProductImage::where('ecommerce_product_id',$product->id)->get();
        $productRelatedProducts = ProductRelatedProduct::where('ecommerce_product_id',$product->id)->get();
        $datas['id'] = $product->id;
        $datas['name'] = $product->name;
        $datas['desc'] = $product->desc;
        $datas['short_desc'] = $product->short_desc;
        $datas['start_date'] = $product->start_date;
        $datas['end_date'] = $product->end_date;
        $datas['sku'] = $product->sku;
        $datas['price'] = $product->price;
        $datas['price_unit'] = $product->price_unit;
        $datas['shipping_cost'] = $product->shipping_cost;
        $datas['vat'] = $product->vat;
        $datas['source_order'] = $product->source_order;
        $datas['out_of_stock_status'] = $product->out_of_stock_status;
        $datas['status'] = $product->status;
        $datas['reward_point'] = $product->reward_point;
        $datas['product_brand'] = $product->product_brand;
        $datas['product_name'] = $product->product_name;
        $datas['product_size'] = $product->product_size;
        $datas['categories'] = [];
        $datas['meta'] = [];
        $datas['discount'] = [];
        $datas['images'] = [];
        $datas['product_related_products'] = [];
        $datas['tracking_url'] = [];
        if($trackingBc){
            $datas['tracking_url'] = $trackingBc->toArray();
        }
        if($productCategories->count() > 0){
            foreach ($productCategories as $key => $productCategory) {
                $datas['categories'][$key] = $productCategory->ecommerce_category_id;
            }
        }
        if($productMeta){
            $datas['meta']['title'] = $productMeta->title;
            $datas['meta']['keyword'] = $productMeta->keyword;
            $datas['meta']['desc'] = $productMeta->desc;
        }
        if($productDiscount){
            $datas['discount']['priority'] = $productDiscount->priority;
            $datas['discount']['discount_price'] = $productDiscount->discount_price;
            $datas['discount']['start_date'] = $productDiscount->start_date;
            $datas['discount']['end_date'] = $productDiscount->end_date;
            $datas['discount']['created_at'] = $productDiscount->created_at;
            $datas['discount']['updated_at'] = $productDiscount->updated_at;
        }
        if($productImages->count() > 0){
            foreach ($productImages as $key => $productImage) {
                $imageData = $productImage->image;
                $datas['images'][$key]['img_url'] = "";
                $datas['images'][$key]['img_size'] = "";
                if($imageData){
                    $datas['images'][$key]['img_url'] = $imageData->img_url;
                    $datas['images'][$key]['img_size'] = $imageData->img_size;
                }
                $datas['images'][$key]['product_img_id'] = $productImage->product_img_id;
                $datas['images'][$key]['name'] = $productImage->name;
                $datas['images'][$key]['seq'] = $productImage->seq;
                $datas['images'][$key]['is_base'] = $productImage->is_base;
                $datas['images'][$key]['is_small'] = $productImage->is_small;
                $datas['images'][$key]['is_thumbnail'] = $productImage->is_thumbnail;
            }
        }
        if($productRelatedProducts->count() > 0){
            foreach ($productRelatedProducts as $key => $productRelatedProduct) {
                // $datas['product_related_products'][$key]['value'] = $productRelatedProduct->value;
                $datas['product_related_products'][$key] = $productRelatedProduct;
            }
        }

        return response()->json([
            'datas' => $datas,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $product = Product::find($id);
        $baseUrl = \URL::to('/');
        $trackingBc = TrackingBc::find($product->tracking_bc_id);
        if($trackingBc){
            if(isset($request->tracking_url) && $request['tracking_url']['code'] != "" && $trackingBc->code != $request['tracking_url']['code']){
                $trackingBcSearch = TrackingBc::where('code',$request['tracking_url']['code'])->first();
                if($trackingBcSearch){
                  return response()->json([
                      'msg_return' => 'ชื่อ Tracking ซ้ำกัน',
                      'code_return' => 21,
                  ]);
                }
            }
            $trackingBcDatas = $request['tracking_url'];
            // if($request['tracking_url']['is_route_name'] == 0){
            //     $trackingBcDatas['code'] = TrackingBc::generateCode();
            // }
            
            // $request['original_url'] = $baseUrl.'/dashboard?type=coupon#/coupon/'.$coupon->id;
            // $trackingBcDatas['original_url'] = 'original_url' => $baseUrl."/full-ecommerce#/detail/".$product->id;
            $trackingBcDatas['generated_short_url'] = $baseUrl."/bc/".$trackingBcDatas['code'];
            $trackingBcDatas['generated_full_url'] = $baseUrl."/bc/".$trackingBcDatas['code']."?"."yl_source=".$trackingBcDatas['tracking_source']."&yl_campaign=".$trackingBcDatas['tracking_campaign']."&yl_ref=".$trackingBcDatas['tracking_ref'];
            $trackingBc->update($trackingBcDatas);
            $request['tracking_bc_id'] = $trackingBc->id;
        }else{
            if(isset($request->tracking_url) && $request['tracking_url']['code'] != ""){
                $trackingBcSearch = TrackingBc::where('code',$request['tracking_url']['code'])->first();
                if($trackingBcSearch){
                  return response()->json([
                      'msg_return' => 'ชื่อ Tracking ซ้ำกัน',
                      'code_return' => 21,
                  ]);
                }
            }

            $baseUrl = \URL::to('/');
            $trackingBcDatas = $request['tracking_url'];
            if($request['tracking_url']['is_route_name'] == 0){
                $trackingBcDatas['code'] = TrackingBc::generateCode();
            }
            // $request['original_url'] = $baseUrl.'/dashboard?type=coupon#/coupon/'.$coupon->id;
            $trackingBcDatas['original_url'] = $baseUrl."/full-ecommerce#/detail/".$product->id;
            $trackingBcDatas['generated_short_url'] = $baseUrl."/bc/".$trackingBcDatas['code'];
            $trackingBcDatas['generated_full_url'] = $baseUrl."/bc/".$trackingBcDatas['code']."?"."yl_source=".$trackingBcDatas['tracking_source']."&yl_campaign=".$trackingBcDatas['tracking_campaign']."&yl_ref=".$trackingBcDatas['tracking_ref'];
            $trackingBc = TrackingBc::create($trackingBcDatas);
            $request['tracking_bc_id'] = $trackingBc->id;
        }

        $product->update($request->all());
        $productCategories = ProductCategory::where('ecommerce_product_id',$product->id);
        $productMeta = ProductMeta::where('ecommerce_product_id',$product->id)->first();
        $productDiscount = ProductDiscount::where('ecommerce_product_id',$product->id)->first();
        $productImages = ProductImage::where('ecommerce_product_id',$product->id);
        $productRelatedProducts = ProductRelatedProduct::where('ecommerce_product_id',$product->id);
        if($productCategories->count() > 0){
            $productCategories->delete();
        }

        if(isset($request->categories)){
            $categories = $request->categories;
            foreach ($categories as $key => $category) {
                ProductCategory::create([
                    'ecommerce_product_id' => $product->id,
                    'ecommerce_category_id' => $category,
                ]);
            }
        }

        if(isset($request->meta)){
            $productMetaData = $request->meta;
            if($productMeta){
                $productMeta->update($productMetaData);
            }else{
                $productMetaData['ecommerce_product_id'] = $product->id;
                ProductMeta::create($productMetaData);
            }
        }

        if(isset($request->discount)){
            $productDiscountData = $request->discount;
            if($productDiscount){
                $productDiscount->update($productDiscountData);
            }else{
                $productDiscountData['ecommerce_product_id'] = $product->id;
                ProductDiscount::create($productDiscountData);
            }
            
        }

        if($productImages->count() > 0){
            $productImages->delete();
        }

        if(isset($request->images)){
            $productImages = $request->images;
            foreach ($productImages as $key => $productImage) {
                $productImage['ecommerce_product_id'] = $product->id;
                ProductImage::create($productImage);
            }
        }

        if($productRelatedProducts->count() > 0){
            $productRelatedProducts->delete();
        }

        if(isset($request->product_related_products)){
            $productRelatedProducts = $request->product_related_products;
            foreach ($productRelatedProducts as $key => $productRelatedProduct) {
                $productRelatedProduct['ecommerce_product_id'] = $product->id;
                ProductRelatedProduct::create($productRelatedProduct);
            }
        }

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
            'id' => $product->id,
            'tracking_bc' => $product->trackingBc,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $product = Product::find($id);
        $productCategories = ProductCategory::where('ecommerce_product_id',$product->id);
        if($productCategories->count() > 0){
            $productCategories->delete();
        }
        $productMeta = ProductMeta::where('ecommerce_product_id',$product->id)->first();
        if($productMeta){
            $productMeta->delete();
        }
        $productDiscount = ProductDiscount::where('ecommerce_product_id',$product->id)->first();
        if($productDiscount){
            $productDiscount->delete();
        }
        $productImages = ProductImage::where('ecommerce_product_id',$product->id);
        if($productImages->count() > 0){
            $productImages->delete();
        }
        $productRelatedProducts = ProductRelatedProduct::where('ecommerce_product_id',$product->id);
        if($productRelatedProducts->count() > 0){
            $productRelatedProducts->delete();
        }
        $trackingBc = TrackingBc::find($product->tracking_bc_id);
        if($trackingBc){
            $trackingBc->delete();
        }
        $product->delete();

        return response()->json([
            'msg_return' => 'ลบสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function getDataProductByCategory($id)
    {

        $categoryId = $id;
        $datas = [];
        $products = Product::whereHas('productCategories', function ($query) use ($categoryId) {
            $query->where('ecommerce_category_id',$categoryId);
        })->get();
        foreach ($products as $key => $product) {
            $datas[$key]['id'] = $product->id;
            $datas[$key]['name'] = $product->name;
            $datas[$key]['price'] = $product->price;
            $datas[$key]['start_date'] = $product->start_date;
            $datas[$key]['end_date'] = $product->end_date;
            $datas[$key]['status'] = $product->status;
            $datas[$key]['categories'] = [];
        }

        return response()->json([
            'datas' => $datas,
        ]);
    }

    public function onlyName()
    {

        $datas = [];
        $products = Product::all();
        foreach ($products as $key => $product) {
            $datas[$key]['id'] = $product->id;
            $datas[$key]['name'] = $product->name;
            $datas[$key]['product_name'] = $product->product_name;
        }

        return response()->json([
            'datas' => $datas,
        ]);
    }

    public function onlyDiscount($id)
    {

        $datas = [];
        $product = Product::find($id);
        $productDiscount = ProductDiscount::where('ecommerce_product_id',$product->id)->first();
        $datas['id'] = $product->id;
        $datas['price'] = $product->price;
        $datas['discount'] = [];
        if($productDiscount){
            $datas['discount']['priority'] = $productDiscount->priority;
            $datas['discount']['discount_price'] = $productDiscount->discount_price;
            $datas['discount']['start_date'] = $productDiscount->start_date;
            $datas['discount']['end_date'] = $productDiscount->end_date;
            $datas['discount']['created_at'] = $productDiscount->created_at;
            $datas['discount']['updated_at'] = $productDiscount->updated_at;
        }

        return response()->json([
            'datas' => $datas,
        ]);
    }
}
