<?php

namespace YellowProject\Http\Controllers\API\v1\Ecommerce;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\CustomPage\CustomPage;
use YellowProject\TrackingBc;

class CustomPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $customPages = CustomPage::orderBy('created_at','desc')->get();

        return response()->json([
            'datas' => $customPages,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        if(isset($request->tracking_url)){
            $code = $request['tracking_url']['code'];
            $trackingBcSearch = TrackingBc::where('code',$code)->first();
            if($trackingBcSearch){
              return response()->json([
                  'msg_return' => 'ชื่อ Tracking ซ้ำกัน',
                  'code_return' => 21,
              ]);
            }
        }

        $baseUrl = \URL::to('/');
        $trackingBcDatas = $request['tracking_url'];
        if($request['tracking_url']['is_route_name'] == 0){
            $trackingBcDatas['code'] = TrackingBc::generateCode();
        }

        $request['url'] = $baseUrl."/full-ecommerce#/custom/".TrackingBc::generateCode();
        $trackingBcDatas['original_url'] = $request->url;
        $trackingBcDatas['code'] = TrackingBc::generateCode();
        $trackingBcDatas['generated_short_url'] = $baseUrl."/bc/".$trackingBcDatas['code'];
        $trackingBcDatas['generated_full_url'] = $baseUrl."/bc/".$trackingBcDatas['code']."?"."yl_source=".$trackingBcDatas['tracking_source']."&yl_campaign=".$trackingBcDatas['tracking_campaign']."&yl_ref=".$trackingBcDatas['tracking_ref'];
        $trackingBc = TrackingBc::create($trackingBcDatas);
        $request['tracking_bc_id'] = $trackingBc->id;

        $customPage = CustomPage::create($request->all());

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
            'tracking_url' => $trackingBc,
            'custom_page_id' => $customPage->id,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $datas = [];
        $customPage = CustomPage::find($id);
        $datas = $customPage;
        $trackingBc = TrackingBc::find($customPage->tracking_bc_id);
        $datas['tracking_url'] = $trackingBc;
        // $trackingBc = $customPage->trackingBc;
        
        return response()->json([
            'datas' => $datas,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $customPage = CustomPage::find($id);

        $baseUrl = \URL::to('/');
        $trackingBc = TrackingBc::find($customPage->tracking_bc_id);
        if($trackingBc){
            if(isset($request->tracking_url) && $request['tracking_url']['code'] != "" && $trackingBc->code != $request['tracking_url']['code']){
                $trackingBcSearch = TrackingBc::where('code',$request['tracking_url']['code'])->first();
                if($trackingBcSearch){
                  return response()->json([
                      'msg_return' => 'ชื่อ Tracking ซ้ำกัน',
                      'code_return' => 21,
                  ]);
                }
            }
            $trackingBcDatas = $request['tracking_url'];
            if($request['tracking_url']['is_route_name'] == 0){
                $trackingBcDatas['code'] = TrackingBc::generateCode();
            }
            // $request['original_url'] = $baseUrl.'/dashboard?type=coupon#/coupon/'.$coupon->id;
            // $trackingBcDatas['original_url'] = 'original_url' => $baseUrl."/full-ecommerce#/detail/".$product->id;
            $trackingBcDatas['generated_short_url'] = $baseUrl."/bc/".$trackingBcDatas['code'];
            $trackingBcDatas['generated_full_url'] = $baseUrl."/bc/".$trackingBcDatas['code']."?"."yl_source=".$trackingBcDatas['tracking_source']."&yl_campaign=".$trackingBcDatas['tracking_campaign']."&yl_ref=".$trackingBcDatas['tracking_ref'];
            $trackingBc->update($trackingBcDatas);
            $request['tracking_bc_id'] = $trackingBc->id;
        }else{
            if(isset($request->tracking_url) && $request['tracking_url']['code'] != ""){
                $trackingBcSearch = TrackingBc::where('code',$request['tracking_url']['code'])->first();
                if($trackingBcSearch){
                  return response()->json([
                      'msg_return' => 'ชื่อ Tracking ซ้ำกัน',
                      'code_return' => 21,
                  ]);
                }
            }

            $baseUrl = \URL::to('/');
            $trackingBcDatas = $request['tracking_url'];
            if($request['tracking_url']['is_route_name'] == 0){
                $trackingBcDatas['code'] = TrackingBc::generateCode();
            }
            // $request['original_url'] = $baseUrl.'/dashboard?type=coupon#/coupon/'.$coupon->id;
            $request['url'] = $baseUrl."/full-ecommerce#/custom/".TrackingBc::generateCode();
            $trackingBcDatas['original_url'] = $request->url;
            $trackingBcDatas['generated_short_url'] = $baseUrl."/bc/".$trackingBcDatas['code'];
            $trackingBcDatas['generated_full_url'] = $baseUrl."/bc/".$trackingBcDatas['code']."?"."yl_source=".$trackingBcDatas['tracking_source']."&yl_campaign=".$trackingBcDatas['tracking_campaign']."&yl_ref=".$trackingBcDatas['tracking_ref'];
            $trackingBc = TrackingBc::create($trackingBcDatas);
            $request['tracking_bc_id'] = $trackingBc->id;
        }

        // $trackingBc = TrackingBc::find($customPage->tracking_bc_id);
        $customPage->update($request->all());

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
            'tracking_url' => $trackingBc,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $customPage = CustomPage::find($id);
        $trackingBc = TrackingBc::find($customPage->tracking_bc_id);
        if($trackingBc){
            $trackingBc->delete();
        }
        $customPage->delete();

        return response()->json([
            'msg_return' => 'ลบสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function getDataByOriginalURL(Request $request)
    {
        $url = $request->original_url;
        $datas = \DB::table('dim_tracking_bc')
            ->leftJoin('dim_ecommerce_custom_page', 'dim_tracking_bc.id', '=', 'dim_ecommerce_custom_page.tracking_bc_id')
            ->select('dim_ecommerce_custom_page.id'
              ,'dim_ecommerce_custom_page.name'
              ,'dim_ecommerce_custom_page.content_body'
              ,'dim_ecommerce_custom_page.img_header'
              ,'dim_ecommerce_custom_page.url'
              ,'dim_ecommerce_custom_page.is_active'
              ,'dim_tracking_bc.original_url'
              ,'dim_tracking_bc.tracking_source'
              ,'dim_tracking_bc.tracking_campaign'
              ,'dim_tracking_bc.tracking_ref'
              ,'dim_tracking_bc.generated_full_url'
              ,'dim_tracking_bc.generated_short_url'
              ,'dim_tracking_bc.desc'
              ,'dim_tracking_bc.is_route_name'
              ,'dim_tracking_bc.campaign_id'
            )
            ->where('dim_tracking_bc.original_url',$url)
            ->whereNotNull('dim_ecommerce_custom_page.id')
            ->first();

        return response()->json([
            'datas' => $datas,
        ]);
    }
}
