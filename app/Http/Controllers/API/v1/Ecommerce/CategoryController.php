<?php

namespace YellowProject\Http\Controllers\API\v1\Ecommerce;

use App\Product\Products;
use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\Category;
use YellowProject\Ecommerce\CategoryImage;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $datas = [];

        $customers = Products::all();
        foreach ($customers as $key => $customer) {
            $customerTaxBillingAddress = $customer->customerTaxBillingAddress;
            $customerShippingAddresses = $customer->customerShippingAddresses;
            if($customerShippingAddresses){
                $customerShippingAddresses = $customerShippingAddresses->first();
            }
            $datas[$key]['id'] = $customer->id;
            $datas[$key]['no'] = $runningNumber;
            $datas[$key]['customer_id'] = $customer->id;
            $datas[$key]['customer_code'] = $customer->customer_code;
            $datas[$key]['customer_name'] = $customer->first_name;
            $datas[$key]['customer_last_name'] = $customer->last_name;
            $datas[$key]['customer_phone_number'] = $customer->phone_number;
            $datas[$key]['customer_post_code'] = ($customerShippingAddresses)? $customerShippingAddresses->post_code : "";
            $datas[$key]['customer_province'] = ($customerShippingAddresses)? $customerShippingAddresses->province : "";
            $datas[$key]['customer_register_date'] = $customer->created_at->format('d/m/Y H:i');
            $runningNumber++;
        }

        return response()->json([
            'datas' => $datas,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $category = Category::create($request->all());
        
        if(isset($request->images)){
            $categoryImages = $request->images;
            foreach ($categoryImages as $key => $categoryImage) {
                $categoryImage['ecommerce_category_id'] = $category->id;
                CategoryImage::create($categoryImage);
            }
        }

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $datas = [];
        $category = Category::find($id);
        $categoryImages = CategoryImage::where('ecommerce_category_id',$category->id)->get();
        $datas['id'] = $category->id;
        $datas['is_sub_category'] = $category->is_sub_category;
        $datas['main_category_id'] = $category->main_category_id;
        $datas['name'] = $category->name;
        $datas['desc'] = $category->desc;
        $datas['meta_title'] = $category->meta_title;
        $datas['meta_keywords'] = $category->meta_keywords;
        $datas['meta_desc'] = $category->meta_desc;
        $datas['status'] = $category->status;
        $datas['products'] = [];
        $datas['images'] = [];
        $productCategories = $category->productCategories;
        if($productCategories){
            foreach ($productCategories as $index => $productCategory) {
                $product = $productCategory->product;
                $productDiscount = $product->productDiscount;
                $datas['products'][$index]['id'] = $product->id;
                $datas['products'][$index]['name'] = $product->name;
                $datas['products'][$index]['category'] = $category->name;
                $datas['products'][$index]['sku'] = $product->sku;
                $datas['products'][$index]['price'] = $product->price;
                $datas['products'][$index]['sale_price'] = ($productDiscount)? $productDiscount->discount_price : null;
                $datas['products'][$index]['created_at'] = $product->created_at->format('d/m/Y');
                $datas['products'][$index]['status'] = $product->status;
            }
        }
        if($categoryImages->count() > 0){
            foreach ($categoryImages as $key => $categoryImage) {
                $imageData = $categoryImage->image;
                $datas['images'][$key]['img_url'] = "";
                $datas['images'][$key]['img_size'] = "";
                if($imageData){
                    $datas['images'][$key]['img_url'] = $imageData->img_url;
                    $datas['images'][$key]['img_size'] = $imageData->img_size;
                }
                $datas['images'][$key]['product_img_id'] = $categoryImage->product_img_id;
                $datas['images'][$key]['name'] = $categoryImage->name;
                $datas['images'][$key]['seq'] = $categoryImage->seq;
                $datas['images'][$key]['is_base'] = $categoryImage->is_base;
                $datas['images'][$key]['is_small'] = $categoryImage->is_small;
                $datas['images'][$key]['is_thumbnail'] = $categoryImage->is_thumbnail;
            }
        }

        return response()->json([
            'datas' => $datas,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $category = Category::find($id);
        $category->update($request->all());
        $categoryImages = CategoryImage::where('ecommerce_category_id',$category->id);
        if($categoryImages->count() > 0){
            $categoryImages->delete();
        }
        if(isset($request->images)){
            $categoryImages = $request->images;
            foreach ($categoryImages as $key => $categoryImage) {
                $categoryImage['ecommerce_category_id'] = $category->id;
                CategoryImage::create($categoryImage);
            }
        }

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $category = Category::find($id);
        $categoryImages = CategoryImage::where('ecommerce_category_id',$category->id);
        if($categoryImages->count() > 0){
            $categoryImages->delete();
        }
        $category->delete();

        return response()->json([
            'msg_return' => 'ลบสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function getSubMainCategory()
    {

        $datas = [];
        $categories = Category::where('is_sub_category',1)->get();
        foreach ($categories as $key => $category) {
            $datas[$key]['id'] = $category->id;
            $datas[$key]['is_sub_category'] = $category->is_sub_category;
            $datas[$key]['main_category_id'] = $category->main_category_id;
            $datas[$key]['name'] = $category->name;
            $datas[$key]['desc'] = $category->desc;
            $datas[$key]['status'] = $category->status;
            $datas[$key]['products'] = [];
            $productCategories = $category->productCategories;
            if($productCategories){
                foreach ($productCategories as $index => $productCategory) {
                    $product = $productCategory->product;
                    $datas[$key]['products'][$index]['id'] = $product->id;
                    $datas[$key]['products'][$index]['name'] = $product->name;
                    $datas[$key]['products'][$index]['status'] = $product->status;
                }
            }
        }

        return response()->json([
            'datas' => $datas,
        ]);
    }
}
