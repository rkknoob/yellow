<?php

namespace YellowProject\Http\Controllers\API\v1\Ecommerce;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\ProductRecommend\ProductRecommend;

class ProductRecommendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $datas = [];
        $productRecommends = ProductRecommend::all();
        foreach ($productRecommends as $key => $productRecommend) {
            $datas[$key]['id'] = $productRecommend->id;
            $datas[$key]['name'] = $productRecommend->name;
            $datas[$key]['product_1'] = ($productRecommend->product1)? $productRecommend->product1->name : "";
            $datas[$key]['product_2'] = ($productRecommend->product2)? $productRecommend->product2->name : "";
            $datas[$key]['product_3'] = ($productRecommend->product3)? $productRecommend->product3->name : "";
            $datas[$key]['product_4'] = ($productRecommend->product4)? $productRecommend->product4->name : "";
            $datas[$key]['is_active'] = $productRecommend->is_active;
            $datas[$key]['start_date'] = $productRecommend->start_date;
            $datas[$key]['end_date'] = $productRecommend->end_date;
        }

        return response()->json([
            'datas' => $datas,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $productRecommendActive = ProductRecommend::where('is_active',1)->first();
        if($productRecommendActive){
            if($request->is_active == 1){
                return response()->json([
                    'msg_return' => 'ไม่สามารถบันทึกข้อมูลได้เนื่องจากมีอันอื่นที่ active อยู่',
                    'code_return' => 2,
                ]);
            }
        }
        ProductRecommend::create($request->all());

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $productRecommend = ProductRecommend::find($id);
        if(!$productRecommend){
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูล',
                'code_return' => 2,
            ]);
        }

        return response()->json([
            'datas' => $productRecommend,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $productRecommend = ProductRecommend::find($id);
        if($productRecommend->is_active != $request->is_active){
            if($request->is_active == 1){
                $productRecommendActive = ProductRecommend::where('is_active',1)->first();
                if($productRecommendActive){
                    return response()->json([
                        'msg_return' => 'ไม่สามารถบันทึกข้อมูลได้เนื่องจากมีอันอื่นที่ active อยู่',
                        'code_return' => 2,
                    ]);
                }
            }
        }
        $productRecommend->update($request->all());
        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $productRecommend = ProductRecommend::find($id);
        if($productRecommend->is_active == 1){
            return response()->json([
                'msg_return' => 'ไม่สามารถลบได้เนื่องจากมีสถานะ Active',
                'code_return' => 2,
            ]);
        }

        return response()->json([
            'msg_return' => 'ลบข้อมูลสำเร็จ',
            'code_return' => 1,
        ]);
    }
}
