<?php

namespace YellowProject\Http\Controllers\API\v1\Ecommerce\LineMessageTemplate;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\LineTemplateMessage\OrderConfirmationAfterPurchase\OrderConfirmationAfterPurchase as LineTemplate;
use YellowProject\Ecommerce\LineTemplateMessage\OrderConfirmationAfterPurchase\OrderConfirmationAfterPurchaseItem as LineTemplateItem;
use YellowProject\Ecommerce\LineTemplateMessage\OrderConfirmationAfterPurchase\OrderConfirmationAfterPurchaseMessage as LineTemplateMessage;
use YellowProject\Ecommerce\LineTemplateMessage\OrderConfirmationAfterPurchase\OrderConfirmationAfterPurchaseSticker as LineTemplateSticker;
use YellowProject\LineMessageType;

class OrderConfirmationAfterPurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lineTemplates  = LineTemplate::all();
        
        return response()->json([
            'datas'          => $lineTemplates,
            'countAutoReply' => $lineTemplates->count()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lineTemplate = LineTemplate::where('active',1)->first();
        // if($request->active == 1 && $lineTemplate){
        //     return response()->json([
        //         'msg_return' => 'LINE ORDER CONFIRMATION IS ACTIVE',
        //         'code_return' => 2,
        //     ]);
        // }
        $arrErrsField = array();
        $colectErrsField= collect();
        if (!isset($request->title) || (isset($request->title) && trim($request->title) == "") ) {
            $arrErrsField[] = 'title';
        }
        if (sizeof($arrErrsField) > 0) {

            return response()->json([
                'msg_return' => 'VALID_REQUIRE',
                'code_return' => 1,
                'items'       => $arrErrsField
            ]);
        }

        $dup = LineTemplate::checkDuplicate($request->title);
        if ($dup) {

            return response()->json([
                'msg_return' => 'VALID_REQUIRE',
                'code_return' => 1,
                'items'       => ['title']
            ]);
        }

        $lineTemplate = LineTemplate::firstOrNew([
            // 'active'                => false,
            'active'                => $request->active,
            'title'                 => $request->title,
            'alt_text'              => $request->alt_text
        ]);

        if ($lineTemplate->exists) {
            $arrErrsField[] = 'title';

            return response()->json([
                'msg_return' => 'VALID_REQUIRE',
                'code_return' => 1,
                'items'       => ['title']
            ]);

        } else {
            // autoReplyDefault created from 'new'; does not exist in database.
            $lineTemplate->save();
            if (isset($request->items) && sizeof($request->items) > 0) {
                foreach ($request->items as $item) {
                    switch ($item['type']) {
                        case 'text':
                            foreach ($item['value'] as $value) {
                                $lineTemplateMessage = new LineTemplateMessage([
                                    'message' => $value['playload'],
                                    'display' => $value['display'],
                                    //'display'
                                ]);
                            }
                            $lineTemplateMessage->save();
                            $lineTemplateItem = new LineTemplateItem([
                                'ecommerce_line_temp_order_confirmation_after_purchase_id' => $lineTemplate->id,
                                'message_type_id'           => $item['type'],
                                'seq_no'                    => $item['seq_no'],
                                'message_id'                => $lineTemplateMessage->id,
                                'sticker_id'                => null,
                                'richmessage_id'            => null,
                            ]);
                            $lineTemplateItem->save();
                           break;
                        case 'sticker':
                            foreach ($item['value'] as $value) {
                                $lineTemplateSticker = new LineTemplateSticker([
                                    'packageId' => $value['package_id'],
                                    'stickerId' => $value['stricker_id'],
                                    'display'   => $value['display'],
                                ]);
                            }
                            $lineTemplateSticker->save();
                            $lineTemplateItem = new LineTemplateItem([
                                'ecommerce_line_temp_order_confirmation_after_purchase_id' => $lineTemplate->id,
                                'message_type_id'           => $item['type'],
                                'seq_no'                    => $item['seq_no'],
                                'message_id'                => null,
                                'sticker_id'                => $lineTemplateSticker->id,
                                'richmessage_id'            => null,
                            ]);    
                            $lineTemplateItem->save();
                           break;
                        case 'imagemap':
                            $lineTemplateItem = new LineTemplateItem([
                                'ecommerce_line_temp_order_confirmation_after_purchase_id' => $lineTemplate->id,
                                'message_type_id'           => $item['type'],
                                'seq_no'                    => $item['seq_no'],
                                'message_id'     => null,
                                'sticker_id'     => null,
                                'richmessage_id' => $item['value'][0]['auto_reply_richmessage_id'],
                            ]);
                            $lineTemplateItem->save();
                           break;
                       default:
                           # code...
                           break;
                    }
                }
            }

            return response()->json([
                'msg_return' => 'INSERT_SUCCESS',
                'code_return' => 1,
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lineTemplate  = LineTemplate::find($id);
        if ($lineTemplate) {
            $items  = $lineTemplate->lineItems;

            $arr = array();
            foreach ($items as $item) {
               $arr[] = array(
                    "id" => $item->id ,
                    "seq_no" => $item->seq_no,
                    "type" => $item->messageType->type,
                    "value" => array(
                        $item->show_message
                    )
                );
            }

            $data  = array(
                "id"        => $lineTemplate->id,
                "title"     => $lineTemplate->title,
                "active"    => $lineTemplate->active,
                "alt_text"  => $lineTemplate->alt_text,
                "items"     => $arr
            );

    
            return json_encode($data, JSON_UNESCAPED_UNICODE);
        }

        return response()->json([
            'msg_return' => 'ERR_NOTFOUND',
            'code_return' => 1,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $lineTemplate  = LineTemplate::find($id);
        $lineTemplateActive = LineTemplate::where('active',1)->first();
        // if($lineTemplateActive && $request->active == 1){
        //     if($lineTemplateActive->active != 1){
        //         return response()->json([
        //             'msg_return' => 'LINE ORDER CONFIRMATION IS ACTIVE',
        //             'code_return' => 2,
        //         ]);
        //     }
        // }
        if ($lineTemplate) {

                $arrErrsField = array();
                $colectErrsField= collect();

                if (!isset($request->title) || (isset($request->title) && trim($request->title) == "") ) {
                    $arrErrsField[] = 'title';
                }

                if (sizeof($arrErrsField) > 0) {
                    
                    return response()->json([
                        'msg_return' => 'VALID_REQUIRE',
                        'code_return' => 1,
                        'items'       => ['title']
                    ]);
                }

                $lineTemplate = $lineTemplate->update([
                    'active'        => $request->active,
                    'title'         => $request->title,
                    'alt_text'      => $request->alt_text
                ]);
                $lineTemplate  = LineTemplate::find($id);

                if (isset($request->items) && sizeof($request->items) > 0) {
                  
                    $lineTemplate->lineItems()->delete();
                        
                    foreach ($request->items as $item) {
                            switch ($item['type']) {
                                case 'text':
                                    foreach ($item['value'] as $value) {
                                        $lineTemplateMessage = new LineTemplateMessage([
                                            'message' => $value['playload'],
                                            'display' => $value['display'],
                                            //'display'
                                        ]);
                                    }
                                    $lineTemplateMessage->save();
                                    $lineTemplateItem = new LineTemplateItem([
                                        'ecommerce_line_temp_order_confirmation_after_purchase_id' => $lineTemplate->id,
                                        'message_type_id'           => $item['type'],
                                        'seq_no'                    => $item['seq_no'],
                                        'message_id'                => $lineTemplateMessage->id,
                                        'sticker_id'                => null,
                                        'richmessage_id'            => null,
                                    ]);
                                    $lineTemplateItem->save();
                                   break;
                                case 'sticker':
                                    foreach ($item['value'] as $value) {
                                        $lineTemplateSticker = new LineTemplateSticker([
                                            'packageId' => $value['package_id'],
                                            'stickerId' => $value['stricker_id'],
                                            'display'   => $value['display'],
                                        ]);
                                    }
                                    $lineTemplateSticker->save();
                                    $lineTemplateItem = new LineTemplateItem([
                                        'ecommerce_line_temp_order_confirmation_after_purchase_id' => $lineTemplate->id,
                                        'message_type_id'           => $item['type'],
                                        'seq_no'                    => $item['seq_no'],
                                        'message_id'                => null,
                                        'sticker_id'                => $lineTemplateSticker->id,
                                        'richmessage_id'            => null,
                                    ]);    
                                    $lineTemplateItem->save();
                                   break;
                                case 'imagemap':
                                    $lineTemplateItem = new LineTemplateItem([
                                        'ecommerce_line_temp_order_confirmation_after_purchase_id' => $lineTemplate->id,
                                        'message_type_id'           => $item['type'],
                                        'seq_no'                    => $item['seq_no'],
                                        'message_id'     => null,
                                        'sticker_id'     => null,
                                        'richmessage_id' => $item['value'][0]['auto_reply_richmessage_id'],
                                    ]);
                                    $lineTemplateItem->save();
                                   break;
                               default:
                                   # code...
                                   break;
                            }
                    }

                    return response()->json([
                        'msg_return' => 'UPDATE_SUCCESS',
                        'code_return' => 1,
                    ]);
                }
        }

        return response()->json([
            'msg_return' => 'ERR_NOTFOUND',
            'code_return' => 1,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lineTemplate  = LineTemplate::find($id);
        if ($lineTemplate) {
            if ($lineTemplate->active) {
                return response()->json([
                    'msg_return' => 'ลบไม่สำเร็จ ข้อมูลถูกใช้งานอยู่',
                    'code_return' => 1,
                ]);
            } else {
               $lineTemplate->delete();

                return response()->json([
                    'msg_return' => 'ลบสำเร็จ',
                    'code_return' => 1,
                ]); 
            }
            
        }
    }

    public function postActive(Request $request, $id)
    {

        $lineTemplates = LineTemplate::all();
        foreach ($lineTemplates as  $lineTemplate) {
            $lineTemplate->update([
                    'active'  => false,
            ]);
        }

        $lineTemplate  = LineTemplate::find($id);
        if ($lineTemplate) {
            if ($request->active ) {
                //w8 confirm solution
                $lineTemplate->update([
                    'active'  => true,
                ]);

                return response()->json([
                    'msg_return' => 'บันทึกสำเร็จ',
                    'code_return' => 1,
                ]);
            }
        }
    }
    
}
