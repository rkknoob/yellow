<?php

namespace YellowProject\Http\Controllers\API\v1\Ecommerce\LineMessageTemplate;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\LineTemplateMessage\LineTemplateMessageAuto\CustomerConfirmPaymentBankTransfer;
use YellowProject\Ecommerce\LineTemplateMessage\LineTemplateMessageAuto\CustomerConfirmPaymentCOD;
use YellowProject\Ecommerce\LineTemplateMessage\LineTemplateMessageAuto\OrderConfirmationConfirm;
use YellowProject\Ecommerce\LineTemplateMessage\LineTemplateMessageAuto\OrderConfirmationOutStock;
use YellowProject\Ecommerce\LineTemplateMessage\LineTemplateMessageAuto\ReadyToShip;
use YellowProject\Ecommerce\LineTemplateMessage\LineTemplateMessageAuto\ShippingConfirm;
use YellowProject\Ecommerce\LineTemplateMessage\LineTemplateMessageAuto\OrderEnd;
use YellowProject\Ecommerce\LineTemplateMessage\LineTemplateMessageAuto\FirstRegister;
use YellowProject\Ecommerce\LineTemplateMessage\LineTemplateMessageAuto\CustomerPaymentOverdue48;
use YellowProject\Ecommerce\LineTemplateMessage\LineTemplateMessageAuto\CustomerPaymentOverdue72;
use YellowProject\Ecommerce\LineTemplateMessage\LineTemplateMessageAuto\OrderConfirmationAfterPurchase;
use YellowProject\Ecommerce\LineTemplateMessage\LineTemplateMessageAuto\OrderConfirmationNoStock;

class LineMessageTemplateAutoController extends Controller
{
    public function customerConfirmPaymentBankTransfer(Request $request)
    {

    	$customerConfirmPaymentBankTransfer = CustomerConfirmPaymentBankTransfer::first();
    	if($customerConfirmPaymentBankTransfer){
    		$customerConfirmPaymentBankTransfer->update($request->all());
    	}else{
    		CustomerConfirmPaymentBankTransfer::create($request->all());
    	}
    	return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function getCustomerConfirmPaymentBankTransfer(Request $request)
    {

    	$customerConfirmPaymentBankTransfer = CustomerConfirmPaymentBankTransfer::first();
    	
    	return response()->json([
            'datas' => $customerConfirmPaymentBankTransfer,
        ]);
    }

    public function customerConfirmPaymentCOD(Request $request)
    {

    	$customerConfirmPaymentCOD = CustomerConfirmPaymentCOD::first();
    	if($customerConfirmPaymentCOD){
    		$customerConfirmPaymentCOD->update($request->all());
    	}else{
    		CustomerConfirmPaymentCOD::create($request->all());
    	}
    	return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function getCustomerConfirmPaymentCOD(Request $request)
    {

    	$customerConfirmPaymentCOD = CustomerConfirmPaymentCOD::first();
    	
    	return response()->json([
            'datas' => $customerConfirmPaymentCOD,
        ]);
    }

    public function orderConfirmationConfirm(Request $request)
    {

    	$orderConfirmationConfirm = OrderConfirmationConfirm::first();
    	if($orderConfirmationConfirm){
    		$orderConfirmationConfirm->update($request->all());
    	}else{
    		OrderConfirmationConfirm::create($request->all());
    	}
    	return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function getOrderConfirmationConfirm(Request $request)
    {

    	$orderConfirmationConfirm = OrderConfirmationConfirm::first();
    	
    	return response()->json([
            'datas' => $orderConfirmationConfirm,
        ]);
    }

    public function orderConfirmationOutStock(Request $request)
    {

    	$orderConfirmationOutStock = OrderConfirmationOutStock::first();
    	if($orderConfirmationOutStock){
    		$orderConfirmationOutStock->update($request->all());
    	}else{
    		OrderConfirmationOutStock::create($request->all());
    	}
    	return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function getOrderConfirmationOutStock(Request $request)
    {

    	$orderConfirmationOutStock = OrderConfirmationOutStock::first();
    	
    	return response()->json([
            'datas' => $orderConfirmationOutStock,
        ]);
    }

    public function readyToShip(Request $request)
    {

    	$readyToShip = ReadyToShip::first();
    	if($readyToShip){
    		$readyToShip->update($request->all());
    	}else{
    		ReadyToShip::create($request->all());
    	}
    	return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function getReadyToShip(Request $request)
    {

    	$readyToShip = ReadyToShip::first();
    	
    	return response()->json([
            'datas' => $readyToShip,
        ]);
    }

    public function shippingConfirm(Request $request)
    {

    	$shippingConfirm = ShippingConfirm::first();
    	if($shippingConfirm){
    		$shippingConfirm->update($request->all());
    	}else{
    		ShippingConfirm::create($request->all());
    	}
    	return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function getShippingConfirm(Request $request)
    {

    	$shippingConfirm = ShippingConfirm::first();
    	
    	return response()->json([
            'datas' => $shippingConfirm,
        ]);
    }

    public function endOrder(Request $request)
    {

        $orderEnd = OrderEnd::first();
        if($orderEnd){
            $orderEnd->update($request->all());
        }else{
            OrderEnd::create($request->all());
        }
        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function getEndOrder(Request $request)
    {

        $orderEnd = OrderEnd::first();
        
        return response()->json([
            'datas' => $orderEnd,
        ]);
    }

    public function firstRegister(Request $request)
    {

        $firstRegister = FirstRegister::first();
        if($firstRegister){
            $firstRegister->update($request->all());
        }else{
            FirstRegister::create($request->all());
        }
        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function getFirstRegister(Request $request)
    {

        $firstRegister = FirstRegister::first();
        
        return response()->json([
            'datas' => $firstRegister,
        ]);
    }

    public function paymentOverdue48(Request $request)
    {

        $customerPaymentOverdue48 = CustomerPaymentOverdue48::first();
        if($customerPaymentOverdue48){
            $customerPaymentOverdue48->update($request->all());
        }else{
            CustomerPaymentOverdue48::create($request->all());
        }
        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function getPaymentOverdue48(Request $request)
    {

        $customerPaymentOverdue48 = CustomerPaymentOverdue48::first();
        
        return response()->json([
            'datas' => $customerPaymentOverdue48,
        ]);
    }

    public function paymentOverdue72(Request $request)
    {

        $customerPaymentOverdue72 = CustomerPaymentOverdue72::first();
        if($customerPaymentOverdue72){
            $customerPaymentOverdue72->update($request->all());
        }else{
            CustomerPaymentOverdue72::create($request->all());
        }
        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function getPaymentOverdue72(Request $request)
    {

        $customerPaymentOverdue72 = CustomerPaymentOverdue72::first();
        
        return response()->json([
            'datas' => $customerPaymentOverdue72,
        ]);
    }

    public function orderConfirmationAfterPurchase(Request $request)
    {

        $orderConfirmationAfterPurchase = OrderConfirmationAfterPurchase::first();
        if($orderConfirmationAfterPurchase){
            $orderConfirmationAfterPurchase->update($request->all());
        }else{
            OrderConfirmationAfterPurchase::create($request->all());
        }
        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function getOrderConfirmationAfterPurchase(Request $request)
    {

        $orderConfirmationAfterPurchase = OrderConfirmationAfterPurchase::first();
        
        return response()->json([
            'datas' => $orderConfirmationAfterPurchase,
        ]);
    }

    public function orderConfirmationNoStock(Request $request)
    {

        $orderConfirmationNoStock = OrderConfirmationNoStock::first();
        if($orderConfirmationNoStock){
            $orderConfirmationNoStock->update($request->all());
        }else{
            OrderConfirmationNoStock::create($request->all());
        }
        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function getOrderConfirmationNoStock(Request $request)
    {

        $orderConfirmationNoStock = OrderConfirmationNoStock::first();
        
        return response()->json([
            'datas' => $orderConfirmationNoStock,
        ]);
    }
}
