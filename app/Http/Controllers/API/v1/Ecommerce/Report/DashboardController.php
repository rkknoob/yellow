<?php

namespace YellowProject\Http\Controllers\API\v1\Ecommerce\Report;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\Order\Order;
use YellowProject\Ecommerce\RecieveKeyword\RecieveKeyword;
use YellowProject\Ecommerce\ShoppingCart\ShoppingCart;
use YellowProject\Ecommerce\ShoppingCart\ShoppingCartLog;
use YellowProject\Ecommerce\Product;
use YellowProject\TrackingRecieveBc;
use YellowProject\Ecommerce\Log\LogSession;
use YellowProject\Ecommerce\Log\LogViewProduct;
use YellowProject\Ecommerce\DTManagement\DTManagement;

class DashboardController extends Controller
{
    public function getDataDashboardShoppingBehavior(Request $request)
    {
    	$datas = [];
    	$datas['shopping_behavior']['header']['all_sessions'] = 0;
    	$datas['shopping_behavior']['header']['sessions_with_product_views'] = 0;
    	$datas['shopping_behavior']['header']['sessions_with_add_to_cart'] = 0;
    	$datas['shopping_behavior']['header']['sessions_with_check_out'] = 0;
        $datas['shopping_behavior']['header']['sessions_with_transactions'] = 0;
        $datas['shopping_behavior']['header']['cancel_by_admin'] = 0;
    	$datas['shopping_behavior']['header']['cancel_by_system'] = 0;
    	$datas['shopping_behavior']['body']['all_sessions'] = 0;
    	$datas['shopping_behavior']['body']['sessions_with_product_views'] = 0;
    	$datas['shopping_behavior']['body']['sessions_with_add_to_cart'] = 0;
    	$datas['shopping_behavior']['body']['sessions_with_check_out'] = 0;
        $datas['shopping_behavior']['body']['sessions_with_transactions'] = 0;
        $datas['shopping_behavior']['body']['cancel_by_admin'] = 0;
    	$datas['shopping_behavior']['body']['cancel_by_system'] = 0;
    	$datas['shopping_behavior']['footer']['no_shipping_activity'] = 0;
    	$datas['shopping_behavior']['footer']['no_cart_addition'] = 0;
    	$datas['shopping_behavior']['footer']['cart_abandonment'] = 0;
    	$datas['shopping_behavior']['footer']['check_out_abandonment'] = 0;
    	$startDate = $request->start_date;
    	$endDate = $request->end_date;

        $orders = \DB::table('dim_ecommerce_order')
            ->whereDate('created_at','>=',$startDate)
            ->whereDate('created_at','<=',$endDate)
            ->get();

    	$logSessions = \DB::table('fact_ecommerce_log_session')
    		->whereDate('created_at','>=',$startDate)
    		->whereDate('created_at','<=',$endDate);
    	$trackingRecieveAllSessionCount = $logSessions->count();

    	$trackingRecieveBcProducts = clone $logSessions;
    	$trackingRecieveBcProductCount = $trackingRecieveBcProducts->where('is_product_views',1)->count();

    	$shoppingCarts = clone $logSessions;
    	$shoppingCartCount = $shoppingCarts->where('is_add_to_cart',1)->count();

    	$orderCheckOut = clone $logSessions;
    	$orderCheckOutCount = $orderCheckOut->where('is_check_out',1)->count();

    	$orderWithTransaction = clone $logSessions;
    	$orderWithTransactionCount = $orderWithTransaction->where('is_transaction',1)->count();

    	$noShoppingActivity = clone $logSessions;
    	$noShoppingActivityCount = $noShoppingActivity->where('is_product_views',0)->where('is_add_to_cart',0)->where('is_check_out',0)->where('is_transaction',0)->count();

    	$noCartAddition = clone $logSessions;
    	$noCartAdditionCount = $noCartAddition->where('is_add_to_cart',0)->count();

    	$noCartAbandonment = clone $logSessions;
    	$noCartAbandonmentCount = $noCartAbandonment->where('is_add_to_cart',1)->where('is_check_out',0)->count();

    	$checkOutAbandonment = clone $logSessions;
    	$checkOutAbandonmentCount = $checkOutAbandonment->where('is_add_to_cart',1)->where('is_check_out',1)->where('is_transaction',0)->count();

        $cancelByAdmin = $orders->filter(function ($value, $key) {
            return $value->order_status == 'Cancel By Admin';
        });

        $cancelBySystem = $orders->filter(function ($value, $key) {
            return $value->order_status == 'Cancel By System';
        });

    	$datas['shopping_behavior']['header']['all_sessions'] = $trackingRecieveAllSessionCount;
    	$datas['shopping_behavior']['header']['sessions_with_product_views'] = $trackingRecieveBcProductCount;
    	$datas['shopping_behavior']['header']['sessions_with_add_to_cart'] = $shoppingCartCount;
    	$datas['shopping_behavior']['header']['sessions_with_check_out'] = $orderCheckOutCount;
    	$datas['shopping_behavior']['header']['sessions_with_transactions'] = $orderWithTransactionCount;
        $datas['shopping_behavior']['header']['cancel_by_admin'] = $cancelByAdmin->count();
        $datas['shopping_behavior']['header']['cancel_by_system'] = $cancelBySystem->count();

    	$datas['shopping_behavior']['body']['all_sessions'] = $trackingRecieveAllSessionCount;
    	$datas['shopping_behavior']['body']['sessions_with_product_views'] = $trackingRecieveBcProductCount;
    	$datas['shopping_behavior']['body']['sessions_with_add_to_cart'] = $shoppingCartCount;
    	$datas['shopping_behavior']['body']['sessions_with_check_out'] = $orderCheckOutCount;
    	$datas['shopping_behavior']['body']['sessions_with_transactions'] = $orderWithTransactionCount;
        $datas['shopping_behavior']['body']['cancel_by_admin'] = $cancelByAdmin->count();
        $datas['shopping_behavior']['body']['cancel_by_system'] = $cancelBySystem->count();

    	$datas['shopping_behavior']['footer']['no_shipping_activity'] = $noShoppingActivityCount;
    	$datas['shopping_behavior']['footer']['no_cart_addition'] = $noCartAdditionCount;
    	$datas['shopping_behavior']['footer']['cart_abandonment'] = $noCartAbandonmentCount;
    	$datas['shopping_behavior']['footer']['check_out_abandonment'] = $checkOutAbandonmentCount;

    	return response()->json([
            'datas' => $datas,
        ]);
    }

    public function getDataDashboardReturningVisitor(Request $request)
    {
    	$datas = [];
    	$datas['returning_visistor']['header']['all_sessions'] = 0;
    	$datas['returning_visistor']['header']['sessions_with_product_views'] = 0;
    	$datas['returning_visistor']['header']['sessions_with_add_to_cart'] = 0;
    	$datas['returning_visistor']['header']['sessions_with_check_out'] = 0;
    	$datas['returning_visistor']['header']['sessions_with_transactions'] = 0;
    	$datas['returning_visistor']['body']['all_sessions'] = 0;
    	$datas['returning_visistor']['body']['sessions_with_product_views'] = 0;
    	$datas['returning_visistor']['body']['sessions_with_add_to_cart'] = 0;
    	$datas['returning_visistor']['body']['sessions_with_check_out'] = 0;
    	$datas['returning_visistor']['body']['sessions_with_transactions'] = 0;
    	$datas['returning_visistor']['footer']['no_shipping_activity'] = 0;
    	$datas['returning_visistor']['footer']['no_cart_addition'] = 0;
    	$datas['returning_visistor']['footer']['cart_abandonment'] = 0;
    	$datas['returning_visistor']['footer']['check_out_abandonment'] = 0;
    	$startDate = $request->start_date;
    	$endDate = $request->end_date;

    	$customerLineIdArrays = \DB::table('fact_ecommerce_log_session')
    		->groupBy('line_user_id')
    		->havingRaw('count(*) > 1')
    		->pluck('line_user_id')
    		->toArray();

    	$logSessions = \DB::table('fact_ecommerce_log_session')
    		->whereIn('line_user_id',$customerLineIdArrays)
    		->whereDate('created_at','>=',$startDate)
    		->whereDate('created_at','<=',$endDate);
    	$trackingRecieveAllSessionCount = $logSessions->count();

    	$trackingRecieveBcProducts = clone $logSessions;
    	$trackingRecieveBcProductCount = $trackingRecieveBcProducts->where('is_product_views',1)->count();

    	$shoppingCarts = clone $logSessions;
    	$shoppingCartCount = $shoppingCarts->where('is_add_to_cart',1)->count();

    	$orderCheckOut = clone $logSessions;
    	$orderCheckOutCount = $orderCheckOut->where('is_check_out',1)->count();

    	$orderWithTransaction = clone $logSessions;
    	$orderWithTransactionCount = $orderWithTransaction->where('is_transaction',1)->count();

    	$noShoppingActivity = clone $logSessions;
    	$noShoppingActivityCount = $noShoppingActivity->where('is_product_views',0)->where('is_add_to_cart',0)->where('is_check_out',0)->where('is_transaction',0)->count();

    	$noCartAddition = clone $logSessions;
    	$noCartAdditionCount = $noCartAddition->where('is_add_to_cart',0)->count();

    	$noCartAbandonment = clone $logSessions;
    	$noCartAbandonmentCount = $noCartAbandonment->where('is_add_to_cart',1)->where('is_check_out',0)->count();

    	$checkOutAbandonment = clone $logSessions;
    	$checkOutAbandonmentCount = $checkOutAbandonment->where('is_add_to_cart',1)->where('is_check_out',1)->where('is_transaction',0)->count();

    	$datas['returning_visistor']['header']['all_sessions'] = $trackingRecieveAllSessionCount;
    	$datas['returning_visistor']['header']['sessions_with_product_views'] = $trackingRecieveBcProductCount;
    	$datas['returning_visistor']['header']['sessions_with_add_to_cart'] = $shoppingCartCount;
    	$datas['returning_visistor']['header']['sessions_with_check_out'] = $orderCheckOutCount;
    	$datas['returning_visistor']['header']['sessions_with_transactions'] = $orderWithTransactionCount;
    	$datas['returning_visistor']['body']['all_sessions'] = $trackingRecieveAllSessionCount;
    	$datas['returning_visistor']['body']['sessions_with_product_views'] = $trackingRecieveBcProductCount;
    	$datas['returning_visistor']['body']['sessions_with_add_to_cart'] = $shoppingCartCount;
    	$datas['returning_visistor']['body']['sessions_with_check_out'] = $orderCheckOutCount;
    	$datas['returning_visistor']['body']['sessions_with_transactions'] = $orderWithTransactionCount;

    	$datas['returning_visistor']['footer']['no_shipping_activity'] = $noShoppingActivityCount;
    	$datas['returning_visistor']['footer']['no_cart_addition'] = $noCartAdditionCount;
    	$datas['returning_visistor']['footer']['cart_abandonment'] = $noCartAbandonmentCount;
    	$datas['returning_visistor']['footer']['check_out_abandonment'] = $checkOutAbandonmentCount;

    	return response()->json([
            'datas' => $datas,
        ]);
    }

    public function getDataDashboardNewVisitor(Request $request)
    {
    	$datas = [];
    	$datas['new_visistor']['header']['all_sessions'] = 0;
    	$datas['new_visistor']['header']['sessions_with_product_views'] = 0;
    	$datas['new_visistor']['header']['sessions_with_add_to_cart'] = 0;
    	$datas['new_visistor']['header']['sessions_with_check_out'] = 0;
    	$datas['new_visistor']['header']['sessions_with_transactions'] = 0;
    	$datas['new_visistor']['body']['all_sessions'] = 0;
    	$datas['new_visistor']['body']['sessions_with_product_views'] = 0;
    	$datas['new_visistor']['body']['sessions_with_add_to_cart'] = 0;
    	$datas['new_visistor']['body']['sessions_with_check_out'] = 0;
    	$datas['new_visistor']['body']['sessions_with_transactions'] = 0;
    	$datas['new_visistor']['footer']['no_shipping_activity'] = 0;
    	$datas['new_visistor']['footer']['no_cart_addition'] = 0;
    	$datas['new_visistor']['footer']['cart_abandonment'] = 0;
    	$datas['new_visistor']['footer']['check_out_abandonment'] = 0;
    	$startDate = $request->start_date;
    	$endDate = $request->end_date;

    	$customerLineIdArrays = \DB::table('fact_ecommerce_log_session')
    		->groupBy('line_user_id')
    		->havingRaw('count(*) = 1')
    		->pluck('line_user_id')
    		->toArray();

    	$logSessions = \DB::table('fact_ecommerce_log_session')
    		->whereIn('line_user_id',$customerLineIdArrays)
    		->whereDate('created_at','>=',$startDate)
    		->whereDate('created_at','<=',$endDate);
    	$trackingRecieveAllSessionCount = $logSessions->count();

    	$trackingRecieveBcProducts = clone $logSessions;
    	$trackingRecieveBcProductCount = $trackingRecieveBcProducts->where('is_product_views',1)->count();

    	$shoppingCarts = clone $logSessions;
    	$shoppingCartCount = $shoppingCarts->where('is_add_to_cart',1)->count();

    	$orderCheckOut = clone $logSessions;
    	$orderCheckOutCount = $orderCheckOut->where('is_check_out',1)->count();

    	$orderWithTransaction = clone $logSessions;
    	$orderWithTransactionCount = $orderWithTransaction->where('is_transaction',1)->count();

    	$noShoppingActivity = clone $logSessions;
    	$noShoppingActivityCount = $noShoppingActivity->where('is_product_views',0)->where('is_add_to_cart',0)->where('is_check_out',0)->where('is_transaction',0)->count();

    	$noCartAddition = clone $logSessions;
    	$noCartAdditionCount = $noCartAddition->where('is_add_to_cart',0)->count();

    	$noCartAbandonment = clone $logSessions;
    	$noCartAbandonmentCount = $noCartAbandonment->where('is_add_to_cart',1)->where('is_check_out',0)->count();

    	$checkOutAbandonment = clone $logSessions;
    	$checkOutAbandonmentCount = $checkOutAbandonment->where('is_add_to_cart',1)->where('is_check_out',1)->where('is_transaction',0)->count();

    	$datas['new_visistor']['header']['all_sessions'] = $trackingRecieveAllSessionCount;
    	$datas['new_visistor']['header']['sessions_with_product_views'] = $trackingRecieveBcProductCount;
    	$datas['new_visistor']['header']['sessions_with_add_to_cart'] = $shoppingCartCount;
    	$datas['new_visistor']['header']['sessions_with_check_out'] = $orderCheckOutCount;
    	$datas['new_visistor']['header']['sessions_with_transactions'] = $orderWithTransactionCount;
    	$datas['new_visistor']['body']['all_sessions'] = $trackingRecieveAllSessionCount;
    	$datas['new_visistor']['body']['sessions_with_product_views'] = $trackingRecieveBcProductCount;
    	$datas['new_visistor']['body']['sessions_with_add_to_cart'] = $shoppingCartCount;
    	$datas['new_visistor']['body']['sessions_with_check_out'] = $orderCheckOutCount;
    	$datas['new_visistor']['body']['sessions_with_transactions'] = $orderWithTransactionCount;

    	$datas['new_visistor']['footer']['no_shipping_activity'] = $noShoppingActivityCount;
    	$datas['new_visistor']['footer']['no_cart_addition'] = $noCartAdditionCount;
    	$datas['new_visistor']['footer']['cart_abandonment'] = $noCartAbandonmentCount;
    	$datas['new_visistor']['footer']['check_out_abandonment'] = $checkOutAbandonmentCount;

    	return response()->json([
            'datas' => $datas,
        ]);
    }

    public function getDataDashboardProductOrBanner(Request $request)
    {
    	$datas = [];
    	$datas['product_banner_click'] = [];
    	$startDate = $request->start_date;
    	$endDate = $request->end_date;
    	$products = $request->products;
    	$logSessions = \DB::table('fact_ecommerce_log_view_product')
    		->select(\DB::raw('DATE(created_at) as date'), \DB::raw('count(*) as count_all'))
    		->whereDate('created_at','>=',$startDate)
    		->whereDate('created_at','<=',$endDate);
    	if(count($products) > 0){
    		$logSessions = $logSessions->whereIn('product_id',$products);
    	}
    	$logSessions = $logSessions->groupBy('date')->get();

    	return response()->json([
            'product_banner_click' => $logSessions,
        ]);
    }

    public function getDataDashboardProductPerformance1(Request $request)
    {
    	$datas = [];
    	$datas['product_performance'] = [];
    	$startDate = $request->start_date;
    	$endDate = $request->end_date;
    	$products = $request->products;
    	$orderProducts = \DB::table('fact_ecommerce_order_product as eop')
    		->select(
                'eop.product_id',
                'ep.name',
                'ep.sku',
                'ep.price',
                \DB::raw('SUM(eop.quanlity) as quantity'),
                \DB::raw('SUM(eop.total) as product_revenue'),
                \DB::raw('SUM(eop.total)/SUM(eop.quanlity) as average_price')
            )
    		->leftjoin('dim_ecommerce_product as ep','eop.product_id','=','ep.id');
    	if($startDate != "" && $endDate != ""){
    		$orderProducts = $orderProducts->whereDate('eop.created_at','>=',$startDate)->whereDate('eop.created_at','<=',$endDate);
    	}
    	if(count($products) > 0){
    		$orderProducts = $orderProducts->whereIn('eop.product_id',$products);
    	}
    	$orderProducts = $orderProducts->groupBy('eop.product_id')->get();
    	foreach ($orderProducts as $key => $orderProduct) {
            $datas['product_performance'][$key]['sku'] = $orderProduct->sku;
            $datas['product_performance'][$key]['price_unit'] = $orderProduct->price;
    		$datas['product_performance'][$key]['product'] = $orderProduct->name;
    		$datas['product_performance'][$key]['quantity'] = $orderProduct->quantity;
    		$datas['product_performance'][$key]['product_revenue'] = $orderProduct->product_revenue;
    		$datas['product_performance'][$key]['average_price'] = number_format($orderProduct->average_price,2);
    	}


    	return response()->json([
            'datas' => $datas,
        ]);
    }

    public function getDataDashboardProductPerformance2(Request $request)
    {
    	$datas = [];
    	$datas['performace']['header']['total_order'] = 0;
    	$datas['performace']['header']['total_sales'] = 0;
        $datas['performace']['header']['average_per_order'] = 0;
    	$datas['performace']['header']['re_purchase_customer'] = 0;
    	$datas['performace']['orders_list'] = [];
    	$datas['performace']['keywords_list'] = [];
    	$startDate = $request->start_date;
    	$endDate = $request->end_date;
    	$orders = \DB::table('dim_ecommerce_order as eor')
    		->leftjoin('dim_ecommerce_dt_management as edtm', 'eor.dt_id', '=', 'edtm.id')
    		->whereDate('eor.created_at','>=',$startDate)
    		->whereDate('eor.created_at','<=',$endDate);
    	$orderCount = clone $orders;
    	$orderCount = $orderCount->count();
    	$ordertotalSales = clone $orders;
    	$ordertotalSales = $ordertotalSales->sum('total_due');
        $rePurchaseCustomers = clone $orders;
        $rePurchaseCustomers->select(
            'eor.ecommerce_customer_id',
            \DB::raw('count(eor.id) as count_data')
        )->groupBy('eor.ecommerce_customer_id');
        $rePurchaseCustomers = $rePurchaseCustomers->get();
        $rePurchaseCustomers->filter(function ($value, $key) {
            return $value->count_data > 1;
        });
        // dd($rePurchaseCustomers->get());

    	$averagePerOrder = ($orderCount > 0)? $ordertotalSales/$orderCount : 0;

    	$recieveKeywords = \DB::table('fact_ecommerce_recieve_keyword_search')
    			->whereDate('created_at','>=',$startDate)
    			->whereDate('created_at','<=',$endDate)
                ->select('keyword', \DB::raw('count(*) as total'))
                ->groupBy('keyword')
                ->orderByDesc('total')
                ->take(5)
                ->get();
        foreach ($recieveKeywords as $key => $recieveKeyword) {
        	$datas['performace']['keywords_list'][$key]['keywords'] = $recieveKeyword->keyword;
        	$datas['performace']['keywords_list'][$key]['number_of_search'] = $recieveKeyword->total;
        }

        $orders = $orders->orderByDesc('eor.created_at')->take(5)->get();
        foreach ($orders as $key => $order) {
        	$quanlity = \DB::table('fact_ecommerce_order_product')
        		->where('order_id',$order->id)
        		->sum('quanlity');
        	$datas['performace']['orders_list'][$key]['_no'] = $key+1;
        	$datas['performace']['orders_list'][$key]['order_id'] = $order->order_id;
        	$datas['performace']['orders_list'][$key]['quantity'] = $quanlity;
        	$datas['performace']['orders_list'][$key]['grand_total'] = $order->total_due;
        	$datas['performace']['orders_list'][$key]['dt_name'] = $order->name;
        	$datas['performace']['orders_list'][$key]['datetime'] = $order->created_at;
        }


    	$datas['performace']['header']['total_order'] = $orderCount;
    	$datas['performace']['header']['total_sales'] = $ordertotalSales;
    	$datas['performace']['header']['average_per_order'] = number_format($averagePerOrder);
        $datas['performace']['header']['re_purchase_customer'] = $rePurchaseCustomers->count();

    	return response()->json([
            'datas' => $datas,
        ]);
    }

    public function productClick(Request $request)
    {
        $datas = [];
        $startDate = $request->start_date;
        $endDate = $request->end_date;
        $products = $request->products;
        $categorys = $request->categorys;

        $queryDatas = \DB::table('fact_ecommerce_log_view_product as lvp')
            ->select(
                'lvp.ecommerce_customer_id',
                'lvp.product_id',
                'p.name'
            )
            ->leftjoin('dim_ecommerce_product as p','lvp.product_id','=','p.id');
        if($startDate != "" && $endDate != ""){
            $queryDatas = $queryDatas->where('lvp.created_at','>=',$startDate)
                ->where('lvp.created_at','<=',$endDate);
        }
        if(count($products) > 0){
            $queryDatas = $queryDatas->whereIn('lvp.product_id',$products);
        }
        // if(count($categorys) > 0){
        //     $queryDatas = $queryDatas->whereIn('p.product_id',$categorys);
        // }


        $queryDatas =  $queryDatas->get()->groupBy('name');

        $count = 0;
        foreach ($queryDatas as $productName => $productDatas) {
            $datas[$count]['product_name'] = $productName;
            $datas[$count]['total_click'] = $productDatas->count();
            $datas[$count]['quantity_click'] = $productDatas->unique('ecommerce_customer_id')->count();
            $count++;
        }

        return response()->json([
            'datas' => $datas,
        ]);
    }

    public function getViewMoreOrder(Request $request)
    {
        $datas = [];
        $limit = $request->limit;
        $offset = $request->offset;
        $order = $request->order;
        $sort = $request->sort;
        $filters = $request->filter_items;
        $startDate = $filters['start_date'];
        $endDate = $filters['end_date'];
        $queryDatas = \DB::table('dim_ecommerce_order as eo')
            ->select(
                'eo.id',
                'eo.order_id',
                \DB::raw('count(eop.id) as quantity'),
                'eo.grand_total',
                'edm.name',
                'eo.created_at as date_time'
            )
            ->leftjoin('fact_ecommerce_order_product as eop','eo.id','=','eop.order_id')
            ->leftjoin('dim_ecommerce_dt_management as edm','eo.dt_id','=','edm.id');
        if($startDate != "" && $endDate != ""){
            $queryDatas = $queryDatas->where('eo.created_at','>=',$startDate)
                ->where('eo.created_at','<=',$endDate);
        }
        $queryDatas = $queryDatas->groupBy('eo.order_id');
        $count = $queryDatas->count();
        $queryDatas = $queryDatas->skip($offset)->take($limit)->get();

        return response()->json([
            'rows' => $queryDatas,
            'total' => $count
        ]);
    }

    public function getProductSKU()
    {
        $datas = [];

        $datas = Product::select(
            'id',
            'sku as name'
        )->get();

        return response()->json([
            'datas' => $datas,
        ]);
    }

    public function getOrder()
    {
        $datas = [];
        $orders = Order::orderByDesc('order_id')->get();
        $count = 0;
        foreach ($orders as $key => $order) {
            $DTManagement = DTManagement::find($order->dt_id);
            $datas[$count]['order_running_id'] = $count;
            $datas[$count]['id'] = $order->id;
            $datas[$count]['order_id'] = $order->order_id;
            $datas[$count]['dt'] = ($DTManagement)? $DTManagement->name : null ;
            $datas[$count]['purchased_on'] = $order->created_at->format('d/m/Y H:i');
            $datas[$count]['customer'] = $order->customer->first_name;
            $datas[$count]['ship_to'] = $order->customer->first_name;
            $datas[$count]['base_price'] = $order->grand_total;
            $datas[$count]['purchased_price'] = $order->total_due;
            $datas[$count]['status'] = $order->order_status;
            $count++;
        }

        return response()->json([
            'datas' => $datas,
        ]);
    }
}
