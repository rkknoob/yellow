<?php

namespace YellowProject\Http\Controllers\API\v1\Ecommerce\Report;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\Conversion\ConversionData;

class ReportConversionController extends Controller
{
    public function getDataConversion(Request $request)
    {
    	$datas = [];
    	$queryDatas = \DB::table('fact_recieve_tracking_bc')
            ->leftJoin('dim_line_user_table', 'fact_recieve_tracking_bc.line_user_id', '=', 'dim_line_user_table.id')
            ->leftJoin('dim_ecommerce_customer', 'fact_recieve_tracking_bc.line_user_id', '=', 'dim_ecommerce_customer.line_user_id')
            ->where('fact_recieve_tracking_bc.tracking_source','eCOM')
            ->whereNotNull('dim_ecommerce_customer.id')
            ->select('dim_line_user_table.id as line_user_id'
              ,'dim_line_user_table.mid'
              ,'dim_ecommerce_customer.id as ecommerce_customer_id'
              ,'dim_ecommerce_customer.first_name'
              ,'dim_ecommerce_customer.last_name'
              ,'dim_ecommerce_customer.phone_number'
              ,'dim_ecommerce_customer.market_name as shop_name'
              ,'dim_ecommerce_customer.market_type as shop_type'
              ,\DB::raw('count(*) as total_click'))
            ->groupBy('fact_recieve_tracking_bc.line_user_id')
            ->get();
        foreach ($queryDatas as $key => $queryData) {
        	$isCompleteProfile = 0;
        	if($queryData->first_name != "" && $queryData->last_name != "" && $queryData->phone_number != "" && $queryData->shop_name != "" && $queryData->shop_type != ""){
        		$isCompleteProfile = 1;
        	}
        	$queryOrderDatas = \DB::table('dim_ecommerce_order')
        		->where('ecommerce_customer_id',$queryData->ecommerce_customer_id);
        	$lastPurchaseDate = ($queryOrderDatas->first())? $queryOrderDatas->orderByDesc('created_at')->first()->created_at : null;
        	$isCompleteOrder = $queryOrderDatas->where('order_status','Customer Payment Confirmation')->count();
        	$datas[$key]['customer_name'] = $queryData->first_name;
        	$datas[$key]['customer_phone'] = $queryData->phone_number;
        	$datas[$key]['total_click'] = $queryData->total_click;
        	$datas[$key]['completed_profile'] = $isCompleteProfile;
        	$datas[$key]['completed_order'] = $isCompleteOrder;
        	$datas[$key]['last_purchase'] = $lastPurchaseDate;
        }

        return response()->json([
            'datas' => $datas,
        ]);
    }

    public function importData(Request $request)
    {
    	$csvDatas = $request->csv;
    	foreach ($csvDatas as $key => $csvData) {
    		$datas = [];
    		$datas['cpn_mobile'] = $csvData['CPN_MOBILE'];
    		$datas['name'] = $csvData['NAME'];
    		$datas['sur_name'] = $csvData['SURNAME'];
    		$datas['bussiness_name'] = $csvData['Business name'];
    		$datas['bussiness_type'] = $csvData['Business Type'];
    		$datas['post_code'] = $csvData['Postcode'];
    		$conversionData = ConversionData::where('cpn_mobile',$csvData['CPN_MOBILE'])->first();
    		if($conversionData){
    			$conversionData->update($datas);
    		}else{
    			ConversionData::create($datas);
    		}
    	}

    	return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function getDataConversionExisting(Request $request)
    {
    	$datas = [];
    	$queryDatas = \DB::table('fact_recieve_tracking_bc')
            ->leftJoin('dim_line_user_table', 'fact_recieve_tracking_bc.line_user_id', '=', 'dim_line_user_table.id')
            ->leftJoin('dim_ecommerce_customer', 'fact_recieve_tracking_bc.line_user_id', '=', 'dim_ecommerce_customer.line_user_id')
            ->leftJoin('dim_ecommerce_conversion_mapping_data', 'dim_ecommerce_customer.phone_number', '=', 'dim_ecommerce_conversion_mapping_data.cpn_mobile')
            ->where('fact_recieve_tracking_bc.tracking_source','eCOM')
            ->whereNotNull('dim_ecommerce_customer.id')
            ->whereNotNull('dim_ecommerce_conversion_mapping_data.id')
            ->select('dim_line_user_table.id as line_user_id'
              ,'dim_line_user_table.mid'
              ,'dim_ecommerce_customer.id as ecommerce_customer_id'
              ,'dim_ecommerce_customer.first_name'
              ,'dim_ecommerce_customer.last_name'
              ,'dim_ecommerce_customer.phone_number'
              ,'dim_ecommerce_customer.market_name as shop_name'
              ,'dim_ecommerce_customer.market_type as shop_type'
              ,\DB::raw('count(*) as total_click'))
            ->groupBy('fact_recieve_tracking_bc.line_user_id')
            ->get();
        foreach ($queryDatas as $key => $queryData) {
        	$isCompleteProfile = 0;
        	if($queryData->first_name != "" && $queryData->last_name != "" && $queryData->phone_number != "" && $queryData->shop_name != "" && $queryData->shop_type != ""){
        		$isCompleteProfile = 1;
        	}
        	$queryOrderDatas = \DB::table('dim_ecommerce_order')
        		->where('ecommerce_customer_id',$queryData->ecommerce_customer_id);
        	$lastPurchaseDate = ($queryOrderDatas->first())? $queryOrderDatas->orderByDesc('created_at')->first()->created_at : null;
        	$isCompleteOrder = $queryOrderDatas->where('order_status','Customer Payment Confirmation')->count();
        	$datas[$key]['customer_name'] = $queryData->first_name;
        	$datas[$key]['customer_phone'] = $queryData->phone_number;
        	$datas[$key]['total_click'] = $queryData->total_click;
        	$datas[$key]['completed_profile'] = $isCompleteProfile;
        	$datas[$key]['completed_order'] = $isCompleteOrder;
        	$datas[$key]['last_purchase'] = $lastPurchaseDate;
        }

        return response()->json([
            'datas' => $datas,
        ]);
    }
}
