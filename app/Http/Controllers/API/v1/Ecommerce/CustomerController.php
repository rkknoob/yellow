<?php

namespace YellowProject\Http\Controllers\API\v1\Ecommerce;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\Customer\Customer;
use YellowProject\Ecommerce\Customer\CustomerShippingAddress;
use YellowProject\Ecommerce\Customer\CustomerTaxBillingAddress;
use YellowProject\Ecommerce\OnlineExclusiveModule\OnlineExclusiveModuleCourse;
use YellowProject\Ecommerce\OnlineExclusiveModule\OnlineExclusiveModuleCourseVideo;
use YellowProject\Ecommerce\OnlineExclusiveModule\OnlineExclusiveModuleCustomerReDeem;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $datas = [];
        $runningNumber = 1;
        $customers = Customer::all();
        foreach ($customers as $key => $customer) {
            $customerTaxBillingAddress = $customer->customerTaxBillingAddress;
            $customerShippingAddresses = $customer->customerShippingAddresses;
            if($customerShippingAddresses){
                $customerShippingAddresses = $customerShippingAddresses->first();
            }
            $datas[$key]['id'] = $customer->id;
            $datas[$key]['no'] = $runningNumber;
            $datas[$key]['customer_id'] = $customer->id;
            $datas[$key]['customer_code'] = $customer->customer_code;
            $datas[$key]['customer_name'] = $customer->first_name;
            $datas[$key]['customer_last_name'] = $customer->last_name;
            $datas[$key]['customer_phone_number'] = $customer->phone_number;
            $datas[$key]['customer_post_code'] = ($customerShippingAddresses)? $customerShippingAddresses->post_code : "";
            $datas[$key]['customer_province'] = ($customerShippingAddresses)? $customerShippingAddresses->province : "";
            $datas[$key]['customer_register_date'] = $customer->created_at->format('d/m/Y H:i');
            $runningNumber++;
        }

        return response()->json([
            'datas' => $datas,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $datas = [];
        $datas['details']['customer'] = [];
        $datas['details']['billing_address'] = [];
        $datas['details']['shipping_address'] = [];
        $datas['details']['total_sales'] = 0.00;
        $datas['details']['orders'] = [];
        $datas['orders'] = [];
        $datas['wishlists'] = [];
        $datas['cancellations'] = [];
        $datas['reward_point_or_coupons'] = [];
        $datas['redeemed_items'] = [];

        $customer = Customer::find($id);
        $customerBillingAddress = $customer->CustomerTaxBillingAddress;
        $customerShippingAddresses = $customer->customerShippingAddresses;
        $customerWishlists = $customer->customerWishlists;
        $customerCoupons = $customer->customerCoupons;
        $customerRewardKitchenWares = $customer->customerRewardKitchenWares;
        $customerRewardExclusives = $customer->customerRewardExclusives;
        // $customerCancellations = $customer->customerCancellations;
        $orders  = $customer->orders;
        $orderNormals  = ($orders)? $orders->whereNotIn('order_status',['Cancle by Admin','Customer ask to Cancel']) : null;
        $orderCancels  = ($orders)? $orders->whereIn('order_status',['Cancle by Admin','Customer ask to Cancel']) : null;

        if($customer){
            $datas['details']['customer']['id'] = $customer->first_name." ".$customer->id;
            $datas['details']['customer']['customer_code'] = $customer->customer_code;
            $datas['details']['customer']['name'] = $customer->first_name;
            $datas['details']['customer']['last_name'] = $customer->last_name;
            $datas['details']['customer']['email'] = ($customer->lineUserProfile)? $customer->lineUserProfile->email : null;
            $datas['details']['customer']['city'] = $customer->city;
            $datas['details']['customer']['phone_number'] = $customer->phone_number;
            $datas['details']['customer']['reward_point'] = $customer->reward_point;
            $datas['details']['customer']['market_name'] = $customer->market_name;
            $datas['details']['customer']['market_type'] = $customer->market_type;
            $datas['details']['customer']['line_user_id'] = $customer->line_user_id;
        }

        if($customerBillingAddress){
            $datas['details']['billing_address']['id'] = $customerBillingAddress->id;
            $datas['details']['billing_address']['name'] = $customerBillingAddress->name;
            $datas['details']['billing_address']['last_name'] = $customerBillingAddress->last_name;
            $datas['details']['billing_address']['phone_number'] = $customerBillingAddress->phone_number;
            $datas['details']['billing_address']['company_name'] = $customerBillingAddress->company_name;
            $datas['details']['billing_address']['address'] = $customerBillingAddress->address;
            $datas['details']['billing_address']['district'] = $customerBillingAddress->district;
            $datas['details']['billing_address']['sub_district'] = $customerBillingAddress->sub_district;
            $datas['details']['billing_address']['province'] = $customerBillingAddress->province;
            $datas['details']['billing_address']['post_code'] = $customerBillingAddress->post_code;
            $datas['details']['billing_address']['tel'] = $customerBillingAddress->tel;
            $datas['details']['billing_address']['tax_id'] = $customerBillingAddress->tax_id;
        }

        if($customerShippingAddresses){
            foreach ($customerShippingAddresses as $key => $customerShippingAddress) {
                $datas['details']['shipping_address'][$key]['id'] = $customerShippingAddress->id;
                $datas['details']['shipping_address'][$key]['name'] = $customerShippingAddress->name;
                $datas['details']['shipping_address'][$key]['last_name'] = $customerShippingAddress->last_name;
                $datas['details']['shipping_address'][$key]['company_name'] = $customerShippingAddress->company_name;
                $datas['details']['shipping_address'][$key]['address'] = $customerShippingAddress->address;
                $datas['details']['shipping_address'][$key]['district'] = $customerShippingAddress->district;
                $datas['details']['shipping_address'][$key]['sub_district'] = $customerShippingAddress->sub_district;
                $datas['details']['shipping_address'][$key]['province'] = $customerShippingAddress->province;
                $datas['details']['shipping_address'][$key]['post_code'] = $customerShippingAddress->post_code;
                $datas['details']['shipping_address'][$key]['tel'] = $customerShippingAddress->tel;
                $datas['details']['shipping_address'][$key]['latitude'] = $customerShippingAddress->latitude;
                $datas['details']['shipping_address'][$key]['longitude'] = $customerShippingAddress->longitude;
                $datas['details']['shipping_address'][$key]['store_name'] = $customerShippingAddress->store_name;
                $datas['details']['shipping_address'][$key]['store_type'] = $customerShippingAddress->store_type;
            }
        }

        if($orders){
            foreach ($orders as $key => $order) {
                $orderProducts = $order->orderProducts;
                $datas['details']['orders'][$key]['id'] = $order->id;
                $datas['details']['orders'][$key]['order_id'] = $order->order_id;
                $datas['details']['orders'][$key]['placed_on'] = $order->created_at->format('d/m/Y H:i');
                $datas['details']['orders'][$key]['total'] = $order->total_due;
                $datas['details']['orders'][$key]['status'] = $order->order_status;
                $datas['details']['orders'][$key]['items'] = [];
                $datas['details']['total_sales'] = $datas['details']['total_sales'] + $order->total_due;
            }
        }

        if($orderNormals){
            foreach ($orderNormals as $key => $order) {
                $orderProducts = $order->orderProducts;
                $datas['orders'][$key]['order_id'] = $order->order_id;
                $datas['orders'][$key]['placed_on'] = $order->created_at->format('d/m/Y H:i');
                $datas['orders'][$key]['total'] = $order->total_due;
                $datas['orders'][$key]['status'] = $order->order_status;
                $datas['orders'][$key]['items'] = [];
                if($orderProducts){
                    foreach ($orderProducts as $index => $orderProduct) {
                        $datas['orders'][$key]['items'][$index]['product_img'] = [];
                        $product = $orderProduct->product;
                        $productImages = $product->productImages;
                        if($productImages){
                            foreach ($productImages as $indexImg => $productImage) {
                                $datas['orders'][$key]['items'][$index]['product_img'][$indexImg]['img_url'] = ($productImage->image)? $productImage->image->img_url : "";
                            }
                        }
                        $datas['orders'][$key]['items'][$index]['product_name'] = $product->name;
                    }
                }
            }
        }

        if($orderCancels){
            foreach ($orderCancels as $key => $order) {
                $orderProducts = $order->orderProducts;
                $datas['cancellations'][$key]['order_id'] = $order->order_id;
                $datas['cancellations'][$key]['placed_on'] = $order->created_at->format('d/m/Y H:i');
                $datas['cancellations'][$key]['total'] = $order->sub_total;
                $datas['cancellations'][$key]['status'] = $order->order_status;
                $datas['cancellations'][$key]['items'] = [];
                if($orderProducts){
                    foreach ($orderProducts as $index => $orderProduct) {
                        $datas['cancellations'][$key]['items'][$index]['product_img'] = [];
                        $product = $orderProduct->product;
                        $productImages = $product->productImages;
                        if($productImages){
                            foreach ($productImages as $indexImg => $productImage) {
                                $datas['cancellations'][$key]['items'][$index]['product_img'][$indexImg]['img_url'] = ($productImage->image)? $productImage->image->img_url : "";
                            }
                        }
                        $datas['cancellations'][$key]['items'][$index]['product_name'] = $product->name;
                    }
                }
                // $datas['details']['total_sales'] = $datas['details']['total_sales'] + $order->sub_total;
            }
        }

        if($customerWishlists){
            foreach ($customerWishlists as $key => $customerWishlist) {
                $product = $customerWishlist->product;
                $productDiscount = ($product->productDiscount)? $product->productDiscount : null;
                $productImages = $product->productImages;
                $datas['wishlists']['items'][$key]['product_name'] = $product->name;
                $datas['wishlists']['items'][$key]['product_desc'] = $product->desc;
                $datas['wishlists']['items'][$key]['product_short_desc'] = $product->short_desc;
                $datas['wishlists']['items'][$key]['product_price'] = $product->price;
                $datas['wishlists']['items'][$key]['product_price_unit'] = $product->price_unit;
                if($productImages){
                    foreach ($productImages as $productImageIndex => $productImage) {
                        $image = $productImage->image;
                        $datas['wishlists']['items'][$key]['product_images'][$productImageIndex]['name'] = $productImage->name;
                        $datas['wishlists']['items'][$key]['product_images'][$productImageIndex]['seq'] = $productImage->seq;
                        $datas['wishlists']['items'][$key]['product_images'][$productImageIndex]['is_base'] = $productImage->is_base;
                        $datas['wishlists']['items'][$key]['product_images'][$productImageIndex]['is_small'] = $productImage->is_small;
                        $datas['wishlists']['items'][$key]['product_images'][$productImageIndex]['is_thumbnail'] = $productImage->is_thumbnail;
                        $datas['wishlists']['items'][$key]['product_images'][$productImageIndex]['image_url'] = $productImage->image->img_url;
                    }
                }
                // $datas['wishlists']['items'][$key]['product_image'] = ($productImage)? $productImage->image->img_url : "";
                if($productDiscount){
                    $datas['wishlists']['items'][$key]['product_discount_priority'] = $productDiscount->priority;
                    $datas['wishlists']['items'][$key]['product_discount_price'] = $productDiscount->discount_price;
                    $datas['wishlists']['items'][$key]['product_discount_start_date'] = $productDiscount->start_date;
                    $datas['wishlists']['items'][$key]['product_discount_end_date'] = $productDiscount->end_date;
                }
            }
        }

        $datas['reward_point_or_coupons']['reward_point'] = $customer->reward_point;

        if($customerCoupons){
            foreach ($customerCoupons as $index => $customerCoupon) {
                $coupon = $customerCoupon->coupon;
                $couponDiscounts = $coupon->Discounts;
                $couponProducts = $coupon->Products;
                $couponCategories = $coupon->Categories;
                $datas['reward_point_or_coupons']['items'][$index]['no'] = $coupon->is_running_number;
                $datas['reward_point_or_coupons']['items'][$index]['coupon_name'] = $coupon->name;
                $datas['reward_point_or_coupons']['items'][$index]['coupon_code'] = $coupon->prefix_name.$coupon->code;
                if($couponDiscounts){
                    $couponDiscount = $couponDiscounts->first();
                    $datas['reward_point_or_coupons']['items'][$index]['type'] = $couponDiscount->type;
                    $datas['reward_point_or_coupons']['items'][$index]['discount'] = $couponDiscount->discount;
                }
                // foreach ($couponDiscounts as $couponDiscountIndex => $couponDiscount) {
                //     $datas['reward_point_or_coupons']['items'][$index]['coupon_discounts'][$couponDiscountIndex]['type'] = $couponDiscount->type;
                //     $datas['reward_point_or_coupons']['items'][$index]['coupon_discounts'][$couponDiscountIndex]['discount'] = $couponDiscount->discount;
                //     $datas['reward_point_or_coupons']['items'][$index]['coupon_discounts'][$couponDiscountIndex]['total_amount'] = $couponDiscount->total_amount;
                // }
                foreach ($couponProducts as $couponProductIndex => $couponProduct) {
                    $datas['reward_point_or_coupons']['items'][$index]['products'][$couponProductIndex]['product_name'] = $couponProduct->product->name;
                }
                foreach ($couponCategories as $couponCategoryIndex => $couponCategory) {
                    $datas['reward_point_or_coupons']['items'][$index]['categories'][$couponCategoryIndex]['category_name'] = $couponCategory->productCategory->name;
                }
                // $datas['reward_point_or_coupons']['items'][$index]['coupon_type'] = $customerCoupon->coupon_type;
                // $datas['reward_point_or_coupons']['items'][$index]['coupon_discount'] = $customerCoupon->coupon_discount;
                // $datas['reward_point_or_coupons']['items'][$index]['specific_product'] = $customerCoupon->specific_product;
                // $datas['reward_point_or_coupons']['items'][$index]['specific_category'] = $customerCoupon->specific_category;
            }
        }

        // $rewardPointOrCoupons = collect([
        //     'reward_point' => $customer->reward_point,
        //     'items' => [[
        //         'no' => 1,
        //         'coupon_name' => "Discount All 500",
        //         'coupon_code' => "ALL500B12345",
        //         'coupon_type' => "bath",
        //         'coupon_discount' => 500,
        //         'specific_product' => "Product A",
        //         'specific_category' => "-",
        //     ],
        //     [
        //         'no' => 2,
        //         'coupon_name' => "Discount All 10%",
        //         'coupon_code' => "ALL10PER12345",
        //         'coupon_type' => "percent",
        //         'coupon_discount' => 10,
        //         'specific_product' => "-",
        //         'specific_category' => "Category A",
        //     ]]
        // ]);

        // $redeemedItems = collect([
        //     'redeemed_items' => [
        //         [
        //             'category_name' => 'EXCLUSIVE ONLINE MODULE',
        //             'items' => [
        //                 [
        //                     'no' => 1,
        //                     'curriculum' => 'Professionnal Restaurant start-up',
        //                     'hours' => 3,
        //                     'ep' => 3,
        //                     'redeemed_date' => '20/11/2017',
        //                 ],
        //                 [
        //                     'no' => 2,
        //                     'curriculum' => 'Branding for restaurant bussiness',
        //                     'hours' => 3,
        //                     'ep' => 3,
        //                     'redeemed_date' => '20/11/2017',
        //                 ],
        //             ]
        //         ],
        //         [
        //             'category_name' => 'PREMIUM KITCHENWARE OR EQUIPMENT',
        //             'items' => [
        //                 [
        //                     'no' => 1,
        //                     'curriculum' => 'Premium 1',
        //                     'hours' => "",
        //                     'ep' => 30,
        //                     'redeemed_date' => '20/11/2017',
        //                 ],
        //                 [
        //                     'no' => 2,
        //                     'curriculum' => 'Premium 2',
        //                     'hours' => "",
        //                     'ep' => 30,
        //                     'redeemed_date' => '20/11/2017',
        //                 ],
        //             ]
        //         ],
        //     ],
        // ]);

        // foreach ($redeemedItems['redeemed_items'] as $key => $redeemedItem) {
        //     $datas['redeemed_items'][$key]['category_name'] = $redeemedItem['category_name'];
        //     foreach ($redeemedItem['items'] as $index => $item) {
        //         $datas['redeemed_items'][$key]['items'][$index]['no'] = $item['no'];
        //         $datas['redeemed_items'][$key]['items'][$index]['curriculum'] = $item['curriculum'];
        //         $datas['redeemed_items'][$key]['items'][$index]['hours'] = $item['hours'];
        //         $datas['redeemed_items'][$key]['items'][$index]['ep'] = $item['ep'];
        //         $datas['redeemed_items'][$key]['items'][$index]['redeemed_date'] = $item['redeemed_date'];
        //     }
        // }



        // $datas['redeemed_items']

        return response()->json([
            'datas' => $datas,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updateCustomerData(Request $request, $id)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }

        $customer = Customer::find($id);
        if($customer){
            $customer->update($request->all());
        }else{
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูล customer',
                'code_return' => 2,
            ]);
        }

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function updateCustomerShippingAddressData(Request $request, $id)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $customerShippingAddress = CustomerShippingAddress::find($id);
        if($customerShippingAddress){
            $customerShippingAddress->update($request->all());
        }else{
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูล customer shipping address',
                'code_return' => 2,
            ]);
        }

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function updateCustomerTaxBillingAddressData(Request $request, $id)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $customerTaxBillingAddress = CustomerTaxBillingAddress::find($id);
        if($customerTaxBillingAddress){
            $customerTaxBillingAddress->update($request->all());
        }else{
            return response()->json([
                'msg_return' => 'ไม่พบข้อมูล customer billing address',
                'code_return' => 2,
            ]);
        }

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }
}
