<?php

namespace YellowProject\Http\Controllers\API\v1\Ecommerce;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Ecommerce\Coupon\Coupon;
use YellowProject\Ecommerce\Coupon\CouponSection;
use YellowProject\Ecommerce\Coupon\CouponItem;
use YellowProject\Ecommerce\Coupon\CouponItemCss;
use YellowProject\Ecommerce\Coupon\CouponItemSetting;
use YellowProject\Ecommerce\Coupon\CouponGenerate;
use YellowProject\Ecommerce\Coupon\CouponProduct;
use YellowProject\Ecommerce\Coupon\CouponProductCategory;
use YellowProject\Ecommerce\Coupon\CouponDiscount;
use YellowProject\Ecommerce\Coupon\ImageFile as ImageFile;
// use YellowProject\Ecommerce\Coupon\CouponStaticForm;
// use YellowProject\Ecommerce\Coupon\CouponReedeem;
// use YellowProject\Ecommerce\Coupon\UserCheckCoupon;
// use YellowProject\Ecommerce\Coupon\CouponUser;
// use YellowProject\Ecommerce\Coupon\CouponReedeemCode;
use YellowProject\TrackingBc;
use YellowProject\TrackingRecieveBc;
use YellowProject\LineUserProfile;
use Excel;
use Carbon\Carbon;
use URL;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $startDate = $request->start_date;
        $endDate = $request->end_date;
        $datas = [];
        $coupons = Coupon::orderBy('updated_at','desc')->get();
        foreach ($coupons as $key => $coupon) {
            $trackingBc = TrackingBc::find($coupon->tracking_bc_id);
            $datas[$key]['id'] = $coupon->id;
            $datas[$key]['name'] = $coupon->name;
            $datas[$key]['code'] = $coupon->code;
            $datas[$key]['user_per_coupon'] = $coupon->user_per_coupon;
            $datas[$key]['user_per_customer'] = $coupon->user_per_customer;
            $datas[$key]['start_date'] = $coupon->start_date;
            $datas[$key]['end_date'] = $coupon->end_date;
            $datas[$key]['discount'] = "20";
            $datas[$key]['discount_type'] = "bath";
            $datas[$key]['coupon_is_reedeem'] = 5;
            $datas[$key]['coupon_is_uses'] = 2;
            $datas[$key]['coupon_is_reedeemed'] = 5;
            $datas[$key]['status'] = "Active";
            $datas[$key]['tracking_url'] = "";
            $datas[$key]['minimum_purchase'] = $coupon->minimum_purchase;
            // if($startDate != "" && $endDate != ""){
            //     $couponUsers = $coupon->couponUsers->whereBetween('created_at', array($startDate, $endDate));
            //     $couponUserReedeems = $coupon->couponUserReedeems->whereBetween('created_at', array($startDate, $endDate));
            //     $couponUserChecks = $coupon->userCheckCoupons->whereBetween('created_at', array($startDate, $endDate));
            //     $couponUserUsed = CouponUser::where('coupon_id',$coupon->id)->whereNotNull('reedeem_date')->whereBetween('created_at', array($startDate, $endDate));
            //     $trackingRecieveBcs = TrackingRecieveBc::where('tracking_bc_id',$coupon->tracking_bc_id)->whereBetween('created_at', array($startDate, $endDate))->get();
            // }else{
            //     $couponUsers = $coupon->couponUsers;
            //     $couponUserReedeems = $coupon->couponUserReedeems;
            //     $couponUserChecks = $coupon->userCheckCoupons;
            //     $couponUserUsed = CouponUser::where('coupon_id',$coupon->id)->whereNotNull('reedeem_date');
            //     $trackingRecieveBcs = TrackingRecieveBc::where('tracking_bc_id',$coupon->tracking_bc_id)->get();
            // }
            
            // $trackingBc = $coupon->trackingBc;
            // $datas[$key] = $coupon->toArray();
            // // $datas[$key]['coupon_is_reedeem'] = $couponUsers->count();
            // // $datas[$key]['coupon_is_uses'] = $couponUserChecks->count();
            // // $datas[$key]['coupon_is_reedeemed'] = $couponUserReedeems->count();
            // $datas[$key]['coupon_is_reedeem'] = $couponUsers->count();
            // $datas[$key]['coupon_is_uses'] = $couponUserUsed->count();
            // $datas[$key]['coupon_is_reedeemed'] = $couponUserReedeems->count();
            // $datas[$key]['coupon_click_count'] = $trackingRecieveBcs->count();
            // $datas[$key]['coupon_click_count_percent'] = $datas[$key]['coupon_is_reedeem']/$datas[$key]['coupon_click_count'];
            // $datas[$key]['coupon_unique_click_count'] = ($trackingRecieveBcs->count() > 0)? $trackingRecieveBcs->unique('line_user_id')->count() : 0;
            // $datas[$key]['coupon_unique_click_count_percent'] = $datas[$key]['coupon_is_reedeem']/$datas[$key]['coupon_unique_click_count'];
        }
        return response()->json([
            'datas' => $datas,
        ]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $couponSearch = Coupon::where('name',$request->name)->first();
        if($couponSearch){
          return response()->json([
              'msg_return' => 'ชื่อซ้ำกัน',
              'code_return' => 2,
          ]);
        }

        if(isset($request->code) && $request->code != ""){
            $trackingBcSearch = TrackingBc::where('code',$request->code)->first();
            if($trackingBcSearch){
              return response()->json([
                  'msg_return' => 'ชื่อ Tracking ซ้ำกัน',
                  'code_return' => 21,
              ]);
            }
        }

        $baseUrl = \URL::to('/');
        if($request->is_route_name == 0){
            $request['code'] = TrackingBc::generateCode();
        }
        $request['generated_short_url'] = $baseUrl."/bc/".$request['code'];
        $request['generated_full_url'] = $baseUrl."/bc/".$request['code']."?"."yl_source=".$request->tracking_source."&yl_campaign=".$request->tracking_campaign."&yl_ref=".$request->tracking_ref;
        $trackingBcDatas = $request->all();
        $trackingBcDatas['desc'] = $trackingBcDatas['tracking_coupon_description'];
        $trackingBc = TrackingBc::create($trackingBcDatas);
        $request['tracking_bc_id'] = $trackingBc->id;

        $products = $request->products;
        $categoryProducts = $request->categories;
        $discounts = $request->discounts;

        $datas = $request->all();
        $coupon = Coupon::create($request->all());

        foreach ($products as $key => $product) {
            CouponProduct::create([
                'ecommerce_coupon_id' => $coupon->id,
                'ecommerce_product_id' => $product['value']
            ]);
        }

        foreach ($categoryProducts as $key => $categoryProduct) {
            CouponProductCategory::create([
                'ecommerce_coupon_id' => $coupon->id,
                'ecommerce_product_category_id' => $categoryProduct['value']
            ]);
        }

        foreach ($discounts as $key => $discount) {
            $discount['ecommerce_coupon_id'] = $coupon->id;
            CouponDiscount::create($discount);
        }

        $trackingBc->update([
            // 'original_url' => $baseUrl.'/dashboard?type=coupon#/coupon/'.$coupon->id,
            'original_url' => $baseUrl.'/sb-coupon#/show/'.$coupon->id,
        ]);

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
            'id' => $coupon->id,
            'tracking_bc' => $coupon->trackingBc
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $datas = collect();
        $coupon = Coupon::find($id);
        $couponProducts = $coupon->Products;
        $couponProductCategories = $coupon->Categories;
        if($couponProductCategories->count() > 0){
            foreach ($couponProductCategories as $key => $couponProductCategory) {
                $couponProductCategory->productCategory;
            }
        }
        $couponProductDiscounts = $coupon->Discounts;
        $trackingBc = $coupon->trackingBc;
        $trackingBc = $coupon->image;
        $datas = $coupon->toArray();
        
        return response()->json([
            'datas' => $datas,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        // dd($request->all());
        $coupon = Coupon::find($id);

        $couponSearch = Coupon::where('name',$request->name)->first();
        if($couponSearch && $request->name != $coupon->name){
          return response()->json([
              'msg_return' => 'ชื่อซ้ำกัน',
              'code_return' => 2,
          ]);
        }

        $baseUrl = \URL::to('/');
        $trackingBc = TrackingBc::find($coupon->tracking_bc_id);
        if($trackingBc){
            if(isset($request->code) && $request->code != "" && $trackingBc->code != $request->code){
                $trackingBcSearch = TrackingBc::where('code',$request->code)->first();
                if($trackingBcSearch){
                  return response()->json([
                      'msg_return' => 'ชื่อ Tracking ซ้ำกัน',
                      'code_return' => 21,
                  ]);
                }
            }
            if($request->is_route_name == 0){
                $request['code'] = TrackingBc::generateCode();
            }
            // $request['original_url'] = $baseUrl.'/dashboard?type=coupon#/coupon/'.$coupon->id;
            $request['original_url'] = $baseUrl.'/sb-coupon#/show/'.$coupon->id;
            $request['generated_short_url'] = $baseUrl."/bc/".$request['code'];
            $request['generated_full_url'] = $baseUrl."/bc/".$request['code']."?"."yl_source=".$request->tracking_source."&yl_campaign=".$request->tracking_campaign."&yl_ref=".$request->tracking_ref;
            $trackingBcDatas = $request->all();
            $trackingBcDatas['desc'] = $trackingBcDatas['tracking_coupon_description'];
            $trackingBc->update($trackingBcDatas);
            $request['tracking_bc_id'] = $trackingBc->id;
        }else{
            if(isset($request->code) && $request->code != ""){
                $trackingBcSearch = TrackingBc::where('code',$request->code)->first();
                if($trackingBcSearch){
                  return response()->json([
                      'msg_return' => 'ชื่อ Tracking ซ้ำกัน',
                      'code_return' => 21,
                  ]);
                }
            }

            $baseUrl = \URL::to('/');
            if($request->is_route_name == 0){
                $request['code'] = TrackingBc::generateCode();
            }
            // $request['original_url'] = $baseUrl.'/dashboard?type=coupon#/coupon/'.$coupon->id;
            $request['original_url'] = $baseUrl.'/sb-coupon#/show/'.$coupon->id;
            $request['generated_short_url'] = $baseUrl."/bc/".$request['code'];
            $request['generated_full_url'] = $baseUrl."/bc/".$request['code']."?"."yl_source=".$request->tracking_source."&yl_campaign=".$request->tracking_campaign."&yl_ref=".$request->tracking_ref;
            $trackingBcDatas = $request->all();
            $trackingBcDatas['desc'] = $trackingBcDatas['tracking_coupon_description'];
            $trackingBc = TrackingBc::create($trackingBcDatas);
            $request['tracking_bc_id'] = $trackingBc->id;
        }

        $products = $request->products;
        $categoryProducts = $request->categories;
        $discounts = $request->discounts;
        $datas = $request->all();
        $coupon->update($datas);
        $couponProducts = $coupon->Products;
        $couponProductCategories = $coupon->Categories;
        $couponDiscounts = $coupon->Discounts;

        if($couponProducts){
            foreach ($couponProducts as $key => $couponProduct) {
                $couponProduct->delete();
            }
        }

        if($couponProductCategories){
            foreach ($couponProductCategories as $key => $couponProductCategory) {
                $couponProductCategory->delete();
            }
        }

        if($couponDiscounts){
            foreach ($couponDiscounts as $key => $couponDiscount) {
                $couponDiscount->delete();
            }
        }

        foreach ($products as $key => $product) {
            CouponProduct::create([
                'ecommerce_coupon_id' => $coupon->id,
                'ecommerce_product_id' => $product['value']
            ]);
        }

        foreach ($categoryProducts as $key => $categoryProduct) {
            CouponProductCategory::create([
                'ecommerce_coupon_id' => $coupon->id,
                'ecommerce_product_category_id' => $categoryProduct['value']
            ]);
        }

        foreach ($discounts as $key => $discount) {
            $discount['ecommerce_coupon_id'] = $coupon->id;
            CouponDiscount::create($discount);
        }

        // CouponGenerate::storeCoupon($coupon);

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
            'tracking_bc' => $coupon->trackingBc
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $coupon = Coupon::find($id);
        $couponProducts = $coupon->Products;
        $couponDiscounts = $coupon->Discounts;
        $couponProductCategories = $coupon->ProductCategories;

        if($couponProducts){
            foreach ($couponProducts as $key => $couponProduct) {
                $couponProduct->delete();
            }
        }

        if($couponProductCategories){
            foreach ($couponProductCategories as $key => $couponProductCategory) {
                $couponProductCategory->delete();
            }
        }

        if($couponDiscounts){
            foreach ($couponDiscounts as $key => $couponDiscount) {
                $couponDiscount->delete();
            }
        }

        TrackingBc::where('id',$coupon->tracking_bc_id)->delete();
        $coupon->delete();
        return response()->json([
            'msg_return' => 'ลบข้อมูลสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function uploadMultiple(Request $request)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        // dd($request->all());
        ImageFile::checkFolderDefaultPath();
        $datas = collect();
        if($request->img_items){
            foreach ($request->img_items as $key => $img_item) {
                $dateNow = Carbon::now()->format('dmY_His');
                $fileImage = $img_item;
                $type = null;
                // ImageFile::checkFolderDefaultPath();
                $destinationPath = 'ecommerce/coupon'; // upload path
                $extension = $fileImage->getClientOriginalExtension(); // getting image extension
                $fileName = $dateNow.'-'.$key.'.'.$extension; // renameing image
                $fileImage->move($destinationPath, $fileName); // uploading file to given path

                $imageFile = ImageFile::create([
                    'img_url' => URL::to('/')."/".$destinationPath."/".$fileName,
                    'img_size' => null,
                    'type' => null,
                ]);

                $datas->put($key,$imageFile);
            }
        }

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
            'datas' => $datas,
        ]);
    }

    // public function downloadDataCouponAll(Request $request)
    // {
    //     $startDate = $request->start_date;
    //     $endDate = $request->end_date;
    //     $couponId = $request->coupon_id;
    //     $datas = [];
    //     if($couponId != -1){
    //         $coupons = Coupon::whereNull('is_famliy')->where('id',$couponId)->whereBetween('created_at', array($startDate, $endDate))->get();
    //     }else{
    //         $coupons = Coupon::whereNull('is_famliy')->whereBetween('created_at', array($startDate, $endDate))->get();
    //     }
    //     foreach ($coupons as $key => $coupon) {
    //         $userCheckCoupons = $coupon->userCheckCoupons;
    //         // $couponUserReedeems = $coupon->couponUserReedeems;
    //         $couponUserUsed = CouponUser::where('coupon_id',$coupon->id)->whereNotNull('reedeem_date');
    //         $couponStaticForm = $coupon->couponStaticForm;
    //         $datas[$key]['No.'] = $key+1;
    //         $datas[$key]['Coupon Name'] = $coupon->name;
    //         $datas[$key]['Alt Text'] = $coupon->alt_text;
    //         $datas[$key]['Reedeemed'] = $userCheckCoupons->count();
    //         $datas[$key]['Maximum Winner'] = ($coupon->is_winner_nolimit == 1)? 'unlimit' : $coupon->maximum_winner;
    //         $datas[$key]['Used'] = $couponUserUsed->count();
    //         $datas[$key]['Set Winning Odds (%)'] = $coupon->winning_odds;
    //         $datas[$key]['Prefix Name'] = $coupon->prefix_name;
    //         $datas[$key]['Form Name'] = ($couponStaticForm)? $couponStaticForm->name : null;
    //         $datas[$key]['Start date'] = $coupon->start_date;
    //         $datas[$key]['end date'] = $coupon->end_date;
    //     }

    //     $dateNow = Carbon::now()->format('dmY_Hi');

    //     Excel::create('coupon_data_'.$dateNow, function($excel) use ($datas) {
    //         $excel->sheet('coupon_data', function($sheet) use ($datas)
    //         {
    //             $sheet->fromArray($datas);
    //         });
    //     })->download('csv');
    // }

    // public function couponStaticFormDownloadDataNew(Request $request)
    // {
    //     $count = 0;
    //     $startDate = $request->start_date;
    //     $endDate = $request->end_date;
    //     $staticFormId = $request->static_form_id;
    //     $datas = [];
    //     $couponStaticForm = CouponStaticForm::find($staticFormId);
    //     $couponStaticFormItems = $couponStaticForm->items->where('is_active',1);
    //     $couponStaticFormDatas = $couponStaticForm->datas;
    //     $coupons = $couponStaticForm->coupons;
    //     foreach ($couponStaticFormDatas->groupBy('line_user_id','coupon_id') as $key => $couponStaticFormData) {
    //         $count++;
    //         $lineUserProfile = $couponStaticFormData->first()->lineUserProfile;
    //         $coupon = Coupon::find($couponStaticFormData->first()->coupon_id);
    //         $couponReedeem = CouponReedeem::where('coupon_id',$coupon->id)->where('line_user_id',$lineUserProfile->id)->first();
    //         $userCheckCode = UserCheckCoupon::where('coupon_id',$coupon->id)->where('line_user_id',$lineUserProfile->id)->first();
    //         $couponUser = CouponUser::where('coupon_id',$coupon->id)->where('line_user_id',$lineUserProfile->id)->first();
    //         $couponReedeemCode = CouponReedeemCode::where('coupon_id',$coupon->id)->where('line_user_id',$lineUserProfile->id)->first();
    //         $code = "";
    //         $reedeemDate = "";
    //         $isReedeem = "";
    //         if($couponReedeemCode){
    //             if($coupon->is_running_number == 1){
    //                 $code = $couponReedeemCode->prefix_code.$couponReedeemCode->running_code;
    //             }else{
    //                 $code = $couponReedeemCode->prefix_code;
    //             }
    //         }

    //         if($couponUser){
    //             $reedeemDate = $couponUser->reedeem_date;
    //             $isReedeem = ($couponUser->flag_status == 'reedeem')? 'yes' : 'no';
    //         }
    //         $datas[$key]['No.'] = $count;
    //         $datas[$key]['Source'] = 'Line';
    //         $datas[$key]['Line User ID'] = $key;
    //         // dd($couponStaticFormItems);
    //         foreach ($couponStaticFormItems as $staticFormItemIndex => $couponStaticFormItem) {
    //             // dd($couponStaticFormItem->settingPlaceholder);
    //             $header = "";
    //             $label = $couponStaticFormItem->settingLabel;
    //             $placeHolder = $couponStaticFormItem->settingPlaceholder;
    //             if($label != null || $placeHolder != null){
    //                 if($label != null){
    //                     $header = $label->values;
    //                 }else{
    //                     $header = $placeHolder->values;
    //                 }
    //             }else{
    //                 $header = $couponStaticFormItem->title;
    //             }
    //             if($couponStaticFormItem->el_id == 'google_map_location'){
    //                 $couponStaticFormItem->el_id = 'google_address';
    //                 $header = 'google_address';
    //             }
    //             $data =  $couponStaticFormData->where('el_id',$couponStaticFormItem->el_id)->first();
    //             if($couponStaticFormItem->el_id == 'txt_tel'){
    //                 $datas[$key][$header] = ($data)? "'".$data->value : null;
    //             }else{
    //                 $datas[$key][$header] = ($data)? $data->value : null;
    //             }
    //         }

    //         $datas[$key]['Activity'] = $coupon->name;
    //         $datas[$key]['From Register Date'] = $data->created_at;
    //         $datas[$key]['Activity Download Date'] = ($couponUser)? $couponUser->created_at : '-';
    //         $datas[$key]['Activity Use Date'] = ($couponUser)? $couponUser->reedeem_date : '-' ;
    //         $datas[$key]['Coupon Redeem Code'] = ($couponReedeemCode)? $code : '-' ;
    //     }
        
    //     $dateNow = Carbon::now()->format('dmY_Hi');

    //     Excel::create('coupon_static_data_new_'.$dateNow, function($excel) use ($datas) {
    //         $excel->sheet('sheet1', function($sheet) use ($datas)
    //         {
    //             $sheet->fromArray($datas);
    //         });
    //     })->download('csv');
    // }

    // public function couponDetail(Request $request,$id)
    // {
    //     $datas = [];
    //     $startDate = $request->start_date;
    //     $endDate = $request->end_date;
    //     $coupon = Coupon::find($id);
    //     $couponGenerate = CouponGenerate::where('coupon_id',$coupon->id)->first();
    //     if($startDate != "" && $endDate != ""){
    //         $couponUsers = $coupon->couponUsers->whereBetween('created_at', array($startDate, $endDate));
    //         $couponUserReedeems = $coupon->couponUserReedeems->whereBetween('created_at', array($startDate, $endDate));
    //         $couponUserChecks = $coupon->userCheckCoupons->whereBetween('created_at', array($startDate, $endDate));
    //         $couponUserUsed = CouponUser::where('coupon_id',$coupon->id)->whereNotNull('reedeem_date')->whereBetween('created_at', array($startDate, $endDate));
    //         $trackingRecieveBcs = TrackingRecieveBc::where('tracking_bc_id',$coupon->tracking_bc_id)->whereBetween('created_at', array($startDate, $endDate))->get();
    //         $trackingRecieveBcGroupByLineUserIds = TrackingRecieveBc::select('*', \DB::raw('count(*) as countClick'))
    //             ->where('tracking_bc_id',$coupon->tracking_bc_id)
    //             ->whereBetween('created_at', array($startDate, $endDate))
    //             ->groupBy('line_user_id')->orderByDesc('created_at')->get();
    //     }else{
    //         $couponUsers = $coupon->couponUsers;
    //         $couponUserReedeems = $coupon->couponUserReedeems;
    //         $couponUserChecks = $coupon->userCheckCoupons;
    //         $couponUserUsed = CouponUser::where('coupon_id',$coupon->id)->whereNotNull('reedeem_date');
    //         $trackingRecieveBcs = TrackingRecieveBc::where('tracking_bc_id',$coupon->tracking_bc_id)->get();
    //         $trackingRecieveBcGroupByLineUserIds = TrackingRecieveBc::select('*', \DB::raw('count(*) as countClick'))
    //             ->where('tracking_bc_id',$coupon->tracking_bc_id)
    //             ->groupBy('line_user_id')->orderByDesc('created_at')->get();
    //     }

    //     $datas = $coupon->toArray();
    //     $datas['coupon_is_reedeem'] = $couponUsers->count();
    //     $datas['coupon_is_uses'] = $couponUserUsed->count();
    //     $datas['coupon_is_reedeemed'] = $couponUserReedeems->count();
    //     $datas['coupon_reedeem_count'] = $couponUserReedeems->count();
    //     $datas['coupon_used_count'] = $couponUserUsed->count();
    //     $datas['coupon_click_count'] = $trackingRecieveBcs->count();
    //     $datas['coupon_click_count_percent'] = $datas['coupon_is_reedeem']/$datas['coupon_click_count'];
    //     $datas['coupon_unique_click_count'] = ($trackingRecieveBcs->count() > 0)? $trackingRecieveBcs->unique('line_user_id')->count() : 0;
    //     $datas['coupon_unique_click_count_percent'] = $datas['coupon_is_reedeem']/$datas['coupon_unique_click_count'];
    //     $datas['coupon_img_url'] = $couponGenerate->coupon_section_img_url;
    //     foreach ($trackingRecieveBcGroupByLineUserIds as $key => $trackingRecieveBcGroupByLineUserId) {
    //         $lineUserProfile = LineUserProfile::find($trackingRecieveBcGroupByLineUserId->line_user_id);
    //         $datas['users'][$key]['line_user_id'] = $lineUserProfile->id;
    //         $datas['users'][$key]['display_name'] = $lineUserProfile->name;
    //         $datas['users'][$key]['display_img_url'] = $lineUserProfile->avatar;
    //         $datas['users'][$key]['click_total'] = $trackingRecieveBcGroupByLineUserId->countClick;
    //     }

    //     $clickGroupByDates = $trackingRecieveBcs->groupBy(function($date) {
    //         return Carbon::parse($date->created_at)->format('Y-m-d');
    //     });

    //     foreach ($clickGroupByDates as $key => $clickGroupByDate) {
    //         $datas['graphs'][$key]['count_coupon_click'] = $clickGroupByDate->count();
    //     }

    //     $reedeemGroupByDates = $couponUserReedeems->groupBy(function($date) {
    //         return Carbon::parse($date->created_at)->format('Y-m-d');
    //     });

    //     foreach ($reedeemGroupByDates as $key => $reedeemGroupByDate) {
    //         $datas['graphs'][$key]['count_coupon_reedeem'] = $reedeemGroupByDate->count();
    //     }

    //     $couponUserUsed = $couponUserUsed->get();

    //     $usedGroupByDates = $couponUserUsed->groupBy(function($date) {
    //         return Carbon::parse($date->created_at)->format('Y-m-d');
    //     });

    //     foreach ($usedGroupByDates as $key => $usedGroupByDate) {
    //         $datas['graphs'][$key]['count_coupon_used'] = $usedGroupByDate->count();
    //     }

    //     return response()->json([
    //         'datas' => $datas,
    //     ]);
    // }

    // public function couponDetailExport(Request $request,$id)
    // {
    //     $datas = [];
    //     $startDate = $request->start_date;
    //     $endDate = $request->end_date;
    //     $coupon = Coupon::find($id);
    //     $filter = $request->filter;
    //     if($startDate != "" && $endDate != ""){
    //         $trackingRecieveBcGroupByLineUserIds = TrackingRecieveBc::select('*', \DB::raw('count(*) as countClick'))
    //         ->where('tracking_bc_id',$coupon->tracking_bc_id)
    //         ->whereBetween('created_at', array($startDate, $endDate))
    //         ->groupBy('line_user_id')->orderByDesc('created_at')->get();
    //     }else{
    //         $trackingRecieveBcGroupByLineUserIds = TrackingRecieveBc::select('*', \DB::raw('count(*) as countClick'))
    //         ->where('tracking_bc_id',$coupon->tracking_bc_id)
    //         ->groupBy('line_user_id')->orderByDesc('created_at')->get();
    //     }
    //     $count = 1;

    //     foreach ($trackingRecieveBcGroupByLineUserIds as $key => $trackingRecieveBcGroupByLineUserId) {
    //         $lineUserProfile = LineUserProfile::find($trackingRecieveBcGroupByLineUserId->line_user_id);
    //         $lineUserData = CouponStaticFixForm::where('line_user_id',$lineUserProfile->id)->first();
    //         $trackingRecieveBcs = TrackingRecieveBc::orderByDesc('created_at')->where('tracking_bc_id',$coupon->tracking_bc_id)->where('line_user_id',$trackingRecieveBcGroupByLineUserId->line_user_id)->get();
    //         $couponUserRedeem = UserCheckCoupon::orderByDesc('created_at')->where('coupon_id',$coupon->id)->where('line_user_id',$lineUserProfile->id)->whereIn('flag_status',['success','fail'])->first();
    //         $couponUserUsed = CouponUser::where('coupon_id',$coupon->id)->where('line_user_id',$lineUserProfile->id)->whereNotNull('reedeem_date')->first();
    //         $couponReedeemCode = CouponReedeemCode::where('coupon_id',$coupon->id)->where('line_user_id',$lineUserProfile->id)->first();
    //         $datas[$count]['No.'] = $count;
    //         $datas[$count]['Coupon Title'] = $coupon->name;
    //         $datas[$count]['User ID'] = $lineUserProfile->id;
    //         $datas[$count]['Name'] = "";
    //         $datas[$count]['Tel'] = "";
    //         $datas[$count]['email'] = "";

    //         if($filter != "Coupon Redeemed" && $filter != "Coupon Used"){
    //             $datas[$count]['Click'] = $trackingRecieveBcs->count();
    //             $datas[$count]['Click Date'] = $trackingRecieveBcs->first()->created_at->format('d/m/Y H:i:s');
    //         }

    //         if($filter != "Coupon Click" && $filter != "Coupon Used"){
    //             $datas[$count]['Redeem'] = ($couponUserRedeem)? "Yes" : "No";
    //             $datas[$count]['Redeem Date'] = ($couponUserRedeem)? $couponUserRedeem->created_at->format('d/m/Y H:i:s') : "";
    //         }
            
    //         if($filter != "Coupon Click" && $filter != "Coupon Redeemed"){
    //             $datas[$count]['Used'] = ($couponUserUsed)? "Yes" : "No";
    //             $datas[$count]['Used Date'] = ($couponUserUsed)? $couponUserUsed->created_at->format('d/m/Y H:i:s') : "";
    //         }

    //         $datas[$count]['Winning Rate'] = ($couponUserRedeem)? $couponUserRedeem->user_get_coupon_percent : 0;
    //         $datas[$count]['Coupon Code'] = ($couponReedeemCode)? $couponReedeemCode->prefix_code.$couponReedeemCode->running_code : 0;

    //         if($lineUserData){
    //             $datas[$count]['Name'] = $lineUserData->name;
    //             $datas[$count]['Tel'] = $lineUserData->tel;
    //             $datas[$count]['email'] = $lineUserData->email;
    //         }
            
    //         $count++;
    //     }

    //     $dateNow = Carbon::now()->format('dmY_Hi');

    //     Excel::create('coupon_report_'.$dateNow, function($excel) use ($datas) {
    //         $excel->sheet('sheet1', function($sheet) use ($datas)
    //         {
    //             $sheet->fromArray($datas);
    //         });
    //     })->download('xls');
    // }
}
