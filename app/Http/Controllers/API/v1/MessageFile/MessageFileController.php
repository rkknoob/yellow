<?php

namespace YellowProject\Http\Controllers\API\v1\MessageFile;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\MessageFile\MessageFile;
use Carbon\Carbon;
use URL;

class MessageFileController extends Controller
{
    public function uploadMultiple(Request $request)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
    	MessageFile::checkFolderMessageFile();
        $datas = collect();
        if($request->img_items){
            foreach ($request->img_items as $key => $img_item) {
                $dateNow = Carbon::now()->format('dmY_His');
                $fileImage = $img_item;
                $type = null;
                $destinationPath = 'message-file'; // upload path
                $extension = $fileImage->getClientOriginalExtension(); // getting image extension
                $fileName = $dateNow.'-'.$key.'.'.$extension; // renameing image
                $fileImage->move($destinationPath, $fileName); // uploading file to given path

                $imageFile = MessageFile::create([
                    'img_url' => URL::to('/')."/".$destinationPath."/".$fileName,
                    'img_size' => null,
                ]);

                $datas->put($key,$imageFile);
            }
        }

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
            'datas' => $datas,
        ]);
    }
}
