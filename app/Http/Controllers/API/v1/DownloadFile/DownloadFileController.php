<?php

namespace YellowProject\Http\Controllers\API\v1\DownloadFile;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\DownloadFile\DownloadFile;

class DownloadFileController extends Controller
{
    public function getFileSubscriber(Request $request)
    {

    	$downloadFileSusbcribers = DownloadFile::orderByDesc('created_at')->get();

    	return response()->json([
            'datas' => $downloadFileSusbcribers,
        ]);
    }
}
