<?php

namespace YellowProject\Http\Controllers\API\v1\Eplus\Salesman;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Eplus\Salesman\TempMessage;
use YellowProject\Eplus\Salesman\TempMessageMain;

class TempMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $datas = collect();
        $tempMessages = TempMessage::all();
        $tempMessageMain = TempMessageMain::first();
        $datas->put('title',($tempMessageMain)? $tempMessageMain->title : null);
        $datas->put('alt_text',($tempMessageMain)? $tempMessageMain->alt_text : null);
        $dataArrays = [];
        foreach ($tempMessages as $key => $tempMessage) {
            $dataArrays[$key]['seq_no'] = $tempMessage->seq;
            $dataArrays[$key]['type'] = $tempMessage->type;
            $dataArrays[$key]['value']['display'] = $tempMessage->display;
            $dataArrays[$key]['value']['playload'] = $tempMessage->playload;
        }
        $datas->put('items',$dataArrays);

        return response()->json([
            'datas' => $datas,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        TempMessage::truncate();
        TempMessageMain::truncate();
        TempMessageMain::create([
            'title' => $request->title,
            'alt_text' => $request->alt_text,
        ]);
        $items = $request->items;
        foreach ($items as $key => $item) {
            TempMessage::create([
                'seq' => $item['seq_no'],
                'type' => $item['type'],
                'display' => $item['value']['display'],
                'playload' => $item['value']['playload'],
            ]);
        }

        return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
