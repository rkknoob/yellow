<?php

namespace YellowProject\Http\Controllers\API\v1\Eplus\Salesman;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Eplus\Salesman\SalesmanDataMapping;
use YellowProject\Eplus\Salesman\SalesmanRegisterData;

class SalesmanController extends Controller
{
    public function getIndexData()
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
    	$datas = [];
    	$salesmanDataMappings = SalesmanDataMapping::all();

    	return response()->json([
            'datas'          => $salesmanDataMappings,
        ]);
    }

    public function storeMappingData(Request $request)
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
    	$csvs = $request->csv;
    	foreach ($csvs as $key => $csv) {
    		$salesmanDataMapping = SalesmanDataMapping::where('banner_code',$csv['banner_code'])->first();
    		if($salesmanDataMapping){
    			$salesmanDataMapping->update($csv);
    		}else{
    			SalesmanDataMapping::create($csv);
    		}
    	}

    	return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }

    public function getSalesmanCode()
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
        $datas = collect();
        $salesmanDataMappings = SalesmanDataMapping::all();
        $salesmanDataMappingDataPlucks = $salesmanDataMappings->pluck('fs_salesman_code')->unique();
        $dataArrays = [];
        foreach ($salesmanDataMappingDataPlucks as $key => $salesmanDataMappingDataPluck) {
            $dataArrays[] = $salesmanDataMappingDataPluck;
        }
        $datas->push($dataArrays);

        return response()->json([
            'datas' => $dataArrays,
        ]);
    }
}
