<?php

namespace YellowProject\Http\Controllers\API\v1\Eplus\Customer;

use Illuminate\Http\Request;
use YellowProject\Http\Controllers\Controller;
use YellowProject\Eplus\Customer\CustomerDataMapping;
use YellowProject\Eplus\Customer\CustomerRegisterData;
use YellowProject\Eplus\Salesman\SalesmanRegisterData;
use YellowProject\Eplus\TriggerMessage\TriggerMessage;

class CustomerController extends Controller
{
    public function getIndexData()
    {
        $msg = "";
        $headers = $request->header();
        if(array_key_exists('atoken', $headers)){
            $isAPI = \YellowProject\GeneralFunction\CoreFunction::checkToken($headers['user-id'][0],$headers['atoken'][0]);
            if($isAPI){
                // $msg = "ข้อมูลการ Authen ถูกต้อง";
            }else{
                $msg = "ข้อมูลการ Authen ไม่ถูกต้อง";
                return response()->json([
                    'code_return' => 88,
                    'msg' => $msg,
                ]);
            }
        }else{
            $msg = "ไม่พบข้อมูล Authen";
            return response()->json([
                'code_return' => 99,
                'msg' => $msg,
            ]);
        }
    	$datas = [];
    	$customerDataMappings = CustomerDataMapping::all();

    	return response()->json([
            'datas'          => $customerDataMappings,
        ]);
    }

    public function storeMappingData(Request $request)
    {
    	$csvs = $request->csv;
    	foreach ($csvs as $key => $csv) {
    		$customerDataMapping = CustomerDataMapping::where('banner_code',$csv['banner_code'])->first();
    		if($customerDataMapping){
    			$customerDataMapping->update($csv);
    		}else{
    			$customerDataMapping = CustomerDataMapping::create($csv);
    		}

            $customerRegisterDatas = CustomerRegisterData::where('banner_code',$customerDataMapping->banner_code)->get();
            $salesmanRegisterDatas = SalesmanRegisterData::where('fs_salesman_code',$customerDataMapping->fs_salesman_code)->get();
            if($customerRegisterDatas->count() > 0){
                foreach ($customerRegisterDatas as $key => $customerRegisterData) {
                    TriggerMessage::sentMessageCustomer($customerDataMapping,$customerRegisterData);
                }
            }
            if($salesmanRegisterDatas->count() > 0){
                foreach ($salesmanRegisterDatas as $key => $salesmanRegisterData) {
                    TriggerMessage::sentMessageSalesman($customerDataMapping,$salesmanRegisterData);
                }
            }
    	}

    	return response()->json([
            'msg_return' => 'บันทึกสำเร็จ',
            'code_return' => 1,
        ]);
    }
}
