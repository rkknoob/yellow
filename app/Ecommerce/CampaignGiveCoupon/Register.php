<?php

namespace YellowProject\Ecommerce\CampaignGiveCoupon;

use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'fact_ufs_customer_register_coupon';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'line_user_id',
    	'first_name',
        'last_name',
		'address',
		'post_code',
		'phone_number',
		'email',
		'shop_name',
		'shop_type',
		'embroidered_name',
		'is_active_term',
		'is_active_news',
    ];

}
