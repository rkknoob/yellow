<?php

namespace YellowProject\Ecommerce\OnlineExclusiveModule;

use Illuminate\Database\Eloquent\Model;

class OnlineExclusiveModuleCourseVideo extends Model
{
    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dim_online_exclusive_course_videos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'online_exclusive_course_id',
    	'name',
        'thumbnail_img_url',
        'manual_video_url',
        'upload_video_url',
    ];
}