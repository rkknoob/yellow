<?php

namespace YellowProject\Ecommerce\OnlineExclusiveModule;

use Illuminate\Database\Eloquent\Model;

class OnlineExclusiveModuleCustomerReDeem extends Model
{
    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'fact_online_exclusive_course_customer_redeem';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'line_user_id',
    	'customer_id',
        'online_exclusive_course_id',
        'course_name',
        'description',
        'img_url',
        'ep',
        'redeem_date',
    ];
}
