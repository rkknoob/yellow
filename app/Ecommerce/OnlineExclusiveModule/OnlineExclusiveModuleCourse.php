<?php

namespace YellowProject\Ecommerce\OnlineExclusiveModule;

use Illuminate\Database\Eloquent\Model;

class OnlineExclusiveModuleCourse extends Model
{
    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dim_online_exclusive_course';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'course_name',
    	'description',
        'img_url',
        'ep',
        'status',
    ];
}
