<?php

namespace YellowProject\Ecommerce\OnlineExclusiveModule;

use Illuminate\Database\Eloquent\Model;

class OnlineExclusiveModuleCourseFiles extends Model
{
    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dim_online_exclusive_course_files';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'url_path',
    	'type',
    ];

    public static function checkFolderDefaultPath($path)
    {
        if (!\File::isDirectory($path)){
            self::createFolder($path);
        }
    }

    public static function createFolder($path_save_image)
    {
        $result = \File::makeDirectory($path_save_image, 0775, true);
        return $result;
    }
}
