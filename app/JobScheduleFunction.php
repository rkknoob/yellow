<?php

namespace YellowProject;

use Illuminate\Database\Eloquent\Model;
use YellowProject\LineWebHooks;
use YellowProject\LineSettingBusiness;
use YellowProject\DownloadFile\DownloadFile;
use YellowProject\DownloadFile\DownloadFileMain;
use YellowProject\DownloadFile\DownloadFileMainFunction;
use YellowProject\Ecommerce\Product;
use YellowProject\Ecommerce\ProductRecommend\ProductRecommend;
use YellowProject\Ecommerce\Coupon\Coupon;
use YellowProject\Ecommerce\Order\Order;
use YellowProject\Ecommerce\CoreLineFunction\OrderOverDuePayment48;
use YellowProject\Ecommerce\CoreLineFunction\OrderOverDuePayment72;
use YellowProject\Ecommerce\Customer\Customer;
use YellowProject\ScheduleCheck\ScheduleCheck;
use YellowProject\ScheduleCheck\ScheduleCheckStatus;
use Carbon\Carbon;

class JobScheduleFunction extends Model
{
    public static function checkFunctionDownload()
    {
        $downloadFileMains = DownloadFileMain::where('is_active',1)->get();
        foreach ($downloadFileMains as $key => $downloadFileMain) {
            if($downloadFileMain->type == 'subscriber_single'){
                $downloadFileMain->update([
                    'is_active' => 0
                ]);
                DownloadFileMainFunction::downloadFileSubscriberSingle($downloadFileMain->main_id);
            }else if($downloadFileMain->type == 'keyword_inquiry'){
                $downloadFileMain->update([
                    'is_active' => 0
                ]);
                DownloadFileMainFunction::downloadFileKeywordInquiry(unserialize($downloadFileMain->requests));
            }else if($downloadFileMain->type == 'order_all'){
                $downloadFileMain->update([
                    'is_active' => 0
                ]);
                DownloadFileMainFunction::downloadOrderAll(unserialize($downloadFileMain->requests));
            }else if($downloadFileMain->type == 'order_dt'){
                $downloadFileMain->update([
                    'is_active' => 0
                ]);
                DownloadFileMainFunction::downloadOrderDt(unserialize($downloadFileMain->requests));
            }
        }
    }

    public static function refreshToken()
    {
        $refreshToken = LineWebHooks::refreshToken();
        $lineSettingBusiness = LineSettingBusiness::where('active',1)->first();
        $lineSettingBusiness->update([
            'channel_access_token' => $refreshToken->access_token,
        ]);
    }

    public static function checkEcommerceProduct()
    {
        $products = Product::all();
        $dateNow = Carbon::now()->format('Y-m-d H:i:s');
        foreach ($products as $key => $product) {
            if($product->start_date <= $dateNow && $product->end_date >= $dateNow){
                $product->update([
                    'status' => 'Published'
                ]);
            }else{
                $product->update([
                    'status' => 'Not Published'
                ]);
            }
        }
    }

    public static function checkEcommerceProductRecommend()
    {
        $productRecommends = ProductRecommend::all();
        $dateNow = Carbon::now()->format('Y-m-d H:i:s');
        foreach ($productRecommends as $key => $productRecommend) {
            if($productRecommend->start_date <= $dateNow && $productRecommend->end_date >= $dateNow){
                $productRecommend->update([
                    'is_active' => 1
                ]);
            }else{
                $productRecommend->update([
                    'is_active' => 0
                ]);
            }
        }
    }

    public static function checkEcommerceCoupon()
    {
        $coupons = Coupon::all();
        $dateNow = Carbon::now()->format('Y-m-d H:i:s');
        foreach ($coupons as $key => $coupon) {
            if($coupon->start_date <= $dateNow && $coupon->end_date >= $dateNow){
                $coupon->update([
                    'disply_on_main_page' => 1
                ]);
            }else{
                $coupon->update([
                    'disply_on_main_page' => 0
                ]);
            }
        }
    }

    public static function checkFileDownloadExp()
    {
        // $downloadFiles = DownloadFile::whereRaw('TIMESTAMPDIFF(MINUTE, now(), deleted_at) <= 0')->get();
        // foreach ($downloadFiles as $key => $downloadFile) {
        //     $downloadFile->delete();
        // }
        $dateNow = Carbon::now()->format('Y-m-d H:i:s');
        DownloadFileMain::where('is_active',0)->delete();
        $downloadFiles = DownloadFile::where('deleted_at','<=',$dateNow)->get();
        foreach ($downloadFiles as $key => $downloadFile) {
            \Storage::delete($downloadFile->file_link_path);
            $downloadFile->delete();
            // dd($downloadFile);
        }
        // dd($downloadFiles);
    }

    public static function checkPaymentOverdue48()
    {
        $dateNow = Carbon::now()->format('Y-m-d H:i:s');
        $orders = Order::where('order_status','Waiting for Payment')
          ->where('send_message_48_hours',0)
          ->where(\DB::raw('TIMESTAMPDIFF(HOUR, created_at, "'.$dateNow.'")'),'>=',48)
          ->get();
        foreach ($orders as $key => $order) {
            $customer = Customer::find($order->ecommerce_customer_id);
            $order->update([
                'send_message_48_hours' => 1,
            ]);
            OrderOverDuePayment48::sentMessageCustomerPaymentOverdue48($customer,$order);
            
        }
    }

    public static function checkPaymentOverdue72()
    {
        $dateNow = Carbon::now()->format('Y-m-d H:i:s');
        $orders = Order::where('order_status','Waiting for Payment')
          ->where('send_message_48_hours',1)
          ->where('send_message_72_hours',0)
          ->where(\DB::raw('TIMESTAMPDIFF(HOUR, created_at, "'.$dateNow.'")'),'>=',72)
          ->get();
        foreach ($orders as $key => $order) {
            $customer = Customer::find($order->ecommerce_customer_id);
            $order->update([
                'send_message_72_hours' => 1,
                'order_status' => 'Cancel By System',
            ]);
            OrderOverDuePayment72::sentMessageCustomerPaymentOverdue72($customer,$order);
        }
    }

    public static function runCheckSchedule()
    {
        $scheduleCheck = ScheduleCheck::first();
        if($scheduleCheck){
            if($scheduleCheck->is_active == 1){
                $scheduleCheck->update([
                    'is_active' => 0
                ]);
                ScheduleCheckStatus::testPushMessage();
            }
        }
    }
}
