<?php

namespace YellowProject\LineFunction;

use Illuminate\Database\Eloquent\Model;
use YellowProject\LineSettingBusiness;

class LineFunctionLiff extends Model
{
    public static function createLiff($url)
    {
    	$results = [];
        $lineSettingBusiness = LineSettingBusiness::where('active',true)->first();
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.line.me/liff/v1/apps",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "{\r\n  \"view\":{\r\n    \"type\":\"full\",\r\n    \"url\":\"".$url."\"\r\n  }\r\n}",
		  CURLOPT_HTTPHEADER => array(
		    "Authorization: Bearer ".$lineSettingBusiness->channel_access_token,
		    "Cache-Control: no-cache",
		    "Content-Type: application/json",
		    "Postman-Token: 382b8ee0-4301-48e1-a166-4364926488c2"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			$results['is_liff'] = 0;
		  	\Log::debug("LiffLog cURL Error #:" . $err);
		} else {
			$results['is_liff'] = 1;
			$response = json_decode($response);
			dd($response);
			$results['liff_id'] = $response->liffId;
		}

		return $results;
    }

    public static function updateLiff($url,$liffId)
    {
    	$lineSettingBusiness = LineSettingBusiness::where('active',true)->first();
    	$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.line.me/liff/v1/apps/".$liffId."/view",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "PUT",
		  CURLOPT_POSTFIELDS => "{\r\n  \"type\":\"tall\",\r\n  \"url\":\"".$url."\"\r\n}",
		  CURLOPT_HTTPHEADER => array(
		    "Authorization: Bearer ".$lineSettingBusiness->channel_access_token,
		    "Cache-Control: no-cache",
		    "Content-Type: application/json",
		    "Postman-Token: c8a6db88-c41c-4be5-bb16-2f404a399699"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  \Log::debug("LiffLog cURL Error #:" . $err);
		}
    }
}
