<?php

namespace YellowProject\ScheduleCheck;

use Illuminate\Database\Eloquent\Model;

class ScheduleCheck extends Model
{
    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dim_schedule_run';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'is_active',
    ];
}
