<?php

namespace YellowProject\ScheduleCheck;

use Illuminate\Database\Eloquent\Model;

class ScheduleCheckStatus extends Model
{
    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'fact_chedule_run_items';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mid',
        'message',
    ];

    public static function testPushMessage()
    {
    	$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.line.me/v2/bot/message/push",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "{\r\n    \"to\": \"U82759f4b97da598171d2d2e135550ae3\",\r\n    \"messages\":[\r\n        {\r\n            \"type\":\"text\",\r\n            \"text\":\"Hello, world1\"\r\n        },\r\n        {\r\n            \"type\":\"text\",\r\n            \"text\":\"Hello, world2\"\r\n        }\r\n    ]\r\n}",
		  CURLOPT_HTTPHEADER => array(
		    "Authorization: Bearer  AsTGyIVuJe9aKj/F7rA7OfA9fvMuZg2Q6eQm38WyvcXlTcPX1P8g6Ml/GTJthtwXNk3s1CC9mWWsnHCG6+y9J1dLKwHfa9SLlZACsc9B4zfGCV2KkqLKYDJckdED/Nhkx6QezA6emm3dw2B2oMO95Ey2YsWTVRc8Nid2mqOezNUl+kvMVrWOT+Lg+UudrjI/",
		    "Cache-Control: no-cache",
		    "Content-Type: application/json",
		    "Postman-Token: 3fc2dc6c-4570-47aa-90e9-2df563636955"
		  ),
		));
		$text = "";

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  // echo "cURL Error #:" . $err;
			$text = "cURL Error #:" . $err;
		} else {
		  // echo $response;
		}

		ScheduleCheck::create([
			'mid' => 'U82759f4b97da598171d2d2e135550ae3',
			'message' => $text
		]);
    }
}
