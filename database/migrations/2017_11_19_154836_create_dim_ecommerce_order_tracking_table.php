<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimEcommerceOrderTrackingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_ecommerce_order_tracking', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id');
            $table->integer('img_id')->nullable();
            $table->string('tracking_status')->nullable();
            $table->string('tracking')->nullable();
            $table->string('check_tracking')->nullable();
            $table->integer('email_to_customer_template_id')->nullable();
            $table->string('email_to_customer_status')->nullable();
            $table->integer('email_to_dt_template_id')->nullable();
            $table->string('email_to_dt_status')->nullable();
            $table->integer('email_to_admin_template_id')->nullable();
            $table->string('email_to_admin_status')->nullable();
            $table->integer('line_to_customer_template_id')->nullable();
            $table->string('line_to_customer_status')->nullable();
            $table->integer('line_to_dt_template_id')->nullable();
            $table->string('line_to_dt_status')->nullable();
            $table->integer('line_to_admin_template_id')->nullable();
            $table->string('line_to_admin_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_ecommerce_order_tracking');
    }
}
