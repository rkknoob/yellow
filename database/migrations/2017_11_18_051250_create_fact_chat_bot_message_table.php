<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactChatBotMessageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_chat_bot_message', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('line_user_id');
            $table->string('message');
            $table->boolean('is_move_message')->default(0);
            $table->string('message_type');
            $table->string('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_chat_bot_message');
    }
}
