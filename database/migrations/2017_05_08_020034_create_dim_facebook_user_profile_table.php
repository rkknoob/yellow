<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimFacebookUserProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_facebook_user_profile', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('line_user_id')->nullable();
            $table->string('facebook_id')->nullable();
            $table->string('birthday')->nullable();
            $table->string('first_name')->nullable();
            $table->string('gender')->nullable();
            $table->string('last_name')->nullable();
            $table->string('link')->nullable();
            $table->string('name')->nullable();
            $table->text('picture')->nullable();
            $table->text('thumbnail')->nullable();
            $table->string('email')->nullable();
            $table->string('platform')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_facebook_user_profile');
    }
}
