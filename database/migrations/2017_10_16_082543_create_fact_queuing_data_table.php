<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactQueuingDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_queuing_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('auto_reply_keyword_id')->nullable();
            $table->integer('campaign_id')->nullable();
            $table->string('type');
            $table->dateTime('sent_time');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_queuing_data');
    }
}
