<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimScgUserTriggerKeywordRecieveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_scg_user_trigger_keyword_recieve', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('line_user_id');
            $table->string('trigger_value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_scg_user_trigger_keyword_recieve');
    }
}
