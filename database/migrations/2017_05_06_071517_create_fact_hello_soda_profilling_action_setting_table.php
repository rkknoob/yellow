<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactHelloSodaProfillingActionSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_hello_soda_profilling_action_setting_table', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hello_soda_profilling_action_id');
            $table->string('key');
            $table->string('label');
            $table->string('type');
            $table->text('values')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_hello_soda_profilling_action_setting_table');
    }
}
