<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactRecieveMessageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_recieve_message', function (Blueprint $table) {
            $table->increments('id');
            $table->string('keyword');
            $table->string('mid');
            $table->integer('line_biz_con_id');
            $table->string('bot_conf');
            $table->text('bot_reply');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_recieve_message');
    }
}
