<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FactShareLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_share_location', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('share_location_id');
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->longtext('image_url')->nullable();
            $table->longtext('link_url_1')->nullable();
            $table->longtext('link_url_2')->nullable();
            $table->longtext('link_url_3')->nullable();
            $table->string('tel_1')->nullable();
            $table->string('tel_2')->nullable();
            $table->string('tel_3')->nullable();
            $table->string('keyword_text_1')->nullable();
            $table->string('keyword_text_2')->nullable();
            $table->string('keyword_text_3')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longtitude')->nullable();
            $table->integer('auto_reply_keyword_id')->nullable();
            $table->string('autoreply_folder_name')->nullable();
            $table->string('autoreply_title')->nullable();
            $table->string('auto_reply_message')->nullable();
            $table->string('auto_reply_keyword')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_share_location');
    }
}
