<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactEcommerceShoppingCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_ecommerce_shopping_cart', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ecommerce_shopping_cart_id');
            $table->integer('product_id')->nullable();
            $table->string('product_name')->nullable();
            $table->text('product_img_url')->nullable();
            $table->string('short_name')->nullable();
            $table->decimal('discount_price',10,2)->default(0.00);
            $table->decimal('price',10,2)->default(0.00);
            $table->integer('quanlity')->default(0);
            $table->decimal('summary',10,2)->default(0.00);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_ecommerce_shopping_cart');
    }
}
