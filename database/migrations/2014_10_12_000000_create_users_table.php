<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use YellowProject\User;
use Illuminate\Support\Facades\Hash;


class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('username')->nullable();
            $table->string('email')->nullable();
            $table->integer('rule_id')->nullable();
            $table->boolean('is_active')->nullable();
            // $table->string('email')->unique();
            $table->string('password')->nullable();
            $table->longtext('mid')->nullable();
            $table->longtext('avatar')->nullable();
            $table->boolean('is_dt')->default(0);
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });

        DB::table('users')->insert(
            array(
                'name'  =>  'linebizcon',
                'email'  =>  'developer@yellow-idea.com',
                'username'  =>  'linebizcon',
                'password'  =>  Hash::make('Aa88888888'),
                'rule_id'  =>  '1',
                'is_active'  =>  '1',
                'remember_token'  =>  '7Id3a4SX1NeHw0t29xjkwU9nCcSIvdSASTjZUICzgwro3ko3imMXi4N4YYEl',
                'is_dt'  =>  '0',

            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
