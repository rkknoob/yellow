<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('folder_id');
            $table->string('name');
            // $table->enum('type', ['boolean', 'string','integer','decimal','date','behaviour']);
            $table->string('type');
            $table->string('description')->nullable();
            $table->boolean('is_personalize');
            $table->boolean('primary_key');
            $table->boolean('is_segment');
            $table->string('subscriber_id');
            $table->string('field_name');
            $table->string('personalize_default')->nullable();
            $table->boolean('is_required');
            $table->boolean('is_readonly');
            $table->boolean('is_api');
            $table->boolean('is_encrypt');
            $table->string('api_url')->nullable();
            $table->boolean('is_master_subscriber')->nullable();
            $table->boolean('is_master_subscriber_update')->nullable();
            $table->integer('mapping_master_field')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_fields');
    }
}
