<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactChatMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_chat_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('main_chat_id');
            $table->enum('type',['sent','recieve']);
            $table->text('message');
            $table->boolean('is_read');
            $table->text('reply_token')->nullable();
            $table->string('message_type')->nullable();
            $table->string('alt_text')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_chat_messages');
    }
}
