<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimForwardUrlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_forward_url', function (Blueprint $table) {
            $table->increments('id');
            $table->longtext('forward_url')->nullable();
            $table->longtext('agent_url')->nullable();
            $table->string('taxcert_year')->nullable();
            $table->longtext('send_sms_url')->nullable();
            $table->longtext('send_taxcert_url')->nullable();
            $table->longtext('get_policy_url')->nullable();
            $table->longtext('get_user_infomation_url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_forward_url');
    }
}
