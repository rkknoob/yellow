<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimEcommerceConversionMappingDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_ecommerce_conversion_mapping_data', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cpn_mobile')->nullable();
            $table->string('name')->nullable();
            $table->string('sur_name')->nullable();
            $table->string('bussiness_name')->nullable();
            $table->string('bussiness_type')->nullable();
            $table->string('post_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_ecommerce_conversion_mapping_data');
    }
}
