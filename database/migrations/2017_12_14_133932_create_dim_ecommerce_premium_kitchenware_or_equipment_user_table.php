<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimEcommercePremiumKitchenwareOrEquipmentUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_ecommerce_premium_kitchenware_or_equipment_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('premium_kitchenware_id');
            $table->integer('ecommerce_customer_id');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_ecommerce_premium_kitchenware_or_equipment_user');
    }
}
