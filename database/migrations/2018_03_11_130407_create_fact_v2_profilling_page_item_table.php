<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactV2ProfillingPageItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_v2_profilling_page_item', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profilling_page_id');
            $table->string('title')->nullable();
            $table->string('type')->nullable();
            $table->string('question')->nullable();
            $table->integer('subscribe_id')->nullable();
            $table->integer('field_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_v2_profilling_page_item');
    }
}
