<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimSMdmPictureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_s_mdm_picture', function (Blueprint $table) {
            $table->increments('id');
            $table->string('MATNR')->nullable();
            $table->string('G_GROUP')->nullable();
            $table->string('TYPE')->nullable();
            $table->string('SIZE_P')->nullable();
            $table->string('PIC')->nullable();
            $table->string('ADDRESS')->nullable();
            $table->string('F_PATH')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_s_mdm_picture');
    }
}
