<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('campaign_id');
            $table->integer('message_type_id')->nullable();
            $table->integer('seq_no')->nullable();
            $table->integer('campaign_message_id')->nullable();
            $table->integer('campaign_sticker_id')->nullable();
            $table->integer('campaign_richmessage_id')->nullable();
            $table->text('original_content_url')->nullable();
            $table->text('preview_image_url')->nullable();
            $table->integer('template_message_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fact_campaigns');
    }
}
