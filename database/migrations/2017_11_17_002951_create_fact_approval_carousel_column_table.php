<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactApprovalCarouselColumnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_approval_carousel_column', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('approval_carousel_id');
            $table->string('title');
            $table->string('text');
            $table->boolean('is_use')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_approval_carousel_column');
    }
}
