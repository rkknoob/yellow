<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactCouponStaticFormSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_coupon_static_form_settigs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('coupon_static_form_item_id');
            $table->string('type')->nullable();
            $table->string('key')->nullable();
            $table->string('values')->nullable();
            $table->string('label')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_coupon_static_form_settigs');
    }
}
