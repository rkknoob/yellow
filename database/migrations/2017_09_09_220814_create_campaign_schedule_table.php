<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_schedule', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('schedule_id')->nullable();
            $table->string('schedule_type')->nullable();
            $table->dateTime('schedule_start_date')->nullable();
            $table->dateTime('schedule_end_date')->nullable();
            $table->string('schedule_recurrence_type')->nullable();
            $table->boolean('schedule_recurrence_sun')->nullable();
            $table->boolean('schedule_recurrence_mon')->nullable();
            $table->boolean('schedule_recurrence_tue')->nullable();
            $table->boolean('schedule_recurrence_wed')->nullable();
            $table->boolean('schedule_recurrence_thu')->nullable();
            $table->boolean('schedule_recurrence_fri')->nullable();
            $table->boolean('schedule_recurrence_sat')->nullable();
            $table->string('schedule_recurrence_running')->nullable();
            $table->time('schedule_schedule_send_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_schedule');
    }
}
