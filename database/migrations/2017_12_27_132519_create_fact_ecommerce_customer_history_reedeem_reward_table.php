<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactEcommerceCustomerHistoryReedeemRewardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_ecommerce_customer_history_reedeem_reward', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ecommerce_customer_id');
            $table->string('type')->nullable();
            $table->integer('ep')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_ecommerce_customer_history_reedeem_reward');
    }
}
