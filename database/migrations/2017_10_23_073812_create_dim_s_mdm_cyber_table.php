<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimSMdmCyberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_s_mdm_cyber', function (Blueprint $table) {
            $table->increments('id');
            $table->string('CB_ID')->nullable();
            $table->string('MATNR')->nullable();
            $table->string('MAKTX')->nullable();
            $table->string('MVGR1')->nullable();
            $table->string('MVGR2')->nullable();
            $table->string('MVGR3')->nullable();
            $table->string('MVGR4')->nullable();
            $table->string('MVGR5')->nullable();
            $table->string('MVGR6')->nullable();
            $table->string('MVGR7')->nullable();
            $table->string('FET_ID1')->nullable();
            $table->string('FET_ID2')->nullable();
            $table->string('FET_ID3')->nullable();
            $table->string('FET_ID4')->nullable();
            $table->string('FET_ID5')->nullable();
            $table->string('PD_DIMEN')->nullable();
            $table->decimal('NTGEW',10,3)->nullable();
            $table->text('FILENAME')->nullable();
            $table->text('EX_META_T_TH')->nullable();
            $table->text('EX_META_T_EN')->nullable();
            $table->text('EX_META_K_TH')->nullable();
            $table->text('EX_META_K_EN')->nullable();
            $table->longtext('EX_META_D_TH')->nullable();
            $table->longtext('EX_META_D_EN')->nullable();
            $table->longtext('EX_DETAIL_TH')->nullable();
            $table->longtext('EX_DETAIL_EN')->nullable();
            $table->text('EX_P_TITLE_TH')->nullable();
            $table->text('EX_P_TITLE_EN')->nullable();
            $table->string('EX_CDF')->nullable();
            $table->string('EX_CLEAF')->nullable();
            $table->integer('EX_CONTENT_ID')->nullable();
            $table->string('EX_UNIMODEL')->nullable();
            $table->string('SLUG')->nullable();
            $table->string('EX_PORT')->nullable();
            $table->dateTime('EX_PORT_UPDATED_DATE')->nullable();
            $table->integer('EX_STOCK')->nullable();
            $table->dateTime('EX_NEXT_JOB')->nullable();
            $table->string('EX_PROMOTION')->nullable();
            $table->dateTime('EX_IMPORT_DATE')->nullable();
            $table->string('EX_INSTALLMENT')->nullable();
            $table->string('VD_VENDOR_ID')->nullable();
            $table->string('VD_DLV_TYPE')->nullable();
            $table->longtext('VD_FET_TH')->nullable();
            $table->longtext('VD_FET_EN')->nullable();
            $table->longtext('VD_WRT_TH')->nullable();
            $table->longtext('VD_WRT_EN')->nullable();
            $table->integer('VD_PRICE_NORMAL')->nullable();
            $table->integer('VD_PRICE_SPECIAL')->nullable();
            $table->integer('VD_PRICE_SALE')->nullable();
            $table->dateTime('VD_PRICE_SALE_EXPIRED')->nullable();
            $table->integer('VD_PRODUCT_CATEGORY_ID')->nullable();
            $table->integer('EX_VIEW')->nullable();
            $table->integer('EX_PRIORITY')->nullable();
            $table->integer('IS_RECOMMEND')->nullable();
            $table->integer('EX_SALE_PERCENT')->nullable();
            $table->integer('EX_RATING_COUNT')->nullable();
            $table->integer('EX_RATING')->nullable();
            $table->integer('EX_AVG_RATING')->nullable();
            $table->string('STY_ID')->nullable();
            $table->dateTime('CREATE_DATE')->nullable();
            $table->string('CREATE_BY')->nullable();
            $table->dateTime('UPDATE_DATE')->nullable();
            $table->string('UPDATE_BY')->nullable();
            $table->string('EX_SHOW')->nullable();
            $table->integer('DEFAULT_PICTURE_ID')->nullable();
            $table->string('EX_IS_PUBLISH')->nullable();
            $table->longtext('EX_DESC_F_TH')->nullable();
            $table->longtext('EX_DESC_F_EN')->nullable();
            $table->integer('EX_NEW_ARRIVAL_INDEX')->nullable();
            $table->string('STY2_ID')->nullable();
            $table->string('F_CYBER')->nullable();
            $table->string('DEPT_ID1')->nullable();
            $table->string('SAV_ID')->nullable();
            $table->string('MTART')->nullable();
            $table->string('LOAD')->nullable();
            $table->string('ORI_ID')->nullable();
            $table->string('META_T')->nullable();
            $table->text('META_K')->nullable();
            $table->longtext('META_D')->nullable();
            $table->longtext('DETAIL')->nullable();
            $table->string('WRT_ID')->nullable();
            $table->string('F_ONL')->nullable();
            $table->string('SHPINF_ID')->nullable();
            $table->string('SHP_ID')->nullable();
            $table->string('F_MAIL')->nullable();
            $table->integer('BRGEW')->nullable();
            $table->integer('VOLUM')->nullable();
            $table->string('GROES')->nullable();
            $table->string('DLV_ID')->nullable();
            $table->string('MAX')->nullable();
            $table->string('MIN')->nullable();
            $table->string('F_GIFT')->nullable();
            $table->integer('GIFTF')->nullable();
            $table->string('F_INST')->nullable();
            $table->string('F_NEW')->nullable();
            $table->string('SHPAV_ID')->nullable();
            $table->longtext('DESC_F')->nullable();
            $table->string('SHPAR_ID')->nullable();
            $table->string('CS_ID')->nullable();
            $table->string('SDEPT_ID')->nullable();
            $table->string('BEHVO')->nullable();
            $table->string('DEPT_ID2')->nullable();
            $table->string('DEPT_ID3')->nullable();
            $table->text('P_TITLE')->nullable();
            $table->integer('VD_PROD_ID')->nullable();
            $table->integer('VD_DLV_COST_BKK')->nullable();
            $table->integer('VD_DLV_COST_OTHER')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_s_mdm_cyber');
    }
}
