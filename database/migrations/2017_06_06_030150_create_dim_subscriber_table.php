<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimSubscriberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_subscriber', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('folder_id');
            $table->integer('category_id')->nullable();
            $table->string('name');
            $table->text('desc');
            $table->boolean('is_master')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_subscriber');
    }
}
