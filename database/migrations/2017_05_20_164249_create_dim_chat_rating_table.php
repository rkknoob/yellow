<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimChatRatingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_chat_rating', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('main_chat_id');
            $table->integer('agent_id');
            $table->integer('line_user_id');
            $table->integer('rating');
            $table->boolean('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_chat_rating');
    }
}
