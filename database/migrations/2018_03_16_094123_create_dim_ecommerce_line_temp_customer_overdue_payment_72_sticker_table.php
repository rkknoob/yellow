<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimEcommerceLineTempCustomerOverduePayment72StickerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_ecommerce_line_temp_customer_overdue_payment_72_sticker', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('packageId')->nullable();
            $table->integer('stickerId')->nullable();
            $table->longText('display')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_ecommerce_line_temp_customer_overdue_payment_72_sticker');
    }
}
