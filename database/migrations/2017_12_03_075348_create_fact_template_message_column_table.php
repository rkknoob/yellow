<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactTemplateMessageColumnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_template_message_column', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('template_message_id');
            $table->string('title')->nullable();
            $table->string('desc')->nullable();
            $table->text('img_url')->nullable();
            $table->integer('seq');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_template_message_column');
    }
}
