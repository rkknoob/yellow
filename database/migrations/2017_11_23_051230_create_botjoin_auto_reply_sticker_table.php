<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBotjoinAutoReplyStickerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('botjoin_auto_reply_sticker', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('packageId')->nullable();
            $table->integer('stickerId')->nullable();
            $table->longText('display')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('botjoin_auto_reply_sticker');
    }
}
