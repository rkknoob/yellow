<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactCampaignScheduleRecurringTaskTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_campaign_schedule_recurring_task', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('campaign_schedule_id');
            $table->integer('campaign_id');
            $table->dateTime('schedule_start_date')->nullable();
            $table->dateTime('schedule_end_date')->nullable();
            $table->dateTime('schedule_send_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_campaign_schedule_recurring_task');
    }
}
