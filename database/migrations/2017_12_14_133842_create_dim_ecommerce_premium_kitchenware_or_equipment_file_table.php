<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimEcommercePremiumKitchenwareOrEquipmentFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_ecommerce_premium_kitchenware_or_equipment_file', function (Blueprint $table) {
            $table->increments('id');
            $table->text('link_path');
            $table->enum('type',['video','pdf']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_ecommerce_premium_kitchenware_or_equipment_file');
    }
}
