<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactEcommerceCustomerSurveyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_ecommerce_customer_survey', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ecommerce_customer_survey_id');
            $table->string('label');
            $table->string('value');
            $table->integer('seq');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_ecommerce_customer_survey');
    }
}
