<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimSettingJsonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_setting_json', function (Blueprint $table) {
            $table->increments('id');
            $table->text('host')->nullable();
            $table->string('port')->nullable();
            $table->string('user')->nullable();
            $table->string('password')->nullable();
            $table->string('hour')->nullable();
            $table->string('min')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_setting_json');
    }
}
