<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimSettingLinePayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_setting_line_pay', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->text('api_uri')->nullable();
            $table->string('channel_id')->nullable();
            $table->string('channel_secret')->nullable();
            $table->boolean('is_active')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_setting_line_pay');
    }
}
