<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactEcommerceCouponDiscountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_ecommerce_coupon_discount', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ecommerce_coupon_id');
            $table->string('type');
            $table->decimal('discount',10,2)->default(0.00);
            $table->decimal('total_amount',10,2)->default(0.00);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_ecommerce_coupon_discount');
    }
}
