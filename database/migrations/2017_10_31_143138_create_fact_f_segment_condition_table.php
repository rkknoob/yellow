<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactFSegmentConditionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_f_segment_condition', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('segment_condition_id');
            $table->string('title')->nullable();
            $table->string('condition1')->nullable();
            $table->string('condition2')->nullable();
            $table->string('condition3')->nullable();
            $table->string('condition4')->nullable();
            $table->string('condition5')->nullable();
            $table->string('condition6')->nullable();
            $table->string('condition7')->nullable();
            $table->string('condition8')->nullable();
            $table->string('condition9')->nullable();
            $table->string('condition10')->nullable();
            $table->string('condition11')->nullable();
            $table->string('condition12')->nullable();
            $table->string('condition13')->nullable();
            $table->string('condition14')->nullable();
            $table->string('condition15')->nullable();
            $table->string('condition16')->nullable();
            $table->string('condition17')->nullable();
            $table->string('condition18')->nullable();
            $table->string('condition19')->nullable();
            $table->string('condition20')->nullable();
            $table->string('value1')->nullable();
            $table->string('value2')->nullable();
            $table->string('value3')->nullable();
            $table->string('value4')->nullable();
            $table->string('value5')->nullable();
            $table->string('value6')->nullable();
            $table->string('value7')->nullable();
            $table->string('value8')->nullable();
            $table->string('value9')->nullable();
            $table->string('value10')->nullable();
            $table->string('value11')->nullable();
            $table->string('value12')->nullable();
            $table->string('value13')->nullable();
            $table->string('value14')->nullable();
            $table->string('value15')->nullable();
            $table->string('value16')->nullable();
            $table->string('value17')->nullable();
            $table->string('value18')->nullable();
            $table->string('value19')->nullable();
            $table->string('value20')->nullable();
            $table->string('remark1')->nullable();
            $table->string('remark2')->nullable();
            $table->string('remark3')->nullable();
            $table->string('remark4')->nullable();
            $table->string('remark5')->nullable();
            $table->string('remark6')->nullable();
            $table->string('remark7')->nullable();
            $table->string('remark8')->nullable();
            $table->string('remark9')->nullable();
            $table->string('remark10')->nullable();
            $table->string('remark11')->nullable();
            $table->string('remark12')->nullable();
            $table->string('remark13')->nullable();
            $table->string('remark14')->nullable();
            $table->string('remark15')->nullable();
            $table->string('remark16')->nullable();
            $table->string('remark17')->nullable();
            $table->string('remark18')->nullable();
            $table->string('remark19')->nullable();
            $table->string('remark20')->nullable();
            $table->boolean('is_empty')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_f_segment_condition');
    }
}
