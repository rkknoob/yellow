<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimEcommerceCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_ecommerce_category', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('is_sub_category')->default(0);
            $table->integer('main_category_id')->nullable();
            $table->string('name');
            $table->text('desc');
            $table->string('meta_title')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->string('meta_desc')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_ecommerce_category');
    }
}
