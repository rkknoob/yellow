<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimEcommerceShoppingCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_ecommerce_shopping_cart', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ecommerce_customer_id');
            $table->integer('quanlity')->default(0);
            $table->string('coupon_code')->nullable();
            $table->decimal('coupon_discount',10,2)->default(0.00);
            $table->boolean('is_survey_discount')->default(0);
            $table->decimal('survey_discount',10,2)->default(0.00);
            $table->decimal('summary',10,2)->default(0.00);
            $table->integer('reward_point')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_ecommerce_shopping_cart');
    }
}
