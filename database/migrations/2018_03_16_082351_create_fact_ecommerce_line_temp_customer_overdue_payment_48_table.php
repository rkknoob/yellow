<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactEcommerceLineTempCustomerOverduePayment48Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_ecommerce_line_temp_customer_overdue_payment_48', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ecommerce_line_temp_customer_overdue_payment_48_id');
            $table->integer('message_type_id')->nullable();
            $table->integer('seq_no')->nullable();
            $table->integer('message_id')->nullable();
            $table->integer('sticker_id')->nullable();
            $table->integer('richmessage_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_ecommerce_line_temp_customer_overdue_payment_48');
    }
}
