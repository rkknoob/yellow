<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactEcommerceCustomerCancellationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_ecommerce_customer_cancellations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ecommerce_customer_id');
            $table->integer('order_id');
            $table->string('status');
            $table->dateTime('cancle_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_ecommerce_customer_cancellations');
    }
}
