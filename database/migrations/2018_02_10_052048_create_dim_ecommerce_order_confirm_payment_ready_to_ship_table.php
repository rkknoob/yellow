<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimEcommerceOrderConfirmPaymentReadyToShipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_ecommerce_order_confirm_payment_ready_to_ship', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id');
            $table->date('dt_shipping_date')->nullable();
            $table->string('dt_shipping_date_time')->nullable();
            $table->string('shipping_status')->nullable();
            $table->text('comment')->nullable();
            $table->integer('email_to_customer_template_id')->nullable();
            $table->integer('email_to_dt_template_id')->nullable();
            $table->integer('email_to_admin_template_id')->nullable();
            $table->integer('line_to_customer_template_id')->nullable();
            $table->integer('line_to_dt_template_id')->nullable();
            $table->integer('line_to_admin_template_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_ecommerce_order_confirm_payment_ready_to_ship');
    }
}
