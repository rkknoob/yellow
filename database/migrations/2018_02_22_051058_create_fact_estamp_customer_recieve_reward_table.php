<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactEstampCustomerRecieveRewardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_estamp_customer_recieve_reward', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('line_user_id');
            $table->integer('estamp_id');
            $table->integer('stamp_amount');
            $table->string('reward');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_estamp_customer_recieve_reward');
    }
}
