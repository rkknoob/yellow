<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimRichmenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_richmenu', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('width');
            $table->integer('height');
            $table->integer('selected');
            $table->text('img_url');
            $table->integer('segment_id');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->string('name')->nullable();
            $table->string('chatBarText')->nullable();
            $table->string('line_richmenu_id')->nullable();
            $table->string('segment_type')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_richmenu');
    }
}
