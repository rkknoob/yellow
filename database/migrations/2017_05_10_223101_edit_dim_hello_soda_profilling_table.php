<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditDimHelloSodaProfillingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dim_hello_soda_profilling', function (Blueprint $table) {
            $table->longtext('url_redirect_fwd_max')->nullable()->after('url_connect_hello_soda');
            $table->longtext('url_redirect_gaming')->nullable()->after('url_redirect_fwd_max');
            $table->longtext('url_call_back')->nullable()->after('url_redirect_gaming');
            $table->string('product_id')->nullable()->after('url_call_back');
            $table->string('facebook_app_id')->nullable()->after('product_id');
            $table->string('facebook_secret_id')->nullable()->after('facebook_app_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dim_hello_soda_profilling', function (Blueprint $table) {
            $table->dropColumn('url_redirect_fwd_max');
            $table->dropColumn('url_redirect_gaming');
            $table->dropColumn('url_call_back');
            $table->dropColumn('product_id');
            $table->dropColumn('facebook_app_id');
            $table->dropColumn('facebook_secret_id');
        });
    }
}
