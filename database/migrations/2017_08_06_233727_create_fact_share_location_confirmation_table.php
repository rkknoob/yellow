<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactShareLocationConfirmationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_share_location_confirmation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('share_location_confirmation_id');
            $table->integer('share_location_item_id');
            $table->decimal('distance',10,2);
            $table->boolean('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_share_location_confirmation');
    }
}
