<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FactLogFamilyMemberLogInTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_log_family_member_log_in', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('line_user_id');
            $table->string('scg_id');
            $table->text('scg_token')->nullable();
            $table->boolean('is_success');
            $table->string('msg_return');
            $table->dateTime('expire_date')->nullable();
            $table->boolean('is_login');
            $table->string('name');
            $table->string('last_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_log_family_member_log_in');
    }
}
