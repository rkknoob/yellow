<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutoReplyKeywordItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_auto_reply_keywords', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dim_auto_reply_keyword_id');
            $table->integer('message_type_id')->nullable();
            $table->integer('seq_no')->nullable();
            $table->integer('auto_reply_message_id')->nullable();
            $table->integer('auto_reply_sticker_id')->nullable();
            $table->integer('auto_reply_richmessage_id')->nullable();
            // $table->integer('auto_reply_photo_id')->nullable();
            // $table->integer('auto_reply_video_id')->nullable();
            $table->text('original_content_url')->nullable();
            $table->text('preview_image_url')->nullable();
            $table->integer('template_message_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fact_auto_reply_keywords');
    }
}
