<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimAutoReplyLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_auto_reply_location', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('alt_text');
            $table->integer('folder_id')->nullable();
            $table->integer('limit_corosel');
            $table->integer('limit_time_confirmation')->nullable();
            $table->string('confirmation_name')->nullable();
            $table->string('confirmation_reply')->nullable();
            $table->boolean('is_active');
            $table->boolean('is_confirmation');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_auto_reply_location');
    }
}
