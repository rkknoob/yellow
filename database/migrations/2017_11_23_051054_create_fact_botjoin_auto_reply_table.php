<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactBotjoinAutoReplyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_botjoin_auto_reply', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dim_botjoin_auto_reply_id');
            $table->integer('message_type_id')->nullable();
            $table->integer('seq_no')->nullable();
            $table->integer('auto_reply_message_id')->nullable();
            $table->integer('auto_reply_sticker_id')->nullable();
            $table->integer('auto_reply_richmessage_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_botjoin_auto_reply');
    }
}
