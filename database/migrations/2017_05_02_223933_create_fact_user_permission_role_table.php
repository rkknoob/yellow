<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use YellowProject\ListMenu;
use YellowProject\UserPermissionRole;

class CreateFactUserPermissionRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_user_permission_role', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('menu_id');
            $table->string('action');
            $table->boolean('active');
            $table->timestamps();
        });

        $listMenuPermissions = ["page_access","form_create","form_edit","form_delete"];
        $listMenus = ListMenu::whereNotIn('id', [31,32])->get();

        foreach ($listMenus as $listMenu) {
            foreach ($listMenuPermissions as $listMenuPermission) {
                $data = [];
                $data['user_id'] = 1;
                $data['menu_id'] = $listMenu->id;
                $data['action'] = $listMenuPermission;
                $data['active'] = 1;
                UserPermissionRole::create($data);
            }
            
        }

        $data = [];
        $data['user_id'] = 1;
        $data['menu_id'] = 31;
        $data['action'] = 'page_access';
        $data['active'] = 1;
        UserPermissionRole::create($data);

        $data = [];
        $data['user_id'] = 1;
        $data['menu_id'] = 32;
        $data['action'] = 'page_access';
        $data['active'] = 1;
        UserPermissionRole::create($data);

        $data = [];
        $data['user_id'] = 1;
        $data['menu_id'] = 32;
        $data['action'] = 'chat_setting';
        $data['active'] = 1;
        UserPermissionRole::create($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_user_permission_role');
    }
}
