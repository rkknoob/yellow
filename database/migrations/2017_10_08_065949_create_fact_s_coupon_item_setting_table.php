<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactSCouponItemSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_s_coupon_item_setting', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('coupon_item_id');
            $table->string('key')->nullable();
            $table->string('label')->nullable();
            $table->string('type')->nullable();
            $table->text('value')->nullable();
            $table->integer('master_subscriber_field_id')->nullable();
            $table->boolean('master_subscriber_on')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_s_coupon_item_setting');
    }
}
