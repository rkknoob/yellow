<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimCouponSectionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_coupon_section', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('coupon_id');
            $table->string('section_name')->nullable();
            $table->string('img_size')->nullable();
            $table->string('img_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_coupon_section');
    }
}
