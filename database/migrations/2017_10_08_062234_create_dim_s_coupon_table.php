<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimSCouponTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_s_coupon', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('folder_id')->nullable();
            $table->string('name')->nullable();
            $table->string('alt_text')->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->integer('winning_odds')->nullable();
            $table->integer('maximum_winner')->nullable();
            $table->boolean('is_active')->nullable();
            $table->boolean('is_winner_nolimit')->nullable();
            $table->text('desc')->nullable();
            $table->boolean('is_exeed_max_winner')->nullable();
            $table->string('prefix_name')->nullable();
            $table->integer('static_form_id')->nullable();
            $table->boolean('is_running_number')->nullable();
            $table->boolean('is_famliy')->nullable();
            $table->string('prefix_api')->nullable();
            $table->integer('seq')->nullable();
            $table->boolean('is_display')->nullable();
            $table->integer('tracking_bc_id')->nullable();
            $table->text('remark')->nullable();
            $table->text('share_message')->nullable();
            $table->boolean('is_discount_thb')->nullable();
            $table->boolean('is_discount_percentage')->nullable();
            $table->integer('discount_value')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_s_coupon');
    }
}
