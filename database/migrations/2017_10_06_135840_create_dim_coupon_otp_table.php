<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use YellowProject\SB\CouponOTP;

class CreateDimCouponOtpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_coupon_otp', function (Blueprint $table) {
            $table->increments('id');
            $table->text('url_api')->nullable();
            $table->string('username')->nullable();
            $table->string('password')->nullable();
            $table->string('sender')->nullable();
            $table->text('msg')->nullable();
            $table->timestamps();
        });

        CouponOTP::create([
            'url_api' => 'http://v2.smsthai.net/DATAReceiver.php',
            'username' => 'sbfurniture',
            'password' => 'e10adc3949ba59abbe56e057f20f883e',
            'sender' => 'SBFurniture',
            'msg' => '([ OTP ])',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_coupon_otp');
    }
}
