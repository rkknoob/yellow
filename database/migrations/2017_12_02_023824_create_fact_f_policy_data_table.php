<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactFPolicyDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_f_policy_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('linebizcon_customer_id');
            $table->string('code')->nullable();
            $table->string('plan')->nullable();
            $table->string('billing_mode')->nullable();
            $table->string('policy_num')->nullable();
            $table->string('paid_to_date')->nullable();
            $table->decimal('modal_premium',10,2)->default(0.00);
            $table->string('barcode')->nullable();
            $table->string('ref01')->nullable();
            $table->string('ref02')->nullable();
            $table->string('payment')->nullable();
            $table->string('autopay')->nullable();
            $table->text('paymentgateway')->nullable();
            $table->text('linepaygateway')->nullable();
            $table->dateTime('expire_date')->nullable();
            $table->boolean('unit_link')->nullable();
            $table->decimal('billing_premium',10,2)->default(0.00);
            $table->integer('period_year')->nullable();
            $table->integer('period_installment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_f_policy_data');
    }
}
