<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimMypolicyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_mypolicy', function (Blueprint $table) {
            $table->increments('id');
            $table->longtext('payload_1')->nullable();
            $table->longtext('display_1')->nullable();
            $table->longtext('payload_2')->nullable();
            $table->longtext('display_2')->nullable();
            $table->longtext('payload_3')->nullable();
            $table->longtext('display_3')->nullable();
            $table->longtext('payload_4')->nullable();
            $table->longtext('display_4')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_mypolicy');
    }
}
