<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactEstampRewardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_estamp_reward', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('estamp_id');
            $table->text('img_url')->nullable();
            $table->string('reward_name')->nullable();
            $table->integer('total_use_stamp')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_estamp_reward');
    }
}
