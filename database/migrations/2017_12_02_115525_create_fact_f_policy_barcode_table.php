<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactFPolicyBarcodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_f_policy_barcode', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('linebizcon_customer_id');
            $table->string('code');
            $table->string('comp_code');
            $table->string('name');
            $table->string('policy_num');
            $table->decimal('modal_premium',10,2)->default(0.00);
            $table->string('ref01');
            $table->string('ref02');
            $table->string('barcode');
            $table->text('barcode_url')->nullable();
            $table->decimal('billing_premium',10,2)->default(0.00);
            $table->integer('period_year')->nullable();
            $table->integer('period_installment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_f_policy_barcode');
    }
}
