<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactRecieveMessageGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_recieve_message_group', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('line_user_id');
            $table->string('group_id');
            $table->string('keyword');
            $table->string('bot_conf');
            $table->text('bot_reply');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_recieve_message_group');
    }
}
