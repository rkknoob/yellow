<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('location_id');
            $table->string('location_name')->nullable();
            $table->string('location_description')->nullable();
            $table->longtext('location_image_url')->nullable();
            $table->longtext('location_url')->nullable();
            $table->string('location_tel')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longtitude')->nullable();
            $table->longtext('keyword')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_locations');
    }
}
