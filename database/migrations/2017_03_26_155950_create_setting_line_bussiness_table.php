<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingLineBussinessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('line_setting_business', function (Blueprint $table) {
            $table->increments('id');
            $table->string('channel_id')->nullable();
            $table->string('channel_secret')->nullable();
            $table->longText('channel_access_token')->nullable();
            $table->string('name')->nullable();
            $table->boolean('active')->default(false);
            $table->string('refresh_token')->nullable();
            $table->longText('line_url_calback')->nullable();
            $table->timestamp('expire')->nullable();
            $table->integer('max_message')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('line_setting_business');
    }
}
