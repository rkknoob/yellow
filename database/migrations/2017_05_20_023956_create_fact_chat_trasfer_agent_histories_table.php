<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactChatTrasferAgentHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_chat_trasfer_agent_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('from_agent_id');
            $table->integer('to_agent_id');
            $table->integer('main_chat_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_chat_trasfer_agent_histories');
    }
}
