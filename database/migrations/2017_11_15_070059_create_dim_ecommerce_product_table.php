<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimEcommerceProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_ecommerce_product', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('desc');
            $table->text('short_desc');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->string('sku')->nullable();
            $table->decimal('price',10,2)->default(0.00);
            $table->string('price_unit')->nullable();
            $table->decimal('shipping_cost',10,2)->default(0.00);
            $table->string('vat')->nullable();
            $table->string('source_order')->nullable();
            $table->string('out_of_stock_status')->nullable();
            $table->string('status')->nullable();
            $table->integer('reward_point')->default(0);
            $table->string('product_brand')->nullable();
            $table->string('product_name')->nullable();
            $table->string('product_size')->nullable();
            $table->integer('tracking_bc_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_ecommerce_product');
    }
}
