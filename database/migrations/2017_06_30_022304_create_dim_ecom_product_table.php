<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimEcomProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_ecom_product', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_category_id')->nullable();
            $table->string('name')->nullable();
            $table->text('desc')->nullable();
            $table->longtext('product_detail')->nullable();
            $table->longtext('special_detail')->nullable();
            $table->boolean('is_active')->nullable();
            $table->decimal('product_price',10,2)->nullable();
            $table->decimal('shipping_cost',10,2)->nullable();
            $table->decimal('price',10,2)->nullable();
            $table->string('unit_price')->nullable();
            $table->integer('quantity')->nullable();
            $table->integer('sort_order')->nullable();
            $table->decimal('discount_price',10,2)->nullable();
            $table->datetime('start_time_product')->nullable();
            $table->datetime('end_time_product')->nullable();
            $table->datetime('start_time_product_discount')->nullable();
            $table->datetime('end_time_product_discount')->nullable();
            $table->integer('img_main_id')->nullable();
            $table->string('type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_ecom_product');
    }
}
