<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimScgUserTriggerKeywordRegisterDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_scg_user_trigger_keyword_register_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('line_user_id');
            $table->string('code');
            $table->string('display_name')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('tel')->nullable();
            $table->string('question_select_value')->nullable();
            $table->text('text_area_value')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_scg_user_trigger_keyword_register_data');
    }
}
