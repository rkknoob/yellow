<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactSCouponGenerateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_s_coupon_generate', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('coupon_id');
            $table->text('coupon_section_img_url')->nullable();
            $table->text('coupon_fail_img_url')->nullable();
            $table->text('coupon_reedeem_img_url')->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->string('flag_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_s_coupon_generate');
    }
}
