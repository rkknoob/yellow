<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactCouponStaticFormDatasV2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_coupon_static_form_datas_v2', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('coupon_id');
            $table->integer('line_user_id');
            $table->integer('static_form_id');
            $table->integer('static_form_item_id');
            $table->string('el_id');
            $table->string('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_coupon_static_form_datas_v2');
    }
}
