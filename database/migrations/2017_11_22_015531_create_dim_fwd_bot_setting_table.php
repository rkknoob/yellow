<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimFwdBotSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_fwd_bot_setting', function (Blueprint $table) {
            $table->increments('id');
            $table->string('api_url')->nullable();
            $table->boolean('is_active')->default(0);
            $table->longtext('message_send_transfer')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_fwd_bot_setting');
    }
}
