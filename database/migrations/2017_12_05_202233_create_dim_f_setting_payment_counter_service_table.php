<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimFSettingPaymentCounterServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_f_setting_payment_counter_service', function (Blueprint $table) {
            $table->increments('id');
            $table->string('text_message');
            $table->text('logo_url');
            $table->boolean('is_show_on_screen');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_f_setting_payment_counter_service');
    }
}
