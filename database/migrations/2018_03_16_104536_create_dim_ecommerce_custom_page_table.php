<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimEcommerceCustomPageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_ecommerce_custom_page', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->text('content_body')->nullable();
            $table->string('img_header')->nullable();
            $table->text('url')->nullable();
            $table->boolean('is_active')->default(0);
            $table->integer('tracking_bc_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_ecommerce_custom_page');
    }
}
