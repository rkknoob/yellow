<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactSCouponItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_s_coupon_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('coupon_section_id');
            $table->string('title')->nullable();
            $table->string('el_id')->nullable();
            $table->integer('index')->nullable();
            $table->string('type')->nullable();
            $table->integer('height')->nullable();
            $table->integer('width')->nullable();
            $table->integer('x')->nullable();
            $table->integer('y')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_s_coupon_items');
    }
}
