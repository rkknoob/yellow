<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimEcomProductPaymentDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_ecom_product_payment_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ecom_product_order_list_id')->nullable();
            $table->integer('img_id')->nullable();
            $table->string('account_bank_name')->nullable();
            $table->dateTime('payment_date')->nullable();
            $table->decimal('payment_amount',10,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_ecom_product_payment_detail');
    }
}
