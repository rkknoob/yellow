<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimSettingApprovalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_setting_approval', function (Blueprint $table) {
            $table->increments('id');
            $table->text('api_base_url');
            $table->string('api_key');
            $table->string('api_yellow_key');
            $table->text('text_entry');
            $table->text('text_exit');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_setting_approval');
    }
}
