<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use YellowProject\FWDToken;

class CreateDimFwdTokenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_fwd_token', function (Blueprint $table) {
            $table->increments('id');
            $table->string('token');
            $table->string('name');
            $table->timestamps();
        });

        FWDToken::create([
            'name' => 'Hello Soda',
            'token' =>  FWDToken::generateRandomString(),
        ]);

        FWDToken::create([
            'name' => 'FWD Max',
            'token' =>  FWDToken::generateRandomString(),
        ]);

        FWDToken::create([
            'name' => 'FWD Gammimg',
            'token' =>  FWDToken::generateRandomString(),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_fwd_token');
    }
}
