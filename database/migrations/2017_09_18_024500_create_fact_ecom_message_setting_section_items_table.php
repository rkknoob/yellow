<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactEcomMessageSettingSectionItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_ecom_message_setting_section_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ecom_message_setting_section_id');
            $table->text('display')->nullable();
            $table->text('playload')->nullable();
            $table->integer('package_id')->nullable();
            $table->integer('stricker_id')->nullable();
            $table->integer('auto_reply_richmessage_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_ecom_message_setting_section_items');
    }
}
