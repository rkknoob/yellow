<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactExclusiveOnlineVideoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_exclusive_online_video', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('exclusive_online_id')->nullable();//id อ้างอิง
            $table->integer('exclusive_online_file')->nullable();//id อ้างอิงไฟล์
            $table->string('thumbnail_img_url')->nullable();//
            $table->string('manual_video_url')->nullable();//
            $table->string('upload_video_url')->nullable();//
            $table->string('name')->nullable();//ชื่อ label
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_exclusive_online_video');
    }
}
