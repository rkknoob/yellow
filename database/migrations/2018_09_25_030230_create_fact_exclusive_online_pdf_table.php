<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactExclusiveOnlinePdfTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_exclusive_online_pdf', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('exclusive_online_id')->nullable();//id อ้างอิง
            $table->integer('exclusive_online_file')->nullable();//id อ้างอิงไฟล์
            $table->string('name')->nullable();//ชื่อ label
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_exclusive_online_pdf');
    }
}
