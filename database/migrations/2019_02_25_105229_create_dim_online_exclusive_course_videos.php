<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimOnlineExclusiveCourseVideos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_online_exclusive_course_videos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('online_exclusive_course_id')->nullable();
            $table->string('name')->nullable();
            $table->text('thumbnail_img_url')->nullable();
            $table->text('manual_video_url')->nullable();
            $table->text('upload_video_url')->nullable();
            $table->timestamps();
        });


    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_online_exclusive_course_videos');
    }
}
