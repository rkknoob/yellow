<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimEcommercePremiumKitchenwareOrEquipmentVideoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_ecommerce_premium_kitchenware_or_equipment_video', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('premium_kitchenware_id');
            $table->integer('ecommerce_premium_kitchenware_or_equipment_file_id');
            $table->string('label')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_ecommerce_premium_kitchenware_or_equipment_video');
    }
}
