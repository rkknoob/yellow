<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimEcommerceLineTempAutoCustomerOverduePayment48Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_ecommerce_line_temp_auto_customer_overdue_payment_48', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('line_to_customer_template_id')->nullable();
            $table->integer('line_to_dt_template_id')->nullable();
            $table->integer('line_to_admin_template_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_ecommerce_line_temp_auto_customer_overdue_payment_48');
    }
}
