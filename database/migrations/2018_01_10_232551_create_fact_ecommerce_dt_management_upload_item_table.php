<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactEcommerceDtManagementUploadItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_ecommerce_dt_management_upload_item', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ecommerce_dt_management_upload_id');
            $table->string('name')->nullable();
            $table->text('address')->nullable();
            $table->string('province')->nullable();
            $table->string('post_code')->nullable();
            $table->integer('ecommerce_dt_management_group_id')->nullable();
            $table->string('status')->nullable();
            $table->string('dt_code_login')->nullable();
            $table->text('dt_url_login')->nullable();
            $table->string('remark')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_ecommerce_dt_management_upload_item');
    }
}
