<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CretaeDimEcommerceOrderDtPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_ecommerce_order_dt_payment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id');
            $table->integer('dt_id');
            $table->integer('ecommerce_customer_id');
            $table->string('payment_code');
            $table->text('payment_url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_ecommerce_order_dt_payment');
    }
}
