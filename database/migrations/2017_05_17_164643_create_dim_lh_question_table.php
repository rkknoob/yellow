<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use YellowProject\LHQuestion;

class CreateDimLhQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_lh_question', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        LHQuestion::create([
            'name' => 'สนใจที่อยู่ประเภทใด?'
        ]);

        LHQuestion::create([
            'name' => 'ภูมิภาคที่สนใจ ?'
        ]);

        LHQuestion::create([
            'name' => 'งบซื้อบ้านของคุณ ?'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_lh_question');
    }
}
