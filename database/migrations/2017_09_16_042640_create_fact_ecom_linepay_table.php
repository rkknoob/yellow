<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactEcomLinepayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_ecom_linepay', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ecom_product_order_list_id');
            $table->string('order_id')->nullable();
            $table->decimal('amount',10,2)->nullable();
            $table->string('line_pay_transaction_id')->nullable();
            $table->string('line_pay_refund_transaction_id')->nullable();
            $table->string('line_pay_step')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_ecom_linepay');
    }
}
