<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimLineUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_line_user_table', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mid');
            $table->string('avatar')->nullable();
            $table->string('name')->nullable();
            $table->string('user_type');
            $table->string('flag_status')->nullable();
            $table->longtext('flag_comment')->nullable();
            $table->boolean('is_follow')->default(1);
            $table->boolean('is_auto_kick')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_line_user_table');
    }
}
