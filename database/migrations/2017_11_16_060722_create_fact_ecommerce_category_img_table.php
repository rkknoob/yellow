<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactEcommerceCategoryImgTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_ecommerce_category_img', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ecommerce_category_id');
            $table->integer('product_img_id');
            $table->string('name');
            $table->integer('seq');
            $table->boolean('is_base');
            $table->boolean('is_small');
            $table->boolean('is_thumbnail');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_ecommerce_category_img');
    }
}
