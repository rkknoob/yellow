<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimEstampTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_estamp', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('is_active');
            $table->string('name');
            $table->longtext('image_banner');
            $table->longtext('image_board_stamp_on_finsh');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->longtext('text_in_popup')->nullable();
            $table->string('action_for_new_collect')->nullable();
            $table->string('action_for_recive_stamp')->nullable();
            $table->string('action_for_redeem')->nullable();
            $table->integer('total_stamp')->default(0);
            $table->integer('total_row')->default(0);
            $table->integer('total_column')->default(0);
            $table->boolean('is_auto_renew');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_estamp');
    }
}
