<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimFSettingPaymentBankTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_f_setting_payment_bank', function (Blueprint $table) {
            $table->increments('id');
            $table->text('logo_url');
            $table->text('link_url');
            $table->string('comp_code');
            $table->string('comp_code2');
            $table->boolean('is_show_on_screen');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_f_setting_payment_bank');
    }
}
