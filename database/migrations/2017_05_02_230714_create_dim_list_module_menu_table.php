<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use YellowProject\ListModuleMenu;

class CreateDimListModuleMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_list_module_menu', function (Blueprint $table) {
            $table->increments('id');
            $table->string('module_name');
            $table->timestamps();
        });

        $arrayListMenus = [
            "Dashboard",
            "Line",
            "FWD",
            "Setting",
            "Report",
            "Admin",
            "LH",
            "Line Coupon",
            "Line Ecommerce",
            "SB Design Square",
        ];

        foreach ($arrayListMenus as $moduleName) {
            ListModuleMenu::create([
                "module_name"  =>  $moduleName,
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_list_module_menu');
    }
}
