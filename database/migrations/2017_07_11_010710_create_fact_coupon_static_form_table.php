<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactCouponStaticFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_coupon_static_form', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('coupon_static_form_id');
            $table->string('type')->nullable();
            $table->string('el_id')->nullable();
            $table->integer('seq_no')->nullable();
            $table->string('title')->nullable();
            $table->boolean('is_active')->nullable();
            $table->integer('master_subscriber_field_id')->nullable();
            $table->boolean('master_subscriber_on')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_coupon_static_form');
    }
}
