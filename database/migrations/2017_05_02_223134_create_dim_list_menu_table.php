<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use YellowProject\ListMenu;
use YellowProject\UserPermissionRole;

class CreateDimListMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_list_menu', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('module_id');
            $table->enum('menu_type',['core','folder','setting','integrated','report']);
            $table->string('menu_name');
            $table->timestamps();
        });

        /*1*/ $arraysData[] = ["menu_type" => "core","menu_name" => "Dashboard", "module_id" => 1];
        /*2*/ $arraysData[] = ["menu_type" => "core","menu_name" => "Campagin", "module_id" => 2];
        /*3*/ $arraysData[] = ["menu_type" => "core","menu_name" => "Auto Reply Default", "module_id" => 2];
        /*4*/ $arraysData[] = ["menu_type" => "core","menu_name" => "Auto Reply Keyword", "module_id" => 2];
        /*5*/ $arraysData[] = ["menu_type" => "core","menu_name" => "Auto Reply Location", "module_id" => 2];
        /*6*/ $arraysData[] = ["menu_type" => "core","menu_name" => "Rich Message", "module_id" => 2];
        /*7*/ $arraysData[] = ["menu_type" => "folder","menu_name" => "Campagin", "module_id" => 2];
        /*8*/ $arraysData[] = ["menu_type" => "folder","menu_name" => "Auto Reply", "module_id" => 2];
        /*9*/ $arraysData[] = ["menu_type" => "folder","menu_name" => "Rich Message", "module_id" => 2];
        /*10*/ $arraysData[] = ["menu_type" => "setting","menu_name" => "Line Business Connect", "module_id" => 2];
        /*11*/  $arraysData[] = ["menu_type" => "setting","menu_name" => "OA Tracking URL", "module_id" => 2];
        /*12*/ $arraysData[] = ["menu_type" => "setting","menu_name" => "Profilling", "module_id" => 2];
        /*13*/ $arraysData[] = ["menu_type" => "core","menu_name" => "My Policy", "module_id" => 3];
        /*14*/ $arraysData[] = ["menu_type" => "core","menu_name" => "Forward URL", "module_id" => 3];
        /*15*/ $arraysData[] = ["menu_type" => "setting","menu_name" => "OTP", "module_id" => 3];
        /*16*/ $arraysData[] = ["menu_type" => "setting","menu_name" => "Hello Soda", "module_id" => 3];
        /*17*/ $arraysData[] = ["menu_type" => "integrated","menu_name" => "Agent Connect", "module_id" => 3];
        /*18*/ $arraysData[] = ["menu_type" => "report","menu_name" => "Auto Reply", "module_id" => 3];
        /*19*/ $arraysData[] = ["menu_type" => "report","menu_name" => "My Policy", "module_id" => 3];
        /*20*/ $arraysData[] = ["menu_type" => "report","menu_name" => "Taxcer", "module_id" => 3];
        /*21*/ $arraysData[] = ["menu_type" => "report","menu_name" => "New Register", "module_id" => 3];
        /*22*/ $arraysData[] = ["menu_type" => "core","menu_name" => "Segment", "module_id" => 4];
        /*23*/ $arraysData[] = ["menu_type" => "folder","menu_name" => "Segment", "module_id" => 4];
        /*24*/ $arraysData[] = ["menu_type" => "setting","menu_name" => "Field", "module_id" => 5];
        /*25*/ $arraysData[] = ["menu_type" => "setting","menu_name" => "Subscriber List", "module_id" => 5];
        /*26*/ $arraysData[] = ["menu_type" => "setting","menu_name" => "Location", "module_id" => 5];
        /*27*/ $arraysData[] = ["menu_type" => "setting","menu_name" => "User", "module_id" => 7];
        /*28*/ $arraysData[] = ["menu_type" => "setting","menu_name" => "Log", "module_id" => 7];
        /*29*/ $arraysData[] = ["menu_type" => "setting","menu_name" => "Message Error", "module_id" => 7];
        /*30*/ $arraysData[] = ["menu_type" => "setting","menu_name" => "Setting Bot", "module_id" => 4];
        /*31*/ $arraysData[] = ["menu_type" => "report","menu_name" => "One on One Chat Report", "module_id" => 3];
        /*32*/ $arraysData[] = ["menu_type" => "core","menu_name" => "One on One Chat", "module_id" => 3];
        /*33*/ $arraysData[] = ["menu_type" => "setting","menu_name" => "BC Tracking", "module_id" => 2];
        /*34*/ $arraysData[] = ["menu_type" => "report","menu_name" => "Report Auto Reply", "module_id" => 7];
        /*35*/ $arraysData[] = ["menu_type" => "report","menu_name" => "Report New Register", "module_id" => 7];
        /*36*/ $arraysData[] = ["menu_type" => "report","menu_name" => "Report Questionnaire", "module_id" => 7];
        /*37*/ $arraysData[] = ["menu_type" => "folder","menu_name" => "Field", "module_id" => 2];
        /*38*/ $arraysData[] = ["menu_type" => "folder","menu_name" => "Subscriber", "module_id" => 2];
        /*39*/ $arraysData[] = ["menu_type" => "folder","menu_name" => "Profilling", "module_id" => 2];
        /*40*/ $arraysData[] = ["menu_type" => "folder","menu_name" => "Carousel", "module_id" => 2];
        /*41*/ $arraysData[] = ["menu_type" => "core","menu_name" => "Carousel", "module_id" => 2];
        /*42*/ $arraysData[] = ["menu_type" => "setting","menu_name" => "Image Center", "module_id" => 2];
        /*43*/ $arraysData[] = ["menu_type" => "report","menu_name" => "One on One Chat Report2", "module_id" => 3];
        /*44*/ $arraysData[] = ["menu_type" => "core","menu_name" => "Auto Reply Beacon", "module_id" => 2];
        /*45*/ $arraysData[] = ["menu_type" => "setting","menu_name" => "Setting Beacon", "module_id" => 2];
        /*46*/ $arraysData[] = ["menu_type" => "setting","menu_name" => "Json SFTP", "module_id" => 3];
        /*47*/ $arraysData[] = ["menu_type" => "setting","menu_name" => "Coupon Form", "module_id" => 8];
        /*48*/ $arraysData[] = ["menu_type" => "setting","menu_name" => "Coupon", "module_id" => 8];
        /*49*/ $arraysData[] = ["menu_type" => "setting","menu_name" => "Coupon SCG Family", "module_id" => 8];
        /*50*/ $arraysData[] = ["menu_type" => "setting","menu_name" => "SCG Family Dropdown", "module_id" => 8];
        /*51*/ $arraysData[] = ["menu_type" => "folder","menu_name" => "Coupon", "module_id" => 8];
        /*52*/ $arraysData[] = ["menu_type" => "folder","menu_name" => "Coupon Family", "module_id" => 8];
        /*53*/ $arraysData[] = ["menu_type" => "report","menu_name" => "Coupon", "module_id" => 8];
        /*54*/ $arraysData[] = ["menu_type" => "report","menu_name" => "Coupon Family", "module_id" => 8];
        /*55*/ $arraysData[] = ["menu_type" => "report","menu_name" => "Coupon Form Family", "module_id" => 8];
        /*56*/ $arraysData[] = ["menu_type" => "core","menu_name" => "Product", "module_id" => 9];
        /*57*/ $arraysData[] = ["menu_type" => "core","menu_name" => "Order Status", "module_id" => 9];
        /*58*/ $arraysData[] = ["menu_type" => "setting","menu_name" => "Category", "module_id" => 9];
        /*59*/ $arraysData[] = ["menu_type" => "setting","menu_name" => "Coupon", "module_id" => 9];
        /*60*/ $arraysData[] = ["menu_type" => "report","menu_name" => "Ecommerce", "module_id" => 9];
        /*61*/ $arraysData[] = ["menu_type" => "folder","menu_name" => "Upload Images", "module_id" => 4];
        /*62*/ $arraysData[] = ["menu_type" => "report","menu_name" => "Bot Train", "module_id" => 5];
        /*63*/ $arraysData[] = ["menu_type" => "core","menu_name" => "Auto Reply Beacon", "module_id" => 2];
        /*64*/ $arraysData[] = ["menu_type" => "setting","menu_name" => "Beacon", "module_id" => 2];
        /*65*/ $arraysData[] = ["menu_type" => "core","menu_name" => "Share Location Carousel", "module_id" => 2];
        /*66*/ $arraysData[] = ["menu_type" => "report","menu_name" => "Facebook User", "module_id" => 3];
        /*67*/ $arraysData[] = ["menu_type" => "setting","menu_name" => "Family Connect", "module_id" => 4];
        /*68*/ $arraysData[] = ["menu_type" => "report","menu_name" => "Report Share Location", "module_id" => 5];
        /*69*/ $arraysData[] = ["menu_type" => "xxxxx","menu_name" => "xxxxxx", "module_id" => 0];
        /*70*/ $arraysData[] = ["menu_type" => "setting","menu_name" => "Coupon OTP", "module_id" => 10];
        /*71*/ $arraysData[] = ["menu_type" => "setting","menu_name" => "Coupon", "module_id" => 10];
        /*72*/ $arraysData[] = ["menu_type" => "setting","menu_name" => "Coupon Form", "module_id" => 10];
        /*73*/ $arraysData[] = ["menu_type" => "folder","menu_name" => "Coupon", "module_id" => 10];
        /*74*/ $arraysData[] = ["menu_type" => "report","menu_name" => "Coupon", "module_id" => 10];

        foreach ($arraysData as $data) {
            $listMenu = ListMenu::create($data);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_list_menu');
    }
}
