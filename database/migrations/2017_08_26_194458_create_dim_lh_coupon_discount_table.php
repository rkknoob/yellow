<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimLhCouponDiscountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_lh_coupon_project_discount', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('coupon_type_data_id');
            $table->integer('coupon_location_data_id');
            $table->integer('coupon_project_name_data_id');
            $table->string('name');
            $table->integer('discount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_lh_coupon_project_discount');
    }
}
