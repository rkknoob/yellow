<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactSUserCheckCouponTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_s_user_check_coupon', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('coupon_id');
            $table->integer('line_user_id');
            $table->string('flag_status')->nullable();
            $table->integer('user_get_coupon_percent');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_s_user_check_coupon');
    }
}
