<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactAutoReplyBeaconTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_auto_reply_beacon', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('auto_reply_beacon_id');
            $table->integer('richmessage_id')->nullable();
            $table->integer('seq_no');
            $table->string('type');
            $table->integer('packageId')->nullable();
            $table->integer('stickerId')->nullable();
            $table->longtext('payload')->nullable();
            $table->longtext('display')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_auto_reply_beacon');
    }
}
