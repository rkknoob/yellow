<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimCouponReedeemCodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_coupon_reedeem_code', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('coupon_id')->nullable();
            $table->integer('line_user_id')->nullable();
            $table->string('prefix_code')->nullable();
            $table->string('running_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_coupon_reedeem_code');
    }
}
