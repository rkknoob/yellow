<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimTrackingBcTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_tracking_bc', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->longtext('original_url');
            $table->string('tracking_source');
            $table->string('tracking_campaign');
            $table->string('tracking_ref');
            $table->longtext('generated_full_url');
            $table->longtext('generated_short_url');
            $table->string('desc')->nullable();
            $table->boolean('is_route_name')->nullable();
            $table->integer('campaign_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_tracking_bc');
    }
}
