<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimSettingBotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_setting_bot', function (Blueprint $table) {
            $table->increments('id');
            $table->text('bot_train_url');
            $table->text('bot_remove_url');
            $table->text('bot_reply_url');
            $table->text('bot_restart_url');
            $table->decimal('conf',1,1);
            $table->boolean('is_active');
            $table->boolean('is_open_bot');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_setting_bot');
    }
}
