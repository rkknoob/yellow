<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimImageRichmessageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_image_richmessage', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('richmessage_folder_id');
            $table->string('name');
            $table->string('alt_text');
            $table->string('original_photo_path');
            $table->string('image_size');
            $table->longtext('rich_message_url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_image_richmessage');
    }
}
