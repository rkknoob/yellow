<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactEcommerceOrderCustomizeLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_ecommerce_order_customize_log', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('dt_id')->nullable();
            $table->integer('order_id');
            $table->integer('product_id');
            $table->decimal('original_price',10,2)->default(0.00);
            $table->decimal('price',10,2)->default(0.00);
            $table->integer('quanlity')->default(0);
            $table->decimal('tax_amount',10,2)->default(0.00);
            $table->integer('tax_percent')->default(0);
            $table->decimal('discount_amount',10,2)->default(0.00);
            $table->decimal('total',10,2)->default(0.00);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_ecommerce_order_customize_log');
    }
}
