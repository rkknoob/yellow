<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimExclusiveOnlineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*

        Schema::create('dim_exclusive_online', function (Blueprint $table) {
            $table->increments('id');
            $table->string('course_name');//ชื่อ
            $table->text('description')->nullable();// รายละเอียด
            $table->integer('ep')->nullable();//แต้มแลก
            $table->string('img_url')->nullable();//path รูป
            $table->string('status')->nullable();//สถานะ
            $table->timestamps();
        }); */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_exclusive_online');
    }
}
