<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactApprovalCarouselActionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_approval_carousel_action', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('approval_carousel_column_id');
            $table->string('type')->nullable();
            $table->string('label')->nullable();
            $table->text('data')->nullable();
            $table->string('text')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_approval_carousel_action');
    }
}
