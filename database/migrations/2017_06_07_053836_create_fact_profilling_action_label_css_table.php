<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactProfillingActionLabelCssTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_profilling_action_label_css', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profilling_action_id');
            $table->string('key')->nullable();
            $table->string('label')->nullable();
            $table->string('type')->nullable();
            $table->text('values')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_profilling_action_label_css');
    }
}
