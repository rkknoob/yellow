<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimSettingConfirmationCarouselTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_setting_confirmation_carousel', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('alt_text')->nullable();
            $table->string('confimation_yes')->nullable();
            $table->string('confimation_no')->nullable();
            $table->integer('max_display')->nullable();
            $table->integer('is_confimation')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_setting_confirmation_carousel');
    }
}
