<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimSMdmMd6tTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_s_mdm_md6t', function (Blueprint $table) {
            $table->increments('id');
            $table->string('MANDT')->nullable();
            $table->string('SPRAS')->nullable();
            $table->string('NODE')->nullable();
            $table->string('LTEXT')->nullable();
            $table->string('LTEXT_T')->nullable();
            $table->string('CREATE_BY')->nullable();
            $table->dateTime('CREATE_DATE')->nullable();
            $table->string('UPDATE_BY')->nullable();
            $table->dateTime('UPDATE_DATE')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_s_mdm_md6t');
    }
}
