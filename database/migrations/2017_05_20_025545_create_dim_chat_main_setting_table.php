<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimChatMainSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_chat_main_setting', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('max_agent_support')->nullable();
            $table->text('data_detail')->nullable();
            $table->boolean('is_auto_kick')->nullable();
            $table->integer('time_auto_kick')->nullable();
            $table->text('text_auto_kick')->nullable();
            $table->text('text_after_end_chat')->nullable();
            $table->time('office_start')->nullable();
            $table->time('office_end')->nullable();
            $table->string('keyword_start_chat')->nullable();
            $table->string('keyword_end_chat')->nullable();
            $table->string('is_office_open')->nullable();
            $table->string('text_welcome')->nullable();
            $table->string('text_user_end_chat')->nullable();
            $table->string('text_agent_end_chat')->nullable();
            $table->string('text_out_office_chat')->nullable();
            $table->string('text_end_chat_issue')->nullable();
            $table->integer('richmessage_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_chat_main_setting');
    }
}
