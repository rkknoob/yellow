<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('alt_text')->nullable();
            $table->boolean('is_active')->default(false);
            $table->integer('folder_id')->nullable();
            $table->integer('segment_id')->nullable();
            $table->enum('send_status',['send_it','schedule'])->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->integer('schedule_id')->nullable();
            $table->integer('send_time')->nullable();
            $table->dateTime('datetime')->nullable();
            $table->boolean('is_start_schedule')->default(false);
            $table->string('segment_type')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dim_campaigns');
    }
}
