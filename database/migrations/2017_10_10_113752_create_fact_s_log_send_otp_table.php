<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactSLogSendOtpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_s_log_send_otp', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone_number');
            $table->string('send_status');
            $table->string('otp_ref');
            $table->string('otp_code');
            $table->text('msg')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_s_log_send_otp');
    }
}
