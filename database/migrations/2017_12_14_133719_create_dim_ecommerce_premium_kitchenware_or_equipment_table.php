<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimEcommercePremiumKitchenwareOrEquipmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_ecommerce_premium_kitchenware_or_equipment', function (Blueprint $table) {
            $table->increments('id');
            $table->string('course_name');
            $table->text('desc')->nullable();
            $table->date('avaliable_strat_date')->nullable();
            $table->date('avaliable_end_date')->nullable();
            $table->integer('hours_duration')->nullable();
            $table->integer('ep')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_ecommerce_premium_kitchenware_or_equipment');
    }
}
