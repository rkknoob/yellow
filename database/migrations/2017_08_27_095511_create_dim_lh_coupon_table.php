<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimLhCouponTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_lh_coupon', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('static_form_id');
            $table->integer('line_user_id');
            $table->text('img_path_url');
            $table->text('richmessage_path_url');
            $table->integer('coupon_type_data_id');
            $table->integer('coupon_location_data_id');
            $table->integer('coupon_project_name_data_id');
            $table->integer('coupon_discount_data_data_id');
            $table->string('code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_lh_coupon');
    }
}
