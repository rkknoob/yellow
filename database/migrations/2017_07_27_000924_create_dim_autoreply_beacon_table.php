<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimAutoreplyBeaconTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_autoreply_beacon', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('alt_text')->nullable();
            $table->integer('beacon_id')->nullable();
            $table->integer('segment_id')->nullable();
            $table->dateTime('sent_date')->nullable();
            $table->dateTime('last_sent_date')->nullable();
            $table->boolean('is_active')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_autoreply_beacon');
    }
}
