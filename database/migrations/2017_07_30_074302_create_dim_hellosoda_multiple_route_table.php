<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimHellosodaMultipleRouteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_hellosoda_multiple_route', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hellosoda_form_id')->nullable();
            $table->text('source_route')->nullable();
            $table->text('redirect_route')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_hellosoda_multiple_route');
    }
}
