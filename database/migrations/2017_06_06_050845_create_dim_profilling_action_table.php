<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimProfillingActionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_profilling_action', function (Blueprint $table) {
            $table->increments('pri_id');
            $table->string('id');
            $table->integer('profilling_id');
            $table->string('title')->nullable();
            $table->string('type')->nullable();
            $table->integer('field_id')->nullable();
            $table->integer('seq_no')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_profilling_action');
    }
}
