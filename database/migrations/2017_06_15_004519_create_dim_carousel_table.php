<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimCarouselTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_carousel', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('folder_id')->nullable();
            $table->string('name')->nullable();
            $table->text('desc')->nullable();
            $table->string('action_1')->nullable();
            $table->string('action_2')->nullable();
            $table->string('action_3')->nullable();
            $table->string('label_1')->nullable();
            $table->string('label_2')->nullable();
            $table->string('label_3')->nullable();
            $table->decimal('conf',10,2)->nullable();
            $table->string('alt_text')->nullable();
            $table->boolean('is_autoreply')->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_carousel');
    }
}
