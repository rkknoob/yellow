<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactRecieveTrackingAutoReplyKeywordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_recieve_tracking_auto_reply_keyword', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tracking_auto_reply_keyword_id');
            $table->string('line_user_id')->nullable();
            $table->string('ip')->nullable();
            $table->string('device')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_recieve_tracking_auto_reply_keyword');
    }
}
