<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrateFactV2ProfillingItemStylesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_v2_profilling_item_styles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profilling_item_id');
            $table->integer('ref')->default(0);
            $table->string('title')->nullable();
            $table->string('key')->nullable();
            $table->string('value')->nullable();
            $table->text('output')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_v2_profilling_item_styles');
    }
}
