<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimEcommerceCouponTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_ecommerce_coupon', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code')->nullable();
            $table->string('prefix_name')->nullable();
            $table->boolean('is_running_number')->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->integer('user_per_coupon')->default(1);
            $table->integer('user_per_customer')->default(1);
            $table->integer('sort_order')->default(0);
            $table->string('disply_on_main_page')->nullable();
            $table->string('coupon_image')->nullable();
            $table->text('desc')->nullable();
            $table->integer('tracking_bc_id')->nullable();
            $table->decimal('minimum_purchase',10,2)->default(0.00);
            $table->boolean('apply_to_customer_wallet')->default(0);
            $table->integer('img_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_ecommerce_coupon');
    }
}
