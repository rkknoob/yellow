<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactRecieveBeaconTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_recieve_beacon', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('beacon_id')->nullable();
            $table->string('beacon_identifier')->nullable();
            $table->integer('line_user_id')->nullable();
            $table->string('trigger_event')->nullable();
            $table->dateTime('leave_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_recieve_beacon');
    }
}
