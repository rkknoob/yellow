<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimEcommerceCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_ecommerce_customer', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_code')->nullable();
            $table->integer('line_user_id')->nullable();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->nullable();
            $table->string('city')->nullable();
            $table->string('phone_number')->nullable();
            $table->integer('reward_point')->default(0);
            $table->integer('img_id')->nullable();
            $table->string('market_name')->nullable();
            $table->string('market_type')->nullable();
            $table->string('post_code')->nullable();
            $table->integer('dt_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_ecommerce_customer');
    }
}
