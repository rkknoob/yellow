<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactEcomLinepayLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_ecom_linepay_log', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ecom_linepay_id');
            $table->string('type')->nullable();
            $table->string('return_code')->nullable();
            $table->string('return_msg')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_ecom_linepay_log');
    }
}
