<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimEcommerceOrderPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_ecommerce_order_payment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id');
            $table->dateTime('payment_date_submit')->nullable();
            $table->string('payment_transaction_id')->nullable();
            $table->decimal('payment_amount',10,2)->default(0.00);
            $table->dateTime('payment_date_refund')->nullable();
            $table->string('payment_refund_transaction_id')->nullable();
            $table->string('payment_refund_status')->nullable();
            $table->decimal('payment_refund_amount',10,2)->default(0.00);
            $table->string('payment_type')->nullable();
            $table->integer('payment_img_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_ecommerce_order_payment');
    }
}
