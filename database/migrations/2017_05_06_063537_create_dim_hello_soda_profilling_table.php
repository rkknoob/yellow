<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimHelloSodaProfillingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_hello_soda_profilling', function (Blueprint $table) {
            $table->increments('id');
            // $table->enum('lead_form_group',['iservice','mypolicy','taxcert']);
            $table->string('lead_form_name');
            $table->string('color_wallpaper');
            $table->longtext('url_connect_hello_soda')->nullable();
            // $table->longtext('url_redirect_fwd_max')->nullable();
            // $table->longtext('url_redirect_gaming')->nullable();
            // $table->longtext('url_call_back')->nullable();
            // $table->string('product_id')->nullable();
            // $table->string('facebook_app_id')->nullable();
            // $table->string('facebook_secret_id')->nullable();
            $table->boolean('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_hello_soda_profilling');
    }
}
