<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimProfillingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_profilling', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('folder_id')->nullable();
            $table->boolean('is_route_expire')->nullable();
            $table->dateTime('route_expire_date')->nullable();
            $table->text('route_expire_url')->nullable();
            $table->string('lead_form_name')->nullable();
            $table->integer('subscriber_id')->nullable();
            $table->string('color_wallpaper')->nullable();
            $table->string('page_color')->nullable();
            $table->string('route')->nullable();
            $table->string('redirect_route')->nullable();
            $table->boolean('is_active')->nullable();
            $table->integer('max_page_width')->nullable();
            $table->boolean('is_use_hellosoda')->nullable();
            $table->text('route_hellosoda')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_profilling');
    }
}
