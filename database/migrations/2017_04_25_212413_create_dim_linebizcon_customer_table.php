<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimLinebizconCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_linebizcon_customer', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('line_user_profile_id')->nullable();
            // $table->string('mid');
            $table->string('client_no');
            $table->string('name');
            $table->string('last_name');
            $table->string('email');
            $table->string('phone_number');
            $table->string('birth_day');
            $table->string('birth_month');
            $table->string('birth_year');
            $table->string('gender');
            // $table->string('display_name');
            // $table->longtext('display_img_url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_linebizcon_customer');
    }
}
