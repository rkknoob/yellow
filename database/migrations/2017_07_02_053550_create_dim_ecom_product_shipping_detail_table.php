<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimEcomProductShippingDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_ecom_product_shipping_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ecom_product_order_list_id')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('tel')->nullable();
            $table->string('email')->nullable();
            $table->text('address')->nullable();
            $table->string('province')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('name_tax')->nullable();
            $table->string('tel_tax')->nullable();
            $table->text('address_tax')->nullable();
            $table->string('province_tax')->nullable();
            $table->string('zip_code_tax')->nullable();
            $table->string('tax_id')->nullable();
            $table->text('comment')->nullable();
            $table->boolean('is_tax_document')->nullable();
            $table->string('scg_family_id')->nullable();
            $table->boolean('address_copy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_ecom_product_shipping_detail');
    }
}
