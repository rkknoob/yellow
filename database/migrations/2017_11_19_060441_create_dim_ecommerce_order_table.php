<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimEcommerceOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_ecommerce_order', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ecommerce_customer_id');
            $table->string('order_id');
            $table->dateTime('order_date');
            $table->string('order_status');
            $table->string('payment_information')->nullable();
            $table->decimal('sub_total',10,2)->default(0.00);
            $table->decimal('tax',10,2)->default(0.00);
            $table->decimal('shipping_cost',10,2)->default(0.00);
            $table->decimal('grand_total',10,2)->default(0.00);
            $table->string('coupon_code_discount')->nullable();
            $table->decimal('coupon_code_discount_amount',10,2)->default(0.00);
            $table->decimal('total_paid',10,2)->default(0.00);
            $table->decimal('total_refunded',10,2)->default(0.00);
            $table->decimal('total_due',10,2)->default(0.00);
            $table->integer('total_reward_point')->default(0);
            $table->integer('order_reward_point')->default(0);
            $table->integer('ecommerce_shipping_address_id')->nullable();
            $table->dateTime('operator_shipping_date')->nullable();
            $table->string('operator_shipping_time')->nullable();
            $table->dateTime('dt_shipping_date')->nullable();
            $table->integer('dt_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_ecommerce_order');
    }
}
