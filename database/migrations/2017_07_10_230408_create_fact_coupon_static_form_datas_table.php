<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactCouponStaticFormDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_coupon_static_form_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('static_form_id');
            $table->integer('coupon_id');
            $table->integer('line_user_id');
            $table->string('name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->nullable();
            $table->string('mobile_phone')->nullable();
            $table->dateTime('start_date_construction')->nullable();
            $table->string('service')->nullable();
            $table->integer('img_id')->nullable();
            $table->string('province')->nullable();
            $table->string('postcode')->nullable();
            $table->text('address')->nullable();
            $table->string('lat')->nullable();
            $table->string('long')->nullable();
            $table->text('desc')->nullable();
            $table->string('address_format')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_coupon_static_form_datas');
    }
}
