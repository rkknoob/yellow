<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimSettingFamilyConnectionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_setting_family_connection', function (Blueprint $table) {
            $table->increments('id');
            $table->text('api_save_special_attribute')->nullable();
            $table->text('api_map_coupon_code')->nullable();
            $table->text('api_get_line_promotion_list')->nullable();
            $table->text('api_member_login')->nullable();
            $table->text('api_member_login_save_special_attribute')->nullable();
            $table->text('api_member_logout')->nullable();
            $table->text('api_request_forgot_password')->nullable();
            $table->text('api_get_member_point_summary')->nullable();
            $table->text('api_get_voucher_book')->nullable();
            $table->text('api_get_promotion_list')->nullable();
            $table->text('api_get_promotion_list_detail')->nullable();
            $table->text('api_get_point_exchange_list')->nullable();
            $table->text('api_get_point_exchange_list_detail')->nullable();
            $table->text('img_path_url')->nullable();
            $table->string('scg_token_fixed')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_setting_family_connection');
    }
}
