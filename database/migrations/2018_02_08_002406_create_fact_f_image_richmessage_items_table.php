<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactFImageRichmessageItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_f_image_richmessage_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rich_message_id');
            $table->string('section_name');
            $table->enum('action',['keyword','url','no-action']);
            $table->string('x');
            $table->string('y');
            $table->string('height');
            $table->string('width');
            $table->integer('el_id');
            $table->string('url_protocal')->nullable();
            $table->longtext('url_value')->nullable();
            $table->string('keyword')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_f_image_richmessage_items');
    }
}
