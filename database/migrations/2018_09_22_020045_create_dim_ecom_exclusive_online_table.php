<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimEcomExclusiveOnlineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_exclusive_online', function (Blueprint $table) {
            $table->increments('id');
            $table->string('course_name');
            $table->text('description')->nullable();
            $table->date('avaliable_date')->nullable();
            $table->integer('hours')->nullable();
            $table->integer('ep')->nullable();
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_ecom_exclusive_online');
    }
}
