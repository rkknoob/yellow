<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimScgUserTriggerKeywordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_scg_user_trigger_keyword', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('line_user_id');
            $table->string('keyword');
            $table->boolean('is_flag')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_scg_user_trigger_keyword');
    }
}
