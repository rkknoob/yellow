<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimEcomProductTrackingDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_ecom_product_tracking_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ecom_product_order_list_id')->nullable();
            $table->string('order_status')->nullable();
            $table->string('tracking')->nullable();
            $table->string('tracking_vendor')->nullable();
            $table->text('payload_1')->nullable();
            $table->text('display_1')->nullable();
            $table->text('payload_2')->nullable();
            $table->text('display_2')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_ecom_product_tracking_detail');
    }
}
