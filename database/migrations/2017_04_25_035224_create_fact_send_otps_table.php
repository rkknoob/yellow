<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactSendOtpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_send_otps', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mid');
            $table->string('otp');
            $table->string('ref_otp');
            $table->string('phone_number');
            $table->boolean('status_send');
            $table->boolean('status_receive');
            $table->dateTime('expire_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_send_otps');
    }
}
