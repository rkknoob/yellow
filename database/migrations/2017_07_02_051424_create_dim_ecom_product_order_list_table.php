<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimEcomProductOrderListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_ecom_product_order_list', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('line_user_id')->nullable();
            $table->string('order_id')->nullable();
            $table->string('scg_family_id')->nullable();
            $table->integer('discount_percent')->nullable();
            $table->decimal('sub_total',10,2)->nullable();
            $table->decimal('grand_total',10,2)->nullable();
            $table->dateTime('date_added')->nullable();
            $table->string('coupon_code')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_ecom_product_order_list');
    }
}
