<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactTrainBotCsvLogItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_train_bot_csv_log_item', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('train_bot_csv_log_id');
            $table->string('question');
            $table->string('answer');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_train_bot_csv_log_item');
    }
}
