<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimOtpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_otp', function (Blueprint $table) {
            $table->increments('id');
            $table->string('otp_api');
            $table->string('otp_message');
            $table->integer('otp_time_limit');
            $table->integer('fwd_session_timeout');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_otp');
    }
}
