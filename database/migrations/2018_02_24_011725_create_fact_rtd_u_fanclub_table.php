<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactRtdUFanclubTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_rtd_u_fanclub', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rtd_id');
            $table->integer('quoted_point');
            $table->integer('monthly_point');
            $table->integer('quarter_header_point');
            $table->integer('special_point_from_promotion');
            $table->integer('total_point');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_rtd_u_fanclub');
    }
}
